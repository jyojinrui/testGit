package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Fzxxbab;

/**
 * 房主信息Service接口
 * 
 * @author Carry
 * @date 2020-10-05
 */
public interface IFzxxbabService 
{
    /**
     * 查询房主信息
     * 
     * @param id 房主信息ID
     * @return 房主信息
     */
    public Fzxxbab selectFzxxbabById(Long id);

    /*
     * 根据小区管理员Id查询所管辖的小区信息
     */
    public List<Fzxxbab> selectFzxxbabListByXqAdminId(Long userId,Fzxxbab fzxxbab);
    
    /**
     * 
     * @param userId
     * @param fzxxbab
     * @return
     */
    public List<Fzxxbab> selectFzxxbabListByPcsAdminId(Long userId,Fzxxbab fzxxbab);
    
    public List<Fzxxbab> selectFzxxbabListByDeptId(Long deptId,Long deptIdOfTree,Fzxxbab fzxxbab);
    
    
    /**
     * 查询房主信息列表
     * 
     * @param fzxxbab 房主信息
     * @return 房主信息集合
     */
    public List<Fzxxbab> selectFzxxbabList(Fzxxbab fzxxbab);

    /**
     * 新增房主信息
     * 
     * @param fzxxbab 房主信息
     * @return 结果
     */
    public int insertFzxxbab(Fzxxbab fzxxbab);

    /**
     * 修改房主信息
     * 
     * @param fzxxbab 房主信息
     * @return 结果
     */
    public int updateFzxxbab(Fzxxbab fzxxbab);

    /**
     * 批量删除房主信息
     * 
     * @param ids 需要删除的房主信息ID
     * @return 结果
     */
    public int deleteFzxxbabByIds(Long[] ids);

    /**
     * 删除房主信息信息
     * 
     * @param id 房主信息ID
     * @return 结果
     */
    public int deleteFzxxbabById(Long id);
}

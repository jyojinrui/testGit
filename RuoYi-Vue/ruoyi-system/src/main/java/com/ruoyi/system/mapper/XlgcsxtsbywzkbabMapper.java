package com.ruoyi.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.system.domain.Xlgcsxtsbywzkbab;

/**
 * 雪亮工程摄像头设备运行状况Mapper接口
 * 
 * @author Carry
 * @date 2020-10-03
 */
public interface XlgcsxtsbywzkbabMapper 
{
    /**
     * 查询雪亮工程摄像头设备运行状况
     * 
     * @param id 雪亮工程摄像头设备运行状况ID
     * @return 雪亮工程摄像头设备运行状况
     */
    public Xlgcsxtsbywzkbab selectXlgcsxtsbywzkbabById(Long id);

    /**
     * 查询雪亮工程摄像头设备运行状况列表
     * 
     * @param xlgcsxtsbywzkbab 雪亮工程摄像头设备运行状况
     * @return 雪亮工程摄像头设备运行状况集合
     */
    public List<Xlgcsxtsbywzkbab> selectXlgcsxtsbywzkbabList(Xlgcsxtsbywzkbab xlgcsxtsbywzkbab);

    
    public List<Xlgcsxtsbywzkbab> selectXlgcsxtsbywzkbabListByXqAdminId(@Param("userId")Long userId,@Param("xlgcsxtsbywzkbab")Xlgcsxtsbywzkbab xlgcsxtsbywzkbab);

    
    public List<Xlgcsxtsbywzkbab> selectXlgcsxtsbywzkbabListByDeptId(@Param("deptId")Long userId,@Param("xlgcsxtsbywzkbab")Xlgcsxtsbywzkbab xlgcsxtsbywzkbab);

    /**
     * 新增雪亮工程摄像头设备运行状况
     * 
     * @param xlgcsxtsbywzkbab 雪亮工程摄像头设备运行状况
     * @return 结果
     */
    public int insertXlgcsxtsbywzkbab(Xlgcsxtsbywzkbab xlgcsxtsbywzkbab);

    /**
     * 修改雪亮工程摄像头设备运行状况
     * 
     * @param xlgcsxtsbywzkbab 雪亮工程摄像头设备运行状况
     * @return 结果
     */
    public int updateXlgcsxtsbywzkbab(Xlgcsxtsbywzkbab xlgcsxtsbywzkbab);

    /**
     * 删除雪亮工程摄像头设备运行状况
     * 
     * @param id 雪亮工程摄像头设备运行状况ID
     * @return 结果
     */
    public int deleteXlgcsxtsbywzkbabById(Long id);

    /**
     * 批量删除雪亮工程摄像头设备运行状况
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteXlgcsxtsbywzkbabByIds(Long[] ids);
}

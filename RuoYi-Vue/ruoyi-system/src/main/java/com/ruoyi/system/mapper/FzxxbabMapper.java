package com.ruoyi.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.system.domain.Fzxxbab;

/**
 * 房主信息Mapper接口
 * 
 * @author Carry
 * @date 2020-10-05
 */
public interface FzxxbabMapper 
{
    /**
     * 查询房主信息
     * 
     * @param id 房主信息ID
     * @return 房主信息
     */
    public Fzxxbab selectFzxxbabById(Long id);

    /**
     * 查询房主信息列表
     * 
     * @param fzxxbab 房主信息
     * @return 房主信息集合
     */
    public List<Fzxxbab> selectFzxxbabList(Fzxxbab fzxxbab);
    
    /**
     * 根据小区管理员的id获取房主信息    
     * @return
     * @Param传参数操作  
     */
    public List<Fzxxbab> selectFzxxbabListByXqAdminId(@Param("userId")Long userId,@Param("fzxxbab")Fzxxbab fzxxbab);
    
    
    public List<Fzxxbab> selectFzxxbabListByPcsAdminId(@Param("userId")Long userId,@Param("fzxxbab")Fzxxbab fzxxbab);
    
    public List<Fzxxbab> selectFzxxbabListByDeptId(@Param("deptId")Long deptId,@Param("deptIdOfTree")Long deptIdOfTree,@Param("fzxxbab")Fzxxbab fzxxbab);
    
    
    
    /**
     * 新增房主信息
     * 
     * @param fzxxbab 房主信息
     * @return 结果
     */
    public int insertFzxxbab(Fzxxbab fzxxbab);

    /**
     * 修改房主信息
     * 
     * @param fzxxbab 房主信息
     * @return 结果
     */
    public int updateFzxxbab(Fzxxbab fzxxbab);

    /**
     * 删除房主信息
     * 
     * @param id 房主信息ID
     * @return 结果
     */
    public int deleteFzxxbabById(Long id);

    /**
     * 批量删除房主信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFzxxbabByIds(Long[] ids);
    
    
    public Long getCountByDeptId(long deptId); 
    
    public Long getCountByXqbm(String xqbm); 
    
}

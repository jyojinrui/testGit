package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Xqxxbab;
import com.ruoyi.system.domain.Zzxqxxbab;

/**
 * 小区管理Service接口
 * 
 * @author ruoyi
 * @date 2020-09-26
 */
public interface IXqxxbabService 
{
    /**
     * 查询小区管理
     * 
     * @param id 小区管理ID
     * @return 小区管理
     */
    public Xqxxbab selectXqxxbabById(String id);
    
    /**
     * 
     * @param ssxqbm
     * @return
     */
    public Xqxxbab selectXqxxbabBySsxqbm(String ssxqbm);


    /**
     * 查询小区管理列表
     * 
     * @param xqxxbab 小区管理
     * @return 小区管理集合
     */
    public List<Xqxxbab> selectXqxxbabList(Xqxxbab xqxxbab);

    public List<Xqxxbab> selectXqxxbabListByDeptId(Long deptId,Long deptIdOfTree,Xqxxbab xqxxbab);
    
    /**
     * 新增小区管理
     * 
     * @param xqxxbab 小区管理
     * @return 结果
     */
    public int insertXqxxbab(Xqxxbab xqxxbab);

    /**
     * 根据小区管理员信息查询小区信息
     * @param userId
     * @return
     */
    public Xqxxbab getXqxxByXqAdminId(Long userId);
    /**
     * 修改小区管理
     * 
     * @param xqxxbab 小区管理
     * @return 结果
     */
    public int updateXqxxbab(Xqxxbab xqxxbab);

    /**
     * 批量删除小区管理
     * 
     * @param ids 需要删除的小区管理ID
     * @return 结果
     */
    public int deleteXqxxbabByIds(String[] ids);

    /**
     * 删除小区管理信息
     * 
     * @param id 小区管理ID
     * @return 结果
     */
    public int deleteXqxxbabById(String id);

	public List<Zzxqxxbab> getBzdzxxbabByXqbm(String buildingId);

	public List<Xqxxbab> selectAllXqxxbabList();
}

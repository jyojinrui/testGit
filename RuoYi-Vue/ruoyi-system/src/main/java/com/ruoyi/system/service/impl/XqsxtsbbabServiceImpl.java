package com.ruoyi.system.service.impl;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.XqsxtsbbabMapper;
import com.ruoyi.system.domain.Xqsxtsbbab;
import com.ruoyi.system.service.IXqsxtsbbabService;

/**
 * 小区摄像头设备Service业务层处理
 * 
 * @author Carry
 * @date 2020-10-03
 */
@Service
public class XqsxtsbbabServiceImpl implements IXqsxtsbbabService 
{
    @Autowired
    private XqsxtsbbabMapper xqsxtsbbabMapper;

    /**
     * 查询小区摄像头设备
     * 
     * @param id 小区摄像头设备ID
     * @return 小区摄像头设备
     */
    @Override
    public Xqsxtsbbab selectXqsxtsbbabById(Long id)
    {
        return xqsxtsbbabMapper.selectXqsxtsbbabById(id);
    }

    /**
     * 查询小区摄像头设备列表
     * 
     * @param xqsxtsbbab 小区摄像头设备
     * @return 小区摄像头设备
     */
    @Override
    public List<Xqsxtsbbab> selectXqsxtsbbabList(Xqsxtsbbab xqsxtsbbab)
    {
        return xqsxtsbbabMapper.selectXqsxtsbbabList(xqsxtsbbab);
    }

    /**
     * 新增小区摄像头设备
     * 
     * @param xqsxtsbbab 小区摄像头设备
     * @return 结果
     */
    @Override
    public int insertXqsxtsbbab(Xqsxtsbbab xqsxtsbbab)
    {
        return xqsxtsbbabMapper.insertXqsxtsbbab(xqsxtsbbab);
    }

    /**
     * 修改小区摄像头设备
     * 
     * @param xqsxtsbbab 小区摄像头设备
     * @return 结果
     */
    @Override
    public int updateXqsxtsbbab(Xqsxtsbbab xqsxtsbbab)
    {
        return xqsxtsbbabMapper.updateXqsxtsbbab(xqsxtsbbab);
    }

    /**
     * 批量删除小区摄像头设备
     * 
     * @param ids 需要删除的小区摄像头设备ID
     * @return 结果
     */
    @Override
    public int deleteXqsxtsbbabByIds(Long[] ids)
    {
        return xqsxtsbbabMapper.deleteXqsxtsbbabByIds(ids);
    }

    /**
     * 删除小区摄像头设备信息
     * 
     * @param id 小区摄像头设备ID
     * @return 结果
     */
    @Override
    public int deleteXqsxtsbbabById(Long id)
    {
        return xqsxtsbbabMapper.deleteXqsxtsbbabById(id);
    }
     
	@Override
	public List<Xqsxtsbbab> selectXqsxtsbbabListByXqAdminId(Long userId,Xqsxtsbbab xqsxtsbbab) {
		// TODO Auto-generated method stub
		return xqsxtsbbabMapper.selectXqsxtsbbabListByXqAdminId(userId,xqsxtsbbab);
	}

	@Override
	public List<Xqsxtsbbab> selectXqsxtsbbabListByDeptId(Long deptId, Xqsxtsbbab xqsxtsbbab) {
		// TODO Auto-generated method stub
		return xqsxtsbbabMapper.selectXqsxtsbbabListByDeptId(deptId,xqsxtsbbab);
	}
}

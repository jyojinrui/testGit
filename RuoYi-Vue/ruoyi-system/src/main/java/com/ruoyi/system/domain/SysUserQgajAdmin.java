package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 区公安局管理员与公安局关系表对象 sys_user_qgaj_admin
 * 
 * @author ruoyi
 * @date 2020-11-01
 */
public class SysUserQgajAdmin extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 区公安局编码 */
    @Excel(name = "区公安局编码")
    private String qgajbm;

    /** 用户Id */
    @Excel(name = "用户Id")
    private Long userId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setQgajbm(String qgajbm) 
    {
        this.qgajbm = qgajbm;
    }

    public String getQgajbm() 
    {
        return qgajbm;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("qgajbm", getQgajbm())
            .append("userId", getUserId())
            .toString();
    }
}

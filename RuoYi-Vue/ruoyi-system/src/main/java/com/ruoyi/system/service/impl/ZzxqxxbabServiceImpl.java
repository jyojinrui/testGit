package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ZzxqxxbabMapper;
import com.ruoyi.system.domain.Zzxqxxbab;
import com.ruoyi.system.service.IZzxqxxbabService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-12-20
 */
@Service
public class ZzxqxxbabServiceImpl implements IZzxqxxbabService 
{
    @Autowired
    private ZzxqxxbabMapper zzxqxxbabMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public Zzxqxxbab selectZzxqxxbabById(Long id)
    {
        return zzxqxxbabMapper.selectZzxqxxbabById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param zzxqxxbab 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<Zzxqxxbab> selectZzxqxxbabList(Zzxqxxbab zzxqxxbab)
    {
        return zzxqxxbabMapper.selectZzxqxxbabList(zzxqxxbab);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param zzxqxxbab 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertZzxqxxbab(Zzxqxxbab zzxqxxbab)
    {
        return zzxqxxbabMapper.insertZzxqxxbab(zzxqxxbab);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param zzxqxxbab 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateZzxqxxbab(Zzxqxxbab zzxqxxbab)
    {
        return zzxqxxbabMapper.updateZzxqxxbab(zzxqxxbab);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteZzxqxxbabByIds(Long[] ids)
    {
        return zzxqxxbabMapper.deleteZzxqxxbabByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deleteZzxqxxbabById(Long id)
    {
        return zzxqxxbabMapper.deleteZzxqxxbabById(id);
    }
}

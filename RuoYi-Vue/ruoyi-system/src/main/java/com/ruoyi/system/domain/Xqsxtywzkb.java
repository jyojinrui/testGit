package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 运维状况对象 xqsxtywzkb
 * 
 * @author Carry
 * @date 2020-10-03
 */
public class Xqsxtywzkb extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 小区编码 */
    @Excel(name = "小区编码")
    private String buildingId;

    /** 摄像头编码 */
    @Excel(name = "摄像头编码")
    private String sxtbm;

    /** 是否在线 */
    @Excel(name = "是否在线")
    private String sfzx;

    /** 上线时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "上线时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date sxsj;

    /** 离线时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "离线时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date lxsj;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBuildingId(String buildingId) 
    {
        this.buildingId = buildingId;
    }

    public String getBuildingId() 
    {
        return buildingId;
    }
    public void setSxtbm(String sxtbm) 
    {
        this.sxtbm = sxtbm;
    }

    public String getSxtbm() 
    {
        return sxtbm;
    }
    public void setSfzx(String sfzx) 
    {
        this.sfzx = sfzx;
    }

    public String getSfzx() 
    {
        return sfzx;
    }
    public void setSxsj(Date sxsj) 
    {
        this.sxsj = sxsj;
    }

    public Date getSxsj() 
    {
        return sxsj;
    }
    public void setLxsj(Date lxsj) 
    {
        this.lxsj = lxsj;
    }

    public Date getLxsj() 
    {
        return lxsj;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("buildingId", getBuildingId())
            .append("sxtbm", getSxtbm())
            .append("sfzx", getSfzx())
            .append("sxsj", getSxsj())
            .append("lxsj", getLxsj())
            .toString();
    }
}

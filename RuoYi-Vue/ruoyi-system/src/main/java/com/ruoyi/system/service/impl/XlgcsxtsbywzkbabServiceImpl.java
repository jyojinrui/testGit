package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.XlgcsxtsbywzkbabMapper;
import com.ruoyi.system.domain.Xlgcsxtsbywzkbab;
import com.ruoyi.system.service.IXlgcsxtsbywzkbabService;

/**
 * 雪亮工程摄像头设备运行状况Service业务层处理
 * 
 * @author Carry
 * @date 2020-10-03
 */
@Service
public class XlgcsxtsbywzkbabServiceImpl implements IXlgcsxtsbywzkbabService 
{
    @Autowired
    private XlgcsxtsbywzkbabMapper xlgcsxtsbywzkbabMapper;

    /**
     * 查询雪亮工程摄像头设备运行状况
     * 
     * @param id 雪亮工程摄像头设备运行状况ID
     * @return 雪亮工程摄像头设备运行状况
     */
    @Override
    public Xlgcsxtsbywzkbab selectXlgcsxtsbywzkbabById(Long id)
    {
        return xlgcsxtsbywzkbabMapper.selectXlgcsxtsbywzkbabById(id);
    }

    /**
     * 查询雪亮工程摄像头设备运行状况列表
     * 
     * @param xlgcsxtsbywzkbab 雪亮工程摄像头设备运行状况
     * @return 雪亮工程摄像头设备运行状况
     */
    @Override
    public List<Xlgcsxtsbywzkbab> selectXlgcsxtsbywzkbabList(Xlgcsxtsbywzkbab xlgcsxtsbywzkbab)
    {
        return xlgcsxtsbywzkbabMapper.selectXlgcsxtsbywzkbabList(xlgcsxtsbywzkbab);
    }

    /**
     * 新增雪亮工程摄像头设备运行状况
     * 
     * @param xlgcsxtsbywzkbab 雪亮工程摄像头设备运行状况
     * @return 结果
     */
    @Override
    public int insertXlgcsxtsbywzkbab(Xlgcsxtsbywzkbab xlgcsxtsbywzkbab)
    {
        return xlgcsxtsbywzkbabMapper.insertXlgcsxtsbywzkbab(xlgcsxtsbywzkbab);
    }

    /**
     * 修改雪亮工程摄像头设备运行状况
     * 
     * @param xlgcsxtsbywzkbab 雪亮工程摄像头设备运行状况
     * @return 结果
     */
    @Override
    public int updateXlgcsxtsbywzkbab(Xlgcsxtsbywzkbab xlgcsxtsbywzkbab)
    {
        return xlgcsxtsbywzkbabMapper.updateXlgcsxtsbywzkbab(xlgcsxtsbywzkbab);
    }

    /**
     * 批量删除雪亮工程摄像头设备运行状况
     * 
     * @param ids 需要删除的雪亮工程摄像头设备运行状况ID
     * @return 结果
     */
    @Override
    public int deleteXlgcsxtsbywzkbabByIds(Long[] ids)
    {
        return xlgcsxtsbywzkbabMapper.deleteXlgcsxtsbywzkbabByIds(ids);
    }

    /**
     * 删除雪亮工程摄像头设备运行状况信息
     * 
     * @param id 雪亮工程摄像头设备运行状况ID
     * @return 结果
     */
    @Override
    public int deleteXlgcsxtsbywzkbabById(Long id)
    {
        return xlgcsxtsbywzkbabMapper.deleteXlgcsxtsbywzkbabById(id);
    }
  
	@Override
	public List<Xlgcsxtsbywzkbab> selectXlgcsxtsbywzkbabListByXqAdminId(Long userId,
			Xlgcsxtsbywzkbab xlgcsxtsbywzkbab) {
		// TODO Auto-generated method stub
		return xlgcsxtsbywzkbabMapper.selectXlgcsxtsbywzkbabListByXqAdminId(userId,xlgcsxtsbywzkbab);
	}

	@Override
	public List<Xlgcsxtsbywzkbab> selectXlgcsxtsbywzkbabListByDeptId(Long deptId, Xlgcsxtsbywzkbab xlgcsxtsbywzkbab) {
		// TODO Auto-generated method stub
		return xlgcsxtsbywzkbabMapper.selectXlgcsxtsbywzkbabListByDeptId(deptId,xlgcsxtsbywzkbab);
	}
}

package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Xlgcsxtyxb;

/**
 * 雪亮工程摄像头运行Service接口
 * 
 * @author Carry
 * @date 2020-10-04
 */
public interface IXlgcsxtyxbService 
{
    /**
     * 查询雪亮工程摄像头运行
     * 
     * @param id 雪亮工程摄像头运行ID
     * @return 雪亮工程摄像头运行
     */
    public Xlgcsxtyxb selectXlgcsxtyxbById(Long id);

    /**
     * 查询雪亮工程摄像头运行列表
     * 
     * @param xlgcsxtyxb 雪亮工程摄像头运行
     * @return 雪亮工程摄像头运行集合
     */
    public List<Xlgcsxtyxb> selectXlgcsxtyxbList(Xlgcsxtyxb xlgcsxtyxb);

    public List<Xlgcsxtyxb> selectXlgcsxtyxbListByXqAdminId(Long userId,Xlgcsxtyxb xlgcsxtyxb);
    
    public List<Xlgcsxtyxb> selectXlgcsxtyxbListByDeptId(Long deptId,Xlgcsxtyxb xlgcsxtyxb);
    
    
    /**
     * 新增雪亮工程摄像头运行
     * 
     * @param xlgcsxtyxb 雪亮工程摄像头运行
     * @return 结果
     */
    public int insertXlgcsxtyxb(Xlgcsxtyxb xlgcsxtyxb);

    /**
     * 修改雪亮工程摄像头运行
     * 
     * @param xlgcsxtyxb 雪亮工程摄像头运行
     * @return 结果
     */
    public int updateXlgcsxtyxb(Xlgcsxtyxb xlgcsxtyxb);

    /**
     * 批量删除雪亮工程摄像头运行
     * 
     * @param ids 需要删除的雪亮工程摄像头运行ID
     * @return 结果
     */
    public int deleteXlgcsxtyxbByIds(Long[] ids);

    /**
     * 删除雪亮工程摄像头运行信息
     * 
     * @param id 雪亮工程摄像头运行ID
     * @return 结果
     */
    public int deleteXlgcsxtyxbById(Long id);
}

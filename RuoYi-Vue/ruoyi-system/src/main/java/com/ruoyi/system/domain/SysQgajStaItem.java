package com.ruoyi.system.domain;

public class SysQgajStaItem {
	
	//部门ID
	private long deptId;
	
	//部门名称
	private String deptName;
	
	//部门相关数据项数量
	private long count;

	public long getDeptId() {
		return deptId;
	}

	public void setDeptId(long deptId) {
		this.deptId = deptId;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public SysQgajStaItem() {
		super();
	}

	public SysQgajStaItem(long deptId, String deptName, long count) {
		super();
		this.deptId = deptId;
		this.deptName = deptName;
		this.count = count;
	}

	@Override
	public String toString() {
		return "SysQgajStaItem [deptId=" + deptId + ", deptName=" + deptName + ", count=" + count + "]";
	}
	
	

}

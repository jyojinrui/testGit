package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 区公安局信息对象 qgajxxbab
 * 
 * @author Carry
 * @date 2020-10-25
 */
public class Qgajxxbab extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 区公安局编码 */
    @Excel(name = "区公安局编码")
    private String qgajbm;

    /** 地址二维码编码 */
    @Excel(name = "地址二维码编码")
    private String dzewmbm;

    /** 区公安局名称 */
    @Excel(name = "区公安局名称")
    private String qgajmc;

    /** 区公安局地址 */
    @Excel(name = "区公安局地址")
    private String qgajdz;

    /** 所属区县 */
    @Excel(name = "所属区县")
    private String ssqx;

    /** 区公安局负责人 */
    @Excel(name = "区公安局负责人")
    private String qgajfzr;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String lxdh;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setQgajbm(String qgajbm) 
    {
        this.qgajbm = qgajbm;
    }

    public String getQgajbm() 
    {
        return qgajbm;
    }
    public void setDzewmbm(String dzewmbm) 
    {
        this.dzewmbm = dzewmbm;
    }

    public String getDzewmbm() 
    {
        return dzewmbm;
    }
    public void setQgajmc(String qgajmc) 
    {
        this.qgajmc = qgajmc;
    }

    public String getQgajmc() 
    {
        return qgajmc;
    }
    public void setQgajdz(String qgajdz) 
    {
        this.qgajdz = qgajdz;
    }

    public String getQgajdz() 
    {
        return qgajdz;
    }
    public void setSsqx(String ssqx) 
    {
        this.ssqx = ssqx;
    }

    public String getSsqx() 
    {
        return ssqx;
    }
    public void setQgajfzr(String qgajfzr) 
    {
        this.qgajfzr = qgajfzr;
    }

    public String getQgajfzr() 
    {
        return qgajfzr;
    }
    public void setLxdh(String lxdh) 
    {
        this.lxdh = lxdh;
    }

    public String getLxdh() 
    {
        return lxdh;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("qgajbm", getQgajbm())
            .append("dzewmbm", getDzewmbm())
            .append("qgajmc", getQgajmc())
            .append("qgajdz", getQgajdz())
            .append("ssqx", getSsqx())
            .append("qgajfzr", getQgajfzr())
            .append("lxdh", getLxdh())
            .toString();
    }
}

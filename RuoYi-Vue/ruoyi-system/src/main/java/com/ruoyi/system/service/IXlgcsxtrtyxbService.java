package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Xlgcsxtrtyxb;

/**
 * 雪亮工程摄像头人体运行数据Service接口
 * 
 * @author Carry
 * @date 2020-10-04
 */
public interface IXlgcsxtrtyxbService 
{
    /**
     * 查询雪亮工程摄像头人体运行数据
     * 
     * @param id 雪亮工程摄像头人体运行数据ID
     * @return 雪亮工程摄像头人体运行数据
     */
    public Xlgcsxtrtyxb selectXlgcsxtrtyxbById(Long id);

    
    public List<Xlgcsxtrtyxb> selectXlgcsxtrtyxbListByXqAdminId(Long userId,Xlgcsxtrtyxb xlgcsxtrtyxb);
    
    public List<Xlgcsxtrtyxb> selectXlgcsxtrtyxbListByDeptId(Long deptId,Xlgcsxtrtyxb xlgcsxtrtyxb);
    
    
    /**
     * 查询雪亮工程摄像头人体运行数据列表
     * 
     * @param xlgcsxtrtyxb 雪亮工程摄像头人体运行数据
     * @return 雪亮工程摄像头人体运行数据集合
     */
    public List<Xlgcsxtrtyxb> selectXlgcsxtrtyxbList(Xlgcsxtrtyxb xlgcsxtrtyxb);

    /**
     * 新增雪亮工程摄像头人体运行数据
     * 
     * @param xlgcsxtrtyxb 雪亮工程摄像头人体运行数据
     * @return 结果
     */
    public int insertXlgcsxtrtyxb(Xlgcsxtrtyxb xlgcsxtrtyxb);

    /**
     * 修改雪亮工程摄像头人体运行数据
     * 
     * @param xlgcsxtrtyxb 雪亮工程摄像头人体运行数据
     * @return 结果
     */
    public int updateXlgcsxtrtyxb(Xlgcsxtrtyxb xlgcsxtrtyxb);

    /**
     * 批量删除雪亮工程摄像头人体运行数据
     * 
     * @param ids 需要删除的雪亮工程摄像头人体运行数据ID
     * @return 结果
     */
    public int deleteXlgcsxtrtyxbByIds(Long[] ids);

    /**
     * 删除雪亮工程摄像头人体运行数据信息
     * 
     * @param id 雪亮工程摄像头人体运行数据ID
     * @return 结果
     */
    public int deleteXlgcsxtrtyxbById(Long id);
}

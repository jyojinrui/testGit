package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CmxxbMapper;
import com.ruoyi.system.domain.Cmxxb;
import com.ruoyi.system.service.ICmxxbService;

/**
 * 串码信息Service业务层处理
 * 
 * @author Carry
 * @date 2020-10-04
 */
@Service
public class CmxxbServiceImpl implements ICmxxbService 
{
    @Autowired
    private CmxxbMapper cmxxbMapper;

    /**
     * 查询串码信息
     * 
     * @param id 串码信息ID
     * @return 串码信息
     */
    @Override
    public Cmxxb selectCmxxbById(Long id)
    {
//    	System.out.println("==============>");
//    	System.out.println(cmxxbMapper.selectCmxxbById(id));
//    	System.out.println("==============>");
        return cmxxbMapper.selectCmxxbById(id);
    }

    /**
     * 查询串码信息列表
     * 
     * @param cmxxb 串码信息
     * @return 串码信息
     */
    @Override
    public List<Cmxxb> selectCmxxbList(Cmxxb cmxxb)
    {
        return cmxxbMapper.selectCmxxbList(cmxxb);
    }

    /**
     * 新增串码信息
     * 
     * @param cmxxb 串码信息
     * @return 结果
     */
    @Override
    public int insertCmxxb(Cmxxb cmxxb)
    {
        return cmxxbMapper.insertCmxxb(cmxxb);
    }

    /**
     * 修改串码信息
     * 
     * @param cmxxb 串码信息
     * @return 结果
     */
    @Override
    public int updateCmxxb(Cmxxb cmxxb)
    {
        return cmxxbMapper.updateCmxxb(cmxxb);
    }

    /**
     * 批量删除串码信息
     * 
     * @param ids 需要删除的串码信息ID
     * @return 结果
     */
    @Override
    public int deleteCmxxbByIds(Long[] ids)
    {
        return cmxxbMapper.deleteCmxxbByIds(ids);
    }

    /**
     * 删除串码信息信息
     * 
     * @param id 串码信息ID
     * @return 结果
     */
    @Override
    public int deleteCmxxbById(Long id)
    {
        return cmxxbMapper.deleteCmxxbById(id);
    }

	@Override
	public List<Cmxxb> selectCmxxbListByXqAdminId(Long userId, Cmxxb cmxxb) {
		// TODO Auto-generated method stub
		return cmxxbMapper.selectCmxxbListByXqAdminId(userId,cmxxb);
	}

	@Override
	public List<Cmxxb> selectCmxxbListByDeptId(Long deptId, Cmxxb cmxxb) {
		// TODO Auto-generated method stub
		return cmxxbMapper.selectCmxxbListByDeptId(deptId,cmxxb);
	}
}

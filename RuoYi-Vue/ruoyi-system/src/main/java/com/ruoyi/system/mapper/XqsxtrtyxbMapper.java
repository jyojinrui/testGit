package com.ruoyi.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.system.domain.Xqsxtrtyxb;

/**
 * 摄像头人体运行Mapper接口
 * 
 * @author Carry
 * @date 2020-10-03
 */
public interface XqsxtrtyxbMapper 
{
    /**
     * 查询摄像头人体运行
     * 
     * @param id 摄像头人体运行ID
     * @return 摄像头人体运行
     */
    public Xqsxtrtyxb selectXqsxtrtyxbById(Long id);

    
    public List<Xqsxtrtyxb> selectXqsxtrtyxbListByXqAdminId(@Param("userId")Long userId,@Param("xqsxtrtyxb")Xqsxtrtyxb xqsxtrtyxb);
  
    
    public List<Xqsxtrtyxb> selectXqsxtrtyxbListByDeptId(@Param("deptId")Long deptId,@Param("xqsxtrtyxb")Xqsxtrtyxb xqsxtrtyxb);
    

    /**
     * 查询摄像头人体运行列表
     * 
     * @param xqsxtrtyxb 摄像头人体运行
     * @return 摄像头人体运行集合
     */
    public List<Xqsxtrtyxb> selectXqsxtrtyxbList(Xqsxtrtyxb xqsxtrtyxb);

    /**
     * 新增摄像头人体运行
     * 
     * @param xqsxtrtyxb 摄像头人体运行
     * @return 结果
     */
    public int insertXqsxtrtyxb(Xqsxtrtyxb xqsxtrtyxb);

    /**
     * 修改摄像头人体运行
     * 
     * @param xqsxtrtyxb 摄像头人体运行
     * @return 结果
     */
    public int updateXqsxtrtyxb(Xqsxtrtyxb xqsxtrtyxb);

    /**
     * 删除摄像头人体运行
     * 
     * @param id 摄像头人体运行ID
     * @return 结果
     */
    public int deleteXqsxtrtyxbById(Long id);

    /**
     * 批量删除摄像头人体运行
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteXqsxtrtyxbByIds(Long[] ids);
}

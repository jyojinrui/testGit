package com.ruoyi.system.domain;

import java.util.List;

public class PersonInfo {
	private Integer opetype;
	private String id;
	private String name;
	private List<String> image;
	
	public PersonInfo() {
		super();
	}
	
	public PersonInfo(Integer opetype, String id, String name, List<String> image) {
		super();
		this.opetype = opetype;
		this.id = id;
		this.name = name;
		this.image = image;
	}
	@Override
	public String toString() {
		return "PersonInfo [opeType=" + opetype + ", id=" + id + ", name=" + name + ", image=" + image + "]";
	}
	public Integer getOpetype() {
		return opetype;
	}
	public void setOpetype(Integer opetype) {
		this.opetype = opetype;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getImage() {
		return image;
	}
	public void setImage(List<String> image) {
		this.image = image;
	}
	
	
}


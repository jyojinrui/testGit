package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FzxxbabMapper;
import com.ruoyi.common.utils.sign.AESUtil;
import com.ruoyi.system.domain.Fzxxbab;
import com.ruoyi.system.service.IFzxxbabService;

/**
 * 房主信息Service业务层处理
 * 
 * @author Carry
 * @date 2020-10-05
 */
@Service
public class FzxxbabServiceImpl implements IFzxxbabService 
{
    @Autowired
    private FzxxbabMapper fzxxbabMapper;

    /**
     * 查询房主信息
     * 
     * @param id 房主信息ID
     * @return 房主信息
     */
    @Override
    public Fzxxbab selectFzxxbabById(Long id)
    {
        Fzxxbab fzxxbab=fzxxbabMapper.selectFzxxbabById(id);
        
        fzxxbab.setFwdz(AESUtil.decrypt(fzxxbab.getFwdz()));               //房屋地址解密    
        fzxxbab.setFzgmsfhm(AESUtil.decrypt(fzxxbab.getFzgmsfhm()));       //房主身份证解密
        fzxxbab.setFzlxdh(AESUtil.decrypt(fzxxbab.getFzlxdh()));           //房主联系电话解密
        fzxxbab.setWtrgmsfhm(AESUtil.decrypt(fzxxbab.getWtrgmsfhm()));     //委托人身份证解密
        fzxxbab.setWtrlxdh(AESUtil.decrypt(fzxxbab.getWtrlxdh()));         //委托人联系电话解密
      
       
      //  fzxxbab.setFwdz(AESUtil.decrypt(fzxxbab.getFwdz()));
        return fzxxbab;
    }

    /**
     * 查询房主信息列表
     * 
     * @param fzxxbab 房主信息
     * @return 房主信息
     */
    @Override
    public List<Fzxxbab> selectFzxxbabList(Fzxxbab fzxxbab)
    {
    	/**
    	 * 查询参数加密后才能与数据库匹配
    	 */
    	fzxxbab.setFwdz(AESUtil.encrypt(fzxxbab.getFwdz()));           
    	fzxxbab.setFzgmsfhm(AESUtil.encrypt(fzxxbab.getFzgmsfhm()));
    	fzxxbab.setFzlxdh(AESUtil.encrypt(fzxxbab.getFzlxdh()));
    	fzxxbab.setWtrgmsfhm(AESUtil.encrypt(fzxxbab.getWtrgmsfhm()));
    	fzxxbab.setWtrlxdh(AESUtil.encrypt(fzxxbab.getWtrlxdh()));
    	
    	List<Fzxxbab>  fzxxbabList =fzxxbabMapper.selectFzxxbabList(fzxxbab);
    	
    	/**
    	 * 查询到数据解密后再发送给前端显示
    	 */
    	for(int i=0;i<fzxxbabList.size();i++)
    	{
    		fzxxbabList.get(i).setFwdz(AESUtil.decrypt(fzxxbabList.get(i).getFwdz()));
    		fzxxbabList.get(i).setFzgmsfhm(AESUtil.decrypt(fzxxbabList.get(i).getFzgmsfhm()));
    		fzxxbabList.get(i).setFzlxdh(AESUtil.decrypt(fzxxbabList.get(i).getFzlxdh()));
    		fzxxbabList.get(i).setWtrgmsfhm(AESUtil.decrypt(fzxxbabList.get(i).getWtrgmsfhm()));
    		fzxxbabList.get(i).setWtrlxdh(AESUtil.decrypt(fzxxbabList.get(i).getWtrlxdh()));
    		
    	}
    	
        return fzxxbabList;
    }

    /**
     * 新增房主信息
     * 
     * @param fzxxbab 房主信息
     * @return 结果
     */
    @Override
    public int insertFzxxbab(Fzxxbab fzxxbab)
    {
    	
    	/**
    	 * 插入参数加密后才能插入到数据库
    	 */
    	fzxxbab.setFwdz(AESUtil.encrypt(fzxxbab.getFwdz()));           
    	fzxxbab.setFzgmsfhm(AESUtil.encrypt(fzxxbab.getFzgmsfhm()));
    	fzxxbab.setFzlxdh(AESUtil.encrypt(fzxxbab.getFzlxdh()));
    	fzxxbab.setWtrgmsfhm(AESUtil.encrypt(fzxxbab.getWtrgmsfhm()));
    	fzxxbab.setWtrlxdh(AESUtil.encrypt(fzxxbab.getWtrlxdh()));
        return fzxxbabMapper.insertFzxxbab(fzxxbab);
    }

    /**
     * 修改房主信息
     * 
     * @param fzxxbab 房主信息
     * @return 结果
     */
    @Override
    public int updateFzxxbab(Fzxxbab fzxxbab)
    {
    	/**
    	 * 修改参数加密后才能插入数据库
    	 */
    	fzxxbab.setFwdz(AESUtil.encrypt(fzxxbab.getFwdz()));           
    	fzxxbab.setFzgmsfhm(AESUtil.encrypt(fzxxbab.getFzgmsfhm()));
    	fzxxbab.setFzlxdh(AESUtil.encrypt(fzxxbab.getFzlxdh()));
    	fzxxbab.setWtrgmsfhm(AESUtil.encrypt(fzxxbab.getWtrgmsfhm()));
    	fzxxbab.setWtrlxdh(AESUtil.encrypt(fzxxbab.getWtrlxdh()));
        return fzxxbabMapper.updateFzxxbab(fzxxbab);
    }

    /**
     * 批量删除房主信息
     * 
     * @param ids 需要删除的房主信息ID
     * @return 结果
     */
    @Override
    public int deleteFzxxbabByIds(Long[] ids)
    {
        return fzxxbabMapper.deleteFzxxbabByIds(ids);
    }

    /**
     * 删除房主信息信息
     * 
     * @param id 房主信息ID
     * @return 结果
     */
    @Override
    public int deleteFzxxbabById(Long id)
    {
        return fzxxbabMapper.deleteFzxxbabById(id);
    }

    
    /**
     * 根据小区管理获取管辖的小区信息
     */
     
	@Override
	public List<Fzxxbab> selectFzxxbabListByXqAdminId(Long userId,Fzxxbab fzxxbab) {
		// TODO Auto-generated method stub
		/**
    	 * 查询参数加密后才能与数据库匹配
    	 */
    	fzxxbab.setFwdz(AESUtil.encrypt(fzxxbab.getFwdz()));           
    	fzxxbab.setFzgmsfhm(AESUtil.encrypt(fzxxbab.getFzgmsfhm()));
    	fzxxbab.setFzlxdh(AESUtil.encrypt(fzxxbab.getFzlxdh()));
    	fzxxbab.setWtrgmsfhm(AESUtil.encrypt(fzxxbab.getWtrgmsfhm()));
    	fzxxbab.setWtrlxdh(AESUtil.encrypt(fzxxbab.getWtrlxdh()));
    	
    	List<Fzxxbab>  fzxxbabList =fzxxbabMapper.selectFzxxbabListByXqAdminId(userId,fzxxbab);
    	
    	/**
    	 * 查询到数据解密后再发送给前端显示
    	 */
    	for(int i=0;i<fzxxbabList.size();i++)
    	{
    		fzxxbabList.get(i).setFwdz(AESUtil.decrypt(fzxxbabList.get(i).getFwdz()));
    		fzxxbabList.get(i).setFzgmsfhm(AESUtil.decrypt(fzxxbabList.get(i).getFzgmsfhm()));
    		fzxxbabList.get(i).setFzlxdh(AESUtil.decrypt(fzxxbabList.get(i).getFzlxdh()));
    		fzxxbabList.get(i).setWtrgmsfhm(AESUtil.decrypt(fzxxbabList.get(i).getWtrgmsfhm()));
    		fzxxbabList.get(i).setWtrlxdh(AESUtil.decrypt(fzxxbabList.get(i).getWtrlxdh()));
    		
    	}
    	
		return fzxxbabList;
	}

	@Override
	public List<Fzxxbab> selectFzxxbabListByDeptId(Long deptId,Long deptIdOfTree, Fzxxbab fzxxbab) {
		// TODO Auto-generated method stub
		/**
    	 * 查询参数加密后才能与数据库匹配
    	 */
    	fzxxbab.setFwdz(AESUtil.encrypt(fzxxbab.getFwdz()));           
    	fzxxbab.setFzgmsfhm(AESUtil.encrypt(fzxxbab.getFzgmsfhm()));
    	fzxxbab.setFzlxdh(AESUtil.encrypt(fzxxbab.getFzlxdh()));
    	fzxxbab.setWtrgmsfhm(AESUtil.encrypt(fzxxbab.getWtrgmsfhm()));
    	fzxxbab.setWtrlxdh(AESUtil.encrypt(fzxxbab.getWtrlxdh()));
    	
    	List<Fzxxbab>  fzxxbabList =fzxxbabMapper.selectFzxxbabListByDeptId(deptId, deptIdOfTree,fzxxbab);
    	
    	/**
    	 * 查询到数据解密后再发送给前端显示
    	 */
    	for(int i=0;i<fzxxbabList.size();i++)
    	{
    		fzxxbabList.get(i).setFwdz(AESUtil.decrypt(fzxxbabList.get(i).getFwdz()));
    		fzxxbabList.get(i).setFzgmsfhm(AESUtil.decrypt(fzxxbabList.get(i).getFzgmsfhm()));
    		fzxxbabList.get(i).setFzlxdh(AESUtil.decrypt(fzxxbabList.get(i).getFzlxdh()));
    		fzxxbabList.get(i).setWtrgmsfhm(AESUtil.decrypt(fzxxbabList.get(i).getWtrgmsfhm()));
    		fzxxbabList.get(i).setWtrlxdh(AESUtil.decrypt(fzxxbabList.get(i).getWtrlxdh()));
    		
    	}
		return fzxxbabList;
	}
	public static void main(String []args)
	{
		System.out.println("jiami");
		System.out.println(AESUtil.decrypt(""));
		System.out.println("jiami");
	}

	@Override
	public List<Fzxxbab> selectFzxxbabListByPcsAdminId(Long userId, Fzxxbab fzxxbab) {
		// TODO Auto-generated method stub
		return null;
	}
}

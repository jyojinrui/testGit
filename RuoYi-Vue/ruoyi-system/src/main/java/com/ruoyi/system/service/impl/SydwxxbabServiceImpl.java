package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SydwxxbabMapper;
import com.ruoyi.system.domain.Sydwxxbab;
import com.ruoyi.system.service.ISydwxxbabService;

/**
 * 实有单位信息Service业务层处理
 * 
 * @author Carry
 * @date 2020-10-27
 */
@Service
public class SydwxxbabServiceImpl implements ISydwxxbabService 
{
    @Autowired
    private SydwxxbabMapper sydwxxbabMapper;

    /**
     * 查询实有单位信息
     * 
     * @param id 实有单位信息ID
     * @return 实有单位信息
     */
    @Override
    public Sydwxxbab selectSydwxxbabById(Long id)
    {
        return sydwxxbabMapper.selectSydwxxbabById(id);
    }

    /**
     * 查询实有单位信息列表
     * 
     * @param sydwxxbab 实有单位信息
     * @return 实有单位信息
     */
    @Override
    public List<Sydwxxbab> selectSydwxxbabList(Sydwxxbab sydwxxbab)
    {
        return sydwxxbabMapper.selectSydwxxbabList(sydwxxbab);
    }

    /**
     * 新增实有单位信息
     * 
     * @param sydwxxbab 实有单位信息
     * @return 结果
     */
    @Override
    public int insertSydwxxbab(Sydwxxbab sydwxxbab)
    {
        return sydwxxbabMapper.insertSydwxxbab(sydwxxbab);
    }

    /**
     * 修改实有单位信息
     * 
     * @param sydwxxbab 实有单位信息
     * @return 结果
     */
    @Override
    public int updateSydwxxbab(Sydwxxbab sydwxxbab)
    {
        return sydwxxbabMapper.updateSydwxxbab(sydwxxbab);
    }

    /**
     * 批量删除实有单位信息
     * 
     * @param ids 需要删除的实有单位信息ID
     * @return 结果
     */
    @Override
    public int deleteSydwxxbabByIds(Long[] ids)
    {
        return sydwxxbabMapper.deleteSydwxxbabByIds(ids);
    }

    /**
     * 删除实有单位信息信息
     * 
     * @param id 实有单位信息ID
     * @return 结果
     */
    @Override
    public int deleteSydwxxbabById(Long id)
    {
        return sydwxxbabMapper.deleteSydwxxbabById(id);
    }
}

package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * netty记录表对象 sys_netty_record
 * 
 * @author ruoyi
 * @date 2020-12-17
 */
public class SysNettyRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 所属小区编码 */
    @Excel(name = "所属小区编码")
    private String ssxqbm;

    /** 最请求时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "最请求时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date lastreqtime;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSsxqbm(String ssxqbm) 
    {
        this.ssxqbm = ssxqbm;
    }

    public String getSsxqbm() 
    {
        return ssxqbm;
    }
    public void setLastreqtime(Date lastreqtime) 
    {
        this.lastreqtime = lastreqtime;
    }

    public Date getLastreqtime() 
    {
        return lastreqtime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ssxqbm", getSsxqbm())
            .append("lastreqtime", getLastreqtime())
            .toString();
    }
}

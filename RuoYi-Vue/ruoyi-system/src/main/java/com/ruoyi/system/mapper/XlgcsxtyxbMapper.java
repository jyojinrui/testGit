package com.ruoyi.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.system.domain.Xlgcsxtyxb;

/**
 * 雪亮工程摄像头运行Mapper接口
 * 
 * @author Carry
 * @date 2020-10-04
 */
public interface XlgcsxtyxbMapper 
{
    /**
     * 查询雪亮工程摄像头运行
     * 
     * @param id 雪亮工程摄像头运行ID
     * @return 雪亮工程摄像头运行
     */
    public Xlgcsxtyxb selectXlgcsxtyxbById(Long id);

    /**
     * 查询雪亮工程摄像头运行列表
     * 
     * @param xlgcsxtyxb 雪亮工程摄像头运行
     * @return 雪亮工程摄像头运行集合
     */
    public List<Xlgcsxtyxb> selectXlgcsxtyxbList(Xlgcsxtyxb xlgcsxtyxb);

    
    public List<Xlgcsxtyxb> selectXlgcsxtyxbListByXqAdminId(@Param("userId")Long userId,@Param("xlgcsxtyxb")Xlgcsxtyxb xlgcsxtyxb);
   
    public List<Xlgcsxtyxb> selectXlgcsxtyxbListByDeptId(@Param("deptId")Long deptId,@Param("xlgcsxtyxb")Xlgcsxtyxb xlgcsxtyxb);
    
    
    /**
     * 新增雪亮工程摄像头运行
     * 
     * @param xlgcsxtyxb 雪亮工程摄像头运行
     * @return 结果
     */
    public int insertXlgcsxtyxb(Xlgcsxtyxb xlgcsxtyxb);

    /**
     * 修改雪亮工程摄像头运行
     * 
     * @param xlgcsxtyxb 雪亮工程摄像头运行
     * @return 结果
     */
    public int updateXlgcsxtyxb(Xlgcsxtyxb xlgcsxtyxb);

    /**
     * 删除雪亮工程摄像头运行
     * 
     * @param id 雪亮工程摄像头运行ID
     * @return 结果
     */
    public int deleteXlgcsxtyxbById(Long id);

    /**
     * 批量删除雪亮工程摄像头运行
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteXlgcsxtyxbByIds(Long[] ids);
}

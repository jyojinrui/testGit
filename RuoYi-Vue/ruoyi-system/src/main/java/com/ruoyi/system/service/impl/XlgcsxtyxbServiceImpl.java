package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.XlgcsxtyxbMapper;
import com.ruoyi.system.domain.Xlgcsxtyxb;
import com.ruoyi.system.service.IXlgcsxtyxbService;

/**
 * 雪亮工程摄像头运行Service业务层处理
 * 
 * @author Carry
 * @date 2020-10-04
 */
@Service
public class XlgcsxtyxbServiceImpl implements IXlgcsxtyxbService 
{
    @Autowired
    private XlgcsxtyxbMapper xlgcsxtyxbMapper;

    /**
     * 查询雪亮工程摄像头运行
     * 
     * @param id 雪亮工程摄像头运行ID
     * @return 雪亮工程摄像头运行
     */
    @Override
    public Xlgcsxtyxb selectXlgcsxtyxbById(Long id)
    {
        return xlgcsxtyxbMapper.selectXlgcsxtyxbById(id);
    }

    /**
     * 查询雪亮工程摄像头运行列表
     * 
     * @param xlgcsxtyxb 雪亮工程摄像头运行
     * @return 雪亮工程摄像头运行
     */
    @Override
    public List<Xlgcsxtyxb> selectXlgcsxtyxbList(Xlgcsxtyxb xlgcsxtyxb)
    {
        return xlgcsxtyxbMapper.selectXlgcsxtyxbList(xlgcsxtyxb);
    }

    /**
     * 新增雪亮工程摄像头运行
     * 
     * @param xlgcsxtyxb 雪亮工程摄像头运行
     * @return 结果
     */
    @Override
    public int insertXlgcsxtyxb(Xlgcsxtyxb xlgcsxtyxb)
    {
        return xlgcsxtyxbMapper.insertXlgcsxtyxb(xlgcsxtyxb);
    }

    /**
     * 修改雪亮工程摄像头运行
     * 
     * @param xlgcsxtyxb 雪亮工程摄像头运行
     * @return 结果
     */
    @Override
    public int updateXlgcsxtyxb(Xlgcsxtyxb xlgcsxtyxb)
    {
        return xlgcsxtyxbMapper.updateXlgcsxtyxb(xlgcsxtyxb);
    }

    /**
     * 批量删除雪亮工程摄像头运行
     * 
     * @param ids 需要删除的雪亮工程摄像头运行ID
     * @return 结果
     */
    @Override
    public int deleteXlgcsxtyxbByIds(Long[] ids)
    {
        return xlgcsxtyxbMapper.deleteXlgcsxtyxbByIds(ids);
    }

    /**
     * 删除雪亮工程摄像头运行信息
     * 
     * @param id 雪亮工程摄像头运行ID
     * @return 结果
     */
    @Override
    public int deleteXlgcsxtyxbById(Long id)
    {
        return xlgcsxtyxbMapper.deleteXlgcsxtyxbById(id);
    }

	@Override
	public List<Xlgcsxtyxb> selectXlgcsxtyxbListByXqAdminId(Long userId, Xlgcsxtyxb xlgcsxtyxb) {
		// TODO Auto-generated method stub
		return xlgcsxtyxbMapper.selectXlgcsxtyxbListByXqAdminId(userId,xlgcsxtyxb);
	}

	@Override
	public List<Xlgcsxtyxb> selectXlgcsxtyxbListByDeptId(Long deptId, Xlgcsxtyxb xlgcsxtyxb) {
		// TODO Auto-generated method stub
		return xlgcsxtyxbMapper.selectXlgcsxtyxbListByDeptId(deptId,xlgcsxtyxb);
	}
}

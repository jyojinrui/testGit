package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.QgajbmPcsbmMapper;
import com.ruoyi.system.domain.QgajbmPcsbm;
import com.ruoyi.system.service.IQgajbmPcsbmService;

/**
 * 派出所编码与区公安局编码Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-11-03
 */
@Service
public class QgajbmPcsbmServiceImpl implements IQgajbmPcsbmService 
{
    @Autowired
    private QgajbmPcsbmMapper qgajbmPcsbmMapper;

    /**
     * 查询派出所编码与区公安局编码
     * 
     * @param id 派出所编码与区公安局编码ID
     * @return 派出所编码与区公安局编码
     */
    @Override
    public QgajbmPcsbm selectQgajbmPcsbmById(Long id)
    {
        return qgajbmPcsbmMapper.selectQgajbmPcsbmById(id);
    }

    /**
     * 查询派出所编码与区公安局编码列表
     * 
     * @param qgajbmPcsbm 派出所编码与区公安局编码
     * @return 派出所编码与区公安局编码
     */
    @Override
    public List<QgajbmPcsbm> selectQgajbmPcsbmList(QgajbmPcsbm qgajbmPcsbm)
    {
        return qgajbmPcsbmMapper.selectQgajbmPcsbmList(qgajbmPcsbm);
    }

    /**
     * 新增派出所编码与区公安局编码
     * 
     * @param qgajbmPcsbm 派出所编码与区公安局编码
     * @return 结果
     */
    @Override
    public int insertQgajbmPcsbm(QgajbmPcsbm qgajbmPcsbm)
    {
        return qgajbmPcsbmMapper.insertQgajbmPcsbm(qgajbmPcsbm);
    }

    /**
     * 修改派出所编码与区公安局编码
     * 
     * @param qgajbmPcsbm 派出所编码与区公安局编码
     * @return 结果
     */
    @Override
    public int updateQgajbmPcsbm(QgajbmPcsbm qgajbmPcsbm)
    {
        return qgajbmPcsbmMapper.updateQgajbmPcsbm(qgajbmPcsbm);
    }

    /**
     * 批量删除派出所编码与区公安局编码
     * 
     * @param ids 需要删除的派出所编码与区公安局编码ID
     * @return 结果
     */
    @Override
    public int deleteQgajbmPcsbmByIds(Long[] ids)
    {
        return qgajbmPcsbmMapper.deleteQgajbmPcsbmByIds(ids);
    }

    /**
     * 删除派出所编码与区公安局编码信息
     * 
     * @param id 派出所编码与区公安局编码ID
     * @return 结果
     */
    @Override
    public int deleteQgajbmPcsbmById(Long id)
    {
        return qgajbmPcsbmMapper.deleteQgajbmPcsbmById(id);
    }
}

package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 小区摄像头设备对象 xqsxtsbbab
 * 
 * @author Carry
 * @date 2020-10-03
 */
public class Xqsxtsbbab extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 小区编码 */
    @Excel(name = "小区编码")
    private String buildingId;

    /** 摄像头编码 */
    @Excel(name = "摄像头编码")
    private String sxtbm;

    /** 经度（高德坐标） */
    @Excel(name = "经度", readConverterExp = "高=德坐标")
    private BigDecimal mapx;

    /** 纬度（高德坐标） */
    @Excel(name = "纬度", readConverterExp = "高=德坐标")
    private BigDecimal mapy;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBuildingId(String buildingId) 
    {
        this.buildingId = buildingId;
    }

    public String getBuildingId() 
    {
        return buildingId;
    }
    public void setSxtbm(String sxtbm) 
    {
        this.sxtbm = sxtbm;
    }

    public String getSxtbm() 
    {
        return sxtbm;
    }
    public void setMapx(BigDecimal mapx) 
    {
        this.mapx = mapx;
    }

    public BigDecimal getMapx() 
    {
        return mapx;
    }
    public void setMapy(BigDecimal mapy) 
    {
        this.mapy = mapy;
    }

    public BigDecimal getMapy() 
    {
        return mapy;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("buildingId", getBuildingId())
            .append("sxtbm", getSxtbm())
            .append("mapx", getMapx())
            .append("mapy", getMapy())
            .toString();
    }
}

package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Xqsxtrlyxb;

/**
 * 人脸运行数据Service接口
 * 
 * @author Carry
 * @date 2020-10-03
 */
public interface IXqsxtrlyxbService 
{
    /**
     * 查询人脸运行数据
     * 
     * @param id 人脸运行数据ID
     * @return 人脸运行数据
     */
    public Xqsxtrlyxb selectXqsxtrlyxbById(Long id);

    
    
    public List<Xqsxtrlyxb> selectXqsxtrlyxbListByXqAdminId(Long userId,Xqsxtrlyxb xqsxtrlyxb);
    
    public List<Xqsxtrlyxb> selectXqsxtrlyxbListByDeptId(Long deptId,Xqsxtrlyxb xqsxtrlyxb);
    
    
    
    /**
     * 查询人脸运行数据列表
     * 
     * @param xqsxtrlyxb 人脸运行数据
     * @return 人脸运行数据集合
     */
    public List<Xqsxtrlyxb> selectXqsxtrlyxbList(Xqsxtrlyxb xqsxtrlyxb);

    /**
     * 新增人脸运行数据
     * 
     * @param xqsxtrlyxb 人脸运行数据
     * @return 结果
     */
    public int insertXqsxtrlyxb(Xqsxtrlyxb xqsxtrlyxb);

    /**
     * 修改人脸运行数据
     * 
     * @param xqsxtrlyxb 人脸运行数据
     * @return 结果
     */
    public int updateXqsxtrlyxb(Xqsxtrlyxb xqsxtrlyxb);

    /**
     * 批量删除人脸运行数据
     * 
     * @param ids 需要删除的人脸运行数据ID
     * @return 结果
     */
    public int deleteXqsxtrlyxbByIds(Long[] ids);

    /**
     * 删除人脸运行数据信息
     * 
     * @param id 人脸运行数据ID
     * @return 结果
     */
    public int deleteXqsxtrlyxbById(Long id);
}

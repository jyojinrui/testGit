package com.ruoyi.system.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.system.domain.SysNettyRecord;

/**
 * netty记录表Mapper接口
 * 
 * @author ruoyi
 * @date 2020-12-17
 */
public interface SysNettyRecordMapper 
{
    /**
     * 查询netty记录表
     * 
     * @param id netty记录表ID
     * @return netty记录表
     */
    public SysNettyRecord selectSysNettyRecordById(Long id);

    /**
     * 查询netty记录表列表
     * 
     * @param sysNettyRecord netty记录表
     * @return netty记录表集合
     */
    public List<SysNettyRecord> selectSysNettyRecordList(SysNettyRecord sysNettyRecord);

    /**
     * 新增netty记录表
     * 
     * @param sysNettyRecord netty记录表
     * @return 结果
     */
    public int insertSysNettyRecord(SysNettyRecord sysNettyRecord);

    /**
     * 修改netty记录表
     * 
     * @param sysNettyRecord netty记录表
     * @return 结果
     */
    public int updateSysNettyRecord(SysNettyRecord sysNettyRecord);

    /**
     * 删除netty记录表
     * 
     * @param id netty记录表ID
     * @return 结果
     */
    public int deleteSysNettyRecordById(Long id);

    /**
     * 批量删除netty记录表
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysNettyRecordByIds(Long[] ids);
    
    
    public int ssxqbmExist(String ssxqbm);
    
    public void updateSysNettyRecordBySsxqbm(@Param("ssxqbm")String ssxqbm,@Param("lastreqtime")Date lastreqtime);
    
    public SysNettyRecord selectSysNettyRecordBySsxqbm(String buildingId);
    
}

package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Mjkxxbab;

/**
 * 门禁卡Service接口
 * 
 * @author Carry
 * @date 2020-09-29
 */
public interface IMjkxxbabService 
{
    /**
     * 查询门禁卡
     * 
     * @param id 门禁卡ID
     * @return 门禁卡
     */
    public Mjkxxbab selectMjkxxbabById(Long id);

    /**
     * 查询门禁卡列表
     * 
     * @param mjkxxbab 门禁卡
     * @return 门禁卡集合
     */
    public List<Mjkxxbab> selectMjkxxbabList(Mjkxxbab mjkxxbab);

    
    /**
     * 根据小区管理员id获取门禁卡信息
     * @param userId
     * @param mjkxxbab
     * @return
     */
    public List<Mjkxxbab> selectMjkxxbabByXqAdminId(Long userId,Mjkxxbab mjkxxbab);
    
    public List<Mjkxxbab> selectMjkxxbabListByDeptId(Long deptId,Long deptIdOfTree,Mjkxxbab mjkxxbab);
    
    
    /**
     * 新增门禁卡
     * 
     * @param mjkxxbab 门禁卡
     * @return 结果
     */
    public int insertMjkxxbab(Mjkxxbab mjkxxbab);

    /**
     * 修改门禁卡
     * 
     * @param mjkxxbab 门禁卡
     * @return 结果
     */
    public int updateMjkxxbab(Mjkxxbab mjkxxbab);

    /**
     * 批量删除门禁卡
     * 
     * @param ids 需要删除的门禁卡ID
     * @return 结果
     */
    public int deleteMjkxxbabByIds(Long[] ids);

    /**
     * 删除门禁卡信息
     * 
     * @param id 门禁卡ID
     * @return 结果
     */
    public int deleteMjkxxbabById(Long id);
}

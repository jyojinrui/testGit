package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 门禁卡对象 mjkxxbab
 * 
 * @author Carry
 * @date 2020-09-29
 */
public class Mjkxxbab extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 门禁卡编号 */
    @Excel(name = "门禁卡编号")
    private String mjkbh;

    /** 门禁卡类型 */
    @Excel(name = "门禁卡类型")
    private String mjklx;

    /** 登记时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "登记时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date djsj;

    /** 注销时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "注销时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date zxsj;

    /** 小区编码 */
    @Excel(name = "小区编码")
    private String ssxqbm;

    /** 申报系统 */
    @Excel(name = "申报系统")
    private String sbxt;

    /** 持卡人 */
    @Excel(name = "持卡人")
    private String ckr;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMjkbh(String mjkbh) 
    {
        this.mjkbh = mjkbh;
    }

    public String getMjkbh() 
    {
        return mjkbh;
    }
    public void setMjklx(String mjklx) 
    {
        this.mjklx = mjklx;
    }

    public String getMjklx() 
    {
        return mjklx;
    }
    public void setDjsj(Date djsj) 
    {
        this.djsj = djsj;
    }

    public Date getDjsj() 
    {
        return djsj;
    }
    public void setZxsj(Date zxsj) 
    {
        this.zxsj = zxsj;
    }

    public Date getZxsj() 
    {
        return zxsj;
    }
    public void setSsxqbm(String ssxqbm) 
    {
        this.ssxqbm = ssxqbm;
    }

    public String getSsxqbm() 
    {
        return ssxqbm;
    }
    public void setSbxt(String sbxt) 
    {
        this.sbxt = sbxt;
    }

    public String getSbxt() 
    {
        return sbxt;
    }
    public void setCkr(String ckr) 
    {
        this.ckr = ckr;
    }

    public String getCkr() 
    {
        return ckr;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("mjkbh", getMjkbh())
            .append("mjklx", getMjklx())
            .append("djsj", getDjsj())
            .append("zxsj", getZxsj())
            .append("ssxqbm", getSsxqbm())
            .append("sbxt", getSbxt())
            .append("ckr", getCkr())
            .toString();
    }
}

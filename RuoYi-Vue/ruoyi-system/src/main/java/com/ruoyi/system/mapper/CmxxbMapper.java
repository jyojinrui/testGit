package com.ruoyi.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.system.domain.Cmxxb;

/**
 * 串码信息Mapper接口
 * 
 * @author Carry
 * @date 2020-10-04
 */
public interface CmxxbMapper 
{
    /**
     * 查询串码信息
     * 
     * @param id 串码信息ID
     * @return 串码信息
     */
    public Cmxxb selectCmxxbById(Long id);

    /**
     * 查询串码信息列表
     * 
     * @param cmxxb 串码信息
     * @return 串码信息集合
     */
    public List<Cmxxb> selectCmxxbList(Cmxxb cmxxb);
    
    public List<Cmxxb> selectCmxxbListByXqAdminId(@Param("userId")Long userId,@Param("cmxxb")Cmxxb cmxxb);
    
    
    public List<Cmxxb> selectCmxxbListByDeptId(@Param("deptId")Long deptId,@Param("cmxxb")Cmxxb cmxxb);
    
    /**
     * 新增串码信息
     * 
     * @param cmxxb 串码信息
     * @return 结果
     */
    public int insertCmxxb(Cmxxb cmxxb);

    /**
     * 修改串码信息
     * 
     * @param cmxxb 串码信息
     * @return 结果
     */
    public int updateCmxxb(Cmxxb cmxxb);

    /**
     * 删除串码信息
     * 
     * @param id 串码信息ID
     * @return 结果
     */
    public int deleteCmxxbById(Long id);

    /**
     * 批量删除串码信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCmxxbByIds(Long[] ids);
}

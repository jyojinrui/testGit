package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.QgajbmPcsbm;

/**
 * 派出所编码与区公安局编码Service接口
 * 
 * @author ruoyi
 * @date 2020-11-03
 */
public interface IQgajbmPcsbmService 
{
    /**
     * 查询派出所编码与区公安局编码
     * 
     * @param id 派出所编码与区公安局编码ID
     * @return 派出所编码与区公安局编码
     */
    public QgajbmPcsbm selectQgajbmPcsbmById(Long id);

    /**
     * 查询派出所编码与区公安局编码列表
     * 
     * @param qgajbmPcsbm 派出所编码与区公安局编码
     * @return 派出所编码与区公安局编码集合
     */
    public List<QgajbmPcsbm> selectQgajbmPcsbmList(QgajbmPcsbm qgajbmPcsbm);

    /**
     * 新增派出所编码与区公安局编码
     * 
     * @param qgajbmPcsbm 派出所编码与区公安局编码
     * @return 结果
     */
    public int insertQgajbmPcsbm(QgajbmPcsbm qgajbmPcsbm);

    /**
     * 修改派出所编码与区公安局编码
     * 
     * @param qgajbmPcsbm 派出所编码与区公安局编码
     * @return 结果
     */
    public int updateQgajbmPcsbm(QgajbmPcsbm qgajbmPcsbm);

    /**
     * 批量删除派出所编码与区公安局编码
     * 
     * @param ids 需要删除的派出所编码与区公安局编码ID
     * @return 结果
     */
    public int deleteQgajbmPcsbmByIds(Long[] ids);

    /**
     * 删除派出所编码与区公安局编码信息
     * 
     * @param id 派出所编码与区公安局编码ID
     * @return 结果
     */
    public int deleteQgajbmPcsbmById(Long id);
}

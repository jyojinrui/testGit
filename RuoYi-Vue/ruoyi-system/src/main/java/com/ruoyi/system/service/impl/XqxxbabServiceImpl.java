package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.XqxxbabMapper;
import com.ruoyi.system.domain.Xqxxbab;
import com.ruoyi.system.domain.Zzxqxxbab;
import com.ruoyi.system.service.IXqxxbabService;

/**
 * 小区管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-09-26
 */
@Service
public class XqxxbabServiceImpl implements IXqxxbabService {
	@Autowired
	private XqxxbabMapper xqxxbabMapper;

	/**
	 * 查询小区管理
	 * 
	 * @param id 小区管理ID
	 * @return 小区管理
	 */
	@Override
	public Xqxxbab selectXqxxbabById(String id) {
		return xqxxbabMapper.selectXqxxbabById(id);
	}

	/**
	 * 查询小区管理列表
	 * 
	 * @param xqxxbab 小区管理
	 * @return 小区管理
	 */
	@Override
	public List<Xqxxbab> selectXqxxbabList(Xqxxbab xqxxbab) {
		return xqxxbabMapper.selectXqxxbabList(xqxxbab);
	}

	/** 随机生成9位数的字符串 */
	public static String getRandom(int length) {
		String val = "";
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			val += String.valueOf(random.nextInt(10));
		}
		return val;
	}

	/**
	 * 新增小区管理
	 * 
	 * @param xqxxbab 小区管理
	 * @return 结果
	 */
	@Override
	public int insertXqxxbab(Xqxxbab xqxxbab) {
//        System.out.println("=====================>");
//    	System.out.println(xqxxbab.getBuildingId());
		xqxxbab.setBuildingId(xqxxbab.getBuildingId().toString() + getRandom(9));
//        System.out.println(xqxxbab.getBuildingId());
//        System.out.println("=====================>");
		return xqxxbabMapper.insertXqxxbab(xqxxbab);
	}

	/**
	 * 修改小区管理
	 * 
	 * @param xqxxbab 小区管理
	 * @return 结果
	 */
	@Override
	public int updateXqxxbab(Xqxxbab xqxxbab) {
		return xqxxbabMapper.updateXqxxbab(xqxxbab);
	}

	/**
	 * 批量删除小区管理
	 * 
	 * @param ids 需要删除的小区管理ID
	 * @return 结果
	 */
	@Override
	public int deleteXqxxbabByIds(String[] ids) {
		return xqxxbabMapper.deleteXqxxbabByIds(ids);
	}

	/**
	 * 删除小区管理信息
	 * 
	 * @param id 小区管理ID
	 * @return 结果
	 */
	@Override
	public int deleteXqxxbabById(String id) {
		return xqxxbabMapper.deleteXqxxbabById(id);
	}

	@Override
	public List<Xqxxbab> selectAllXqxxbabList(){
		return xqxxbabMapper.selectAllXqxxbabList();
	}
	
	/**
	 * 根据小区管理员信息查看小区信息
	 */
	@Override
	public Xqxxbab getXqxxByXqAdminId(Long userId) {
		// TODO Auto-generated method stub
		return xqxxbabMapper.getXqxxByXqAdminId((userId));
	}

	@Override
	public List<Xqxxbab> selectXqxxbabListByDeptId(Long deptId,Long deptIdOfTree, Xqxxbab xqxxbab) {
		// TODO Auto-generated method stub
		return xqxxbabMapper.selectXqxxbabListByDeptId(deptId,deptIdOfTree,xqxxbab);
	}

	@Override
	public List<Zzxqxxbab> getBzdzxxbabByXqbm(String buildingId) {
		// TODO Auto-generated method stub
		return xqxxbabMapper.getBzdzxxbabByXqbm(buildingId);
	}

	@Override
	public Xqxxbab selectXqxxbabBySsxqbm(String ssxqbm) {
		// TODO Auto-generated method stub
		return xqxxbabMapper.selectXqxxbabBySsxqbm(ssxqbm);
	}
}

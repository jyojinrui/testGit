package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.XqsxtywzkbMapper;
import com.ruoyi.system.domain.Xqsxtywzkb;
import com.ruoyi.system.service.IXqsxtywzkbService;

/**
 * 运维状况Service业务层处理
 * 
 * @author Carry
 * @date 2020-10-03
 */
@Service
public class XqsxtywzkbServiceImpl implements IXqsxtywzkbService 
{
    @Autowired
    private XqsxtywzkbMapper xqsxtywzkbMapper;

    /**
     * 查询运维状况
     * 
     * @param id 运维状况ID
     * @return 运维状况
     */
    @Override
    public Xqsxtywzkb selectXqsxtywzkbById(Long id)
    {
        return xqsxtywzkbMapper.selectXqsxtywzkbById(id);
    }

    /**
     * 查询运维状况列表
     * 
     * @param xqsxtywzkb 运维状况
     * @return 运维状况
     */
    @Override
    public List<Xqsxtywzkb> selectXqsxtywzkbList(Xqsxtywzkb xqsxtywzkb)
    {
        return xqsxtywzkbMapper.selectXqsxtywzkbList(xqsxtywzkb);
    }

    /**
     * 新增运维状况
     * 
     * @param xqsxtywzkb 运维状况
     * @return 结果
     */
    @Override
    public int insertXqsxtywzkb(Xqsxtywzkb xqsxtywzkb)
    {
        return xqsxtywzkbMapper.insertXqsxtywzkb(xqsxtywzkb);
    }

    /**
     * 修改运维状况
     * 
     * @param xqsxtywzkb 运维状况
     * @return 结果
     */
    @Override
    public int updateXqsxtywzkb(Xqsxtywzkb xqsxtywzkb)
    {
        return xqsxtywzkbMapper.updateXqsxtywzkb(xqsxtywzkb);
    }

    /**
     * 批量删除运维状况
     * 
     * @param ids 需要删除的运维状况ID
     * @return 结果
     */
    @Override
    public int deleteXqsxtywzkbByIds(Long[] ids)
    {
        return xqsxtywzkbMapper.deleteXqsxtywzkbByIds(ids);
    }

    /**
     * 删除运维状况信息
     * 
     * @param id 运维状况ID
     * @return 结果
     */
    @Override
    public int deleteXqsxtywzkbById(Long id)
    {
        return xqsxtywzkbMapper.deleteXqsxtywzkbById(id);
    }

	@Override
	public List<Xqsxtywzkb> selectXqsxtywzkbListByXqAdminId(Long userId, Xqsxtywzkb xqsxtywzkb) {
		// TODO Auto-generated method stub
		return xqsxtywzkbMapper.selectXqsxtywzkbListByXqAdminId(userId,xqsxtywzkb);
	}

	@Override
	public List<Xqsxtywzkb> selectXqsxtywzkbListByDeptId(Long deptId, Xqsxtywzkb xqsxtywzkb) {
		// TODO Auto-generated method stub
		return xqsxtywzkbMapper.selectXqsxtywzkbListByDeptId(deptId,xqsxtywzkb);
	}
}

package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CyryxxbabMapper;
import com.ruoyi.system.domain.Cyryxxbab;
import com.ruoyi.system.service.ICyryxxbabService;

/**
 * 从业人员信息Service业务层处理
 * 
 * @author Carry
 * @date 2020-10-27
 */
@Service
public class CyryxxbabServiceImpl implements ICyryxxbabService 
{
    @Autowired
    private CyryxxbabMapper cyryxxbabMapper;

    /**
     * 查询从业人员信息
     * 
     * @param id 从业人员信息ID
     * @return 从业人员信息
     */
    @Override
    public Cyryxxbab selectCyryxxbabById(Long id)
    {
        return cyryxxbabMapper.selectCyryxxbabById(id);
    }

    /**
     * 查询从业人员信息列表
     * 
     * @param cyryxxbab 从业人员信息
     * @return 从业人员信息
     */
    @Override
    public List<Cyryxxbab> selectCyryxxbabList(Cyryxxbab cyryxxbab)
    {
        return cyryxxbabMapper.selectCyryxxbabList(cyryxxbab);
    }

    /**
     * 新增从业人员信息
     * 
     * @param cyryxxbab 从业人员信息
     * @return 结果
     */
    @Override
    public int insertCyryxxbab(Cyryxxbab cyryxxbab)
    {
        return cyryxxbabMapper.insertCyryxxbab(cyryxxbab);
    }

    /**
     * 修改从业人员信息
     * 
     * @param cyryxxbab 从业人员信息
     * @return 结果
     */
    @Override
    public int updateCyryxxbab(Cyryxxbab cyryxxbab)
    {
        return cyryxxbabMapper.updateCyryxxbab(cyryxxbab);
    }

    /**
     * 批量删除从业人员信息
     * 
     * @param ids 需要删除的从业人员信息ID
     * @return 结果
     */
    @Override
    public int deleteCyryxxbabByIds(Long[] ids)
    {
        return cyryxxbabMapper.deleteCyryxxbabByIds(ids);
    }

    /**
     * 删除从业人员信息信息
     * 
     * @param id 从业人员信息ID
     * @return 结果
     */
    @Override
    public int deleteCyryxxbabById(Long id)
    {
        return cyryxxbabMapper.deleteCyryxxbabById(id);
    }
}

package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PcsxxbabMapper;
import com.ruoyi.system.mapper.QgajbmPcsbmMapper;
import com.ruoyi.common.core.domain.entity.SysPcsxxbab;
import com.ruoyi.system.domain.Pcsxxbab;
import com.ruoyi.system.domain.QgajbmPcsbm;
import com.ruoyi.system.service.IPcsxxbabService;

/**
 * 派出所信息Service业务层处理
 * 
 * @author Carry
 * @date 2020-10-21
 */
@Service
public class PcsxxbabServiceImpl implements IPcsxxbabService 
{
    @Autowired
    private PcsxxbabMapper pcsxxbabMapper;

    
    @Autowired
    private QgajbmPcsbmMapper qgajbmPcsbmMapper;
    /**
     * 查询派出所信息
     * 
     * @param id 派出所信息ID
     * @return 派出所信息
     */
    @Override
    public Pcsxxbab selectPcsxxbabById(Long id)
    {
        return pcsxxbabMapper.selectPcsxxbabById(id);
    }
    
  
    
    /**
     * 查询派出所信息列表
     * 
     * @param pcsxxbab 派出所信息
     * @return 派出所信息
     */
    @Override
    public List<Pcsxxbab> selectPcsxxbabList(Pcsxxbab pcsxxbab)
    {
        return pcsxxbabMapper.selectPcsxxbabList(pcsxxbab);
    }

    /**
     * 新增派出所信息
     * 
     * @param pcsxxbab 派出所信息
     * @return 结果
     */
    @Override
    public int insertPcsxxbab(SysPcsxxbab pcsxxbab)
    {
    	
    	int rows= pcsxxbabMapper.insertPcsxxbab(pcsxxbab);
    	insertPcsbmQgajbm(pcsxxbab);
        return rows;
    }

     public void insertPcsbmQgajbm(SysPcsxxbab pcsxxbab)
     {
    	 if(pcsxxbab.getPcsbm()!=null&&pcsxxbab.getQgajbm()!=null)
    	 {
    		 QgajbmPcsbm qp=new QgajbmPcsbm();
    		 qp.setQgajbm(pcsxxbab.getQgajbm());
    		 qp.setPcsbm(pcsxxbab.getPcsbm());
    		 qgajbmPcsbmMapper.insertQgajbmPcsbm(qp);
    	 }
     }
    /**
     * 修改派出所信息
     * 
     * @param pcsxxbab 派出所信息
     * @return 结果
     */
    @Override
    public int updatePcsxxbab(Pcsxxbab pcsxxbab)
    {
        return pcsxxbabMapper.updatePcsxxbab(pcsxxbab);
    }

    /**
     * 批量删除派出所信息
     * 
     * @param ids 需要删除的派出所信息ID
     * @return 结果
     */
    @Override
    public int deletePcsxxbabByIds(Long[] ids)
    {
        return pcsxxbabMapper.deletePcsxxbabByIds(ids);
    }

    /**
     * 删除派出所信息信息
     * 
     * @param id 派出所信息ID
     * @return 结果
     */
    @Override
    public int deletePcsxxbabById(Long id)
    {
        return pcsxxbabMapper.deletePcsxxbabById(id);
    }

	@Override
	public List<Pcsxxbab> selectPcsxxbabListBySsqx(String ssqx) {
		// TODO Auto-generated method stub
		return pcsxxbabMapper.selectPcsxxbabListBySsqx(ssqx);
	}

	@Override
	public List<Pcsxxbab> selectPcsxxbabListByDeptId(Long deptId, Long deptIdOfTree, Pcsxxbab pcsxxbab) {
		// TODO Auto-generated method stub
		return pcsxxbabMapper.selectPcsxxbabListByDeptId(deptId,deptIdOfTree,pcsxxbab);
	}



	@Override
	public String selectPcsmcByPcsbm(String pcsbm) {
		// TODO Auto-generated method stub
		  return pcsxxbabMapper.selectPcsmcByPcsbm(pcsbm);
	}

//	@Override
//	public List<Pcsxxbab> selectSubDeptByQgajDeptId(Long deptid) {
//		// TODO Auto-generated method stub
//		return pcsxxbabMapper.selectSubDeptByQgajDeptId(deptid);
//	}
}

package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 离开人员管理对象 jzrylkxxbab
 * 
 * @author ruoyi
 * @date 2020-09-28
 */
public class Jzrylkxxbab extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 居住申报流水号 */
    @Excel(name = "居住申报流水号")
    private String sbxxlsh;

    /** 地址二维码编码 */
    @Excel(name = "地址二维码编码")
    private String dzewmbm;
    
	

	/** 小区编码*/
    @Excel(name="小区编码")
    private String buildingId;

    /** 居住地址 */
    @Excel(name = "居住地址")
    private String jzdz;

    /** 身份证号 */
    @Excel(name = "身份证号")
    private String gmsfhm;

    /** 姓名 */
    @Excel(name = "姓名")
    private String xm;

    /** 离开日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "离开日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lkrq;

    /** 离开原因 */
    @Excel(name = "离开原因")
    private String lkyy;

    /** 登记单位 */
    @Excel(name = "登记单位")
    private String djdw;

    /** 登记人姓名 */
    @Excel(name = "登记人姓名")
    private String djrxm;

    /** 登记时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "登记时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date djsj;

    /** 申报系统 */
    @Excel(name = "申报系统")
    private String sbxt;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSbxxlsh(String sbxxlsh) 
    {
        this.sbxxlsh = sbxxlsh;
    }
    public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}
    public String getSbxxlsh() 
    {
        return sbxxlsh;
    }
    public void setDzewmbm(String dzewmbm) 
    {
        this.dzewmbm = dzewmbm;
    }

    public String getDzewmbm() 
    {
        return dzewmbm;
    }
    public void setJzdz(String jzdz) 
    {
        this.jzdz = jzdz;
    }

    public String getJzdz() 
    {
        return jzdz;
    }
    public void setGmsfhm(String gmsfhm) 
    {
        this.gmsfhm = gmsfhm;
    }

    public String getGmsfhm() 
    {
        return gmsfhm;
    }
    public void setXm(String xm) 
    {
        this.xm = xm;
    }

    public String getXm() 
    {
        return xm;
    }
    public void setLkrq(Date lkrq) 
    {
        this.lkrq = lkrq;
    }

    public Date getLkrq() 
    {
        return lkrq;
    }
    public void setLkyy(String lkyy) 
    {
        this.lkyy = lkyy;
    }

    public String getLkyy() 
    {
        return lkyy;
    }
    public void setDjdw(String djdw) 
    {
        this.djdw = djdw;
    }

    public String getDjdw() 
    {
        return djdw;
    }
    public void setDjrxm(String djrxm) 
    {
        this.djrxm = djrxm;
    }

    public String getDjrxm() 
    {
        return djrxm;
    }
    public void setDjsj(Date djsj) 
    {
        this.djsj = djsj;
    }

    public Date getDjsj() 
    {
        return djsj;
    }
    public void setSbxt(String sbxt) 
    {
        this.sbxt = sbxt;
    }

    public String getSbxt() 
    {
        return sbxt;
    }
    
    public Jzrylkxxbab() {
		super();
	}

	public Jzrylkxxbab(Long id, String sbxxlsh, String dzewmbm, String buildingId, String jzdz, String gmsfhm,
			String xm, Date lkrq, String lkyy, String djdw, String djrxm, Date djsj, String sbxt) {
		super();
		this.id = id;
		this.sbxxlsh = sbxxlsh;
		this.dzewmbm = dzewmbm;
		this.buildingId = buildingId;
		this.jzdz = jzdz;
		this.gmsfhm = gmsfhm;
		this.xm = xm;
		this.lkrq = lkrq;
		this.lkyy = lkyy;
		this.djdw = djdw;
		this.djrxm = djrxm;
		this.djsj = djsj;
		this.sbxt = sbxt;
	}
    @Override
	public String toString() {
		return "Jzrylkxxbab [id=" + id + ", sbxxlsh=" + sbxxlsh + ", dzewmbm=" + dzewmbm + ", buildingId=" + buildingId
				+ ", jzdz=" + jzdz + ", gmsfhm=" + gmsfhm + ", xm=" + xm + ", lkrq=" + lkrq + ", lkyy=" + lkyy
				+ ", djdw=" + djdw + ", djrxm=" + djrxm + ", djsj=" + djsj + ", sbxt=" + sbxt + "]";
	}
    
    
}

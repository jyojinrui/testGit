package com.ruoyi.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.common.core.domain.statistics.QgajSta;
import com.ruoyi.system.domain.Qgajxxbab;

/**
 * 区公安局信息Mapper接口
 * 
 * @author Carry
 * @date 2020-10-25
 */
public interface QgajxxbabMapper 
{
    /**
     * 查询区公安局信息
     * 
     * @param id 区公安局信息ID
     * @return 区公安局信息
     */
    public Qgajxxbab selectQgajxxbabById(Long id);

    public Qgajxxbab selectQgajxxbabByAdminId(@Param("adminid")Long adminid);
    

    /**
     * 查询区公安局信息列表
     * 
     * @param qgajxxbab 区公安局信息
     * @return 区公安局信息集合
     */
    public List<Qgajxxbab> selectQgajxxbabList(Qgajxxbab qgajxxbab);
    
    public List<Qgajxxbab> selectQgajxxbabListByDeptId(@Param("deptId")Long deptId, @Param("deptIdOfTree")Long deptIdOfTree, @Param("qgajxxbab")Qgajxxbab qgajxxbab);
    
    public List<QgajSta> selectQgajStaDataList();
    
   
    /**
     * 新增区公安局信息
     * 
     * @param qgajxxbab 区公安局信息
     * @return 结果
     */
    public int insertQgajxxbab(Qgajxxbab qgajxxbab);

    /**
     * 修改区公安局信息
     * 
     * @param qgajxxbab 区公安局信息
     * @return 结果
     */
    public int updateQgajxxbab(Qgajxxbab qgajxxbab);

    /**
     * 删除区公安局信息
     * 
     * @param id 区公安局信息ID
     * @return 结果
     */
    public int deleteQgajxxbabById(Long id);

    /**
     * 批量删除区公安局信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteQgajxxbabByIds(Long[] ids);
}

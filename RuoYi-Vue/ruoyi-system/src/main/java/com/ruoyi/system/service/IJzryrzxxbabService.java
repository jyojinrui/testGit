package com.ruoyi.system.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.commons.net.ntp.TimeStamp;

import com.ruoyi.common.core.domain.transfer.CamUser;
import com.ruoyi.system.domain.Jzryrzxxbab;

/**
 * 居住人员Service接口
 * 
 * @author ruoyi
 * @date 2020-09-27
 */
public interface IJzryrzxxbabService 
{
    /**
     * 查询居住人员
     * 
     * @param id 居住人员ID
     * @return 居住人员
     */
    public Jzryrzxxbab selectJzryrzxxbabById(Long id);

    /***
     * 根据小区管理员id查询入住人员信息
     */
    public List<Jzryrzxxbab> selectJzryrzxxbabListByXqAdminId(Long userId,Jzryrzxxbab jzryrzxxbab);
    
    
    public List<Jzryrzxxbab> selectJzryrzxxbabListByDeptId(Long deptId,Long deptIdOfTree,Jzryrzxxbab jzryrzxxbab);
    
    public List<CamUser> selectInfoToCamByXqId(String buildingId,long lastReqStamp,long rTime);
    
    /**
     * 查询居住人员列表
     * 
     * @param jzryrzxxbab 居住人员
     * @return 居住人员集合
     */
    public List<Jzryrzxxbab> selectJzryrzxxbabList(Jzryrzxxbab jzryrzxxbab);

    /**
     * 新增居住人员
     * 
     * @param jzryrzxxbab 居住人员
     * @return 结果
     * @throws IOException 
     */
    public int insertJzryrzxxbab(Jzryrzxxbab jzryrzxxbab) throws IOException;

    /**
     * 修改居住人员
     * 
     * @param jzryrzxxbab 居住人员
     * @return 结果
     * @throws IOException 
     */
    public int updateJzryrzxxbab(Jzryrzxxbab jzryrzxxbab) throws IOException;

    /**
     * 批量删除居住人员
     * 
     * @param ids 需要删除的居住人员ID
     * @return 结果
     */
    public int deleteJzryrzxxbabByIds(Long[] ids);

    /**
     * 删除居住人员信息
     * 
     * @param id 居住人员ID
     * @return 结果
     */
    public int deleteJzryrzxxbabById(Long id);
    
    
    /**
     * 根据身份证号查询姓名
     */
    public String getXmByGmsfzh(String sfz);
}

package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 pcsxx_xqxx
 * 
 * @author ruoyi
 * @date 2020-10-22
 */
public class PcsxxXqxx extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String pcsbm;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String xqbm;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPcsbm(String pcsbm) 
    {
        this.pcsbm = pcsbm;
    }

    public String getPcsbm() 
    {
        return pcsbm;
    }
    public void setXqbm(String xqbm) 
    {
        this.xqbm = xqbm;
    }

    public String getXqbm() 
    {
        return xqbm;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("pcsbm", getPcsbm())
            .append("xqbm", getXqbm())
            .toString();
    }
}

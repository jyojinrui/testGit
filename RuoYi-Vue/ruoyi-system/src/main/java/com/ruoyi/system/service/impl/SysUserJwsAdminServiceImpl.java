package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysUserJwsAdminMapper;
import com.ruoyi.system.domain.SysUserJwsAdmin;
import com.ruoyi.system.service.ISysUserJwsAdminService;

/**
 * 警务室管理员Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-11-05
 */
@Service
public class SysUserJwsAdminServiceImpl implements ISysUserJwsAdminService 
{
    @Autowired
    private SysUserJwsAdminMapper sysUserJwsAdminMapper;

    /**
     * 查询警务室管理员
     * 
     * @param id 警务室管理员ID
     * @return 警务室管理员
     */
    @Override
    public SysUserJwsAdmin selectSysUserJwsAdminById(Long id)
    {
        return sysUserJwsAdminMapper.selectSysUserJwsAdminById(id);
    }

    /**
     * 查询警务室管理员列表
     * 
     * @param sysUserJwsAdmin 警务室管理员
     * @return 警务室管理员
     */
    @Override
    public List<SysUserJwsAdmin> selectSysUserJwsAdminList(SysUserJwsAdmin sysUserJwsAdmin)
    {
        return sysUserJwsAdminMapper.selectSysUserJwsAdminList(sysUserJwsAdmin);
    }

    /**
     * 新增警务室管理员
     * 
     * @param sysUserJwsAdmin 警务室管理员
     * @return 结果
     */
    @Override
    public int insertSysUserJwsAdmin(SysUserJwsAdmin sysUserJwsAdmin)
    {
        return sysUserJwsAdminMapper.insertSysUserJwsAdmin(sysUserJwsAdmin);
    }

    /**
     * 修改警务室管理员
     * 
     * @param sysUserJwsAdmin 警务室管理员
     * @return 结果
     */
    @Override
    public int updateSysUserJwsAdmin(SysUserJwsAdmin sysUserJwsAdmin)
    {
        return sysUserJwsAdminMapper.updateSysUserJwsAdmin(sysUserJwsAdmin);
    }

    /**
     * 批量删除警务室管理员
     * 
     * @param ids 需要删除的警务室管理员ID
     * @return 结果
     */
    @Override
    public int deleteSysUserJwsAdminByIds(Long[] ids)
    {
        return sysUserJwsAdminMapper.deleteSysUserJwsAdminByIds(ids);
    }

    /**
     * 删除警务室管理员信息
     * 
     * @param id 警务室管理员ID
     * @return 结果
     */
    @Override
    public int deleteSysUserJwsAdminById(Long id)
    {
        return sysUserJwsAdminMapper.deleteSysUserJwsAdminById(id);
    }
}

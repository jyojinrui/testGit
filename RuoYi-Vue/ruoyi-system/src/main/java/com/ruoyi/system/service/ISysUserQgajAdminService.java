package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SysUserQgajAdmin;

/**
 * 区公安局管理员与公安局关系表Service接口
 * 
 * @author ruoyi
 * @date 2020-11-01
 */
public interface ISysUserQgajAdminService 
{
    /**
     * 查询区公安局管理员与公安局关系表
     * 
     * @param id 区公安局管理员与公安局关系表ID
     * @return 区公安局管理员与公安局关系表
     */
    public SysUserQgajAdmin selectSysUserQgajAdminById(Long id);

    /**
     * 查询区公安局管理员与公安局关系表列表
     * 
     * @param sysUserQgajAdmin 区公安局管理员与公安局关系表
     * @return 区公安局管理员与公安局关系表集合
     */
    public List<SysUserQgajAdmin> selectSysUserQgajAdminList(SysUserQgajAdmin sysUserQgajAdmin);

    /**
     * 新增区公安局管理员与公安局关系表
     * 
     * @param sysUserQgajAdmin 区公安局管理员与公安局关系表
     * @return 结果
     */
    public int insertSysUserQgajAdmin(SysUserQgajAdmin sysUserQgajAdmin);

    /**
     * 修改区公安局管理员与公安局关系表
     * 
     * @param sysUserQgajAdmin 区公安局管理员与公安局关系表
     * @return 结果
     */
    public int updateSysUserQgajAdmin(SysUserQgajAdmin sysUserQgajAdmin);

    /**
     * 批量删除区公安局管理员与公安局关系表
     * 
     * @param ids 需要删除的区公安局管理员与公安局关系表ID
     * @return 结果
     */
    public int deleteSysUserQgajAdminByIds(Long[] ids);

    /**
     * 删除区公安局管理员与公安局关系表信息
     * 
     * @param id 区公安局管理员与公安局关系表ID
     * @return 结果
     */
    public int deleteSysUserQgajAdminById(Long id);
}

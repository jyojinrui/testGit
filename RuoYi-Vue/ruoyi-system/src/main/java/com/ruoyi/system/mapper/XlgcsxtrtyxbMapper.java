package com.ruoyi.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.system.domain.Xlgcsxtrtyxb;

/**
 * 雪亮工程摄像头人体运行数据Mapper接口
 * 
 * @author Carry
 * @date 2020-10-04
 */
public interface XlgcsxtrtyxbMapper 
{
    /**
     * 查询雪亮工程摄像头人体运行数据
     * 
     * @param id 雪亮工程摄像头人体运行数据ID
     * @return 雪亮工程摄像头人体运行数据
     */
    public Xlgcsxtrtyxb selectXlgcsxtrtyxbById(Long id);

    /**
     * 查询雪亮工程摄像头人体运行数据列表
     * 
     * @param xlgcsxtrtyxb 雪亮工程摄像头人体运行数据
     * @return 雪亮工程摄像头人体运行数据集合
     */
    public List<Xlgcsxtrtyxb> selectXlgcsxtrtyxbList(Xlgcsxtrtyxb xlgcsxtrtyxb);

    
    public List<Xlgcsxtrtyxb> selectXlgcsxtrtyxbListByXqAdminId(@Param("userId")Long userId, @Param("xlgcsxtrtyxb")Xlgcsxtrtyxb xlgcsxtrtyxb);
    
    public List<Xlgcsxtrtyxb> selectXlgcsxtrtyxbListByDeptId(@Param("deptId")Long deptId, @Param("xlgcsxtrtyxb")Xlgcsxtrtyxb xlgcsxtrtyxb);
    
    
    /**
     * 新增雪亮工程摄像头人体运行数据
     * 
     * @param xlgcsxtrtyxb 雪亮工程摄像头人体运行数据
     * @return 结果
     */
    public int insertXlgcsxtrtyxb(Xlgcsxtrtyxb xlgcsxtrtyxb);

    /**
     * 修改雪亮工程摄像头人体运行数据
     * 
     * @param xlgcsxtrtyxb 雪亮工程摄像头人体运行数据
     * @return 结果
     */
    public int updateXlgcsxtrtyxb(Xlgcsxtrtyxb xlgcsxtrtyxb);

    /**
     * 删除雪亮工程摄像头人体运行数据
     * 
     * @param id 雪亮工程摄像头人体运行数据ID
     * @return 结果
     */
    public int deleteXlgcsxtrtyxbById(Long id);

    /**
     * 批量删除雪亮工程摄像头人体运行数据
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteXlgcsxtrtyxbByIds(Long[] ids);
}

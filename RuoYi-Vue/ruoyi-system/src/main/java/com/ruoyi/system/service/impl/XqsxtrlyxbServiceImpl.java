package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.XqsxtrlyxbMapper;
import com.ruoyi.system.domain.Xqsxtrlyxb;
import com.ruoyi.system.service.IXqsxtrlyxbService;

/**
 * 人脸运行数据Service业务层处理
 * 
 * @author Carry
 * @date 2020-10-03
 */
@Service
public class XqsxtrlyxbServiceImpl implements IXqsxtrlyxbService 
{
    @Autowired
    private XqsxtrlyxbMapper xqsxtrlyxbMapper;

    /**
     * 查询人脸运行数据
     * 
     * @param id 人脸运行数据ID
     * @return 人脸运行数据
     */
    @Override
    public Xqsxtrlyxb selectXqsxtrlyxbById(Long id)
    {
        return xqsxtrlyxbMapper.selectXqsxtrlyxbById(id);
    }

    /**
     * 查询人脸运行数据列表
     * 
     * @param xqsxtrlyxb 人脸运行数据
     * @return 人脸运行数据
     */
    @Override
    public List<Xqsxtrlyxb> selectXqsxtrlyxbList(Xqsxtrlyxb xqsxtrlyxb)
    {
        return xqsxtrlyxbMapper.selectXqsxtrlyxbList(xqsxtrlyxb);
    }

    /**
     * 新增人脸运行数据
     * 
     * @param xqsxtrlyxb 人脸运行数据
     * @return 结果
     */
    @Override
    public int insertXqsxtrlyxb(Xqsxtrlyxb xqsxtrlyxb)
    {
        return xqsxtrlyxbMapper.insertXqsxtrlyxb(xqsxtrlyxb);
    }

    /**
     * 修改人脸运行数据
     * 
     * @param xqsxtrlyxb 人脸运行数据
     * @return 结果
     */
    @Override
    public int updateXqsxtrlyxb(Xqsxtrlyxb xqsxtrlyxb)
    {
        return xqsxtrlyxbMapper.updateXqsxtrlyxb(xqsxtrlyxb);
    }

    /**
     * 批量删除人脸运行数据
     * 
     * @param ids 需要删除的人脸运行数据ID
     * @return 结果
     */
    @Override
    public int deleteXqsxtrlyxbByIds(Long[] ids)
    {
        return xqsxtrlyxbMapper.deleteXqsxtrlyxbByIds(ids);
    }

    /**
     * 删除人脸运行数据信息
     * 
     * @param id 人脸运行数据ID
     * @return 结果
     */
    @Override
    public int deleteXqsxtrlyxbById(Long id)
    {
        return xqsxtrlyxbMapper.deleteXqsxtrlyxbById(id);
    }

	@Override
	public List<Xqsxtrlyxb> selectXqsxtrlyxbListByXqAdminId(Long userId, Xqsxtrlyxb xqsxtrlyxb) {
		// TODO Auto-generated method stub
		return xqsxtrlyxbMapper.selectXqsxtrlyxbListByXqAdminId(userId,xqsxtrlyxb);
	}

	@Override
	public List<Xqsxtrlyxb> selectXqsxtrlyxbListByDeptId(Long deptId, Xqsxtrlyxb xqsxtrlyxb) {
		// TODO Auto-generated method stub
		return xqsxtrlyxbMapper.selectXqsxtrlyxbListByDeptId(deptId,xqsxtrlyxb);
	}
}

package com.ruoyi.system.service;

import java.util.Date;
import java.util.List;
import com.ruoyi.system.domain.SysNettyRecord;

/**
 * netty记录表Service接口
 * 
 * @author ruoyi
 * @date 2020-12-17
 */
public interface ISysNettyRecordService 
{
    /**
     * 查询netty记录表
     * 
     * @param id netty记录表ID
     * @return netty记录表
     */
    public SysNettyRecord selectSysNettyRecordById(Long id);

    /**
     * 查询netty记录表列表
     * 
     * @param sysNettyRecord netty记录表
     * @return netty记录表集合
     */
    public List<SysNettyRecord> selectSysNettyRecordList(SysNettyRecord sysNettyRecord);

    /**
     * 新增netty记录表
     * 
     * @param sysNettyRecord netty记录表
     * @return 结果
     */
    public int insertSysNettyRecord(SysNettyRecord sysNettyRecord);

    /**
     * 修改netty记录表
     * 
     * @param sysNettyRecord netty记录表
     * @return 结果
     */
    public int updateSysNettyRecord(SysNettyRecord sysNettyRecord);

    /**
     * 批量删除netty记录表
     * 
     * @param ids 需要删除的netty记录表ID
     * @return 结果
     */
    public int deleteSysNettyRecordByIds(Long[] ids);

    /**
     * 删除netty记录表信息
     * 
     * @param id netty记录表ID
     * @return 结果
     */
    public int deleteSysNettyRecordById(Long id);
    
    /**
     * 判断所属小区是否存在 0表示不存在 1 表示存在
     * @param ssxqbm
     * @return
     */
    public int ssxqbmExist(String ssxqbm);
    
    /**
     * 根据小区编码更新netty的连接记录
     * @param ssxqbm
     * @param date
     */
    public void updateSysNettyRecordBySsxqbm(String ssxqbm,Date date);
    
    public SysNettyRecord selectSysNettyRecordBySsxqbm(String buildingId);
    
}

package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Wnsjgsdwdm;

/**
 * 渭南数据归属单位代码Mapper接口
 * 
 * @author ruoyi
 * @date 2020-11-03
 */
public interface WnsjgsdwdmMapper 
{
    /**
     * 查询渭南数据归属单位代码
     * 
     * @param id 渭南数据归属单位代码ID
     * @return 渭南数据归属单位代码
     */
    public Wnsjgsdwdm selectWnsjgsdwdmById(Long id);

    /**
     * 查询渭南数据归属单位代码列表
     * 
     * @param wnsjgsdwdm 渭南数据归属单位代码
     * @return 渭南数据归属单位代码集合
     */
    public List<Wnsjgsdwdm> selectWnsjgsdwdmList(Wnsjgsdwdm wnsjgsdwdm);

    /**
     * 新增渭南数据归属单位代码
     * 
     * @param wnsjgsdwdm 渭南数据归属单位代码
     * @return 结果
     */
    public int insertWnsjgsdwdm(Wnsjgsdwdm wnsjgsdwdm);

    /**
     * 修改渭南数据归属单位代码
     * 
     * @param wnsjgsdwdm 渭南数据归属单位代码
     * @return 结果
     */
    public int updateWnsjgsdwdm(Wnsjgsdwdm wnsjgsdwdm);

    /**
     * 删除渭南数据归属单位代码
     * 
     * @param id 渭南数据归属单位代码ID
     * @return 结果
     */
    public int deleteWnsjgsdwdmById(Long id);

    /**
     * 批量删除渭南数据归属单位代码
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteWnsjgsdwdmByIds(Long[] ids);

	public List<Wnsjgsdwdm> selectQgajList();
	public List<Wnsjgsdwdm> selectSubDeptByQgajDeptId(Long deptid);
}

package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Wnsjgsdwdm;

/**
 * 渭南数据归属单位代码Service接口
 * 
 * @author ruoyi
 * @date 2020-11-03
 */
public interface IWnsjgsdwdmService 
{
    /**
     * 查询渭南数据归属单位代码
     * 
     * @param id 渭南数据归属单位代码ID
     * @return 渭南数据归属单位代码
     */
    public Wnsjgsdwdm selectWnsjgsdwdmById(Long id);

    /**
     * 查询渭南数据归属单位代码列表
     * 
     * @param wnsjgsdwdm 渭南数据归属单位代码
     * @return 渭南数据归属单位代码集合
     */
    public List<Wnsjgsdwdm> selectWnsjgsdwdmList(Wnsjgsdwdm wnsjgsdwdm);

    public List<Wnsjgsdwdm> selectQgajList();
    
    public List<Wnsjgsdwdm> selectSubDeptByQgajDeptId(Long deptid);
    
    /**
     * 新增渭南数据归属单位代码
     * 
     * @param wnsjgsdwdm 渭南数据归属单位代码
     * @return 结果
     */
    public int insertWnsjgsdwdm(Wnsjgsdwdm wnsjgsdwdm);

    /**
     * 修改渭南数据归属单位代码
     * 
     * @param wnsjgsdwdm 渭南数据归属单位代码
     * @return 结果
     */
    public int updateWnsjgsdwdm(Wnsjgsdwdm wnsjgsdwdm);

    /**
     * 批量删除渭南数据归属单位代码
     * 
     * @param ids 需要删除的渭南数据归属单位代码ID
     * @return 结果
     */
    public int deleteWnsjgsdwdmByIds(Long[] ids);

    /**
     * 删除渭南数据归属单位代码信息
     * 
     * @param id 渭南数据归属单位代码ID
     * @return 结果
     */
    public int deleteWnsjgsdwdmById(Long id);
}

package com.ruoyi.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.system.domain.Mjsbxxbab;

/**
 * 门禁设备管理Mapper接口
 * 
 * @author Carry
 * @date 2020-09-29
 */
public interface MjsbxxbabMapper 
{
    /**
     * 查询门禁设备管理
     * 
     * @param id 门禁设备管理ID
     * @return 门禁设备管理
     */
    public Mjsbxxbab selectMjsbxxbabById(Long id);

    
    public List<Mjsbxxbab> selectMjsbxxbabListByXqAdminId(@Param("userId")Long userId, @Param("mjsbxxbab")Mjsbxxbab mjsbxxbab);
    
    public List<Mjsbxxbab> selectMjsbxxbabListByDeptId(@Param("deptId")Long deptId,@Param("deptIdOfTree")Long deptIdOfTree,@Param("mjsbxxbab") Mjsbxxbab mjsbxxbab);
    /**
     * 查询门禁设备管理列表
     * 
     * @param mjsbxxbab 门禁设备管理
     * @return 门禁设备管理集合
     */
    public List<Mjsbxxbab> selectMjsbxxbabList(Mjsbxxbab mjsbxxbab);

    
    
    /**
     * 新增门禁设备管理
     * 
     * @param mjsbxxbab 门禁设备管理
     * @return 结果
     */
    public int insertMjsbxxbab(Mjsbxxbab mjsbxxbab);

    /**
     * 修改门禁设备管理
     * 
     * @param mjsbxxbab 门禁设备管理
     * @return 结果
     */
    public int updateMjsbxxbab(Mjsbxxbab mjsbxxbab);

    /**
     * 删除门禁设备管理
     * 
     * @param id 门禁设备管理ID
     * @return 结果
     */
    public int deleteMjsbxxbabById(Long id);

    /**
     * 批量删除门禁设备管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMjsbxxbabByIds(Long[] ids);
    
    public Long getCountByDeptId(long deptId); 
    public Long getCountByXqbm(String xqbm); 
}

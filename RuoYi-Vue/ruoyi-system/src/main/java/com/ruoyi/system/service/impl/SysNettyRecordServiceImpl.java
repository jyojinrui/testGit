package com.ruoyi.system.service.impl;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysNettyRecordMapper;
import com.ruoyi.system.domain.SysNettyRecord;
import com.ruoyi.system.service.ISysNettyRecordService;

/**
 * netty记录表Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-12-17
 */
@Service
public class SysNettyRecordServiceImpl implements ISysNettyRecordService 
{
    @Autowired
    private SysNettyRecordMapper sysNettyRecordMapper;

    /**
     * 查询netty记录表
     * 
     * @param id netty记录表ID
     * @return netty记录表
     */
    @Override
    public SysNettyRecord selectSysNettyRecordById(Long id)
    {
        return sysNettyRecordMapper.selectSysNettyRecordById(id);
    }

    /**
     * 查询netty记录表列表
     * 
     * @param sysNettyRecord netty记录表
     * @return netty记录表
     */
    @Override
    public List<SysNettyRecord> selectSysNettyRecordList(SysNettyRecord sysNettyRecord)
    {
        return sysNettyRecordMapper.selectSysNettyRecordList(sysNettyRecord);
    }

    /**
     * 新增netty记录表
     * 
     * @param sysNettyRecord netty记录表
     * @return 结果
     */
    @Override
    public int insertSysNettyRecord(SysNettyRecord sysNettyRecord)
    {
        return sysNettyRecordMapper.insertSysNettyRecord(sysNettyRecord);
    }

    /**
     * 修改netty记录表
     * 
     * @param sysNettyRecord netty记录表
     * @return 结果
     */
    @Override
    public int updateSysNettyRecord(SysNettyRecord sysNettyRecord)
    {
        return sysNettyRecordMapper.updateSysNettyRecord(sysNettyRecord);
    }

    /**
     * 批量删除netty记录表
     * 
     * @param ids 需要删除的netty记录表ID
     * @return 结果
     */
    @Override
    public int deleteSysNettyRecordByIds(Long[] ids)
    {
        return sysNettyRecordMapper.deleteSysNettyRecordByIds(ids);
    }

    /**
     * 删除netty记录表信息
     * 
     * @param id netty记录表ID
     * @return 结果
     */
    @Override
    public int deleteSysNettyRecordById(Long id)
    {
        return sysNettyRecordMapper.deleteSysNettyRecordById(id);
    }

	@Override
	public int ssxqbmExist(String ssxqbm) {
		// TODO Auto-generated method stub
		return sysNettyRecordMapper.ssxqbmExist(ssxqbm);
	}

	@Override
	public void updateSysNettyRecordBySsxqbm(String ssxqbm, Date lastreqtime) {
		// TODO Auto-generated method stub
		sysNettyRecordMapper.updateSysNettyRecordBySsxqbm(ssxqbm,lastreqtime);
	}

	@Override
	public SysNettyRecord selectSysNettyRecordBySsxqbm(String buildingId) {
		// TODO Auto-generated method stub
		return sysNettyRecordMapper.selectSysNettyRecordBySsxqbm(buildingId);
	}
}

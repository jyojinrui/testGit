package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 统计报表对象 sys_sta
 * 
 * @author ruoyi
 * @date 2020-12-23
 */
public class SysSta extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 小区名称 */
    @Excel(name = "小区名称")
    private String xqmc;

    /** 区公安局编码 */
    @Excel(name = "区公安局编码")
    private String fjbm;

    /** 分局名称 */
    @Excel(name = "分局名称")
    private String fjmc;

    /** 归属派出所 */
    @Excel(name = "归属派出所")
    private String gspcs;

    /** 派出所名称 */
    @Excel(name = "派出所名称")
    private String pcsmc;

    /** 建设公司 */
    @Excel(name = "建设公司")
    private String jsgs;

    /** 实有房屋数量 */
    @Excel(name = "实有房屋数量")
    private Long syfwsl;

    /** 实有人口数量 */
    @Excel(name = "实有人口数量")
    private Long syrksl;

    /** 实有单位数量 */
    @Excel(name = "实有单位数量")
    private Long sydwsl;

    /** 从业人员数量 */
    @Excel(name = "从业人员数量")
    private Long cyrysl;

    /** 门禁设备数量 */
    @Excel(name = "门禁设备数量")
    private Long mjsbsl;

    /** 门禁卡数量 */
    @Excel(name = "门禁卡数量")
    private Long mjksl;

    /** 访客数量 */
    @Excel(name = "访客数量")
    private Long fksl;

    /** 出入记录数量 */
    @Excel(name = "出入记录数量")
    private Long crjlsl;

    /** 出入照片数量 */
    @Excel(name = "出入照片数量")
    private Long crzpsl;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setXqmc(String xqmc) 
    {
        this.xqmc = xqmc;
    }

    public String getXqmc() 
    {
        return xqmc;
    }
    public void setFjbm(String fjbm) 
    {
        this.fjbm = fjbm;
    }

    public String getFjbm() 
    {
        return fjbm;
    }
    public void setFjmc(String fjmc) 
    {
        this.fjmc = fjmc;
    }

    public String getFjmc() 
    {
        return fjmc;
    }
    public void setGspcs(String gspcs) 
    {
        this.gspcs = gspcs;
    }

    public String getGspcs() 
    {
        return gspcs;
    }
    public void setPcsmc(String pcsmc) 
    {
        this.pcsmc = pcsmc;
    }

    public String getPcsmc() 
    {
        return pcsmc;
    }
    public void setJsgs(String jsgs) 
    {
        this.jsgs = jsgs;
    }

    public String getJsgs() 
    {
        return jsgs;
    }
    public void setSyfwsl(Long syfwsl) 
    {
        this.syfwsl = syfwsl;
    }

    public Long getSyfwsl() 
    {
        return syfwsl;
    }
    public void setSyrksl(Long syrksl) 
    {
        this.syrksl = syrksl;
    }

    public Long getSyrksl() 
    {
        return syrksl;
    }
    public void setSydwsl(Long sydwsl) 
    {
        this.sydwsl = sydwsl;
    }

    public Long getSydwsl() 
    {
        return sydwsl;
    }
    public void setCyrysl(Long cyrysl) 
    {
        this.cyrysl = cyrysl;
    }

    public Long getCyrysl() 
    {
        return cyrysl;
    }
    public void setMjsbsl(Long mjsbsl) 
    {
        this.mjsbsl = mjsbsl;
    }

    public Long getMjsbsl() 
    {
        return mjsbsl;
    }
    public void setMjksl(Long mjksl) 
    {
        this.mjksl = mjksl;
    }

    public Long getMjksl() 
    {
        return mjksl;
    }
    public void setFksl(Long fksl) 
    {
        this.fksl = fksl;
    }

    public Long getFksl() 
    {
        return fksl;
    }
    public void setCrjlsl(Long crjlsl) 
    {
        this.crjlsl = crjlsl;
    }

    public Long getCrjlsl() 
    {
        return crjlsl;
    }
    public void setCrzpsl(Long crzpsl) 
    {
        this.crzpsl = crzpsl;
    }

    public Long getCrzpsl() 
    {
        return crzpsl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("xqmc", getXqmc())
            .append("fjbm", getFjbm())
            .append("fjmc", getFjmc())
            .append("gspcs", getGspcs())
            .append("pcsmc", getPcsmc())
            .append("jsgs", getJsgs())
            .append("syfwsl", getSyfwsl())
            .append("syrksl", getSyrksl())
            .append("sydwsl", getSydwsl())
            .append("cyrysl", getCyrysl())
            .append("mjsbsl", getMjsbsl())
            .append("mjksl", getMjksl())
            .append("fksl", getFksl())
            .append("crjlsl", getCrjlsl())
            .append("crzpsl", getCrzpsl())
            .toString();
    }
}

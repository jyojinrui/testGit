package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Xqsxtywzkb;

/**
 * 运维状况Service接口
 * 
 * @author Carry
 * @date 2020-10-03
 */
public interface IXqsxtywzkbService 
{
    /**
     * 查询运维状况
     * 
     * @param id 运维状况ID
     * @return 运维状况
     */
    public Xqsxtywzkb selectXqsxtywzkbById(Long id);

    
    
    /**
     * 根据小区管理员id获取小区摄像头运维状况
     * @param userId
     * @param xqsxtywzkb
     * @return
     */
    public List<Xqsxtywzkb> selectXqsxtywzkbListByXqAdminId(Long userId,Xqsxtywzkb xqsxtywzkb);
    
    public List<Xqsxtywzkb> selectXqsxtywzkbListByDeptId(Long deptId,Xqsxtywzkb xqsxtywzkb);
    
    
    /**
     * 查询运维状况列表
     * 
     * @param xqsxtywzkb 运维状况
     * @return 运维状况集合
     */
    public List<Xqsxtywzkb> selectXqsxtywzkbList(Xqsxtywzkb xqsxtywzkb);

    /**
     * 新增运维状况
     * 
     * @param xqsxtywzkb 运维状况
     * @return 结果
     */
    public int insertXqsxtywzkb(Xqsxtywzkb xqsxtywzkb);

    /**
     * 修改运维状况
     * 
     * @param xqsxtywzkb 运维状况
     * @return 结果
     */
    public int updateXqsxtywzkb(Xqsxtywzkb xqsxtywzkb);

    /**
     * 批量删除运维状况
     * 
     * @param ids 需要删除的运维状况ID
     * @return 结果
     */
    public int deleteXqsxtywzkbByIds(Long[] ids);

    /**
     * 删除运维状况信息
     * 
     * @param id 运维状况ID
     * @return 结果
     */
    public int deleteXqsxtywzkbById(Long id);
}

package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Tcxxbab;

/**
 * 停车信息管理Service接口
 * 
 * @author Carry
 * @date 2020-09-30
 */
public interface ITcxxbabService {
	/**
	 * 查询停车信息管理
	 * 
	 * @param id 停车信息管理ID
	 * @return 停车信息管理
	 */
	public Tcxxbab selectTcxxbabById(Long id);

	/**
	 * 根据小区管理员id获取小区所有停车场中的所有停车信息
	 */
	public List<Tcxxbab> selectTcxxbabListByXqAdminId(Long userId, Tcxxbab tcxxbab);

	public List<Tcxxbab> selectTcxxbabListDeptId(Long deptId, Long deptIdOfTree, Tcxxbab tcxxbab);

	public List<Tcxxbab> selectTodayTcxxbabListByXqAdminId(Long userId, Tcxxbab tcxxbab);

	public List<Tcxxbab> selectTodayTcxxbabListDeptId(Long deptId, Long deptIdOfTree, Tcxxbab tcxxbab);

	/**
	 * 查询停车信息管理列表
	 * 
	 * @param tcxxbab 停车信息管理
	 * @return 停车信息管理集合
	 */
	public List<Tcxxbab> selectTcxxbabList(Tcxxbab tcxxbab);

	/**
	 * 新增停车信息管理
	 * 
	 * @param tcxxbab 停车信息管理
	 * @return 结果
	 */
	public int insertTcxxbab(Tcxxbab tcxxbab);

	/**
	 * 修改停车信息管理
	 * 
	 * @param tcxxbab 停车信息管理
	 * @return 结果
	 */
	public int updateTcxxbab(Tcxxbab tcxxbab);

	/**
	 * 批量删除停车信息管理
	 * 
	 * @param ids 需要删除的停车信息管理ID
	 * @return 结果
	 */
	public int deleteTcxxbabByIds(Long[] ids);

	/**
	 * 删除停车信息管理信息
	 * 
	 * @param id 停车信息管理ID
	 * @return 结果
	 */
	public int deleteTcxxbabById(Long id);
}

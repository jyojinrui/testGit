package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SysUserJwsAdmin;

/**
 * 警务室管理员Service接口
 * 
 * @author ruoyi
 * @date 2020-11-05
 */
public interface ISysUserJwsAdminService 
{
    /**
     * 查询警务室管理员
     * 
     * @param id 警务室管理员ID
     * @return 警务室管理员
     */
    public SysUserJwsAdmin selectSysUserJwsAdminById(Long id);

    /**
     * 查询警务室管理员列表
     * 
     * @param sysUserJwsAdmin 警务室管理员
     * @return 警务室管理员集合
     */
    public List<SysUserJwsAdmin> selectSysUserJwsAdminList(SysUserJwsAdmin sysUserJwsAdmin);

    /**
     * 新增警务室管理员
     * 
     * @param sysUserJwsAdmin 警务室管理员
     * @return 结果
     */
    public int insertSysUserJwsAdmin(SysUserJwsAdmin sysUserJwsAdmin);

    /**
     * 修改警务室管理员
     * 
     * @param sysUserJwsAdmin 警务室管理员
     * @return 结果
     */
    public int updateSysUserJwsAdmin(SysUserJwsAdmin sysUserJwsAdmin);

    /**
     * 批量删除警务室管理员
     * 
     * @param ids 需要删除的警务室管理员ID
     * @return 结果
     */
    public int deleteSysUserJwsAdminByIds(Long[] ids);

    /**
     * 删除警务室管理员信息
     * 
     * @param id 警务室管理员ID
     * @return 结果
     */
    public int deleteSysUserJwsAdminById(Long id);
}

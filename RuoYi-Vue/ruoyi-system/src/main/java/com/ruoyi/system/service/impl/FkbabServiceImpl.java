package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.FkbabMapper;
import com.ruoyi.system.domain.Fkbab;
import com.ruoyi.system.service.IFkbabService;

/**
 * 访客信息管理Service业务层处理
 * 
 * @author Carry
 * @date 2020-09-29
 */
@Service
public class FkbabServiceImpl implements IFkbabService 
{
    @Autowired
    private FkbabMapper fkbabMapper;

    /**
     * 查询访客信息管理
     * 
     * @param id 访客信息管理ID
     * @return 访客信息管理
     */
    @Override
    public Fkbab selectFkbabById(Long id)
    {
        return fkbabMapper.selectFkbabById(id);
    }

    /**
     * 查询访客信息管理列表
     * 
     * @param fkbab 访客信息管理
     * @return 访客信息管理
     */
    @Override
    public List<Fkbab> selectFkbabList(Fkbab fkbab)
    {
        return fkbabMapper.selectFkbabList(fkbab);
    }

    /**
     * 新增访客信息管理
     * 
     * @param fkbab 访客信息管理
     * @return 结果
     */
    @Override
    public int insertFkbab(Fkbab fkbab)
    {
        return fkbabMapper.insertFkbab(fkbab);
    }

    /**
     * 修改访客信息管理
     * 
     * @param fkbab 访客信息管理
     * @return 结果
     */
    @Override
    public int updateFkbab(Fkbab fkbab)
    {
        return fkbabMapper.updateFkbab(fkbab);
    }

    /**
     * 批量删除访客信息管理
     * 
     * @param ids 需要删除的访客信息管理ID
     * @return 结果
     */
    @Override
    public int deleteFkbabByIds(Long[] ids)
    {
        return fkbabMapper.deleteFkbabByIds(ids);
    }

    /**
     * 删除访客信息管理信息
     * 
     * @param id 访客信息管理ID
     * @return 结果
     */
    @Override
    public int deleteFkbabById(Long id)
    {
        return fkbabMapper.deleteFkbabById(id);
    }

	@Override
	public List<Fkbab> selectFkbabListByXqAdminId(Long userId, Fkbab fkbab) {
		// TODO Auto-generated method stub
//		return null;
		return fkbabMapper.selectFkbabListByXqAdminId(userId, fkbab);
	}

	@Override
	public List<Fkbab> selectFkbabListByDeptId(Long deptId,Long deptIdOfTree, Fkbab fkbab) {
		// TODO Auto-generated method stub
		return fkbabMapper.selectFkbabListByDeptId(deptId, deptIdOfTree,fkbab);
	}
}

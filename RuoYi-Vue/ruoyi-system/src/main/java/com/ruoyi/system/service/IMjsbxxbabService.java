package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Mjsbxxbab;

/**
 * 门禁设备管理Service接口
 * 
 * @author Carry
 * @date 2020-09-29
 */
public interface IMjsbxxbabService 
{
	
    /**
     * 查询门禁设备管理
     * 
     * @param id 门禁设备管理ID
     * @return 门禁设备管理
     */
    public Mjsbxxbab selectMjsbxxbabById(Long id);

    /**
     * 查询门禁设备管理列表
     * 
     * @param mjsbxxbab 门禁设备管理
     * @return 门禁设备管理集合
     */
    public List<Mjsbxxbab> selectMjsbxxbabList(Mjsbxxbab mjsbxxbab);

    
    /**
	 *  根据小区管理员id获取门禁信息
	 * @param userId
	 * @param mjsbxxbab
	 */
	public List<Mjsbxxbab> selectMjsbxxbabListByXqAdminId(Long userId,Mjsbxxbab mjsbxxbab);
	
	public List<Mjsbxxbab> selectMjsbxxbabListByDeptId(Long deptId,Long deptIdOfTree,Mjsbxxbab mjsbxxbab);
	
    /**
     * 新增门禁设备管理
     * 
     * @param mjsbxxbab 门禁设备管理
     * @return 结果
     */
    public int insertMjsbxxbab(Mjsbxxbab mjsbxxbab);

    /**
     * 修改门禁设备管理
     * 
     * @param mjsbxxbab 门禁设备管理
     * @return 结果
     */
    public int updateMjsbxxbab(Mjsbxxbab mjsbxxbab);

    /**
     * 批量删除门禁设备管理
     * 
     * @param ids 需要删除的门禁设备管理ID
     * @return 结果
     */
    public int deleteMjsbxxbabByIds(Long[] ids);

    /**
     * 删除门禁设备管理信息
     * 
     * @param id 门禁设备管理ID
     * @return 结果
     */
    public int deleteMjsbxxbabById(Long id);
}

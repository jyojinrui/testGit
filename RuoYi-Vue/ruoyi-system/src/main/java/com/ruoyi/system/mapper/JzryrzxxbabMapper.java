package com.ruoyi.system.mapper;

import java.util.Date;
import java.util.List;

import org.apache.commons.net.ntp.TimeStamp;
import org.apache.ibatis.annotations.Param;

import com.ruoyi.common.core.domain.transfer.CamUser;
import com.ruoyi.system.domain.Jzryrzxxbab;

/**
 * 居住人员Mapper接口
 * 
 * @author ruoyi
 * @date 2020-09-27
 */
public interface JzryrzxxbabMapper 
{
    /**
     * 查询居住人员
     * 
     * @param id 居住人员ID
     * @return 居住人员
     */
    public Jzryrzxxbab selectJzryrzxxbabById(Long id);

    /**
     * 根据小区管理员id查询居住人员信息
     * 
     */
    public List<Jzryrzxxbab> selectJzryrzxxbabListByXqAdminId(@Param("userId")Long userId,@Param("jzryrzxxbab")Jzryrzxxbab jzryrzxxbab);
    
    
    public List<Jzryrzxxbab> selectJzryrzxxbabListByDeptId(@Param("deptId")Long deptId,@Param("deptIdOfTree") Long deptIdOfTree,@Param("jzryrzxxbab")Jzryrzxxbab jzryrzxxbab);
    
    public List<CamUser> selectInfoToCamByXqId(@Param("buildingId")String buildingId,@Param("lastReqStamp") long lastReqStamp,@Param("rTime")long rTime);
    /**
     * 查询居住人员列表
     * 
     * @param jzryrzxxbab 居住人员
     * @return 居住人员集合
     */
    public List<Jzryrzxxbab> selectJzryrzxxbabList(Jzryrzxxbab jzryrzxxbab);

    /**
     * 新增居住人员
     * 
     * @param jzryrzxxbab 居住人员
     * @return 结果
     */
    public int insertJzryrzxxbab(Jzryrzxxbab jzryrzxxbab);
    
    public int updateJzryrzxxbabListIssued(List<CamUser> camUsers);

    /**
     * 修改居住人员
     * 
     * @param jzryrzxxbab 居住人员
     * @return 结果
     */
    public int updateJzryrzxxbab(Jzryrzxxbab jzryrzxxbab);

    /**
     * 删除居住人员
     * 
     * @param id 居住人员ID
     * @return 结果
     */
    public int deleteJzryrzxxbabById(Long id);

    /**
     * 批量删除居住人员
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteJzryrzxxbabByIds(Long[] ids);
    
    public Long getCountByDeptId(long deptId); 
    public Long getCountByXqbm(String xqbm); 
    
    public String getXmByGmsfzh(String sfz);
}

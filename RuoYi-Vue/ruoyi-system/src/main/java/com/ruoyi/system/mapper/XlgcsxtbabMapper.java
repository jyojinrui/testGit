package com.ruoyi.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.system.domain.Xlgcsxtbab;

/**
 * 雪亮工程摄像头设备Mapper接口
 * 
 * @author Carry
 * @date 2020-10-03
 */
public interface XlgcsxtbabMapper 
{
    /**
     * 查询雪亮工程摄像头设备
     * 
     * @param id 雪亮工程摄像头设备ID
     * @return 雪亮工程摄像头设备
     */
    public Xlgcsxtbab selectXlgcsxtbabById(Long id);

    /**
     * 查询雪亮工程摄像头设备列表
     * 
     * @param xlgcsxtbab 雪亮工程摄像头设备
     * @return 雪亮工程摄像头设备集合
     */
    public List<Xlgcsxtbab> selectXlgcsxtbabList(Xlgcsxtbab xlgcsxtbab);

    
    public List<Xlgcsxtbab> selectXlgcsxtbabListByXqAdminId(@Param("userId")Long userId,@Param("xlgcsxtbab")Xlgcsxtbab xlgcsxtbab);

    public List<Xlgcsxtbab> selectXlgcsxtbabListByDeptId(@Param("deptId")Long deptId,@Param("xlgcsxtbab")Xlgcsxtbab xlgcsxtbab);

    
    
    /**
     * 新增雪亮工程摄像头设备
     * 
     * @param xlgcsxtbab 雪亮工程摄像头设备
     * @return 结果
     */
    public int insertXlgcsxtbab(Xlgcsxtbab xlgcsxtbab);

    /**
     * 修改雪亮工程摄像头设备
     * 
     * @param xlgcsxtbab 雪亮工程摄像头设备
     * @return 结果
     */
    public int updateXlgcsxtbab(Xlgcsxtbab xlgcsxtbab);

    /**
     * 删除雪亮工程摄像头设备
     * 
     * @param id 雪亮工程摄像头设备ID
     * @return 结果
     */
    public int deleteXlgcsxtbabById(Long id);

    /**
     * 批量删除雪亮工程摄像头设备
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteXlgcsxtbabByIds(Long[] ids);
}

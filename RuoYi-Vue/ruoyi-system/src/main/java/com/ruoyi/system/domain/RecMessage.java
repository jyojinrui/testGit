package com.ruoyi.system.domain;

public class RecMessage {
	private String operator;
	private RecInfo info;
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public RecInfo getInfo() {
		return info;
	}
	public void setInfo(RecInfo info) {
		this.info = info;
	}
	public RecMessage(String operator, RecInfo info) {
		super();
		this.operator = operator;
		this.info = info;
	}
	public RecMessage() {
		super();
	}
	@Override
	public String toString() {
		return "UpInfo [operator=" + operator + ", info=" + info + "]";
	}
	
	
}

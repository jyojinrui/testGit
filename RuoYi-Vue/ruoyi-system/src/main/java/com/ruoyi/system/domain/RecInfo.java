package com.ruoyi.system.domain;

import java.util.Date;

public class RecInfo extends Info{
	private String customId;
	private String personId;
	private Integer RecordID;
	private Integer SnapID;
	private Integer VerifyStatus;
	private Integer PersonType;
	private String facesluiceId;
	private String facesluiceName;
	private Float similarity1;
	private Float similarity2;
	private String pic;
	private Date time;
	private String persionName;
	private String direction;
	
	public Integer getSnapID() {
		return SnapID;
	}
	public void setSnapID(Integer snapID) {
		SnapID = snapID;
	}
	
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	
	public String getCustomId() {
		return customId;
	}
	public void setCustomId(String customId) {
		this.customId = customId;
	}
	public String getPersonId() {
		return personId;
	}
	public void setPersonId(String personId) {
		this.personId = personId;
	}
	public Integer getRecordID() {
		return RecordID;
	}
	public void setRecordID(Integer recordId) {
		RecordID = recordId;
	}
	public Integer getVerifyStatus() {
		return VerifyStatus;
	}
	public void setVerifyStatus(Integer verifyStatus) {
		VerifyStatus = verifyStatus;
	}
	public Integer getPersonType() {
		return PersonType;
	}
	public void setPersonType(Integer personType) {
		PersonType = personType;
	}
	public String getFacesluiceId() {
		return facesluiceId;
	}
	public void setFacesluiceId(String facesluiceId) {
		this.facesluiceId = facesluiceId;
	}
	public String getFacesluiceName() {
		return facesluiceName;
	}
	public void setFacesluiceName(String facesluiceName) {
		this.facesluiceName = facesluiceName;
	}
	public Float getSimilarity1() {
		return similarity1;
	}
	public void setSimilarity1(Float similarity1) {
		this.similarity1 = similarity1;
	}
	public Float getSimilarity2() {
		return similarity2;
	}
	public void setSimilarity2(Float similarity2) {
		this.similarity2 = similarity2;
	}
	public String getPic() {
		return pic;
	}
	public void setPic(String pic) {
		this.pic = pic;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getPersionName() {
		return persionName;
	}
	public void setPersionName(String persionName) {
		this.persionName = persionName;
	}

	
	
	
	@Override
	public String toString() {
		return "RecInfo [customId=" + customId + ", personId=" + personId + ", RecordID=" + RecordID + ", SnapID="
				+ SnapID + ", VerifyStatus=" + VerifyStatus + ", PersonType=" + PersonType + ", facesluiceId="
				+ facesluiceId + ", facesluiceName=" + facesluiceName + ", similarity1=" + similarity1
				+ ", similarity2=" + similarity2 + ", pic=" + pic + ", time=" + time + ", persionName=" + persionName
				+ ", direction=" + direction + "]";
	}
	public RecInfo(String customId, String personId, Integer recordId, Integer verifyStatus, Integer personType,
			String facesluiceId, String facesluiceName, Float similarity1, Float similarity2, String pic, Date time,
			String persionName) {
		super();
		this.customId = customId;
		this.personId = personId;
		RecordID = recordId;
		VerifyStatus = verifyStatus;
		PersonType = personType;
		this.facesluiceId = facesluiceId;
		this.facesluiceName = facesluiceName;
		this.similarity1 = similarity1;
		this.similarity2 = similarity2;
		this.pic = pic;
		this.time = time;
		this.persionName = persionName;
	}
	public RecInfo() {
		super();
	} 
	
	
}

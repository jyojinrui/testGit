package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MjkxxbabMapper;
import com.ruoyi.system.domain.Mjkxxbab;
import com.ruoyi.system.service.IMjkxxbabService;

/**
 * 门禁卡Service业务层处理
 * 
 * @author Carry
 * @date 2020-09-29
 */
@Service
public class MjkxxbabServiceImpl implements IMjkxxbabService 
{
    @Autowired
    private MjkxxbabMapper mjkxxbabMapper;

    /**
     * 查询门禁卡
     * 
     * @param id 门禁卡ID
     * @return 门禁卡
     */
    @Override
    public Mjkxxbab selectMjkxxbabById(Long id)
    {
        return mjkxxbabMapper.selectMjkxxbabById(id);
    }

    /**
     * 查询门禁卡列表
     * 
     * @param mjkxxbab 门禁卡
     * @return 门禁卡
     */
    @Override
    public List<Mjkxxbab> selectMjkxxbabList(Mjkxxbab mjkxxbab)
    {
        return mjkxxbabMapper.selectMjkxxbabList(mjkxxbab);
    }

    /**
     * 新增门禁卡
     * 
     * @param mjkxxbab 门禁卡
     * @return 结果
     */
    @Override
    public int insertMjkxxbab(Mjkxxbab mjkxxbab)
    {
        return mjkxxbabMapper.insertMjkxxbab(mjkxxbab);
    }

    /**
     * 修改门禁卡
     * 
     * @param mjkxxbab 门禁卡
     * @return 结果
     */
    @Override
    public int updateMjkxxbab(Mjkxxbab mjkxxbab)
    {
        return mjkxxbabMapper.updateMjkxxbab(mjkxxbab);
    }

    /**
     * 批量删除门禁卡
     * 
     * @param ids 需要删除的门禁卡ID
     * @return 结果
     */
    @Override
    public int deleteMjkxxbabByIds(Long[] ids)
    {
        return mjkxxbabMapper.deleteMjkxxbabByIds(ids);
    }

    /**
     * 删除门禁卡信息
     * 
     * @param id 门禁卡ID
     * @return 结果
     */
    @Override
    public int deleteMjkxxbabById(Long id)
    {
        return mjkxxbabMapper.deleteMjkxxbabById(id);
    }

    /**
     * 根据小区管理员id获取小区门禁卡信息
     * 
     */
	@Override
	public List<Mjkxxbab> selectMjkxxbabByXqAdminId(Long userId, Mjkxxbab mjkxxbab) {
		// TODO Auto-generated method stub
		return mjkxxbabMapper.selectMjkxxbabByXqAdminId(userId,mjkxxbab);
	}

	@Override
	public List<Mjkxxbab> selectMjkxxbabListByDeptId(Long deptId,Long deptIdOfTree, Mjkxxbab mjkxxbab) {
		// TODO Auto-generated method stub
		return mjkxxbabMapper.selectMjkxxbabListByDeptId(deptId, deptIdOfTree,mjkxxbab);
	}
}

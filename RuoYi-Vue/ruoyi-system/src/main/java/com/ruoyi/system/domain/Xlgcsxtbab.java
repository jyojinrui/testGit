package com.ruoyi.system.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 雪亮工程摄像头设备对象 xlgcsxtbab
 * 
 * @author Carry
 * @date 2020-10-03
 */
public class Xlgcsxtbab extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 小区编码 */
    @Excel(name = "小区编码")
    private String buildingId;
    
    /** 摄像头编码 */
    @Excel(name = "摄像头编码")
    private String sxtbm;

    /** 单位名称 */
    @Excel(name = "单位名称")
    private String dwmc;

    /** IP地址 */
    @Excel(name = "IP地址")
    private String ipaddr;

    /** 经度(高德) */
    @Excel(name = "经度(高德)")
    private BigDecimal mapx;

    /** 纬度(高德) */
    @Excel(name = "纬度(高德)")
    private BigDecimal mapy;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSxtbm(String sxtbm) 
    {
        this.sxtbm = sxtbm;
    }

    public String getSxtbm() 
    {
        return sxtbm;
    }
    public void setDwmc(String dwmc) 
    {
        this.dwmc = dwmc;
    }

    public String getDwmc() 
    {
        return dwmc;
    }
    public void setIpaddr(String ipaddr) 
    {
        this.ipaddr = ipaddr;
    }

    public String getIpaddr() 
    {
        return ipaddr;
    }
    public void setMapx(BigDecimal mapx) 
    {
        this.mapx = mapx;
    }

    public BigDecimal getMapx() 
    {
        return mapx;
    }
    public void setMapy(BigDecimal mapy) 
    {
        this.mapy = mapy;
    }

    public BigDecimal getMapy() 
    {
        return mapy;
    }

    public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}
	

	public Xlgcsxtbab() {
		super();
	}

	public Xlgcsxtbab(Long id, String buildingId, String sxtbm, String dwmc, String ipaddr, BigDecimal mapx,
			BigDecimal mapy) {
		super();
		this.id = id;
		this.buildingId = buildingId;
		this.sxtbm = sxtbm;
		this.dwmc = dwmc;
		this.ipaddr = ipaddr;
		this.mapx = mapx;
		this.mapy = mapy;
	}

	@Override
	public String toString() {
		return "Xlgcsxtbab [id=" + id + ", buildingId=" + buildingId + ", sxtbm=" + sxtbm + ", dwmc=" + dwmc
				+ ", ipaddr=" + ipaddr + ", mapx=" + mapx + ", mapy=" + mapy + "]";
	}
}

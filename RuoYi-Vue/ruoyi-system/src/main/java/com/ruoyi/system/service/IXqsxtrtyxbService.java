package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Xqsxtrtyxb;

/**
 * 摄像头人体运行Service接口
 * 
 * @author Carry
 * @date 2020-10-03
 */
public interface IXqsxtrtyxbService 
{
    /**
     * 查询摄像头人体运行
     * 
     * @param id 摄像头人体运行ID
     * @return 摄像头人体运行
     */
    public Xqsxtrtyxb selectXqsxtrtyxbById(Long id);

    /**
     * 根据小区管理员id获取小区人体运行情况表
     * @param userId
     * @param xqsxtrtyxb
     * @return
     */
    public List<Xqsxtrtyxb> selectXqsxtrtyxbListByXqAdminId(Long userId,Xqsxtrtyxb xqsxtrtyxb);
    
    public List<Xqsxtrtyxb> selectXqsxtrtyxbListByDeptId(Long deptId,Xqsxtrtyxb xqsxtrtyxb);
    
    
    /**
     * 查询摄像头人体运行列表
     * 
     * @param xqsxtrtyxb 摄像头人体运行
     * @return 摄像头人体运行集合
     */
    public List<Xqsxtrtyxb> selectXqsxtrtyxbList(Xqsxtrtyxb xqsxtrtyxb);

    /**
     * 新增摄像头人体运行
     * 
     * @param xqsxtrtyxb 摄像头人体运行
     * @return 结果
     */
    public int insertXqsxtrtyxb(Xqsxtrtyxb xqsxtrtyxb);

    /**
     * 修改摄像头人体运行
     * 
     * @param xqsxtrtyxb 摄像头人体运行
     * @return 结果
     */
    public int updateXqsxtrtyxb(Xqsxtrtyxb xqsxtrtyxb);

    /**
     * 批量删除摄像头人体运行
     * 
     * @param ids 需要删除的摄像头人体运行ID
     * @return 结果
     */
    public int deleteXqsxtrtyxbByIds(Long[] ids);

    /**
     * 删除摄像头人体运行信息
     * 
     * @param id 摄像头人体运行ID
     * @return 结果
     */
    public int deleteXqsxtrtyxbById(Long id);
}

package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MjsbxxbabMapper;
import com.ruoyi.system.domain.Mjsbxxbab;
import com.ruoyi.system.service.IMjsbxxbabService;

/**
 * 门禁设备管理Service业务层处理
 * 
 * @author Carry
 * @date 2020-09-29
 */
@Service
public class MjsbxxbabServiceImpl implements IMjsbxxbabService 
{
    @Autowired
    private MjsbxxbabMapper mjsbxxbabMapper;

    /**
     * 查询门禁设备管理
     * 
     * @param id 门禁设备管理ID
     * @return 门禁设备管理
     */
    @Override
    public Mjsbxxbab selectMjsbxxbabById(Long id)
    {
        return mjsbxxbabMapper.selectMjsbxxbabById(id);
    }

    /**
     * 查询门禁设备管理列表
     * 
     * @param mjsbxxbab 门禁设备管理
     * @return 门禁设备管理
     */
    @Override
    public List<Mjsbxxbab> selectMjsbxxbabList(Mjsbxxbab mjsbxxbab)
    {
        return mjsbxxbabMapper.selectMjsbxxbabList(mjsbxxbab);
    }

    /**
     * 根据小区管理员Id获取门禁设备信息
     */
    @Override
	public List<Mjsbxxbab> selectMjsbxxbabListByXqAdminId(Long userId, Mjsbxxbab mjsbxxbab) {
		// TODO Auto-generated method stub
		return mjsbxxbabMapper.selectMjsbxxbabListByXqAdminId(userId, mjsbxxbab);
	}
    
    /**
     * 新增门禁设备管理
     * 
     * @param mjsbxxbab 门禁设备管理
     * @return 结果
     */
    @Override
    public int insertMjsbxxbab(Mjsbxxbab mjsbxxbab)
    {
        return mjsbxxbabMapper.insertMjsbxxbab(mjsbxxbab);
    }

    /**
     * 修改门禁设备管理
     * 
     * @param mjsbxxbab 门禁设备管理
     * @return 结果
     */
    @Override
    public int updateMjsbxxbab(Mjsbxxbab mjsbxxbab)
    {
        return mjsbxxbabMapper.updateMjsbxxbab(mjsbxxbab);
    }

    /**
     * 批量删除门禁设备管理
     * 
     * @param ids 需要删除的门禁设备管理ID
     * @return 结果
     */
    @Override
    public int deleteMjsbxxbabByIds(Long[] ids)
    {
        return mjsbxxbabMapper.deleteMjsbxxbabByIds(ids);
    }

    /**
     * 删除门禁设备管理信息
     * 
     * @param id 门禁设备管理ID
     * @return 结果
     */
    @Override
    public int deleteMjsbxxbabById(Long id)
    {
        return mjsbxxbabMapper.deleteMjsbxxbabById(id);
    }

	@Override
	public List<Mjsbxxbab> selectMjsbxxbabListByDeptId(Long deptId,Long deptIdOfTree, Mjsbxxbab mjsbxxbab) {
		// TODO Auto-generated method stub
		return mjsbxxbabMapper.selectMjsbxxbabListByDeptId(deptId, deptIdOfTree,mjsbxxbab);
	}

	
}

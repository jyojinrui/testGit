package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 渭南数据归属单位代码对象 wnsjgsdwdm
 * 
 * @author ruoyi
 * @date 2020-11-03
 */
public class Wnsjgsdwdm extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 数据归属单位名称 */
    @Excel(name = "数据归属单位名称")
    private String sjgsdwmc;

    /** 数据归属单位代码 */
    @Excel(name = "数据归属单位代码")
    private Long sjgsdwdm;

    /** 单位类别 */
    @Excel(name = "单位类别")
    private String dwlb;

    /** 单位类别代码 */
    @Excel(name = "单位类别代码")
    private String dwlbdm;

    /** 注销标识 */
    @Excel(name = "注销标识")
    private String zxbz;

    /** 登记时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "登记时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date djsj;

    /** 最后修改时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date zhxgsj;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSjgsdwmc(String sjgsdwmc) 
    {
        this.sjgsdwmc = sjgsdwmc;
    }

    public String getSjgsdwmc() 
    {
        return sjgsdwmc;
    }
    public void setSjgsdwdm(Long sjgsdwdm) 
    {
        this.sjgsdwdm = sjgsdwdm;
    }

    public Long getSjgsdwdm() 
    {
        return sjgsdwdm;
    }
    public void setDwlb(String dwlb) 
    {
        this.dwlb = dwlb;
    }

    public String getDwlb() 
    {
        return dwlb;
    }
    public void setDwlbdm(String dwlbdm) 
    {
        this.dwlbdm = dwlbdm;
    }

    public String getDwlbdm() 
    {
        return dwlbdm;
    }
    public void setZxbz(String zxbz) 
    {
        this.zxbz = zxbz;
    }

    public String getZxbz() 
    {
        return zxbz;
    }
    public void setDjsj(Date djsj) 
    {
        this.djsj = djsj;
    }

    public Date getDjsj() 
    {
        return djsj;
    }
    public void setZhxgsj(Date zhxgsj) 
    {
        this.zhxgsj = zhxgsj;
    }

    public Date getZhxgsj() 
    {
        return zhxgsj;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sjgsdwmc", getSjgsdwmc())
            .append("sjgsdwdm", getSjgsdwdm())
            .append("dwlb", getDwlb())
            .append("dwlbdm", getDwlbdm())
            .append("zxbz", getZxbz())
            .append("djsj", getDjsj())
            .append("zhxgsj", getZhxgsj())
            .toString();
    }
}

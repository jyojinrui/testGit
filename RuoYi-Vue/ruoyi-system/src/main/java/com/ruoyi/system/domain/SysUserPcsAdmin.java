package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 sys_user_pcs_admin
 * 
 * @author ruoyi
 * @date 2020-10-25
 */
public class SysUserPcsAdmin extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 派出所编码 */
    @Excel(name = "派出所编码")
    private String pcsbm;

    /** 用户编码 */
    @Excel(name = "用户编码")
    private Long userId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPcsbm(String pcsbm) 
    {
        this.pcsbm = pcsbm;
    }

    public String getPcsbm() 
    {
        return pcsbm;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("pcsbm", getPcsbm())
            .append("userId", getUserId())
            .toString();
    }
}

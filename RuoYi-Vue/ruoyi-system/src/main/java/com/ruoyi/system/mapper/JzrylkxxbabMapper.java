package com.ruoyi.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.system.domain.Jzrylkxxbab;

/**
 * 离开人员管理Mapper接口
 * 
 * @author ruoyi
 * @date 2020-09-28
 */
public interface JzrylkxxbabMapper 
{
    /**
     * 查询离开人员管理
     * 
     * @param id 离开人员管理ID
     * @return 离开人员管理
     */
    public Jzrylkxxbab selectJzrylkxxbabById(Long id);

    /**
     * listJzrylkxxbabbyxqadminid
     * 
     */
    public List<Jzrylkxxbab> selectJzrylkxxbabListByXqAdminId(@Param("userId")Long userId,@Param("jzrylkxxbab")Jzrylkxxbab jzrylkxxbab);
  
    
    public List<Jzrylkxxbab> selectJzrylkxxbabListByDeptId(@Param("deptId")Long deprId,@Param("deptIdOfTree")Long deptIdOfTree,@Param("jzrylkxxbab")Jzrylkxxbab jzrylkxxbab);
    
    /**
     * 查询离开人员管理列表
     * 
     * @param jzrylkxxbab 离开人员管理
     * @return 离开人员管理集合
     */
    public List<Jzrylkxxbab> selectJzrylkxxbabList(Jzrylkxxbab jzrylkxxbab);
    
    
   

    /**
     * 新增离开人员管理
     * 
     * @param jzrylkxxbab 离开人员管理
     * @return 结果
     */
    public int insertJzrylkxxbab(Jzrylkxxbab jzrylkxxbab);

    /**
     * 修改离开人员管理
     * 
     * @param jzrylkxxbab 离开人员管理
     * @return 结果
     */
    public int updateJzrylkxxbab(Jzrylkxxbab jzrylkxxbab);

    /**
     * 删除离开人员管理
     * 
     * @param id 离开人员管理ID
     * @return 结果
     */
    public int deleteJzrylkxxbabById(Long id);

    /**
     * 批量删除离开人员管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteJzrylkxxbabByIds(Long[] ids);
    
    public Long getCountByDeptId(long deptId); 
}

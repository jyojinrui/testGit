package com.ruoyi.system.domain;

import java.util.Arrays;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 居住人员对象 jzryrzxxbab
 * 
 * @author ruoyi
 * @date 2020-09-27
 */
public class Jzryrzxxbab extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 申报流水号 */
    @Excel(name = "申报流水号")
    private String sbxxlsh;

    /** 地址二维码编码 */
    @Excel(name = "地址二维码编码")
    private String dzewmbm;

    /** 小区编码 */
    @Excel(name = "小区编码")
    private String buildingId;

    /** 地址编码 */
    @Excel(name = "地址编码")
    private String metaAddrId;

    /** 居住地址 */
    @Excel(name = "居住地址")
    private String jzdz;

    /** 公民身份号码 */
    @Excel(name = "公民身份号码")
    private String gmsfhm;

    /** 姓名 */
    @Excel(name = "姓名")
    private String xm;

    /** 出生日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出生日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date csrq;

    /** 民族 */
    @Excel(name = "民族")
    private String mz;

    /** 户籍住址 */
    @Excel(name = "户籍住址")
    private String zz;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String lxdh;

    /** 工作单位 */
    @Excel(name = "工作单位")
    private String gzdw;

    /** 职业 */
    @Excel(name = "职业")
    private String zy;

    /** 照片Bse64 */
    @Excel(name = "照片Bse64")
    private String zp;
//    private $column.javaType zp;
    
    
    /** 照片Bse64 */
    @Excel(name = "照片Bse64")
    private String blobzp;
//    private $column.javaType zp;

    /** 居住日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "居住日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date jzrq;

    /** 登记单位 */
    @Excel(name = "登记单位")
    private String djdw;

    /** 登记人姓名 */
    @Excel(name = "登记人姓名")
    private String djrxm;

    /** 登记时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "登记时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date djsj;

    /** 申报系统 */
    @Excel(name = "申报系统")
    private String sbxt;

    @Excel(name="操作类型")
    private Integer opetype;
    
    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSbxxlsh(String sbxxlsh) 
    {
        this.sbxxlsh = sbxxlsh;
    }

    public String getSbxxlsh() 
    {
        return sbxxlsh;
    }
    public void setDzewmbm(String dzewmbm) 
    {
        this.dzewmbm = dzewmbm;
    }

    public String getDzewmbm() 
    {
        return dzewmbm;
    }
    public void setBuildingId(String buildingId) 
    {
        this.buildingId = buildingId;
    }

    public String getBuildingId() 
    {
        return buildingId;
    }
    public void setMetaAddrId(String metaAddrId) 
    {
        this.metaAddrId = metaAddrId;
    }

    public String getMetaAddrId() 
    {
        return metaAddrId;
    }
    public void setJzdz(String jzdz) 
    {
        this.jzdz = jzdz;
    }

    public String getJzdz() 
    {
        return jzdz;
    }
    public void setGmsfhm(String gmsfhm) 
    {
        this.gmsfhm = gmsfhm;
    }

    public String getGmsfhm() 
    {
        return gmsfhm;
    }
    public void setXm(String xm) 
    {
        this.xm = xm;
    }

    public String getXm() 
    {
        return xm;
    }
    public void setCsrq(Date csrq) 
    {
        this.csrq = csrq;
    }

    public Date getCsrq() 
    {
        return csrq;
    }
    public void setMz(String mz) 
    {
        this.mz = mz;
    }

    public String getMz() 
    {
        return mz;
    }
    public void setZz(String zz) 
    {
        this.zz = zz;
    }

    public String getZz() 
    {
        return zz;
    }
    public void setLxdh(String lxdh) 
    {
        this.lxdh = lxdh;
    }

    public String getLxdh() 
    {
        return lxdh;
    }
    public void setGzdw(String gzdw) 
    {
        this.gzdw = gzdw;
    }

    public String getGzdw() 
    {
        return gzdw;
    }
    public void setZy(String zy) 
    {
        this.zy = zy;
    }

    public String getZy() 
    {
        return zy;
    }
    public void setZp(String zp) 
    {
        this.zp = zp;
    }

    public String getZp() 
    {
        return zp;
    }
    public void setJzrq(Date jzrq) 
    {
        this.jzrq = jzrq;
    }

    public Date getJzrq() 
    {
        return jzrq;
    }
    public void setDjdw(String djdw) 
    {
        this.djdw = djdw;
    }

    public String getDjdw() 
    {
        return djdw;
    }
    public void setDjrxm(String djrxm) 
    {
        this.djrxm = djrxm;
    }

    public String getDjrxm() 
    {
        return djrxm;
    }
    public void setDjsj(Date djsj) 
    {
        this.djsj = djsj;
    }

    public Date getDjsj() 
    {
        return djsj;
    }
    public void setSbxt(String sbxt) 
    {
        this.sbxt = sbxt;
    }

    public String getSbxt() 
    {
        return sbxt;
    }
    

    public String getBlobzp() {
		return blobzp;
	}

	public void setBlobzp(String blobzp) {
		this.blobzp = blobzp;
	}

	public Integer getOpetype() {
		return opetype;
	}

	public void setOpetype(Integer opetype) {
		this.opetype = opetype;
	}

	
	public Jzryrzxxbab() {
		super();
	}

	public Jzryrzxxbab(Long id, String sbxxlsh, String dzewmbm, String buildingId, String metaAddrId, String jzdz,
			String gmsfhm, String xm, Date csrq, String mz, String zz, String lxdh, String gzdw, String zy, String zp,
			String blobzp, Date jzrq, String djdw, String djrxm, Date djsj, String sbxt, Integer opetype) {
		super();
		this.id = id;
		this.sbxxlsh = sbxxlsh;
		this.dzewmbm = dzewmbm;
		this.buildingId = buildingId;
		this.metaAddrId = metaAddrId;
		this.jzdz = jzdz;
		this.gmsfhm = gmsfhm;
		this.xm = xm;
		this.csrq = csrq;
		this.mz = mz;
		this.zz = zz;
		this.lxdh = lxdh;
		this.gzdw = gzdw;
		this.zy = zy;
		this.zp = zp;
		this.blobzp = blobzp;
		this.jzrq = jzrq;
		this.djdw = djdw;
		this.djrxm = djrxm;
		this.djsj = djsj;
		this.sbxt = sbxt;
		this.opetype = opetype;
	}

	public Jzryrzxxbab(Long id, String sbxxlsh, String dzewmbm, String buildingId, String metaAddrId, String jzdz,
			String gmsfhm, String xm, Date csrq, String mz, String zz, String lxdh, String gzdw, String zy, String zp,
			Date jzrq, String djdw, String djrxm, Date djsj, String sbxt, Integer opetype) {
		
		super();
		this.toString();
		this.id = id;
		this.sbxxlsh = sbxxlsh;
		this.dzewmbm = dzewmbm;
		this.buildingId = buildingId;
		this.metaAddrId = metaAddrId;
		this.jzdz = jzdz;
		this.gmsfhm = gmsfhm;
		this.xm = xm;
		this.csrq = csrq;
		this.mz = mz;
		this.zz = zz;
		this.lxdh = lxdh;
		this.gzdw = gzdw;
		this.zy = zy;
		this.zp = zp;
		this.jzrq = jzrq;
		this.djdw = djdw;
		this.djrxm = djrxm;
		this.djsj = djsj;
		this.sbxt = sbxt;
		this.opetype = opetype;
	}

	@Override
	public String toString() {
		return "Jzryrzxxbab [id=" + id + ", sbxxlsh=" + sbxxlsh + ", dzewmbm=" + dzewmbm + ", buildingId=" + buildingId
				+ ", metaAddrId=" + metaAddrId + ", jzdz=" + jzdz + ", gmsfhm=" + gmsfhm + ", xm=" + xm + ", csrq="
				+ csrq + ", mz=" + mz + ", zz=" + zz + ", lxdh=" + lxdh + ", gzdw=" + gzdw + ", zy=" + zy + ", zp=" + zp
				+ ", blobzp=" + (blobzp) + ", jzrq=" + jzrq + ", djdw=" + djdw + ", djrxm=" + djrxm
				+ ", djsj=" + djsj + ", sbxt=" + sbxt + ", opetype=" + opetype + "]";
	}
	
}

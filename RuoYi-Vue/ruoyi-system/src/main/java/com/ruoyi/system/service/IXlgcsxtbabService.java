package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Xlgcsxtbab;

/**
 * 雪亮工程摄像头设备Service接口
 * 
 * @author Carry
 * @date 2020-10-03
 */
public interface IXlgcsxtbabService 
{
    /**
     * 查询雪亮工程摄像头设备
     * 
     * @param id 雪亮工程摄像头设备ID
     * @return 雪亮工程摄像头设备
     */
    public Xlgcsxtbab selectXlgcsxtbabById(Long id);

    /**
     * 查询雪亮工程摄像头设备列表
     * 
     * @param xlgcsxtbab 雪亮工程摄像头设备
     * @return 雪亮工程摄像头设备集合
     */
    public List<Xlgcsxtbab> selectXlgcsxtbabList(Xlgcsxtbab xlgcsxtbab);

    
    public List<Xlgcsxtbab> selectXlgcsxtbabListByXqAdminId(Long userId,Xlgcsxtbab xlgcsxtbab);
    
    public List<Xlgcsxtbab> selectXlgcsxtbabListByDeptId(Long deptId,Xlgcsxtbab xlgcsxtbab);
    
    
    /**
     * 新增雪亮工程摄像头设备
     * 
     * @param xlgcsxtbab 雪亮工程摄像头设备
     * @return 结果
     */
    public int insertXlgcsxtbab(Xlgcsxtbab xlgcsxtbab);

    /**
     * 修改雪亮工程摄像头设备
     * 
     * @param xlgcsxtbab 雪亮工程摄像头设备
     * @return 结果
     */
    public int updateXlgcsxtbab(Xlgcsxtbab xlgcsxtbab);

    /**
     * 批量删除雪亮工程摄像头设备
     * 
     * @param ids 需要删除的雪亮工程摄像头设备ID
     * @return 结果
     */
    public int deleteXlgcsxtbabByIds(Long[] ids);

    /**
     * 删除雪亮工程摄像头设备信息
     * 
     * @param id 雪亮工程摄像头设备ID
     * @return 结果
     */
    public int deleteXlgcsxtbabById(Long id);
}

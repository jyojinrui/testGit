package com.ruoyi.system.domain;

import java.util.Arrays;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 雪亮工程摄像头运行对象 xlgcsxtyxb
 * 
 * @author Carry
 * @date 2020-10-04
 */
public class Xlgcsxtyxb extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;
    
    /** 小区编码 */
    @Excel(name = "小区编码")
    private String buildingId;

    /** 摄像头编码 */
    @Excel(name = "摄像头编码")
    private String sxtbm;

    /** 人脸照片 */
    @Excel(name = "人脸照片")
    private byte[] rlzp;

    /** 抓拍时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "抓拍时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date zpsj;

    /** 业务流水号 */
    @Excel(name = "业务流水号")
    private String ywlsh;

    /** 比对状态 */
    @Excel(name = "比对状态")
    private String bdzt;

    /** 比对时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "比对时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date bdsj;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSxtbm(String sxtbm) 
    {
        this.sxtbm = sxtbm;
    }

    public String getSxtbm() 
    {
        return sxtbm;
    }
    public void setRlzp(byte[] rlzp) 
    {
        this.rlzp = rlzp;
    }

    public byte[] getRlzp() 
    {
        return rlzp;
    }
    public void setZpsj(Date zpsj) 
    {
        this.zpsj = zpsj;
    }

    public Date getZpsj() 
    {
        return zpsj;
    }
    public void setYwlsh(String ywlsh) 
    {
        this.ywlsh = ywlsh;
    }

    public String getYwlsh() 
    {
        return ywlsh;
    }
    public void setBdzt(String bdzt) 
    {
        this.bdzt = bdzt;
    }

    public String getBdzt() 
    {
        return bdzt;
    }
    public void setBdsj(Date bdsj) 
    {
        this.bdsj = bdsj;
    }

    public Date getBdsj() 
    {
        return bdsj;
    }

    public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public Xlgcsxtyxb() {
		super();
	}

	public Xlgcsxtyxb(Long id, String buildingId, String sxtbm, byte[] rlzp, Date zpsj, String ywlsh, String bdzt,
			Date bdsj) {
		super();
		this.id = id;
		this.buildingId = buildingId;
		this.sxtbm = sxtbm;
		this.rlzp = rlzp;
		this.zpsj = zpsj;
		this.ywlsh = ywlsh;
		this.bdzt = bdzt;
		this.bdsj = bdsj;
	}

	@Override
	public String toString() {
		return "Xlgcsxtyxb [id=" + id + ", buildingId=" + buildingId + ", sxtbm=" + sxtbm + ", rlzp="
				+ Arrays.toString(rlzp) + ", zpsj=" + zpsj + ", ywlsh=" + ywlsh + ", bdzt=" + bdzt + ", bdsj=" + bdsj
				+ "]";
	}
	
}

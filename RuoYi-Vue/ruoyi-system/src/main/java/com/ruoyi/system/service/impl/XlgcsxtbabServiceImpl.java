package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.XlgcsxtbabMapper;
import com.ruoyi.system.domain.Xlgcsxtbab;
import com.ruoyi.system.service.IXlgcsxtbabService;

/**
 * 雪亮工程摄像头设备Service业务层处理
 * 
 * @author Carry
 * @date 2020-10-03
 */
@Service
public class XlgcsxtbabServiceImpl implements IXlgcsxtbabService 
{
    @Autowired
    private XlgcsxtbabMapper xlgcsxtbabMapper;

    /**
     * 查询雪亮工程摄像头设备
     * 
     * @param id 雪亮工程摄像头设备ID
     * @return 雪亮工程摄像头设备
     */
    @Override
    public Xlgcsxtbab selectXlgcsxtbabById(Long id)
    {
        return xlgcsxtbabMapper.selectXlgcsxtbabById(id);
    }

    /**
     * 查询雪亮工程摄像头设备列表
     * 
     * @param xlgcsxtbab 雪亮工程摄像头设备
     * @return 雪亮工程摄像头设备
     */
    @Override
    public List<Xlgcsxtbab> selectXlgcsxtbabList(Xlgcsxtbab xlgcsxtbab)
    {
        return xlgcsxtbabMapper.selectXlgcsxtbabList(xlgcsxtbab);
    }

    /**
     * 新增雪亮工程摄像头设备
     * 
     * @param xlgcsxtbab 雪亮工程摄像头设备
     * @return 结果
     */
    @Override
    public int insertXlgcsxtbab(Xlgcsxtbab xlgcsxtbab)
    {
        return xlgcsxtbabMapper.insertXlgcsxtbab(xlgcsxtbab);
    }

    /**
     * 修改雪亮工程摄像头设备
     * 
     * @param xlgcsxtbab 雪亮工程摄像头设备
     * @return 结果
     */
    @Override
    public int updateXlgcsxtbab(Xlgcsxtbab xlgcsxtbab)
    {
        return xlgcsxtbabMapper.updateXlgcsxtbab(xlgcsxtbab);
    }

    /**
     * 批量删除雪亮工程摄像头设备
     * 
     * @param ids 需要删除的雪亮工程摄像头设备ID
     * @return 结果
     */
    @Override
    public int deleteXlgcsxtbabByIds(Long[] ids)
    {
        return xlgcsxtbabMapper.deleteXlgcsxtbabByIds(ids);
    }

    /**
     * 删除雪亮工程摄像头设备信息
     * 
     * @param id 雪亮工程摄像头设备ID
     * @return 结果
     */
    @Override
    public int deleteXlgcsxtbabById(Long id)
    {
        return xlgcsxtbabMapper.deleteXlgcsxtbabById(id);
    }

	@Override
	public List<Xlgcsxtbab> selectXlgcsxtbabListByXqAdminId(Long userId, Xlgcsxtbab xlgcsxtbab) {
		// TODO Auto-generated method stub
		return xlgcsxtbabMapper.selectXlgcsxtbabListByXqAdminId(userId,xlgcsxtbab);
	}

	@Override
	public List<Xlgcsxtbab> selectXlgcsxtbabListByDeptId(Long deptId, Xlgcsxtbab xlgcsxtbab) {
		// TODO Auto-generated method stub
		return xlgcsxtbabMapper.selectXlgcsxtbabListByDeptId(deptId,xlgcsxtbab);
	}
}

package com.ruoyi.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.system.domain.Rycrbab;

/**
 * 出入人员管理Mapper接口
 * 
 * @author ruoyi
 * @date 2020-09-29
 */
public interface RycrbabMapper 
{
    /**
     * 查询出入人员管理
     * 
     * @param id 出入人员管理ID
     * @return 出入人员管理
     */
    public Rycrbab selectRycrbabById(Long id);

    /**
     * 查询出入人员管理列表
     * 
     * @param rycrbab 出入人员管理
     * @return 出入人员管理集合
     */
    public List<Rycrbab> selectRycrbabList(Rycrbab rycrbab);
    
    
    public List<Rycrbab> selectRycrbabListByXqAdminId(@Param("userId")Long userId,@Param("rycrbab")Rycrbab rycrbab);

    public List<Rycrbab> selectTodayRycrbabListByXqAdminId(@Param("userId")Long userId,@Param("rycrbab")Rycrbab rycrbab);
   
    public List<Rycrbab> selectAllRycrbabListByXqAdminId(@Param("userId")Long userId,@Param("rycrbab")Rycrbab rycrbab);
    
    public List<Rycrbab> selectRycrbabListByDeptId(@Param("deptId")Long deptId,@Param("deptIdOfTree")Long deptIdOfTree,  @Param("rycrbab")Rycrbab rycrbab);
    
    
    public long countAllRycrbabByDeptId(@Param("deptId")Long deptId,@Param("deptIdOfTree")Long deptIdOfTree,  @Param("rycrbab")Rycrbab rycrbab);
    public long countTodayRycrbabByDeptId(@Param("deptId")Long deptId,@Param("deptIdOfTree")Long deptIdOfTree,  @Param("rycrbab")Rycrbab rycrbab);
    
    
   
    
    public long countAllRycrbabByXqAdminId(@Param("userId")Long userId, @Param("rycrbab")Rycrbab rycrbab);
    
    public long countTodayRycrbabByXqAdminId(@Param("userId")Long userId, @Param("rycrbab")Rycrbab rycrbab);
    
    public List<Rycrbab> selectTodayRycrbabListByDeptId(@Param("deptId")Long deptId,@Param("deptIdOfTree")Long deptIdOfTree,  @Param("rycrbab")Rycrbab rycrbab);
    public List<Rycrbab> selectAllRycrbabListByDeptId(@Param("deptId")Long deptId,@Param("deptIdOfTree")Long deptIdOfTree,  @Param("rycrbab")Rycrbab rycrbab);
    
    
    /**
     * 新增出入人员管理
     * 
     * @param rycrbab 出入人员管理
     * @return 结果
     */
    public int insertRycrbab(Rycrbab rycrbab);

    /**
     * 修改出入人员管理
     * 
     * @param rycrbab 出入人员管理
     * @return 结果
     */
    public int updateRycrbab(Rycrbab rycrbab);

    /**
     * 删除出入人员管理
     * 
     * @param id 出入人员管理ID
     * @return 结果
     */
    public int deleteRycrbabById(Long id);

    /**
     * 批量删除出入人员管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRycrbabByIds(Long[] ids);
    
    public Long getCountByDeptId(long deptId); 
    public Long getCountByXqbm(String xqbm); 
}

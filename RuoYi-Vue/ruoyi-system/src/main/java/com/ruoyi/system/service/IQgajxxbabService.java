package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.common.core.domain.statistics.QgajSta;
import com.ruoyi.system.domain.Qgajxxbab;

/**
 * 区公安局信息Service接口
 * 
 * @author Carry
 * @date 2020-10-25
 */
public interface IQgajxxbabService 
{
    /**
     * 查询区公安局信息
     * 
     * @param id 区公安局信息ID
     * @return 区公安局信息
     */
    public Qgajxxbab selectQgajxxbabById(Long id);

    public Qgajxxbab selectQgajxxbabByAdminId(Long adminid);
    
    /**
     * 查询区公安局信息列表
     * 
     * @param qgajxxbab 区公安局信息
     * @return 区公安局信息集合
     */
    public List<Qgajxxbab> selectQgajxxbabList(Qgajxxbab qgajxxbab);
    
    /**
     * 查询区公安局信息列表
     * 
     * @param qgajxxbab 区公安局信息
     * @return 区公安局信息集合
     */
    public List<Qgajxxbab> selectQgajxxbabListByDeptId(Long deptId,Long deptIdOfTree,Qgajxxbab qgajxxbab);
    
    
    /**
     * 统计区公安局信息列表
     * 
     * @param qgajxxbab 区公安局信息
     * @return 区公安局信息集合
     */
    public List<QgajSta> selectQgajStaDataList();
    

    /**
     * 新增区公安局信息
     * 
     * @param qgajxxbab 区公安局信息
     * @return 结果
     */
    public int insertQgajxxbab(Qgajxxbab qgajxxbab);

    /**
     * 修改区公安局信息
     * 
     * @param qgajxxbab 区公安局信息
     * @return 结果
     */
    public int updateQgajxxbab(Qgajxxbab qgajxxbab);

    /**
     * 批量删除区公安局信息
     * 
     * @param ids 需要删除的区公安局信息ID
     * @return 结果
     */
    public int deleteQgajxxbabByIds(Long[] ids);

    /**
     * 删除区公安局信息信息
     * 
     * @param id 区公安局信息ID
     * @return 结果
     */
    public int deleteQgajxxbabById(Long id);
    
    
}

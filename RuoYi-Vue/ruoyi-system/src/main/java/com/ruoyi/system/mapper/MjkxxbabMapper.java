package com.ruoyi.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.system.domain.Mjkxxbab;

/**
 * 门禁卡Mapper接口
 * 
 * @author Carry
 * @date 2020-09-29
 */
public interface MjkxxbabMapper 
{
    /**
     * 查询门禁卡
     * 
     * @param id 门禁卡ID
     * @return 门禁卡
     */
    public Mjkxxbab selectMjkxxbabById(Long id);

    /**
     * 查询门禁卡列表
     * 
     * @param mjkxxbab 门禁卡
     * @return 门禁卡集合
     */
    public List<Mjkxxbab> selectMjkxxbabList(Mjkxxbab mjkxxbab);

    /**
     * 根据小区管理员id获取小区信息
     * 
     */
    public List<Mjkxxbab> selectMjkxxbabByXqAdminId(@Param("userId")Long userId,@Param("mjkxxbab")Mjkxxbab mjkxxbab);
    
    
    public List<Mjkxxbab> selectMjkxxbabListByDeptId(@Param("deptId")Long deptId,@Param("deptIdOfTree")Long deptIdOfTree, @Param("mjkxxbab")Mjkxxbab mjkxxbab);
    /**
     * 新增门禁卡
     * 
     * @param mjkxxbab 门禁卡
     * @return 结果
     */
    public int insertMjkxxbab(Mjkxxbab mjkxxbab);

    /**
     * 修改门禁卡
     * 
     * @param mjkxxbab 门禁卡
     * @return 结果
     */
    public int updateMjkxxbab(Mjkxxbab mjkxxbab);

    /**
     * 删除门禁卡
     * 
     * @param id 门禁卡ID
     * @return 结果
     */
    public int deleteMjkxxbabById(Long id);

    /**
     * 批量删除门禁卡
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteMjkxxbabByIds(Long[] ids);
    public Long getCountByDeptId(long deptId); 
    public Long getCountByXqbm(String xqbm); 
}

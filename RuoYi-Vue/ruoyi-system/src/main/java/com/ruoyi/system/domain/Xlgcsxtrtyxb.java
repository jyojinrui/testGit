package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 雪亮工程摄像头人体运行数据对象 xlgcsxtrtyxb
 * 
 * @author Carry
 * @date 2020-10-04
 */
public class Xlgcsxtrtyxb extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 小区编码 */
    @Excel(name = "小区编码")
    private String buildingId;

    /** 摄像头编码 */
    @Excel(name = "摄像头编码")
    private String sxtbm;

    /** 抓拍人体照片 */
    @Excel(name = "抓拍人体照片")
    private byte[] rtzp;

    /** 抓拍时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "抓拍时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date zpsj;

    /** 业务流水号 */
    @Excel(name = "业务流水号")
    private String ywlsh;

    /** 比对状态 */
    @Excel(name = "比对状态")
    private String bdzt;

    /** 比对时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "比对时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date bdsj;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setBuildingId(String buildingId) 
    {
        this.buildingId = buildingId;
    }

    public String getBuildingId() 
    {
        return buildingId;
    }
    public void setSxtbm(String sxtbm) 
    {
        this.sxtbm = sxtbm;
    }

    public String getSxtbm() 
    {
        return sxtbm;
    }
    public void setRtzp(byte[] rtzp) 
    {
        this.rtzp = rtzp;
    }

    public byte[] getRtzp() 
    {
        return rtzp;
    }
    public void setZpsj(Date zpsj) 
    {
        this.zpsj = zpsj;
    }

    public Date getZpsj() 
    {
        return zpsj;
    }
    public void setYwlsh(String ywlsh) 
    {
        this.ywlsh = ywlsh;
    }

    public String getYwlsh() 
    {
        return ywlsh;
    }
    public void setBdzt(String bdzt) 
    {
        this.bdzt = bdzt;
    }

    public String getBdzt() 
    {
        return bdzt;
    }
    public void setBdsj(Date bdsj) 
    {
        this.bdsj = bdsj;
    }

    public Date getBdsj() 
    {
        return bdsj;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("buildingId", getBuildingId())
            .append("sxtbm", getSxtbm())
            .append("rtzp", getRtzp())
            .append("zpsj", getZpsj())
            .append("ywlsh", getYwlsh())
            .append("bdzt", getBdzt())
            .append("bdsj", getBdsj())
            .toString();
    }
}

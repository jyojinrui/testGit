package com.ruoyi.system.service.impl;

import java.io.IOException;
import java.util.Base64;
import java.util.List;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.transfer.CamUser;
import com.ruoyi.common.utils.ImageHandleUtils;
import com.ruoyi.common.utils.sign.AESUtil;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.domain.FaceInfo;
import com.ruoyi.system.domain.Jzryrzxxbab;
import com.ruoyi.system.domain.Message;
import com.ruoyi.system.mapper.JzryrzxxbabMapper;
import com.ruoyi.system.service.IJzryrzxxbabService;

/**
 * 居住人员Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-09-27
 */
@Service
public class JzryrzxxbabServiceImpl implements IJzryrzxxbabService {
	@Autowired
	private JzryrzxxbabMapper jzryrzxxbabMapper;
	
	@Autowired
	private MqttClient mqttClient;
	

    @Value("${server.mqtt-switch}")
    private boolean mqttSwitch;

	private ImageHandleUtils im = SpringUtils.getBean(ImageHandleUtils.class);
	
    protected final Logger logger = LoggerFactory.getLogger(JzryrzxxbabServiceImpl.class);

//    private ImageHandleController imageHandleController= SpringUtils.getBean(ImageHandleController.class);

	/**
	 * 查询居住人员
	 * 
	 * @param id 居住人员ID
	 * @return 居住人员
	 */
	@Override
	public Jzryrzxxbab selectJzryrzxxbabById(Long id) {
		
		Jzryrzxxbab jzryrzxxbab=jzryrzxxbabMapper.selectJzryrzxxbabById(id);
		jzryrzxxbab.setJzdz(AESUtil.decrypt(jzryrzxxbab.getJzdz()));             //
		jzryrzxxbab.setGmsfhm(AESUtil.decrypt(jzryrzxxbab.getGmsfhm()));
		jzryrzxxbab.setLxdh(AESUtil.decrypt(jzryrzxxbab.getLxdh()));
		jzryrzxxbab.setZp(AESUtil.decrypt(jzryrzxxbab.getZp()));
		return jzryrzxxbab;
	}

	/**
	 * 查询居住人员列表
	 * 
	 * @param jzryrzxxbab 居住人员
	 * @return 居住人员
	 */
	@Override
	public List<Jzryrzxxbab> selectJzryrzxxbabList(Jzryrzxxbab jzryrzxxbab) {
		/**
		 * 对请求参数进行加密处理后才能与数据库中加密的数据进行匹配
		 */
		jzryrzxxbab.setJzdz(AESUtil.encrypt(jzryrzxxbab.getJzdz()));         
		jzryrzxxbab.setGmsfhm(AESUtil.encrypt(jzryrzxxbab.getGmsfhm()));    
		jzryrzxxbab.setLxdh(AESUtil.encrypt(jzryrzxxbab.getLxdh()));        

		List<Jzryrzxxbab> jzryrzxxbabList=jzryrzxxbabMapper.selectJzryrzxxbabList(jzryrzxxbab);
		/**
		 * 对查询到的数据进行解密
		 */
//		for (int i = 0; i < jzryrzxxbabList.size(); i++) {
//			jzryrzxxbabList.get(i).setJzdz(AESUtil.decrypt(jzryrzxxbabList.get(i).getJzdz()));
//			jzryrzxxbabList.get(i).setGmsfhm(AESUtil.decrypt(jzryrzxxbabList.get(i).getGmsfhm()));
//			jzryrzxxbabList.get(i).setLxdh(AESUtil.decrypt(jzryrzxxbabList.get(i).getLxdh()));
//			jzryrzxxbabList.get(i).setZp(AESUtil.decrypt(jzryrzxxbabList.get(i).getZp()));
//			
//		
//		}
		
		return jzryrzxxbabList;
	}

	/**
	 * 新增居住人员
	 * 
	 * @param jzryrzxxbab 居住人员
	 * @return 结果
	 * @throws IOException 
	 * 
	 */
	@Override
	public int insertJzryrzxxbab(Jzryrzxxbab jzryrzxxbab) throws IOException {

		Base64.Encoder encoder = Base64.getEncoder();
		byte[] re = (byte[]) im.getImgBin(jzryrzxxbab.getZp());
		String base64Pic = Base64.getEncoder().encodeToString(re);
		jzryrzxxbab.setZp(AESUtil.encrypt(jzryrzxxbab.getZp()));              //照片路径加密
		
		if(mqttSwitch){
			//发布人员增加的消息到对应话题
			String buildingId=jzryrzxxbab.getBuildingId();
			String publishTopic=Constants.MQTT_TOPIC_PREFIX+buildingId;

			String pic = "data:image/jpeg;base64," + base64Pic;
			FaceInfo faceInfo = new FaceInfo(jzryrzxxbab.getGmsfhm(), jzryrzxxbab.getXm(), 0, 0, "", "", 0, pic, 0,
					jzryrzxxbab.getGmsfhm());
			String messageId = String.valueOf(System.currentTimeMillis());
			String operator = "EditPerson";
			Message downInfo = new Message(messageId, operator, faceInfo);
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(downInfo);
			MqttMessage message = new MqttMessage();
			message.setQos(1);
			message.setRetained(false);
			message.setPayload(json.getBytes());
			try {
				mqttClient.publish(publishTopic, message);
			} catch (MqttException me) {

				logger.error("发布增加人员的消息失败",me);
				jzryrzxxbab.setDzewmbm("发布增加人员的消息失败");
			}
			
		}

		/**
		 * 敏感数据入库前进行加密处理
		 */
		jzryrzxxbab.setJzdz(AESUtil.encrypt(jzryrzxxbab.getJzdz()));          //居住地址加密
		jzryrzxxbab.setGmsfhm(AESUtil.encrypt(jzryrzxxbab.getGmsfhm()));      //公民身份证加密
		jzryrzxxbab.setLxdh(AESUtil.encrypt(jzryrzxxbab.getLxdh()));          //联系电话加密
		
		return jzryrzxxbabMapper.insertJzryrzxxbab(jzryrzxxbab);

	}

	/**
	 * 修改居住人员
	 * 
	 * @param jzryrzxxbab 居住人员
	 * @return 结果
	 * @throws IOException 
	 */
	@Override
	public int updateJzryrzxxbab(Jzryrzxxbab jzryrzxxbab) throws IOException {
		
		Base64.Encoder encoder = Base64.getEncoder();
		byte[] re = (byte[]) im.getImgBin(jzryrzxxbab.getZp());
		String base64Pic = Base64.getEncoder().encodeToString(re);
		
		if(mqttSwitch){
			//发布人员增加的消息到对应话题
			String buildingId=jzryrzxxbab.getBuildingId();
			String publishTopic=Constants.MQTT_TOPIC_PREFIX+buildingId;

			String pic = "data:image/jepg;base64," + base64Pic;
			FaceInfo faceInfo = new FaceInfo(jzryrzxxbab.getGmsfhm(), jzryrzxxbab.getXm(), 0, 0, "", "", 0, pic, 0,
					jzryrzxxbab.getGmsfhm());
			String messageId = String.valueOf(System.currentTimeMillis());
			String operator = "EditPerson";
			Message downInfo = new Message(messageId, operator, faceInfo);
			Gson gson = new GsonBuilder().create();
			String json = gson.toJson(downInfo);
			MqttMessage message = new MqttMessage();
			message.setQos(1);
			message.setRetained(false);
			message.setPayload(json.getBytes());
			try {
				mqttClient.publish(publishTopic, message);
			} catch (MqttException me) {

				logger.error("发布修改人员的消息失败",me);
				jzryrzxxbab.setDzewmbm("发布修改人员的消息失败");
			}
		}
		/**
		 * 敏感数据入库前进行加密处理
		 */
		jzryrzxxbab.setJzdz(AESUtil.encrypt(jzryrzxxbab.getJzdz()));          //居住地址加密
		jzryrzxxbab.setGmsfhm(AESUtil.encrypt(jzryrzxxbab.getGmsfhm()));      //公民身份证加密
		jzryrzxxbab.setLxdh(AESUtil.encrypt(jzryrzxxbab.getLxdh()));          //联系电话加密
		jzryrzxxbab.setZp(AESUtil.encrypt(jzryrzxxbab.getZp()));              //照片路径加密
		
		return jzryrzxxbabMapper.updateJzryrzxxbab(jzryrzxxbab);
	}

	/**
	 * 批量删除居住人员
	 * 
	 * @param ids 需要删除的居住人员ID
	 * @return 结果
	 */
	@Override
	public int deleteJzryrzxxbabByIds(Long[] ids) {
		if(mqttSwitch){
			for(Long id:ids){
				Jzryrzxxbab jzryrzxxbab=selectJzryrzxxbabById(id);
				String buildingId=jzryrzxxbab.getBuildingId();
				String publishTopic=Constants.MQTT_TOPIC_PREFIX+buildingId;
				FaceInfo faceInfo = new FaceInfo(jzryrzxxbab.getGmsfhm(), "", 0, 0, "", "", 0, "", 0, jzryrzxxbab.getGmsfhm());
				String messageId = String.valueOf(System.currentTimeMillis());
				String operator = "DelPerson";
				Message downInfo = new Message(messageId, operator, faceInfo);
				Gson gson = new GsonBuilder().create();
				String json = gson.toJson(downInfo);
				MqttMessage message = new MqttMessage();
				message.setQos(1);
				message.setRetained(false);
				message.setPayload(json.getBytes());
				try {
					mqttClient.publish(publishTopic, message);
				} catch (MqttException me) {

					logger.error("发布删除人员的消息失败",me);
				}
			}
		}
		
		return jzryrzxxbabMapper.deleteJzryrzxxbabByIds(ids);
	}

	/**
	 * 删除居住人员信息
	 * 
	 * @param id 居住人员ID
	 * @return 结果
	 */
	@Override
	public int deleteJzryrzxxbabById(Long id) {
		return jzryrzxxbabMapper.deleteJzryrzxxbabById(id);
	}

	/**
	 * 根据小区管理员id查询实有人口信息
	 * userId 管理员id
	 */
	@Override
	public List<Jzryrzxxbab> selectJzryrzxxbabListByXqAdminId(Long userId, Jzryrzxxbab jzryrzxxbab) {
		// TODO Auto-generated method stub
		List<Jzryrzxxbab> jzryrzxxbabList = jzryrzxxbabMapper.selectJzryrzxxbabListByXqAdminId(userId, jzryrzxxbab);
		
		/**
		 * 查询到实有人口后对敏感数据进行解密
		 */
		for (int i = 0; i < jzryrzxxbabList.size(); i++) {
			jzryrzxxbabList.get(i).setJzdz(AESUtil.decrypt(jzryrzxxbabList.get(i).getJzdz()));
			jzryrzxxbabList.get(i).setGmsfhm(AESUtil.decrypt(jzryrzxxbabList.get(i).getGmsfhm()));
			jzryrzxxbabList.get(i).setLxdh(AESUtil.decrypt(jzryrzxxbabList.get(i).getLxdh()));
			jzryrzxxbabList.get(i).setZp(AESUtil.decrypt(jzryrzxxbabList.get(i).getZp()));
		}
		return jzryrzxxbabList;
	}

	/**
	 * 根据部门id查询实有人口信息
	 * deptId 部门id
	 * depyIdOfTee  部门树的id  
	 */
	@Override
	public List<Jzryrzxxbab> selectJzryrzxxbabListByDeptId(Long deptId, Long deptIdOfTree, Jzryrzxxbab jzryrzxxbab) {
		// TODO Auto-generated method stub

		List<Jzryrzxxbab> jzryrzxxbabList = jzryrzxxbabMapper.selectJzryrzxxbabListByDeptId(deptId, deptIdOfTree,
				jzryrzxxbab);
		/**
		 * 对查询到的实有人口信息中敏感的数据解密
		 */
		for (int i = 0; i < jzryrzxxbabList.size(); i++) {
			jzryrzxxbabList.get(i).setJzdz(AESUtil.decrypt(jzryrzxxbabList.get(i).getJzdz()));
			jzryrzxxbabList.get(i).setGmsfhm(AESUtil.decrypt(jzryrzxxbabList.get(i).getGmsfhm()));
			jzryrzxxbabList.get(i).setLxdh(AESUtil.decrypt(jzryrzxxbabList.get(i).getLxdh()));
			jzryrzxxbabList.get(i).setZp(AESUtil.decrypt(jzryrzxxbabList.get(i).getZp()));
		}

		return jzryrzxxbabList;
	}
 
	/**
	 * 对接中翔设备的函数，将新增的实有人口信息实时下发给中翔人脸识别设备
	 */
	@Override
	public List<CamUser> selectInfoToCamByXqId(String buildingId, long lastReqStamp, long rTime) {
		// TODO Auto-generated method stub
		List<CamUser> camuserlist = jzryrzxxbabMapper.selectInfoToCamByXqId(buildingId, lastReqStamp, rTime);

		try {
			for (int i = 0; i < camuserlist.size(); i++) {
				/** 对身份证信息解密，以身份证信息作为人脸识别设备上的识别ID*/
				String sfz = camuserlist.get(i).getGmsfhm();
				camuserlist.get(i).setGmsfhm(AESUtil.decrypt(sfz));
				
				byte[] re = (byte[]) im.getImgBin(AESUtil.decrypt(camuserlist.get(i).getZp()));//将图片路径进行解密，注意是从本地读取数据还是从外网下载图片过来再转化 
				String encoded = Base64.getEncoder().encodeToString(re);     //对图片信息进行base64加密
				camuserlist.get(i).setBinzp(encoded);

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return camuserlist;
	}

	public static String bytesToHexString(byte[] bytes) {
		StringBuilder sb = new StringBuilder(bytes.length * 2);
		// 使用String的format方法进行转换
		for (byte b : bytes) {
			sb.append(String.format("%02x", new Integer(b & 0xff)));
		}

		return sb.toString();
	}
    /**
     * 默认是加密过后的身份证信息
     */
	@Override
	public String getXmByGmsfzh(String sfz) {
		// TODO Auto-generated method stub
		return jzryrzxxbabMapper.getXmByGmsfzh(sfz);
	}
}

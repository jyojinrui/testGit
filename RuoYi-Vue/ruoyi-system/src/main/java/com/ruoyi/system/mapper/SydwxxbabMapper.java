package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Sydwxxbab;

/**
 * 实有单位信息Mapper接口
 * 
 * @author Carry
 * @date 2020-10-27
 */
public interface SydwxxbabMapper 
{
    /**
     * 查询实有单位信息
     * 
     * @param id 实有单位信息ID
     * @return 实有单位信息
     */
    public Sydwxxbab selectSydwxxbabById(Long id);

    /**
     * 查询实有单位信息列表
     * 
     * @param sydwxxbab 实有单位信息
     * @return 实有单位信息集合
     */
    public List<Sydwxxbab> selectSydwxxbabList(Sydwxxbab sydwxxbab);

    /**
     * 新增实有单位信息
     * 
     * @param sydwxxbab 实有单位信息
     * @return 结果
     */
    public int insertSydwxxbab(Sydwxxbab sydwxxbab);

    /**
     * 修改实有单位信息
     * 
     * @param sydwxxbab 实有单位信息
     * @return 结果
     */
    public int updateSydwxxbab(Sydwxxbab sydwxxbab);

    /**
     * 删除实有单位信息
     * 
     * @param id 实有单位信息ID
     * @return 结果
     */
    public int deleteSydwxxbabById(Long id);

    /**
     * 批量删除实有单位信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSydwxxbabByIds(Long[] ids);
    
    public Long getCountByDeptId(long deptId); 
    public Long getCountByXqbm(String xqbm); 
}

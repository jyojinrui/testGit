package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.XlgcsxtrtyxbMapper;
import com.ruoyi.system.domain.Xlgcsxtrtyxb;
import com.ruoyi.system.service.IXlgcsxtrtyxbService;

/**
 * 雪亮工程摄像头人体运行数据Service业务层处理
 * 
 * @author Carry
 * @date 2020-10-04
 */
@Service
public class XlgcsxtrtyxbServiceImpl implements IXlgcsxtrtyxbService 
{
    @Autowired
    private XlgcsxtrtyxbMapper xlgcsxtrtyxbMapper;

    /**
     * 查询雪亮工程摄像头人体运行数据
     * 
     * @param id 雪亮工程摄像头人体运行数据ID
     * @return 雪亮工程摄像头人体运行数据
     */
    @Override
    public Xlgcsxtrtyxb selectXlgcsxtrtyxbById(Long id)
    {
        return xlgcsxtrtyxbMapper.selectXlgcsxtrtyxbById(id);
    }

    /**
     * 查询雪亮工程摄像头人体运行数据列表
     * 
     * @param xlgcsxtrtyxb 雪亮工程摄像头人体运行数据
     * @return 雪亮工程摄像头人体运行数据
     */
    @Override
    public List<Xlgcsxtrtyxb> selectXlgcsxtrtyxbList(Xlgcsxtrtyxb xlgcsxtrtyxb)
    {
        return xlgcsxtrtyxbMapper.selectXlgcsxtrtyxbList(xlgcsxtrtyxb);
    }

    /**
     * 新增雪亮工程摄像头人体运行数据
     * 
     * @param xlgcsxtrtyxb 雪亮工程摄像头人体运行数据
     * @return 结果
     */
    @Override
    public int insertXlgcsxtrtyxb(Xlgcsxtrtyxb xlgcsxtrtyxb)
    {
        return xlgcsxtrtyxbMapper.insertXlgcsxtrtyxb(xlgcsxtrtyxb);
    }

    /**
     * 修改雪亮工程摄像头人体运行数据
     * 
     * @param xlgcsxtrtyxb 雪亮工程摄像头人体运行数据
     * @return 结果
     */
    @Override
    public int updateXlgcsxtrtyxb(Xlgcsxtrtyxb xlgcsxtrtyxb)
    {
        return xlgcsxtrtyxbMapper.updateXlgcsxtrtyxb(xlgcsxtrtyxb);
    }

    /**
     * 批量删除雪亮工程摄像头人体运行数据
     * 
     * @param ids 需要删除的雪亮工程摄像头人体运行数据ID
     * @return 结果
     */
    @Override
    public int deleteXlgcsxtrtyxbByIds(Long[] ids)
    {
        return xlgcsxtrtyxbMapper.deleteXlgcsxtrtyxbByIds(ids);
    }

    /**
     * 删除雪亮工程摄像头人体运行数据信息
     * 
     * @param id 雪亮工程摄像头人体运行数据ID
     * @return 结果
     */
    public int deleteXlgcsxtrtyxbById(Long id)
    {
        return xlgcsxtrtyxbMapper.deleteXlgcsxtrtyxbById(id);
    }

	@Override
	public List<Xlgcsxtrtyxb> selectXlgcsxtrtyxbListByXqAdminId(Long userId, Xlgcsxtrtyxb xlgcsxtrtyxb) {
		// TODO Auto-generated method stub
		return xlgcsxtrtyxbMapper.selectXlgcsxtrtyxbListByXqAdminId(userId, xlgcsxtrtyxb);

	}

	@Override
	public List<Xlgcsxtrtyxb> selectXlgcsxtrtyxbListByDeptId(Long deptId, Xlgcsxtrtyxb xlgcsxtrtyxb) {
		// TODO Auto-generated method stub
		return xlgcsxtrtyxbMapper.selectXlgcsxtrtyxbListByDeptId(deptId,xlgcsxtrtyxb);
	}
}

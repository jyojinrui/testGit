package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.JzrylkxxbabMapper;
import com.ruoyi.common.utils.sign.AESUtil;
import com.ruoyi.system.domain.Jzrylkxxbab;
import com.ruoyi.system.service.IJzrylkxxbabService;

/**
 * 离开人员管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-09-28
 */
@Service
public class JzrylkxxbabServiceImpl implements IJzrylkxxbabService 
{
    @Autowired
    private JzrylkxxbabMapper jzrylkxxbabMapper;

    /**
     * 查询离开人员管理
     * 
     * @param id 离开人员管理ID
     * @return 离开人员管理
     */
    @Override
    public Jzrylkxxbab selectJzrylkxxbabById(Long id)
    {
    	Jzrylkxxbab jzrylkxxbab=jzrylkxxbabMapper.selectJzrylkxxbabById(id);
    	/**
    	 * 将居住地址等敏感信息解密后发送给前端页面
    	 */
    	jzrylkxxbab.setJzdz(AESUtil.decrypt(jzrylkxxbab.getJzdz()));         
    	jzrylkxxbab.setGmsfhm(AESUtil.decrypt(jzrylkxxbab.getGmsfhm()));         
    	
        return jzrylkxxbab;
    }

    /**
     * 查询离开人员管理列表
     * 
     * @param jzrylkxxbab 离开人员管理
     * @return 离开人员管理
     */
    @Override
    public List<Jzrylkxxbab> selectJzrylkxxbabList(Jzrylkxxbab jzrylkxxbab)
    {
    	
    	/**
    	 * 对前端查询参数加密后才能与数据库数据匹配
    	 * */
    	jzrylkxxbab.setJzdz(AESUtil.encrypt(jzrylkxxbab.getJzdz()));
    	jzrylkxxbab.setGmsfhm(AESUtil.encrypt(jzrylkxxbab.getGmsfhm()));
    	
    	List<Jzrylkxxbab> jzrylkxxbabList=jzrylkxxbabMapper.selectJzrylkxxbabList(jzrylkxxbab);
    	
    	/**
    	 * 将查询到的数据中敏感信息解密后再发送给前端
    	 */
    	for (int i = 0; i < jzrylkxxbabList.size(); i++) {
    		jzrylkxxbabList.get(i).setJzdz(AESUtil.decrypt(jzrylkxxbabList.get(i).getJzdz()));
    		jzrylkxxbabList.get(i).setGmsfhm(AESUtil.decrypt(jzrylkxxbabList.get(i).getGmsfhm()));
		}
        return jzrylkxxbabList;
    }

    /**
     * 新增离开人员管理
     * 
     * @param jzrylkxxbab 离开人员管理
     * @return 结果
     */
    @Override
    public int insertJzrylkxxbab(Jzrylkxxbab jzrylkxxbab)
    {
    	/**
    	 * 对敏感信息加密后再插入到数据库
    	 */
    	jzrylkxxbab.setJzdz(AESUtil.encrypt(jzrylkxxbab.getJzdz()));
    	jzrylkxxbab.setGmsfhm(AESUtil.encrypt(jzrylkxxbab.getGmsfhm()));
    	
        return jzrylkxxbabMapper.insertJzrylkxxbab(jzrylkxxbab);
    }

    /**
     * 修改离开人员管理
     * 
     * @param jzrylkxxbab 离开人员管理
     * @return 结果
     */
    @Override
    public int updateJzrylkxxbab(Jzrylkxxbab jzrylkxxbab)
    {
    	/**
    	 * 对敏感信息加密后再插入到数据库
    	 */
    	jzrylkxxbab.setJzdz(AESUtil.encrypt(jzrylkxxbab.getJzdz()));
    	jzrylkxxbab.setGmsfhm(AESUtil.encrypt(jzrylkxxbab.getGmsfhm()));
        return jzrylkxxbabMapper.updateJzrylkxxbab(jzrylkxxbab);
    }

    /**
     * 批量删除离开人员管理
     * 
     * @param ids 需要删除的离开人员管理ID
     * @return 结果
     */
    @Override
    public int deleteJzrylkxxbabByIds(Long[] ids)
    {
        return jzrylkxxbabMapper.deleteJzrylkxxbabByIds(ids);
    }

    /**
     * 删除离开人员管理信息
     * 
     * @param id 离开人员管理ID
     * @return 结果
     */
    @Override
    public int deleteJzrylkxxbabById(Long id)
    {
        return jzrylkxxbabMapper.deleteJzrylkxxbabById(id);
    }

	@Override
	public List<Jzrylkxxbab> selectJzrylkxxbabListByXqAdminId(Long userId, Jzrylkxxbab jzrylkxxbab) {
		// TODO Auto-generated method stub
	
		/**
    	 * 对前端查询参数加密后才能与数据库数据匹配
    	 * */
    	jzrylkxxbab.setJzdz(AESUtil.encrypt(jzrylkxxbab.getJzdz()));
    	jzrylkxxbab.setGmsfhm(AESUtil.encrypt(jzrylkxxbab.getGmsfhm()));
    	
    	List<Jzrylkxxbab> jzrylkxxbabList=jzrylkxxbabMapper.selectJzrylkxxbabListByXqAdminId(userId, jzrylkxxbab);
    	
    	/**
    	 * 将查询到的数据中敏感信息解密后再发送给前端
    	 */
    	for (int i = 0; i < jzrylkxxbabList.size(); i++) {
    		jzrylkxxbabList.get(i).setJzdz(AESUtil.decrypt(jzrylkxxbabList.get(i).getJzdz()));
    		jzrylkxxbabList.get(i).setGmsfhm(AESUtil.decrypt(jzrylkxxbabList.get(i).getGmsfhm()));
		}
        return jzrylkxxbabList;
	}

	@Override
	public List<Jzrylkxxbab> selectJzrylkxxbabListByDeptId(Long deptId,Long deptIdOfTree, Jzrylkxxbab jzrylkxxbab) {
		// TODO Auto-generated method stub
		
		
		/**
    	 * 对前端查询参数加密后才能与数据库数据匹配
    	 * */
    	jzrylkxxbab.setJzdz(AESUtil.encrypt(jzrylkxxbab.getJzdz()));
    	jzrylkxxbab.setGmsfhm(AESUtil.encrypt(jzrylkxxbab.getGmsfhm()));
    	
    	List<Jzrylkxxbab> jzrylkxxbabList= jzrylkxxbabMapper.selectJzrylkxxbabListByDeptId(deptId,deptIdOfTree,jzrylkxxbab);
    	
    	/**
    	 * 将查询到的数据中敏感信息解密后再发送给前端
    	 */
    	for (int i = 0; i < jzrylkxxbabList.size(); i++) {
    		jzrylkxxbabList.get(i).setJzdz(AESUtil.decrypt(jzrylkxxbabList.get(i).getJzdz()));
    		jzrylkxxbabList.get(i).setGmsfhm(AESUtil.decrypt(jzrylkxxbabList.get(i).getGmsfhm()));
		}
        return jzrylkxxbabList;
		
	}
}

package com.ruoyi.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.system.domain.Fkbab;

/**
 * 访客信息管理Mapper接口
 * 
 * @author Carry
 * @date 2020-09-29
 */
public interface FkbabMapper 
{
    /**
     * 查询访客信息管理
     * 
     * @param id 访客信息管理ID
     * @return 访客信息管理
     */
    public Fkbab selectFkbabById(Long id);

    /**
     * 查询访客信息管理列表
     * 
     * @param fkbab 访客信息管理
     * @return 访客信息管理集合
     */
    public List<Fkbab> selectFkbabList(Fkbab fkbab);

    /**
     * 根据小区管理员id获取小区访客信息
     * @param userId
     * @param fkbab
     * @return
     */
    public List<Fkbab> selectFkbabListByXqAdminId(@Param("userId")Long userId,@Param("fkbab")Fkbab fkbab);

    public List<Fkbab> selectFkbabListByDeptId(@Param("deptId")Long deptId,@Param("deptIdOfTree")Long deptIdOfTree, @Param("fkbab")Fkbab fkbab);

    
    
    /**
     * 新增访客信息管理
     * 
     * @param fkbab 访客信息管理
     * @return 结果
     */
    public int insertFkbab(Fkbab fkbab);

    /**
     * 修改访客信息管理
     * 
     * @param fkbab 访客信息管理
     * @return 结果
     */
    public int updateFkbab(Fkbab fkbab);

    /**
     * 删除访客信息管理
     * 
     * @param id 访客信息管理ID
     * @return 结果
     */
    public int deleteFkbabById(Long id);

    /**
     * 批量删除访客信息管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteFkbabByIds(Long[] ids);
    
    public Long getCountByDeptId(long deptId); 
    public Long getCountByXqbm(String xqbm); 
}

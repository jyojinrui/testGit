package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 小区管理对象 xqxxbab
 * 
 * @author ruoyi
 * @date 2020-09-26
 */
public class Xqxxbab extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成 */
    private String id;

    /** 地址二维码编码 */
    @Excel(name = "地址二维码编码")
    private String dzewmbm;

    /** 小区编号 */
    @Excel(name = "小区编号")
    private String buildingId;
    
    /**治综管理系统小区编码*/
    @Excel(name="治综小区编码")
    private String zzssxqbm;

    /** 小区名称 */
    @Excel(name = "小区名称")
    private String xqmc;

    /** 小区地址 */
    @Excel(name = "小区地址")
    private String xqdz;

    /** 所属区县 */
    @Excel(name = "所属区县")
    private String ssqx;

    /** 辖区派出所 */
    @Excel(name = "辖区派出所")
    private String xqpcs;

    /** 小区负责人 */
    @Excel(name = "小区负责人")
    private String xqfzr;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String lxdh;

    /** 常住人口 */
    @Excel(name = "常住人口")
    private Long czrk;

    /** 自然幢数 */
    @Excel(name = "自然幢数")
    private Long zrzs;

    /** 总建筑面积 */
    @Excel(name = "总建筑面积")
    private Long zjzmj;

    /** 物业公司 */
    @Excel(name = "物业公司")
    private String wygs;

    /** 开发公司 */
    @Excel(name = "开发公司")
    private String kfgs;

    /** 开发日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开发日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date kfrq;

    /** 竣工日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "竣工日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date jgrq;

    /** 预售日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "预售日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date ysrq;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setDzewmbm(String dzewmbm) 
    {
        this.dzewmbm = dzewmbm;
    }

    public String getDzewmbm() 
    {
        return dzewmbm;
    }
    public void setBuildingId(String buildingId) 
    {
        this.buildingId = buildingId;
    }

    public String getBuildingId() 
    {
        return buildingId;
    }
    
    
    
    public String getZzssxqbm() {
		return zzssxqbm;
	}

	public void setZzssxqbm(String zzssxqbm) {
		this.zzssxqbm = zzssxqbm;
	}

	public void setXqmc(String xqmc) 
    {
        this.xqmc = xqmc;
    }

    public String getXqmc() 
    {
        return xqmc;
    }
    public void setXqdz(String xqdz) 
    {
        this.xqdz = xqdz;
    }

    public String getXqdz() 
    {
        return xqdz;
    }
    public void setSsqx(String ssqx) 
    {
        this.ssqx = ssqx;
    }

    public String getSsqx() 
    {
        return ssqx;
    }
    public void setXqpcs(String xqpcs) 
    {
        this.xqpcs = xqpcs;
    }

    public String getXqpcs() 
    {
        return xqpcs;
    }
    public void setXqfzr(String xqfzr) 
    {
        this.xqfzr = xqfzr;
    }

    public String getXqfzr() 
    {
        return xqfzr;
    }
    public void setLxdh(String lxdh) 
    {
        this.lxdh = lxdh;
    }

    public String getLxdh() 
    {
        return lxdh;
    }
    public void setCzrk(Long czrk) 
    {
        this.czrk = czrk;
    }

    public Long getCzrk() 
    {
        return czrk;
    }
    public void setZrzs(Long zrzs) 
    {
        this.zrzs = zrzs;
    }

    public Long getZrzs() 
    {
        return zrzs;
    }
    public void setZjzmj(Long zjzmj) 
    {
        this.zjzmj = zjzmj;
    }

    public Long getZjzmj() 
    {
        return zjzmj;
    }
    public void setWygs(String wygs) 
    {
        this.wygs = wygs;
    }

    public String getWygs() 
    {
        return wygs;
    }
    public void setKfgs(String kfgs) 
    {
        this.kfgs = kfgs;
    }

    public String getKfgs() 
    {
        return kfgs;
    }
    public void setKfrq(Date kfrq) 
    {
        this.kfrq = kfrq;
    }

    public Date getKfrq() 
    {
        return kfrq;
    }
    public void setJgrq(Date jgrq) 
    {
        this.jgrq = jgrq;
    }

    public Date getJgrq() 
    {
        return jgrq;
    }
    public void setYsrq(Date ysrq) 
    {
        this.ysrq = ysrq;
    }

    public Date getYsrq() 
    {
        return ysrq;
    }

    
    public Xqxxbab() {
		super();
	}
    

	public Xqxxbab(String id, String dzewmbm, String buildingId, String zzssxqbm, String xqmc, String xqdz, String ssqx,
			String xqpcs, String xqfzr, String lxdh, Long czrk, Long zrzs, Long zjzmj, String wygs, String kfgs,
			Date kfrq, Date jgrq, Date ysrq) {
		super();
		this.id = id;
		this.dzewmbm = dzewmbm;
		this.buildingId = buildingId;
		this.zzssxqbm = zzssxqbm;
		this.xqmc = xqmc;
		this.xqdz = xqdz;
		this.ssqx = ssqx;
		this.xqpcs = xqpcs;
		this.xqfzr = xqfzr;
		this.lxdh = lxdh;
		this.czrk = czrk;
		this.zrzs = zrzs;
		this.zjzmj = zjzmj;
		this.wygs = wygs;
		this.kfgs = kfgs;
		this.kfrq = kfrq;
		this.jgrq = jgrq;
		this.ysrq = ysrq;
	}

	@Override
	public String toString() {
		return "Xqxxbab [id=" + id + ", dzewmbm=" + dzewmbm + ", buildingId=" + buildingId + ", zzssxqbm=" + zzssxqbm
				+ ", xqmc=" + xqmc + ", xqdz=" + xqdz + ", ssqx=" + ssqx + ", xqpcs=" + xqpcs + ", xqfzr=" + xqfzr
				+ ", lxdh=" + lxdh + ", czrk=" + czrk + ", zrzs=" + zrzs + ", zjzmj=" + zjzmj + ", wygs=" + wygs
				+ ", kfgs=" + kfgs + ", kfrq=" + kfrq + ", jgrq=" + jgrq + ", ysrq=" + ysrq + "]";
	}
    
    
}

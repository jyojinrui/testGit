package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 警务室信息对象 jwsxxbab
 * 
 * @author Carry
 * @date 2020-10-27
 */
public class Jwsxxbab extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 警务室编码 */
    @Excel(name = "警务室编码")
    private String jwsbm;

    /** 地址二维码信息 */
    @Excel(name = "地址二维码信息")
    private String dzewmbm;

    /** 警务室名称 */
    @Excel(name = "警务室名称")
    private String jwsmc;

    /** 警务室地址 */
    @Excel(name = "警务室地址")
    private String jwsdz;

    /** 所属区县 */
    @Excel(name = "所属区县")
    private String ssqx;

    /** 所属派出所 */
    @Excel(name = "所属派出所")
    private String sspcs;

    /** 警务室负责人 */
    @Excel(name = "警务室负责人")
    private String jwsfzr;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String lxdh;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setJwsbm(String jwsbm) 
    {
        this.jwsbm = jwsbm;
    }

    public String getJwsbm() 
    {
        return jwsbm;
    }
    public void setDzewmbm(String dzewmbm) 
    {
        this.dzewmbm = dzewmbm;
    }

    public String getDzewmbm() 
    {
        return dzewmbm;
    }
    public void setJwsmc(String jwsmc) 
    {
        this.jwsmc = jwsmc;
    }

    public String getJwsmc() 
    {
        return jwsmc;
    }
    public void setJwsdz(String jwsdz) 
    {
        this.jwsdz = jwsdz;
    }

    public String getJwsdz() 
    {
        return jwsdz;
    }
    public void setSsqx(String ssqx) 
    {
        this.ssqx = ssqx;
    }

    public String getSsqx() 
    {
        return ssqx;
    }
    public void setSspcs(String sspcs) 
    {
        this.sspcs = sspcs;
    }

    public String getSspcs() 
    {
        return sspcs;
    }
    public void setJwsfzr(String jwsfzr) 
    {
        this.jwsfzr = jwsfzr;
    }

    public String getJwsfzr() 
    {
        return jwsfzr;
    }
    public void setLxdh(String lxdh) 
    {
        this.lxdh = lxdh;
    }

    public String getLxdh() 
    {
        return lxdh;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("jwsbm", getJwsbm())
            .append("dzewmbm", getDzewmbm())
            .append("jwsmc", getJwsmc())
            .append("jwsdz", getJwsdz())
            .append("ssqx", getSsqx())
            .append("sspcs", getSspcs())
            .append("jwsfzr", getJwsfzr())
            .append("lxdh", getLxdh())
            .toString();
    }
}

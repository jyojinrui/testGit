package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.activation.MailcapCommandMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.system.mapper.CyryxxbabMapper;
import com.ruoyi.system.mapper.FkbabMapper;
import com.ruoyi.system.mapper.FzxxbabMapper;
import com.ruoyi.system.mapper.JzryrzxxbabMapper;
import com.ruoyi.system.mapper.MjkxxbabMapper;
import com.ruoyi.system.mapper.MjsbxxbabMapper;
import com.ruoyi.system.mapper.QgajxxbabMapper;
import com.ruoyi.system.mapper.RycrbabMapper;
import com.ruoyi.system.mapper.SydwxxbabMapper;
import com.ruoyi.system.mapper.SysDeptMapper;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.statistics.QgajSta;
import com.ruoyi.system.domain.Qgajxxbab;
import com.ruoyi.system.service.IQgajxxbabService;

/**
 * 区公安局信息Service业务层处理
 * 
 * @author Carry
 * @date 2020-10-25
 */
@Service
public class QgajxxbabServiceImpl implements IQgajxxbabService {
	@Autowired
	private QgajxxbabMapper qgajxxbabMapper;

	@Autowired
	private SysDeptMapper sysDeptMapper;

	@Autowired
	private FzxxbabMapper fzxxbabMapper;

	@Autowired
	private JzryrzxxbabMapper jzryrzxxbabMapper;

	@Autowired
	private SydwxxbabMapper sydwxxbabMapper;

	@Autowired
	private CyryxxbabMapper cyryxxbabMapper;

	@Autowired
	private MjsbxxbabMapper mjsbxxbabMapper;

	@Autowired
	private MjkxxbabMapper mjkxxbabMapper;

	@Autowired
	private FkbabMapper fkbabMapper;

	@Autowired
	private RycrbabMapper RycrbabMapper;

	/**
	 * 查询区公安局信息
	 * 
	 * @param id 区公安局信息ID
	 * @return 区公安局信息
	 */
	@Override
	public Qgajxxbab selectQgajxxbabById(Long id) {
		return qgajxxbabMapper.selectQgajxxbabById(id);
	}

	@Override
	public Qgajxxbab selectQgajxxbabByAdminId(Long adminid) {
		// TODO Auto-generated method stub
		return qgajxxbabMapper.selectQgajxxbabByAdminId(adminid);
	}

	/**
	 * 查询区公安局信息列表
	 * 
	 * @param qgajxxbab 区公安局信息
	 * @return 区公安局信息
	 */
	@Override
	public List<Qgajxxbab> selectQgajxxbabList(Qgajxxbab qgajxxbab) {
		return qgajxxbabMapper.selectQgajxxbabList(qgajxxbab);
	}

	/**
	 * 新增区公安局信息
	 * 
	 * @param qgajxxbab 区公安局信息
	 * @return 结果
	 */
	@Override
	public int insertQgajxxbab(Qgajxxbab qgajxxbab) {
		return qgajxxbabMapper.insertQgajxxbab(qgajxxbab);
	}

	/**
	 * 修改区公安局信息
	 * 
	 * @param qgajxxbab 区公安局信息
	 * @return 结果
	 */
	@Override
	public int updateQgajxxbab(Qgajxxbab qgajxxbab) {
		return qgajxxbabMapper.updateQgajxxbab(qgajxxbab);
	}

	/**
	 * 批量删除区公安局信息
	 * 
	 * @param ids 需要删除的区公安局信息ID
	 * @return 结果
	 */
	@Override
	public int deleteQgajxxbabByIds(Long[] ids) {
		return qgajxxbabMapper.deleteQgajxxbabByIds(ids);
	}

	/**
	 * 删除区公安局信息信息
	 * 
	 * @param id 区公安局信息ID
	 * @return 结果
	 */
	@Override
	public int deleteQgajxxbabById(Long id) {
		return qgajxxbabMapper.deleteQgajxxbabById(id);
	}

	@Override
	public List<QgajSta> selectQgajStaDataList() {
		// TODO Auto-generated method stub

		List<QgajSta> qgajStaDataList = new ArrayList();
		List<SysDept> depts = sysDeptMapper.getQgaj();
		for (int i = 0; i < depts.size(); i++) {
			QgajSta qj = new QgajSta();
			qj.setQgajbm(depts.get(i).getDeptId()); // 分局编码
			qj.setFjmc(depts.get(i).getDeptName()); // 分局名称
			qj.setJsgs("西安景煜网络科技有限公司"); // 建设公司

			// 根据部门id获取实有房屋数据
			long syfw = fzxxbabMapper.getCountByDeptId(depts.get(i).getDeptId());
			qj.setSyfw(syfw);

			// 根据部门id获取实有人口数据
			long syrk = jzryrzxxbabMapper.getCountByDeptId(depts.get(i).getDeptId());
			qj.setSyrk(syrk);

			// 根据部门id获取实有单位信息
            long sydw=sydwxxbabMapper.getCountByDeptId(depts.get(i).getDeptId());
            qj.setSydw(sydw);
			
			// 根据部门id获取从业人员信息
            long cyry =cyryxxbabMapper.getCountByDeptId(depts.get(i).getDeptId());
            qj.setCyry(cyry);

			// 根据部门id获取门禁设备信息
            long mjsb=mjsbxxbabMapper.getCountByDeptId(depts.get(i).getDeptId());
            qj.setMjsb(mjsb);

			// 根据部门id获取门禁卡信息
            long mjk=mjkxxbabMapper.getCountByDeptId(depts.get(i).getDeptId());
            qj.setMjk(mjk);

			// 根据部门id获取访客数据信息
            long fksj=fkbabMapper.getCountByDeptId(depts.get(i).getDeptId());
            qj.setFksj(fksj);
            
			// 根据部门id获取出入记录信息
            long crjl=RycrbabMapper.getCountByDeptId(depts.get(i).getDeptId());
			qj.setCrjl(crjl);
            // 根据部门id获取出入照片数据信息

			qgajStaDataList.add(qj);

		}

//		return qgajxxbabMapper.selectQgajStaDataList();
		return qgajStaDataList;
	}

	@Override
	public List<Qgajxxbab> selectQgajxxbabListByDeptId(Long deptId, Long deptIdOfTree, Qgajxxbab qgajxxbab) {
		// TODO Auto-generated method stub
		return qgajxxbabMapper.selectQgajxxbabListByDeptId(deptId,deptIdOfTree,qgajxxbab);
	}

}

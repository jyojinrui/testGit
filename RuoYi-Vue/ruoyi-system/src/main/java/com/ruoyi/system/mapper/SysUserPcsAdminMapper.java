package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysUserPcsAdmin;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2020-10-25
 */
public interface SysUserPcsAdminMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public SysUserPcsAdmin selectSysUserPcsAdminById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param sysUserPcsAdmin 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<SysUserPcsAdmin> selectSysUserPcsAdminList(SysUserPcsAdmin sysUserPcsAdmin);

    /**
     * 新增【请填写功能名称】
     * 
     * @param sysUserPcsAdmin 【请填写功能名称】
     * @return 结果
     */
    public int insertSysUserPcsAdmin(SysUserPcsAdmin sysUserPcsAdmin);

    /**
     * 修改【请填写功能名称】
     * 
     * @param sysUserPcsAdmin 【请填写功能名称】
     * @return 结果
     */
    public int updateSysUserPcsAdmin(SysUserPcsAdmin sysUserPcsAdmin);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteSysUserPcsAdminById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysUserPcsAdminByIds(Long[] ids);
}

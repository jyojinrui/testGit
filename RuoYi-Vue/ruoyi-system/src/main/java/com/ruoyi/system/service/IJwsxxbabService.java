package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.domain.Jwsxxbab;

/**
 * 警务室信息Service接口
 * 
 * @author Carry
 * @date 2020-10-27
 */
public interface IJwsxxbabService 
{
    /**
     * 查询警务室信息
     * 
     * @param id 警务室信息ID
     * @return 警务室信息
     */
    public Jwsxxbab selectJwsxxbabById(Long id);

    /**
     * 查询警务室信息列表
     * 
     * @param jwsxxbab 警务室信息
     * @return 警务室信息集合
     */
    public List<Jwsxxbab> selectJwsxxbabList(Jwsxxbab jwsxxbab);

    
    public  List<Jwsxxbab> selectJwsxxbabListByDeptId(Long deptId,Long deptIdOfTree,Jwsxxbab jwsxxbab);
    /**
     * 新增警务室信息
     * 
     * @param jwsxxbab 警务室信息
     * @return 结果
     */
    public int insertJwsxxbab(Jwsxxbab jwsxxbab);

    /**
     * 修改警务室信息
     * 
     * @param jwsxxbab 警务室信息
     * @return 结果
     */
    public int updateJwsxxbab(Jwsxxbab jwsxxbab);

    /**
     * 批量删除警务室信息
     * 
     * @param ids 需要删除的警务室信息ID
     * @return 结果
     */
    public int deleteJwsxxbabByIds(Long[] ids);

    /**
     * 删除警务室信息信息
     * 
     * @param id 警务室信息ID
     * @return 结果
     */
    public int deleteJwsxxbabById(Long id);
}

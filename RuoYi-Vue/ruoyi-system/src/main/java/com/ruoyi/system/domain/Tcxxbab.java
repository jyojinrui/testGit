package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 停车信息管理对象 tcxxbab
 * 
 * @author Carry
 * @date 2020-09-30
 */
public class Tcxxbab extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 小区编码*/
    private String buildingId;
    
    /** 停车场编码 */
    @Excel(name = "停车场编码")
    private String lvtccbh;

    
    
    /** 地址二维码编码 */
    @Excel(name = "地址二维码编码")
    private String dzewmb;

    /** 车牌号码 */
    @Excel(name = "车牌号码")
    private String lvcphm;

    /** 车牌类型 */
    @Excel(name = "车牌类型")
    private String lvcplx;

    /** 进口编码 */
    @Excel(name = "进口编码")
    private String jkbm;

    /** 出口编码 */
    @Excel(name = "出口编码")
    private String ckbm;

    /** 过车时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "过车时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date lvgcsj;

    /** 抓拍照片 */
    @Excel(name = "抓拍照片")
    private byte[] zpzpb;

    /** 申报系统 */
    @Excel(name = "申报系统")
    private String sbxt;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public Long getId() 
    {
        return id;
    }
    public void setLvtccbh(String lvtccbh) 
    {
        this.lvtccbh = lvtccbh;
    }

    public String getLvtccbh() 
    {
        return lvtccbh;
    }
    public void setDzewmb(String dzewmb) 
    {
        this.dzewmb = dzewmb;
    }

    public String getDzewmb() 
    {
        return dzewmb;
    }
    public void setLvcphm(String lvcphm) 
    {
        this.lvcphm = lvcphm;
    }

    public String getLvcphm() 
    {
        return lvcphm;
    }
    public void setLvcplx(String lvcplx) 
    {
        this.lvcplx = lvcplx;
    }

    public String getLvcplx() 
    {
        return lvcplx;
    }
    public void setJkbm(String jkbm) 
    {
        this.jkbm = jkbm;
    }

    public String getJkbm() 
    {
        return jkbm;
    }
    public void setCkbm(String ckbm) 
    {
        this.ckbm = ckbm;
    }

    public String getCkbm() 
    {
        return ckbm;
    }
    public void setLvgcsj(Date lvgcsj) 
    {
        this.lvgcsj = lvgcsj;
    }

    public Date getLvgcsj() 
    {
        return lvgcsj;
    }
    public void setZpzpb(byte[] zpzpb) 
    {
        this.zpzpb = zpzpb;
    }

    public byte[] getZpzpb() 
    {
        return zpzpb;
    }
    public void setSbxt(String sbxt) 
    {
        this.sbxt = sbxt;
    }

    public String getSbxt() 
    {
        return sbxt;
    }

    
    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("lvtccbh", getLvtccbh())
            .append("dzewmb", getDzewmb())
            .append("lvcphm", getLvcphm())
            .append("lvcplx", getLvcplx())
            .append("jkbm", getJkbm())
            .append("ckbm", getCkbm())
            .append("lvgcsj", getLvgcsj())
            .append("zpzpb", getZpzpb())
            .append("sbxt", getSbxt())
            .toString();
    }
    
    

	public Tcxxbab() {
		super();
	}

	public Tcxxbab(Long id, String buildingId, String lvtccbh, String dzewmb, String lvcphm, String lvcplx, String jkbm,
			String ckbm, Date lvgcsj, byte[] zpzpb, String sbxt) {
		super();
		this.id = id;
		this.buildingId = buildingId;
		this.lvtccbh = lvtccbh;
		this.dzewmb = dzewmb;
		this.lvcphm = lvcphm;
		this.lvcplx = lvcplx;
		this.jkbm = jkbm;
		this.ckbm = ckbm;
		this.lvgcsj = lvgcsj;
		this.zpzpb = zpzpb;
		this.sbxt = sbxt;
	}
    
    
}

package com.ruoyi.system.service.impl;

import java.io.IOException;
import java.util.Base64;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.RycrbabMapper;
import com.ruoyi.common.utils.ImageHandleUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.sign.AESUtil;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.domain.Rycrbab;
import com.ruoyi.system.domain.Xqxxbab;
import com.ruoyi.system.service.IJzryrzxxbabService;
import com.ruoyi.system.service.IPcsxxbabService;
import com.ruoyi.system.service.IRycrbabService;
import com.ruoyi.system.service.IXqxxbabService;

/**
 * 出入人员管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-09-29
 */
@Service
public class RycrbabServiceImpl implements IRycrbabService {
	private ImageHandleUtils imageHandleUtils = SpringUtils.getBean(ImageHandleUtils.class);
	
	private IJzryrzxxbabService jzryrzxxbabService = SpringUtils.getBean(IJzryrzxxbabService.class);
	
	private IXqxxbabService xqxxbabService = SpringUtils.getBean(IXqxxbabService.class);
	
	private IPcsxxbabService pcsxxbabService = SpringUtils.getBean(IPcsxxbabService.class);
	
	@Autowired
	private RycrbabMapper rycrbabMapper;
	
	

	
	/**
	 * 查询出入人员管理
	 * 
	 * @param id 出入人员管理ID
	 * @return 出入人员管理
	 */
	@Override
	public Rycrbab selectRycrbabById(Long id) {
		
		Rycrbab rycrbab=rycrbabMapper.selectRycrbabById(id);
		rycrbab.setGmsfhm(AESUtil.decrypt(rycrbab.getGmsfhm()));
		return rycrbab;
	}

	/**
	 * 查询出入人员管理列表
	 * 
	 * @param rycrbab 出入人员管理
	 * @return 出入人员管理
	 */
	@Override
	public List<Rycrbab> selectRycrbabList(Rycrbab rycrbab) {
		
		/**
		 * 对前端的查询参数进行加密后才能与数据库相匹配
		 */
		rycrbab.setGmsfhm(AESUtil.encrypt(rycrbab.getGmsfhm()));
		
		List<Rycrbab> rycrbabList = rycrbabMapper.selectRycrbabList(rycrbab);
		
		/**
		 * 获取数据后解密发送给前端
		 */
		for (int i = 0; i < rycrbabList.size(); i++) {
			
			
			rycrbabList.get(i).setGmsfhm(AESUtil.decrypt(rycrbabList.get(i).getGmsfhm()));
			rycrbabList.get(i).setZpzpa(AESUtil.decrypt(rycrbabList.get(i).getZpzpa()));
			rycrbabList.get(i).setZpzpb(AESUtil.decrypt(rycrbabList.get(i).getZpzpb()));
		}
		rycrbab.setGmsfhm(AESUtil.decrypt(rycrbab.getGmsfhm()));
		return rycrbabList;
	}

	/**
	 * 新增出入人员管理
	 * 
	 * @param rycrbab 出入人员管理
	 * @return 结果
	 */
	@Override
	public int insertRycrbab(Rycrbab rycrbab) {
       
		rycrbab.setGmsfhm(AESUtil.encrypt(rycrbab.getGmsfhm()));                //入库前对身份证加密处理
		rycrbab.setXm(jzryrzxxbabService.getXmByGmsfzh(rycrbab.getGmsfhm()));   //根据加密后的身份证去数据库查询姓名信息
		
		/**
		 * Base64加密后的图片 需要解密处理一下
		 */
		Base64.Decoder decoder = Base64.getDecoder();
		if(rycrbab.getZpzpa()==null)rycrbab.setZpzpa("");
		if(rycrbab.getZpzpb()==null)rycrbab.setZpzpb("");
		byte[] a_zpzpa = StringUtils.readByteArrayFromBase64(rycrbab.getZpzpa());
		byte[] b_zpzpb = StringUtils.readByteArrayFromBase64(rycrbab.getZpzpb());
		String path_za = "";
		String path_zb = "";
		try {
			//对返回的图片路径进行加密
			path_za=(String)imageHandleUtils.imgUpload(a_zpzpa, rycrbab.getSsxqbm(), "rycrbabAPhotos").get("imgUrl");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		rycrbab.setZpzpa(AESUtil.encrypt(path_za));
		try {
			//对返回的图片路径进行加密
			path_zb =(String) imageHandleUtils.imgUpload(b_zpzpb, rycrbab.getSsxqbm(), "rycrbabBPhotos").get("imgUrl");
		} catch (IOException e) {
			e.printStackTrace();
		}
		rycrbab.setZpzpb(AESUtil.encrypt(path_zb));
		return rycrbabMapper.insertRycrbab(rycrbab);
	}

	/**
	 * 修改出入人员管理
	 * 
	 * @param rycrbab 出入人员管理
	 * @return 结果
	 */
	@Override
	public int updateRycrbab(Rycrbab rycrbab) {
		return rycrbabMapper.updateRycrbab(rycrbab);
	}

	/**
	 * 批量删除出入人员管理
	 * 
	 * @param ids 需要删除的出入人员管理ID
	 * @return 结果
	 */
	@Override
	public int deleteRycrbabByIds(Long[] ids) {
		return rycrbabMapper.deleteRycrbabByIds(ids);
	}

	/**
	 * 删除出入人员管理信息
	 * 
	 * @param id 出入人员管理ID
	 * @return 结果
	 */
	@Override
	public int deleteRycrbabById(Long id) {
		return rycrbabMapper.deleteRycrbabById(id);
	}

	// 通过小区管理人员的id获取本小区的出入人员信息
	@Override
	public List<Rycrbab> selectRycrbabListByXqAdminId(Long userId, Rycrbab rycrbab) {
		// TODO Auto-generated method stub
		/**
		 * 对前端的查询参数进行加密后才能与数据库相匹配
		 */
		rycrbab.setGmsfhm(AESUtil.encrypt(rycrbab.getGmsfhm()));
		
		List<Rycrbab> rycrbabList = rycrbabMapper.selectRycrbabListByXqAdminId(userId, rycrbab);
		
		/**
		 * 获取数据后解密发送给前端
		 */
		for (int i = 0; i < rycrbabList.size(); i++) {
			String ssxqbm=rycrbabList.get(i).getSsxqbm();  //获取小区编码
			rycrbabList.get(i).setXqmc(xqxxbabService.selectXqxxbabBySsxqbm(ssxqbm).getXqmc());         //根据小区编码获取小区名称
			String pcsbm=ssxqbm.substring(0,9)+"000";
			rycrbabList.get(i).setPcsmc(pcsxxbabService.selectPcsmcByPcsbm(pcsbm)); 
			rycrbabList.get(i).setGmsfhm(AESUtil.decrypt(rycrbabList.get(i).getGmsfhm()));
			rycrbabList.get(i).setZpzpa(AESUtil.decrypt(rycrbabList.get(i).getZpzpa()));
			rycrbabList.get(i).setZpzpb(AESUtil.decrypt(rycrbabList.get(i).getZpzpb()));
		}
		rycrbab.setGmsfhm(AESUtil.decrypt(rycrbab.getGmsfhm()));
		return rycrbabList;
	}

	@Override
	public List<Rycrbab> selectRycrbabListByDeptId(Long deptId,Long deptIdOfTree, Rycrbab rycrbab) {
		// TODO Auto-generated method stub
		
		/**
		 * 对前端的查询参数进行加密后才能与数据库相匹配
		 */
		rycrbab.setGmsfhm(AESUtil.encrypt(rycrbab.getGmsfhm()));
		
		List<Rycrbab> rycrbabList=rycrbabMapper.selectRycrbabListByDeptId(deptId, deptIdOfTree, rycrbab);
		
		/**
		 * 获取数据后解密发送给前端
		 */
		for (int i = 0; i < rycrbabList.size(); i++) {
			String ssxqbm=rycrbabList.get(i).getSsxqbm();  //获取小区编码
			rycrbabList.get(i).setXqmc(xqxxbabService.selectXqxxbabBySsxqbm(ssxqbm).getXqmc());         //根据小区编码获取小区名称
		
			String pcsbm=ssxqbm.substring(0,9)+"000";
			rycrbabList.get(i).setPcsmc(pcsxxbabService.selectPcsmcByPcsbm(pcsbm)); 
			
			rycrbabList.get(i).setGmsfhm(AESUtil.decrypt(rycrbabList.get(i).getGmsfhm()));
			rycrbabList.get(i).setZpzpa(AESUtil.decrypt(rycrbabList.get(i).getZpzpa()));
			rycrbabList.get(i).setZpzpb(AESUtil.decrypt(rycrbabList.get(i).getZpzpb()));
		}
		rycrbab.setGmsfhm(AESUtil.decrypt(rycrbab.getGmsfhm()));
		return rycrbabList;
		
	
	}

	@Override
	public List<Rycrbab> selectTodayRycrbabListByXqAdminId(Long userId, Rycrbab rycrbab) {
		// TODO Auto-generated method stub
		/**
		 * 对前端的查询参数进行加密后才能与数据库相匹配
		 */
		rycrbab.setGmsfhm(AESUtil.encrypt(rycrbab.getGmsfhm()));
		
		List<Rycrbab> rycrbabList= rycrbabMapper.selectTodayRycrbabListByXqAdminId(userId, rycrbab);
		
		/**
		 * 获取数据后解密发送给前端
		 */
		for (int i = 0; i < rycrbabList.size(); i++) {
			
			rycrbabList.get(i).setGmsfhm(AESUtil.decrypt(rycrbabList.get(i).getGmsfhm()));
			rycrbabList.get(i).setZpzpa(AESUtil.decrypt(rycrbabList.get(i).getZpzpa()));
			rycrbabList.get(i).setZpzpb(AESUtil.decrypt(rycrbabList.get(i).getZpzpb()));
		}
		rycrbab.setGmsfhm(AESUtil.decrypt(rycrbab.getGmsfhm()));
		return rycrbabList;
		
	}

	@Override
	public List<Rycrbab> selectTodayRycrbabListByDeptId(Long deptId, Long deptIdOfTree, Rycrbab rycrbab) {
		// TODO Auto-generated method stub
		/**
		 * 对前端的查询参数进行加密后才能与数据库相匹配
		 */
		rycrbab.setGmsfhm(AESUtil.encrypt(rycrbab.getGmsfhm()));
		
		List<Rycrbab> rycrbabList= rycrbabMapper.selectTodayRycrbabListByDeptId(deptId, deptIdOfTree, rycrbab);
		
		/**
		 * 获取数据后解密发送给前端
		 */
		for (int i = 0; i < rycrbabList.size(); i++) {
			
			rycrbabList.get(i).setGmsfhm(AESUtil.decrypt(rycrbabList.get(i).getGmsfhm()));
			rycrbabList.get(i).setZpzpa(AESUtil.decrypt(rycrbabList.get(i).getZpzpa()));
			rycrbabList.get(i).setZpzpb(AESUtil.decrypt(rycrbabList.get(i).getZpzpb()));
		}
		rycrbab.setGmsfhm(AESUtil.decrypt(rycrbab.getGmsfhm()));
		return rycrbabList;
		
	}

	@Override
	public List<Rycrbab> selectAllRycrbabListByXqAdminId(Long userId, Rycrbab rycrbab) {
		// TODO Auto-generated method stub
		List<Rycrbab> rycrbabList= rycrbabMapper.selectAllRycrbabListByXqAdminId(userId, rycrbab);
		
		return rycrbabList;
	}

	@Override
	public List<Rycrbab> selectAllRycrbabListByDeptId(Long deptId, Long deptIdOfTree, Rycrbab rycrbab) {
		// TODO Auto-generated method stub
		List<Rycrbab> rycrbabList= rycrbabMapper.selectAllRycrbabListByDeptId(deptId, deptIdOfTree, rycrbab);
		
		return rycrbabList;
	}

	@Override
	public long countAllRycrbabByDeptId(Long deptId, Long deptIdOfTree, Rycrbab rycrbab) {
		// TODO Auto-generated method stub
		
		return rycrbabMapper.countAllRycrbabByDeptId( deptId,  deptIdOfTree,  rycrbab);
	}

	@Override
	public long countAllRycrbabByXqAdminId(Long userId, Rycrbab rycrbab) {
		// TODO Auto-generated method stub
		
		return rycrbabMapper.countAllRycrbabByXqAdminId(userId,  rycrbab);
	}

	@Override
	public long countTodayRycrbabByDeptId(Long deptId, Long deptIdOfTree, Rycrbab rycrbab) {
		// TODO Auto-generated method stub
		
		return rycrbabMapper.countTodayRycrbabByDeptId( deptId,  deptIdOfTree,  rycrbab);
	}

	@Override
	public long countTodayRycrbabByXqAdminId(Long userId, Rycrbab rycrbab) {
		// TODO Auto-generated method stub
		
		
		return rycrbabMapper.countTodayRycrbabByXqAdminId( userId,  rycrbab);

	}

}

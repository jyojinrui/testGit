package com.ruoyi.system.listener;

import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ServletContextAware;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.system.domain.Jzryrzxxbab;
import com.ruoyi.system.domain.Message;
import com.ruoyi.system.domain.RecInfo;
import com.ruoyi.system.domain.RecMessage;
import com.ruoyi.system.domain.Rycrbab;
import com.ruoyi.system.domain.SnapOrRecordInfo;
import com.ruoyi.system.domain.Xqxxbab;
import com.ruoyi.system.service.IJzryrzxxbabService;
import com.ruoyi.system.service.IRycrbabService;
import com.ruoyi.system.service.IXqxxbabService;

@Component
public class MqttListener implements ServletContextAware {
	
	@Autowired
	private IXqxxbabService xqxxbabService;
	
	@Autowired
	private IRycrbabService rycrbabService;
	
	@Autowired
	private IJzryrzxxbabService jzryrzxxbabService;
	
	@Autowired
	private MqttClient mqttClient;
	
    protected final Logger logger = LoggerFactory.getLogger(MqttListener.class);
    
    @Value("${server.mqtt-switch}")
    private boolean mqttSwitch;
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		if(!mqttSwitch){
			return;
		}
		// TODO Auto-generated method stub
		List<Xqxxbab> xqxxbabList=xqxxbabService.selectXqxxbabList(new Xqxxbab());
		for(Xqxxbab xqxxbab:xqxxbabList){
			String buildingId = xqxxbab.getBuildingId();

			try {
				mqttClient.subscribe(Constants.MQTT_TOPIC_PREFIX + buildingId + Constants.MQTT_TOPIC_SNAP_SUFFIX);
				mqttClient.subscribe(Constants.MQTT_TOPIC_PREFIX + buildingId + Constants.MQTT_TOPIC_REC_SUFFIX);
				mqttClient.subscribe(Constants.MQTT_TOPIC_PREFIX + buildingId + Constants.MQTT_TOPIC_ACK_SUFFIX);

				Gson gson = new GsonBuilder().create();
				mqttClient.setCallback(new MqttCallback() {
					@Override
					public void connectionLost(Throwable throwable) {
						throwable.printStackTrace();
						System.out.println("connectionLost");
					}

					@Override
					public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
						String publishTopic = topic.substring(0, topic.lastIndexOf("/"));
						topic = topic.substring(topic.lastIndexOf("/"));
						switch (topic) {
						case "/Rec":
							RecMessage recMessage = gson.fromJson(mqttMessage.toString(), RecMessage.class);
							// 断网续传模式下，需要回复收到的每条认证记录
							RecInfo recInfo = recMessage.getInfo();
							int recordID = recInfo.getRecordID();
							Message recAckMessage = new Message(String.valueOf(System.currentTimeMillis()), "PushAck",
									new SnapOrRecordInfo("2", String.valueOf(recordID)));
							String jsonString = gson.toJson(recAckMessage);
							MqttMessage message = new MqttMessage();
							message.setQos(1);
							message.setRetained(false);
							message.setPayload(jsonString.getBytes());
							mqttClient.publish(publishTopic, message);
							

							//保存抓拍记录
							String sxtbm = recInfo.getFacesluiceName();
							String buildingId=sxtbm.substring(0,21);
							String mjbh=sxtbm.substring(0,25);
							String gmsfhm=recInfo.getCustomId();
							String crlb=recInfo.getDirection();
							crlb="entr".equals(crlb)?"1":"2";
							String zpzpb=recInfo.getPic();
							String sbxt="西安景煜网络科技有限公司";
							Date zpsj=recInfo.getTime();

							Rycrbab rycrbab=new Rycrbab();
							rycrbab.setSsxqbm(buildingId);
							rycrbab.setMjbh(mjbh);
							rycrbab.setGmsfhm(gmsfhm);
							rycrbab.setCrlb(crlb);
							rycrbab.setZpzpa("");
							rycrbab.setZpzpb(zpzpb);
							rycrbab.setSbxt(sbxt);
							rycrbab.setZpsj(zpsj);
							rycrbabService.insertRycrbab(rycrbab);
							break;
						case "/Snap":
							RecMessage snapMessage = gson.fromJson(mqttMessage.toString(), RecMessage.class);
							System.out.println(snapMessage);
							// 断网续传模式下，需要回复收到的每条认证记录
							RecInfo snapInfo = snapMessage.getInfo();
							int snapID = snapInfo.getSnapID();
							Message snapAckMessage = new Message(String.valueOf(System.currentTimeMillis()), "PushAck",
									new SnapOrRecordInfo("1", String.valueOf(snapID)));
							jsonString = gson.toJson(snapAckMessage);
							message = new MqttMessage();
							message.setQos(1);
							message.setRetained(false);
							message.setPayload(jsonString.getBytes());
							mqttClient.publish(publishTopic, message);

							//保存抓拍记录
							sxtbm = snapInfo.getFacesluiceName();
							buildingId=sxtbm.substring(0,21);
							mjbh=sxtbm.substring(0,25);
							crlb=snapInfo.getDirection();
							crlb="entr".equals(crlb)?"1":"2";
							zpzpb=snapInfo.getPic();
							sbxt="西安景煜网络科技有限公司";
							zpsj=snapInfo.getTime();

							rycrbab=new Rycrbab();
							rycrbab.setSsxqbm(buildingId);
							rycrbab.setMjbh(mjbh);
							rycrbab.setCrlb(crlb);
							rycrbab.setZpzpa("");
							rycrbab.setZpzpb(zpzpb);
							rycrbab.setSbxt(sbxt);
							rycrbab.setZpsj(zpsj);
							rycrbabService.insertRycrbab(rycrbab);
							break;
						case "/Ack":
							String messageStr = mqttMessage.toString();
							logger.info(messageStr);
							JsonObject jsonObject = gson.fromJson(messageStr, JsonObject.class);
							String operator = jsonObject.get("operator").getAsString();
							if("EditPerson-Ack".equals(operator)){
								JsonObject infoObject = jsonObject.getAsJsonObject("info");
								gmsfhm = infoObject.get("customId").getAsString();
								String result = infoObject.get("result").getAsString();
								if("fail".equals(result)){
									String downResult = infoObject.get("detail").getAsString();
									Jzryrzxxbab jzryrzxxbab = new Jzryrzxxbab();
									jzryrzxxbab.setGmsfhm(gmsfhm);
									List<Jzryrzxxbab> jzryrzxxbabs = jzryrzxxbabService.selectJzryrzxxbabList(jzryrzxxbab);
									if(jzryrzxxbabs != null && jzryrzxxbabs.size() > 0){
										for(Jzryrzxxbab jzryrzxxbab2:jzryrzxxbabs){
											jzryrzxxbab2.setDzewmbm(downResult);
											jzryrzxxbabService.updateJzryrzxxbab(jzryrzxxbab2);
										}
									}
								}

							}
							break;
						default:
							break;
						}
					}

					@Override
					public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

					}
				});
			} catch (MqttException e) {
				// TODO Auto-generated catch block
				logger.error("订阅各小区MQTT话题出现异常", e);
			}
		}
	}

}


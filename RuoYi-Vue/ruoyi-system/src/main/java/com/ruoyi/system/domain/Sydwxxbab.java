package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 实有单位信息对象 sydwxxbab
 * 
 * @author Carry
 * @date 2020-10-27
 */
public class Sydwxxbab extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 单位编码 */
    @Excel(name = "单位编码")
    private String dwbm;

    /** 单位名称 */
    @Excel(name = "单位名称")
    private String dwmc;

    /** 单位地址编码 */
    @Excel(name = "单位地址编码")
    private String dwdzbm;

    /** 单位类别 */
    @Excel(name = "单位类别")
    private String dzlb;

    /** 行业单位类型 */
    @Excel(name = "行业单位类型")
    private String hzdwlx;

    /** 单位性质 */
    @Excel(name = "单位性质")
    private String dwxz;

    /** 工商执照号码 */
    @Excel(name = "工商执照号码")
    private String gszzhm;

    /** 组织机构代码 */
    @Excel(name = "组织机构代码")
    private String zzjgdm;

    /** 单位联系电话 */
    @Excel(name = "单位联系电话")
    private String dwlxdh;

    /** 法定代表人_姓名 */
    @Excel(name = "法定代表人_姓名")
    private String fddbwXm;

    /** 法定代表人_公民身份号码 */
    @Excel(name = "法定代表人_公民身份号码")
    private String fddbwGmsfhm;

    /** 法定代表人_证件种类 */
    @Excel(name = "法定代表人_证件种类")
    private String fddbwZjzl;

    /** 法定代表人_证件号码 */
    @Excel(name = "法定代表人_证件号码")
    private String fddbwZjhm;

    /** 法定代表人_外文姓 */
    @Excel(name = "法定代表人_外文姓")
    private String fddbwWwx;

    /** 法定代表人_外文名 */
    @Excel(name = "法定代表人_外文名")
    private String fddbwWwm;

    /** 法定代表人_联系电话 */
    @Excel(name = "法定代表人_联系电话")
    private String fddbwLxdh;

    /** 保卫负责人_姓名 */
    @Excel(name = "保卫负责人_姓名")
    private String bwfzrXm;

    /** 保卫负责人_公民身份号码 */
    @Excel(name = "保卫负责人_公民身份号码")
    private String bwfzrGmsfhm;

    /** 保卫负责人_证件类型 */
    @Excel(name = "保卫负责人_证件类型")
    private String bwfzrZjlx;

    /** 保卫负责人_联系电话 */
    @Excel(name = "保卫负责人_联系电话")
    private String bwfzrLxdh;

    /** 备注 */
    @Excel(name = "备注")
    private String bz;

    /** 注销日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "注销日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date zxrq;

    /** 登记公司名称（公安网注册获取） */
    @Excel(name = "登记公司名称", readConverterExp = "公=安网注册获取")
    private String djdwgajgjgmc;

    /** 登记公司编码（公安网注册获取） */
    @Excel(name = "登记公司编码", readConverterExp = "公=安网注册获取")
    private String djdwgajgjgdm;

    /** 登记时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "登记时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date djsj;

    /** 登记人 */
    @Excel(name = "登记人")
    private String djr;

    /** 对接小区id */
    @Excel(name = "对接小区id")
    private String computerid;

    /** 地址归属单位 */
    @Excel(name = "地址归属单位")
    private String dzgsdw;

    /** 统一社会信用代码 */
    @Excel(name = "统一社会信用代码")
    private String shtydm;

    /** 注册地址 */
    @Excel(name = "注册地址")
    private String zcdz;

    /** 一标三实地址ID */
    @Excel(name = "一标三实地址ID")
    private String dzpid;

    /** 注册日期 */
    @Excel(name = "注册日期")
    private String zcrq;

    /** 公司EMAIL */
    @Excel(name = "公司EMAIL")
    private String dwemail;

    /** 注销时间 */
    @Excel(name = "注销时间")
    private String zxsj;

    /** 操作方式 */
    @Excel(name = "操作方式")
    private String procmode;

    /** 小区申报编码 */
    @Excel(name = "小区申报编码")
    private String ssxqbm;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setDwbm(String dwbm) 
    {
        this.dwbm = dwbm;
    }

    public String getDwbm() 
    {
        return dwbm;
    }
    public void setDwmc(String dwmc) 
    {
        this.dwmc = dwmc;
    }

    public String getDwmc() 
    {
        return dwmc;
    }
    public void setDwdzbm(String dwdzbm) 
    {
        this.dwdzbm = dwdzbm;
    }

    public String getDwdzbm() 
    {
        return dwdzbm;
    }
    public void setDzlb(String dzlb) 
    {
        this.dzlb = dzlb;
    }

    public String getDzlb() 
    {
        return dzlb;
    }
    public void setHzdwlx(String hzdwlx) 
    {
        this.hzdwlx = hzdwlx;
    }

    public String getHzdwlx() 
    {
        return hzdwlx;
    }
    public void setDwxz(String dwxz) 
    {
        this.dwxz = dwxz;
    }

    public String getDwxz() 
    {
        return dwxz;
    }
    public void setGszzhm(String gszzhm) 
    {
        this.gszzhm = gszzhm;
    }

    public String getGszzhm() 
    {
        return gszzhm;
    }
    public void setZzjgdm(String zzjgdm) 
    {
        this.zzjgdm = zzjgdm;
    }

    public String getZzjgdm() 
    {
        return zzjgdm;
    }
    public void setDwlxdh(String dwlxdh) 
    {
        this.dwlxdh = dwlxdh;
    }

    public String getDwlxdh() 
    {
        return dwlxdh;
    }
    public void setFddbwXm(String fddbwXm) 
    {
        this.fddbwXm = fddbwXm;
    }

    public String getFddbwXm() 
    {
        return fddbwXm;
    }
    public void setFddbwGmsfhm(String fddbwGmsfhm) 
    {
        this.fddbwGmsfhm = fddbwGmsfhm;
    }

    public String getFddbwGmsfhm() 
    {
        return fddbwGmsfhm;
    }
    public void setFddbwZjzl(String fddbwZjzl) 
    {
        this.fddbwZjzl = fddbwZjzl;
    }

    public String getFddbwZjzl() 
    {
        return fddbwZjzl;
    }
    public void setFddbwZjhm(String fddbwZjhm) 
    {
        this.fddbwZjhm = fddbwZjhm;
    }

    public String getFddbwZjhm() 
    {
        return fddbwZjhm;
    }
    public void setFddbwWwx(String fddbwWwx) 
    {
        this.fddbwWwx = fddbwWwx;
    }

    public String getFddbwWwx() 
    {
        return fddbwWwx;
    }
    public void setFddbwWwm(String fddbwWwm) 
    {
        this.fddbwWwm = fddbwWwm;
    }

    public String getFddbwWwm() 
    {
        return fddbwWwm;
    }
    public void setFddbwLxdh(String fddbwLxdh) 
    {
        this.fddbwLxdh = fddbwLxdh;
    }

    public String getFddbwLxdh() 
    {
        return fddbwLxdh;
    }
    public void setBwfzrXm(String bwfzrXm) 
    {
        this.bwfzrXm = bwfzrXm;
    }

    public String getBwfzrXm() 
    {
        return bwfzrXm;
    }
    public void setBwfzrGmsfhm(String bwfzrGmsfhm) 
    {
        this.bwfzrGmsfhm = bwfzrGmsfhm;
    }

    public String getBwfzrGmsfhm() 
    {
        return bwfzrGmsfhm;
    }
    public void setBwfzrZjlx(String bwfzrZjlx) 
    {
        this.bwfzrZjlx = bwfzrZjlx;
    }

    public String getBwfzrZjlx() 
    {
        return bwfzrZjlx;
    }
    public void setBwfzrLxdh(String bwfzrLxdh) 
    {
        this.bwfzrLxdh = bwfzrLxdh;
    }

    public String getBwfzrLxdh() 
    {
        return bwfzrLxdh;
    }
    public void setBz(String bz) 
    {
        this.bz = bz;
    }

    public String getBz() 
    {
        return bz;
    }
    public void setZxrq(Date zxrq) 
    {
        this.zxrq = zxrq;
    }

    public Date getZxrq() 
    {
        return zxrq;
    }
    public void setDjdwgajgjgmc(String djdwgajgjgmc) 
    {
        this.djdwgajgjgmc = djdwgajgjgmc;
    }

    public String getDjdwgajgjgmc() 
    {
        return djdwgajgjgmc;
    }
    public void setDjdwgajgjgdm(String djdwgajgjgdm) 
    {
        this.djdwgajgjgdm = djdwgajgjgdm;
    }

    public String getDjdwgajgjgdm() 
    {
        return djdwgajgjgdm;
    }
    public void setDjsj(Date djsj) 
    {
        this.djsj = djsj;
    }

    public Date getDjsj() 
    {
        return djsj;
    }
    public void setDjr(String djr) 
    {
        this.djr = djr;
    }

    public String getDjr() 
    {
        return djr;
    }
    public void setComputerid(String computerid) 
    {
        this.computerid = computerid;
    }

    public String getComputerid() 
    {
        return computerid;
    }
    public void setDzgsdw(String dzgsdw) 
    {
        this.dzgsdw = dzgsdw;
    }

    public String getDzgsdw() 
    {
        return dzgsdw;
    }
    public void setShtydm(String shtydm) 
    {
        this.shtydm = shtydm;
    }

    public String getShtydm() 
    {
        return shtydm;
    }
    public void setZcdz(String zcdz) 
    {
        this.zcdz = zcdz;
    }

    public String getZcdz() 
    {
        return zcdz;
    }
    public void setDzpid(String dzpid) 
    {
        this.dzpid = dzpid;
    }

    public String getDzpid() 
    {
        return dzpid;
    }
    public void setZcrq(String zcrq) 
    {
        this.zcrq = zcrq;
    }

    public String getZcrq() 
    {
        return zcrq;
    }
    public void setDwemail(String dwemail) 
    {
        this.dwemail = dwemail;
    }

    public String getDwemail() 
    {
        return dwemail;
    }
    public void setZxsj(String zxsj) 
    {
        this.zxsj = zxsj;
    }

    public String getZxsj() 
    {
        return zxsj;
    }
    public void setProcmode(String procmode) 
    {
        this.procmode = procmode;
    }

    public String getProcmode() 
    {
        return procmode;
    }
    public void setSsxqbm(String ssxqbm) 
    {
        this.ssxqbm = ssxqbm;
    }

    public String getSsxqbm() 
    {
        return ssxqbm;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("dwbm", getDwbm())
            .append("dwmc", getDwmc())
            .append("dwdzbm", getDwdzbm())
            .append("dzlb", getDzlb())
            .append("hzdwlx", getHzdwlx())
            .append("dwxz", getDwxz())
            .append("gszzhm", getGszzhm())
            .append("zzjgdm", getZzjgdm())
            .append("dwlxdh", getDwlxdh())
            .append("fddbwXm", getFddbwXm())
            .append("fddbwGmsfhm", getFddbwGmsfhm())
            .append("fddbwZjzl", getFddbwZjzl())
            .append("fddbwZjhm", getFddbwZjhm())
            .append("fddbwWwx", getFddbwWwx())
            .append("fddbwWwm", getFddbwWwm())
            .append("fddbwLxdh", getFddbwLxdh())
            .append("bwfzrXm", getBwfzrXm())
            .append("bwfzrGmsfhm", getBwfzrGmsfhm())
            .append("bwfzrZjlx", getBwfzrZjlx())
            .append("bwfzrLxdh", getBwfzrLxdh())
            .append("bz", getBz())
            .append("zxrq", getZxrq())
            .append("djdwgajgjgmc", getDjdwgajgjgmc())
            .append("djdwgajgjgdm", getDjdwgajgjgdm())
            .append("djsj", getDjsj())
            .append("djr", getDjr())
            .append("computerid", getComputerid())
            .append("dzgsdw", getDzgsdw())
            .append("shtydm", getShtydm())
            .append("zcdz", getZcdz())
            .append("dzpid", getDzpid())
            .append("zcrq", getZcrq())
            .append("dwemail", getDwemail())
            .append("zxsj", getZxsj())
            .append("procmode", getProcmode())
            .append("ssxqbm", getSsxqbm())
            .toString();
    }
}

package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 访客信息管理对象 fkbab
 * 
 * @author Carry
 * @date 2020-09-29
 */
public class Fkbab extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 申报流水号 */
    @Excel(name = "申报流水号")
    private String sbxxlsh;

    /** 公民身份号码 */
    @Excel(name = "公民身份号码")
    private String gmsfhm;
    /** 小区编码 */
    @Excel(name = "小区编码")
    private String ssxqbm;
    /** 姓名 */
    @Excel(name = "姓名")
    private String xm;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String lxdh;

    /** 照片 */
    @Excel(name = "照片")
    private String zp;

    /** 登记人姓名 */
    @Excel(name = "登记人姓名")
    private String djrxm;

    /** 与登记人关系 */
    @Excel(name = "与登记人关系")
    private String ydjrgx;

    /** 登记时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "登记时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date djsj;

    /** 有效期限 */
    @Excel(name = "有效期限")
    private String yxqx;

    /** 申报系统 */
    @Excel(name = "申报系统")
    private String sbxt;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSbxxlsh(String sbxxlsh) 
    {
        this.sbxxlsh = sbxxlsh;
    }

    public String getSbxxlsh() 
    {
        return sbxxlsh;
    }
    public void setGmsfhm(String gmsfhm) 
    {
        this.gmsfhm = gmsfhm;
    }

    public String getGmsfhm() 
    {
        return gmsfhm;
    }
    public void setXm(String xm) 
    {
        this.xm = xm;
    }

    public String getXm() 
    {
        return xm;
    }
    public void setLxdh(String lxdh) 
    {
        this.lxdh = lxdh;
    }

    public String getLxdh() 
    {
        return lxdh;
    }
    public void setZp(String zp) 
    {
        this.zp = zp;
    }

    public String getZp() 
    {
        return zp;
    }
    public void setDjrxm(String djrxm) 
    {
        this.djrxm = djrxm;
    }

    public String getDjrxm() 
    {
        return djrxm;
    }
    public void setYdjrgx(String ydjrgx) 
    {
        this.ydjrgx = ydjrgx;
    }

    public String getYdjrgx() 
    {
        return ydjrgx;
    }
    public void setDjsj(Date djsj) 
    {
        this.djsj = djsj;
    }

    public Date getDjsj() 
    {
        return djsj;
    }
    public void setYxqx(String yxqx) 
    {
        this.yxqx = yxqx;
    }

    public String getYxqx() 
    {
        return yxqx;
    }
    public void setSbxt(String sbxt) 
    {
        this.sbxt = sbxt;
    }

    public String getSbxt() 
    {
        return sbxt;
    }

    public String getSsxqbm() {
		return ssxqbm;
	}

	public void setSsxqbm(String ssxqbm) {
		this.ssxqbm = ssxqbm;
	}

	public Fkbab() {
		super();
	}

	public Fkbab(Long id, String sbxxlsh, String gmsfhm, String ssxqbm, String xm, String lxdh, String zp, String djrxm,
			String ydjrgx, Date djsj, String yxqx, String sbxt) {
		super();
		this.id = id;
		this.sbxxlsh = sbxxlsh;
		this.gmsfhm = gmsfhm;
		this.ssxqbm = ssxqbm;
		this.xm = xm;
		this.lxdh = lxdh;
		this.zp = zp;
		this.djrxm = djrxm;
		this.ydjrgx = ydjrgx;
		this.djsj = djsj;
		this.yxqx = yxqx;
		this.sbxt = sbxt;
	}

	public Fkbab(Long id, String sbxxlsh, String gmsfhm,  String xm, String lxdh, String zp, String djrxm,
			String ydjrgx, Date djsj, String yxqx, String sbxt) {
		super();
		this.id = id;
		this.sbxxlsh = sbxxlsh;
		this.gmsfhm = gmsfhm;
		
		this.xm = xm;
		this.lxdh = lxdh;
		this.zp = zp;
		this.djrxm = djrxm;
		this.ydjrgx = ydjrgx;
		this.djsj = djsj;
		this.yxqx = yxqx;
		this.sbxt = sbxt;
	}
	
	@Override
	public String toString() {
		return "Fkbab [id=" + id + ", sbxxlsh=" + sbxxlsh + ", gmsfhm=" + gmsfhm + ", ssxqbm=" + ssxqbm + ", xm=" + xm
				+ ", lxdh=" + lxdh + ", zp=" + zp + ", djrxm=" + djrxm + ", ydjrgx=" + ydjrgx + ", djsj=" + djsj
				+ ", yxqx=" + yxqx + ", sbxt=" + sbxt + "]";
	}
}

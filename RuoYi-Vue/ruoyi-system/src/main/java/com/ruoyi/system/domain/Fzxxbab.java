package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 房主信息对象 fzxxbab
 * 
 * @author Carry
 * @date 2020-10-05
 */
public class Fzxxbab extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 申报流水号 */
    @Excel(name = "申报流水号")
    private String sbxxlsh;

    /** 地址二维码编码 */
    @Excel(name = "地址二维码编码")
    private String dzewmbm;

    /** 小区编码 */
    @Excel(name = "小区编码")
    private String buildingId;

    /** 房屋地址 */
    @Excel(name = "房屋地址")
    private String fwdz;

    /** 房主身份号码 */
    @Excel(name = "房主身份号码")
    private String fzgmsfhm;

    /** 房主姓名 */
    @Excel(name = "房主姓名")
    private String fzxm;

    /** 房主联系电话 */
    @Excel(name = "房主联系电话")
    private String fzlxdh;

    /** 委托人国籍 */
    @Excel(name = "委托人国籍")
    private String wtrgj;

    /** 委托人公民身份证号码 */
    @Excel(name = "委托人公民身份证号码")
    private String wtrgmsfhm;

    /** 委托人姓名 */
    @Excel(name = "委托人姓名")
    private String wtrxm;

    /** 委托人联系电话 */
    @Excel(name = "委托人联系电话")
    private String wtrlxdh;

    /** 备案日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "备案日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date barq;

    /** 房屋性质 */
    @Excel(name = "房屋性质")
    private String fwzxz;

    /** 房屋用途 */
    @Excel(name = "房屋用途")
    private String fwyt;

    /** 是否出租房 */
    @Excel(name = "是否出租房")
    private String sfczf;

    /** 房屋面积 */
    @Excel(name = "房屋面积")
    private Double fwmj;

    /** 房屋层数 */
    @Excel(name = "房屋层数")
    private Long fwcs;

    /** 登记单位 */
    @Excel(name = "登记单位")
    private String djdw;

    /** 登记人姓名 */
    @Excel(name = "登记人姓名")
    private String djrxm;

    /** 登记时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "登记时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date djsj;

    /** 房主国籍 */
    @Excel(name = "房主国籍")
    private String fzgj;

    /** 申报系统 */
    @Excel(name = "申报系统")
    private String sbxt;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSbxxlsh(String sbxxlsh) 
    {
        this.sbxxlsh = sbxxlsh;
    }

    public String getSbxxlsh() 
    {
        return sbxxlsh;
    }
    public void setDzewmbm(String dzewmbm) 
    {
        this.dzewmbm = dzewmbm;
    }

    public String getDzewmbm() 
    {
        return dzewmbm;
    }
    public void setBuildingId(String buildingId) 
    {
        this.buildingId = buildingId;
    }

    public String getBuildingId() 
    {
        return buildingId;
    }
    public void setFwdz(String fwdz) 
    {
        this.fwdz = fwdz;
    }

    public String getFwdz() 
    {
        return fwdz;
    }
    public void setFzgmsfhm(String fzgmsfhm) 
    {
        this.fzgmsfhm = fzgmsfhm;
    }

    public String getFzgmsfhm() 
    {
        return fzgmsfhm;
    }
    public void setFzxm(String fzxm) 
    {
        this.fzxm = fzxm;
    }

    public String getFzxm() 
    {
        return fzxm;
    }
    public void setFzlxdh(String fzlxdh) 
    {
        this.fzlxdh = fzlxdh;
    }

    public String getFzlxdh() 
    {
        return fzlxdh;
    }
    public void setWtrgj(String wtrgj) 
    {
        this.wtrgj = wtrgj;
    }

    public String getWtrgj() 
    {
        return wtrgj;
    }
    public void setWtrgmsfhm(String wtrgmsfhm) 
    {
        this.wtrgmsfhm = wtrgmsfhm;
    }

    public String getWtrgmsfhm() 
    {
        return wtrgmsfhm;
    }
    public void setWtrxm(String wtrxm) 
    {
        this.wtrxm = wtrxm;
    }

    public String getWtrxm() 
    {
        return wtrxm;
    }
    public void setWtrlxdh(String wtrlxdh) 
    {
        this.wtrlxdh = wtrlxdh;
    }

    public String getWtrlxdh() 
    {
        return wtrlxdh;
    }
    public void setBarq(Date barq) 
    {
        this.barq = barq;
    }

    public Date getBarq() 
    {
        return barq;
    }
    public void setFwzxz(String fwzxz) 
    {
        this.fwzxz = fwzxz;
    }

    public String getFwzxz() 
    {
        return fwzxz;
    }
    public void setFwyt(String fwyt) 
    {
        this.fwyt = fwyt;
    }

    public String getFwyt() 
    {
        return fwyt;
    }
    public void setSfczf(String sfczf) 
    {
        this.sfczf = sfczf;
    }

    public String getSfczf() 
    {
        return sfczf;
    }
    public void setFwmj(Double fwmj) 
    {
        this.fwmj = fwmj;
    }

    public Double getFwmj() 
    {
        return fwmj;
    }
    public void setFwcs(Long fwcs) 
    {
        this.fwcs = fwcs;
    }

    public Long getFwcs() 
    {
        return fwcs;
    }
    public void setDjdw(String djdw) 
    {
        this.djdw = djdw;
    }

    public String getDjdw() 
    {
        return djdw;
    }
    public void setDjrxm(String djrxm) 
    {
        this.djrxm = djrxm;
    }

    public String getDjrxm() 
    {
        return djrxm;
    }
    public void setDjsj(Date djsj) 
    {
        this.djsj = djsj;
    }

    public Date getDjsj() 
    {
        return djsj;
    }
    public void setFzgj(String fzgj) 
    {
        this.fzgj = fzgj;
    }

    public String getFzgj() 
    {
        return fzgj;
    }
    public void setSbxt(String sbxt) 
    {
        this.sbxt = sbxt;
    }

    public String getSbxt() 
    {
        return sbxt;
    }

    public Fzxxbab() {
		super();
	}

	public Fzxxbab(Long id, String sbxxlsh, String dzewmbm, String buildingId, String fwdz, String fzgmsfhm,
			String fzxm, String fzlxdh, String wtrgj, String wtrgmsfhm, String wtrxm, String wtrlxdh, Date barq,
			String fwzxz, String fwyt, String sfczf, Double fwmj, Long fwcs, String djdw, String djrxm, Date djsj,
			String fzgj, String sbxt) {
		super();
		this.id = id;
		this.sbxxlsh = sbxxlsh;
		this.dzewmbm = dzewmbm;
		this.buildingId = buildingId;
		this.fwdz = fwdz;
		this.fzgmsfhm = fzgmsfhm;
		this.fzxm = fzxm;
		this.fzlxdh = fzlxdh;
		this.wtrgj = wtrgj;
		this.wtrgmsfhm = wtrgmsfhm;
		this.wtrxm = wtrxm;
		this.wtrlxdh = wtrlxdh;
		this.barq = barq;
		this.fwzxz = fwzxz;
		this.fwyt = fwyt;
		this.sfczf = sfczf;
		this.fwmj = fwmj;
		this.fwcs = fwcs;
		this.djdw = djdw;
		this.djrxm = djrxm;
		this.djsj = djsj;
		this.fzgj = fzgj;
		this.sbxt = sbxt;
	}

	@Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sbxxlsh", getSbxxlsh())
            .append("dzewmbm", getDzewmbm())
            .append("buildingId", getBuildingId())
            .append("fwdz", getFwdz())
            .append("fzgmsfhm", getFzgmsfhm())
            .append("fzxm", getFzxm())
            .append("fzlxdh", getFzlxdh())
            .append("wtrgj", getWtrgj())
            .append("wtrgmsfhm", getWtrgmsfhm())
            .append("wtrxm", getWtrxm())
            .append("wtrlxdh", getWtrlxdh())
            .append("barq", getBarq())
            .append("fwzxz", getFwzxz())
            .append("fwyt", getFwyt())
            .append("sfczf", getSfczf())
            .append("fwmj", getFwmj())
            .append("fwcs", getFwcs())
            .append("djdw", getDjdw())
            .append("djrxm", getDjrxm())
            .append("djsj", getDjsj())
            .append("fzgj", getFzgj())
            .append("sbxt", getSbxt())
            .toString();
    }
}

package com.ruoyi.system.domain;

public class Message {
	private String messageId;
	private String operator;
	private Info info;
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public Info getInfo() {
		return info;
	}
	public void setInfo(Info info) {
		this.info = info;
	}
	public Message(String messageId, String operator, Info info) {
		super();
		this.messageId = messageId;
		this.operator = operator;
		this.info = info;
	}
	public Message() {
		super();
	}
	@Override
	public String toString() {
		return "Message [messageId=" + messageId + ", operator=" + operator + ", info=" + info + "]";
	}
	
	
}

package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysUserQgajAdmin;

/**
 * 区公安局管理员与公安局关系表Mapper接口
 * 
 * @author ruoyi
 * @date 2020-11-01
 */
public interface SysUserQgajAdminMapper 
{
    /**
     * 查询区公安局管理员与公安局关系表
     * 
     * @param id 区公安局管理员与公安局关系表ID
     * @return 区公安局管理员与公安局关系表
     */
    public SysUserQgajAdmin selectSysUserQgajAdminById(Long id);

    /**
     * 查询区公安局管理员与公安局关系表列表
     * 
     * @param sysUserQgajAdmin 区公安局管理员与公安局关系表
     * @return 区公安局管理员与公安局关系表集合
     */
    public List<SysUserQgajAdmin> selectSysUserQgajAdminList(SysUserQgajAdmin sysUserQgajAdmin);

    /**
     * 新增区公安局管理员与公安局关系表
     * 
     * @param sysUserQgajAdmin 区公安局管理员与公安局关系表
     * @return 结果
     */
    public int insertSysUserQgajAdmin(SysUserQgajAdmin sysUserQgajAdmin);

    /**
     * 修改区公安局管理员与公安局关系表
     * 
     * @param sysUserQgajAdmin 区公安局管理员与公安局关系表
     * @return 结果
     */
    public int updateSysUserQgajAdmin(SysUserQgajAdmin sysUserQgajAdmin);

    /**
     * 删除区公安局管理员与公安局关系表
     * 
     * @param id 区公安局管理员与公安局关系表ID
     * @return 结果
     */
    public int deleteSysUserQgajAdminById(Long id);

    /**
     * 批量删除区公安局管理员与公安局关系表
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysUserQgajAdminByIds(Long[] ids);
}

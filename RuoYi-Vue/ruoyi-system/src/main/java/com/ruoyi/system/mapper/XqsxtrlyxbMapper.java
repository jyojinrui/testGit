package com.ruoyi.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.system.domain.Xqsxtrlyxb;

/**
 * 人脸运行数据Mapper接口
 * 
 * @author Carry
 * @date 2020-10-03
 */
public interface XqsxtrlyxbMapper 
{
    /**
     * 查询人脸运行数据
     * 
     * @param id 人脸运行数据ID
     * @return 人脸运行数据
     */
    public Xqsxtrlyxb selectXqsxtrlyxbById(Long id);

    /**
     * 查询人脸运行数据列表
     * 
     * @param xqsxtrlyxb 人脸运行数据
     * @return 人脸运行数据集合
     */
    public List<Xqsxtrlyxb> selectXqsxtrlyxbList(Xqsxtrlyxb xqsxtrlyxb);

    public List<Xqsxtrlyxb> selectXqsxtrlyxbListByXqAdminId(@Param("userId")Long userId, @Param("xqsxtrlyxb")Xqsxtrlyxb xqsxtrlyxb);

    public List<Xqsxtrlyxb> selectXqsxtrlyxbListByDeptId(@Param("deptId")Long deptId, @Param("xqsxtrlyxb")Xqsxtrlyxb xqsxtrlyxb);

    
    /**
     * 新增人脸运行数据
     * 
     * @param xqsxtrlyxb 人脸运行数据
     * @return 结果
     */
    public int insertXqsxtrlyxb(Xqsxtrlyxb xqsxtrlyxb);

    /**
     * 修改人脸运行数据
     * 
     * @param xqsxtrlyxb 人脸运行数据
     * @return 结果
     */
    public int updateXqsxtrlyxb(Xqsxtrlyxb xqsxtrlyxb);

    /**
     * 删除人脸运行数据
     * 
     * @param id 人脸运行数据ID
     * @return 结果
     */
    public int deleteXqsxtrlyxbById(Long id);

    /**
     * 批量删除人脸运行数据
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteXqsxtrlyxbByIds(Long[] ids);
}

package com.ruoyi.system.domain;

public class SnapOrRecordInfo extends Info {
	private String PushAckType;
	private String SnapOrRecordID;
	public String getPushAckType() {
		return PushAckType;
	}
	public void setPushAckType(String pushAckType) {
		PushAckType = pushAckType;
	}
	public String getSnapOrRecordID() {
		return SnapOrRecordID;
	}
	public void setSnapOrRecordID(String snapOrRecordID) {
		SnapOrRecordID = snapOrRecordID;
	}
	public SnapOrRecordInfo(String pushAckType, String snapOrRecordID) {
		super();
		PushAckType = pushAckType;
		SnapOrRecordID = snapOrRecordID;
	}
	public SnapOrRecordInfo() {
		super();
	}
	@Override
	public String toString() {
		return "SnapOrRecordInfo [PushAckType=" + PushAckType + ", SnapOrRecordID=" + SnapOrRecordID + "]";
	}
	
	
}

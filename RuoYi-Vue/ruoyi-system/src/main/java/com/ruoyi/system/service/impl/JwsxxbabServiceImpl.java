package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.JwsxxbabMapper;
import com.ruoyi.system.domain.Jwsxxbab;
import com.ruoyi.system.service.IJwsxxbabService;

/**
 * 警务室信息Service业务层处理
 * 
 * @author Carry
 * @date 2020-10-27
 */
@Service
public class JwsxxbabServiceImpl implements IJwsxxbabService 
{
    @Autowired
    private JwsxxbabMapper jwsxxbabMapper;

    /**
     * 查询警务室信息
     * 
     * @param id 警务室信息ID
     * @return 警务室信息
     */
    @Override
    public Jwsxxbab selectJwsxxbabById(Long id)
    {
        return jwsxxbabMapper.selectJwsxxbabById(id);
    }

    /**
     * 查询警务室信息列表
     * 
     * @param jwsxxbab 警务室信息
     * @return 警务室信息
     */
    @Override
    public List<Jwsxxbab> selectJwsxxbabList(Jwsxxbab jwsxxbab)
    {
        return jwsxxbabMapper.selectJwsxxbabList(jwsxxbab);
    }
    
   

    /**
     * 新增警务室信息
     * 
     * @param jwsxxbab 警务室信息
     * @return 结果
     */
    @Override
    public int insertJwsxxbab(Jwsxxbab jwsxxbab)
    {
        return jwsxxbabMapper.insertJwsxxbab(jwsxxbab);
    }

    /**
     * 修改警务室信息
     * 
     * @param jwsxxbab 警务室信息
     * @return 结果
     */
    @Override
    public int updateJwsxxbab(Jwsxxbab jwsxxbab)
    {
        return jwsxxbabMapper.updateJwsxxbab(jwsxxbab);
    }

    /**
     * 批量删除警务室信息
     * 
     * @param ids 需要删除的警务室信息ID
     * @return 结果
     */
    @Override
    public int deleteJwsxxbabByIds(Long[] ids)
    {
        return jwsxxbabMapper.deleteJwsxxbabByIds(ids);
    }

    /**
     * 删除警务室信息信息
     * 
     * @param id 警务室信息ID
     * @return 结果
     */
    @Override
    public int deleteJwsxxbabById(Long id)
    {
        return jwsxxbabMapper.deleteJwsxxbabById(id);
    }

	@Override
	public List<Jwsxxbab> selectJwsxxbabListByDeptId(Long deptId, Long deptIdOfTree, Jwsxxbab jwsxxbab) {
		// TODO Auto-generated method stub
		 return jwsxxbabMapper.selectJwsxxbabListByDeptId(deptId,deptIdOfTree,jwsxxbab);
	}
}

package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Rycrbab;

/**
 * 出入人员管理Service接口
 * 
 * @author ruoyi
 * @date 2020-09-29
 */
public interface IRycrbabService 
{
    /**
     * 查询出入人员管理
     * 
     * @param id 出入人员管理ID
     * @return 出入人员管理
     */
    public Rycrbab selectRycrbabById(Long id);

    /**
     * 通过小区管理员id获取小区人员出入信息
     * @param userId
     * @param rycrbab
     * @return
     */
    
    public List<Rycrbab> selectRycrbabListByXqAdminId(Long userId,Rycrbab rycrbab);
   
    public List<Rycrbab> selectTodayRycrbabListByXqAdminId(Long userId,Rycrbab rycrbab);
    public List<Rycrbab> selectAllRycrbabListByXqAdminId(Long userId,Rycrbab rycrbab);
    
    public List<Rycrbab> selectRycrbabListByDeptId(Long deptId,Long deptIdOfTree,Rycrbab rycrbab);
    
    public long countAllRycrbabByDeptId(Long deptId,Long deptIdOfTree,Rycrbab rycrbab);
    
    public long countTodayRycrbabByDeptId(Long deptId,Long deptIdOfTree,Rycrbab rycrbab);
    
    
    
    public long countAllRycrbabByXqAdminId(Long userId,Rycrbab rycrbab);
    
    public long countTodayRycrbabByXqAdminId(Long userId,Rycrbab rycrbab);
    
    
    public List<Rycrbab> selectTodayRycrbabListByDeptId(Long deptId,Long deptIdOfTree,Rycrbab rycrbab);
    public List<Rycrbab> selectAllRycrbabListByDeptId(Long deptId,Long deptIdOfTree,Rycrbab rycrbab);
    
    /**
     * 查询出入人员管理列表
     * 
     * @param rycrbab 出入人员管理
     * @return 出入人员管理集合
     */
    public List<Rycrbab> selectRycrbabList(Rycrbab rycrbab);

    /**
     * 新增出入人员管理
     * 
     * @param rycrbab 出入人员管理
     * @return 结果
     */
    public int insertRycrbab(Rycrbab rycrbab);

    /**
     * 修改出入人员管理
     * 
     * @param rycrbab 出入人员管理
     * @return 结果
     */
    public int updateRycrbab(Rycrbab rycrbab);

    /**
     * 批量删除出入人员管理
     * 
     * @param ids 需要删除的出入人员管理ID
     * @return 结果
     */
    public int deleteRycrbabByIds(Long[] ids);

    /**
     * 删除出入人员管理信息
     * 
     * @param id 出入人员管理ID
     * @return 结果
     */
    public int deleteRycrbabById(Long id);
}

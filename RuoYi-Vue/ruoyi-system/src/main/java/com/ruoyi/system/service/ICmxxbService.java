package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Cmxxb;

/**
 * 串码信息Service接口
 * 
 * @author Carry
 * @date 2020-10-04
 */
public interface ICmxxbService 
{
    /**
     * 查询串码信息
     * 
     * @param id 串码信息ID
     * @return 串码信息
     */
    public Cmxxb selectCmxxbById(Long id);

    /**
     * 查询串码信息列表
     * 
     * @param cmxxb 串码信息
     * @return 串码信息集合
     */
    public List<Cmxxb> selectCmxxbList(Cmxxb cmxxb);

    public List<Cmxxb> selectCmxxbListByXqAdminId(Long userId,Cmxxb cmxxb);

    public List<Cmxxb> selectCmxxbListByDeptId(Long deptId,Cmxxb cmxxb);

    
    
    /**
     * 新增串码信息
     * 
     * @param cmxxb 串码信息
     * @return 结果
     */
    public int insertCmxxb(Cmxxb cmxxb);

    /**
     * 修改串码信息
     * 
     * @param cmxxb 串码信息
     * @return 结果
     */
    public int updateCmxxb(Cmxxb cmxxb);

    /**
     * 批量删除串码信息
     * 
     * @param ids 需要删除的串码信息ID
     * @return 结果
     */
    public int deleteCmxxbByIds(Long[] ids);

    /**
     * 删除串码信息信息
     * 
     * @param id 串码信息ID
     * @return 结果
     */
    public int deleteCmxxbById(Long id);
}

package com.ruoyi.system.service;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;

import com.ruoyi.common.core.domain.entity.SysPcsxxbab;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.domain.Pcsxxbab;

/**
 * 派出所信息Service接口
 * 
 * @author Carry
 * @date 2020-10-21
 */
public interface IPcsxxbabService 
{
    /**
     * 查询派出所信息
     * 
     * @param id 派出所信息ID
     * @return 派出所信息
     */
    public Pcsxxbab selectPcsxxbabById(Long id);
    
    /**
     * 根据派出所编码查询派出所名称
     * @param pcsbm
     * @return
     */
    public String selectPcsmcByPcsbm(String  pcsbm);

    /**
     * 查询派出所信息列表
     * 
     * @param pcsxxbab 派出所信息
     * @return 派出所信息集合
     */
    public List<Pcsxxbab> selectPcsxxbabList(Pcsxxbab pcsxxbab);

    
    public  List<Pcsxxbab> selectPcsxxbabListByDeptId(Long deptId,Long deptIdOfTree,Pcsxxbab pcsxxbab);
    	
    
    
    /**
     * 根据所属区县获取所属派出所信息
     * @param ssqx
     * @return
     */
    public List<Pcsxxbab> selectPcsxxbabListBySsqx(String ssqx);
    
//    public List<Pcsxxbab> selectSubDeptByQgajDeptId(Long ssqx);
    
    /**
     * 新增派出所信息
     * 
     * @param pcsxxbab 派出所信息
     * @return 结果
     */
    public int insertPcsxxbab(SysPcsxxbab pcsxxbab);

    /**
     * 修改派出所信息
     * 
     * @param pcsxxbab 派出所信息
     * @return 结果
     */
    public int updatePcsxxbab(Pcsxxbab pcsxxbab);

    /**
     * 批量删除派出所信息
     * 
     * @param ids 需要删除的派出所信息ID
     * @return 结果
     */
    public int deletePcsxxbabByIds(Long[] ids);

    /**
     * 删除派出所信息信息
     * 
     * @param id 派出所信息ID
     * @return 结果
     */
    public int deletePcsxxbabById(Long id);
}

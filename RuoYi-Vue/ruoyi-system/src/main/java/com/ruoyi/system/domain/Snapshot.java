package com.ruoyi.system.domain;

public class Snapshot {
	public Integer getCmdType() {
		return cmdType;
	}
	public void setCmdType(Integer cmdType) {
		this.cmdType = cmdType;
	}
	public String getSxtbm() {
		return sxtbm;
	}
	public void setSxtbm(String sxtbm) {
		this.sxtbm = sxtbm;
	}
	public String getGMSFZH() {
		return GMSFZH;
	}
	public void setGMSFZH(String gMSFZH) {
		GMSFZH = gMSFZH;
	}
	public String getZPZPA() {
		return ZPZPA;
	}
	public void setZPZPA(String zPZPA) {
		ZPZPA = zPZPA;
	}
	public String getZPZPB() {
		return ZPZPB;
	}
	public void setZPZPB(String zPZPB) {
		ZPZPB = zPZPB;
	}
	public String getCRLB() {
		return CRLB;
	}
	public void setCRLB(String cRLB) {
		CRLB = cRLB;
	}
	public Long getZPSJ() {
		return ZPSJ;
	}
	public void setZPSJ(Long zPSJ) {
		ZPSJ = zPSJ;
	}
	
	@Override
	public String toString() {
		return "Snapshot [cmdType=" + cmdType + ", sxtbm=" + sxtbm + ", GMSFZH=" + GMSFZH + ", ZPZPA=" + ZPZPA
				+ ", ZPZPB=" + ZPZPB + ", CRLB=" + CRLB + ", ZPSJ=" + ZPSJ + ", name=" + name + "]";
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Snapshot() {
		super();
	}

	public Snapshot(Integer cmdType, String sxtbm, String gMSFZH, String zPZPA, String zPZPB, String cRLB, Long zPSJ,
			String name) {
		super();
		this.cmdType = cmdType;
		this.sxtbm = sxtbm;
		GMSFZH = gMSFZH;
		ZPZPA = zPZPA;
		ZPZPB = zPZPB;
		CRLB = cRLB;
		ZPSJ = zPSJ;
		this.name = name;
	}

	private Integer cmdType;
	private String sxtbm;
	private String GMSFZH;
	private String ZPZPA;
	private String ZPZPB;
	private String CRLB;
	private Long ZPSJ;
	private String name;
	
}

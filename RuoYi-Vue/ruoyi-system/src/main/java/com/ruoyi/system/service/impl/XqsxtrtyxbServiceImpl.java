package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.XqsxtrtyxbMapper;
import com.ruoyi.system.domain.Xqsxtrtyxb;
import com.ruoyi.system.service.IXqsxtrtyxbService;

/**
 * 摄像头人体运行Service业务层处理
 * 
 * @author Carry
 * @date 2020-10-03
 */
@Service
public class XqsxtrtyxbServiceImpl implements IXqsxtrtyxbService 
{
    @Autowired
    private XqsxtrtyxbMapper xqsxtrtyxbMapper;

    /**
     * 查询摄像头人体运行
     * 
     * @param id 摄像头人体运行ID
     * @return 摄像头人体运行
     */
    @Override
    public Xqsxtrtyxb selectXqsxtrtyxbById(Long id)
    {
        return xqsxtrtyxbMapper.selectXqsxtrtyxbById(id);
    }

    /**
     * 查询摄像头人体运行列表
     * 
     * @param xqsxtrtyxb 摄像头人体运行
     * @return 摄像头人体运行
     */
    @Override
    public List<Xqsxtrtyxb> selectXqsxtrtyxbList(Xqsxtrtyxb xqsxtrtyxb)
    {
        return xqsxtrtyxbMapper.selectXqsxtrtyxbList(xqsxtrtyxb);
    }

    /**
     * 新增摄像头人体运行
     * 
     * @param xqsxtrtyxb 摄像头人体运行
     * @return 结果
     */
    @Override
    public int insertXqsxtrtyxb(Xqsxtrtyxb xqsxtrtyxb)
    {
        return xqsxtrtyxbMapper.insertXqsxtrtyxb(xqsxtrtyxb);
    }

    /**
     * 修改摄像头人体运行
     * 
     * @param xqsxtrtyxb 摄像头人体运行
     * @return 结果
     */
    @Override
    public int updateXqsxtrtyxb(Xqsxtrtyxb xqsxtrtyxb)
    {
        return xqsxtrtyxbMapper.updateXqsxtrtyxb(xqsxtrtyxb);
    }

    /**
     * 批量删除摄像头人体运行
     * 
     * @param ids 需要删除的摄像头人体运行ID
     * @return 结果
     */
    @Override
    public int deleteXqsxtrtyxbByIds(Long[] ids)
    {
        return xqsxtrtyxbMapper.deleteXqsxtrtyxbByIds(ids);
    }

    /**
     * 删除摄像头人体运行信息
     * 
     * @param id 摄像头人体运行ID
     * @return 结果
     */
    @Override
    public int deleteXqsxtrtyxbById(Long id)
    {
        return xqsxtrtyxbMapper.deleteXqsxtrtyxbById(id);
    }

	@Override
	public List<Xqsxtrtyxb> selectXqsxtrtyxbListByXqAdminId(Long userId, Xqsxtrtyxb xqsxtrtyxb) {
		// TODO Auto-generated method stub
		return xqsxtrtyxbMapper.selectXqsxtrtyxbListByXqAdminId(userId,xqsxtrtyxb);
	}

	@Override
	public List<Xqsxtrtyxb> selectXqsxtrtyxbListByDeptId(Long deptId, Xqsxtrtyxb xqsxtrtyxb) {
		// TODO Auto-generated method stub
		return xqsxtrtyxbMapper.selectXqsxtrtyxbListByDeptId(deptId,xqsxtrtyxb);
	}
}

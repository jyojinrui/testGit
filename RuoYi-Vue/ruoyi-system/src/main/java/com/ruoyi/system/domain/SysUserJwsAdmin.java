package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 警务室管理员对象 sys_user_jws_admin
 * 
 * @author ruoyi
 * @date 2020-11-05
 */
public class SysUserJwsAdmin extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 警务室编码 */
    @Excel(name = "警务室编码")
    private String jwsbm;

    /** 用户编码 */
    @Excel(name = "用户编码")
    private Long userId;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setJwsbm(String jwsbm) 
    {
        this.jwsbm = jwsbm;
    }

    public String getJwsbm() 
    {
        return jwsbm;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("jwsbm", getJwsbm())
            .append("userId", getUserId())
            .toString();
    }
}

package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.system.mapper.CyryxxbabMapper;
import com.ruoyi.system.mapper.FkbabMapper;
import com.ruoyi.system.mapper.FzxxbabMapper;
import com.ruoyi.system.mapper.JzryrzxxbabMapper;
import com.ruoyi.system.mapper.MjkxxbabMapper;
import com.ruoyi.system.mapper.MjsbxxbabMapper;
import com.ruoyi.system.mapper.QgajbmPcsbmMapper;
import com.ruoyi.system.mapper.RycrbabMapper;
import com.ruoyi.system.mapper.SydwxxbabMapper;
import com.ruoyi.system.mapper.SysDeptMapper;
import com.ruoyi.system.mapper.SysStaMapper;
import com.ruoyi.system.mapper.XqxxbabMapper;
import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.statistics.QgajSta;
import com.ruoyi.system.domain.SysQgajStaItem;
import com.ruoyi.system.domain.SysSta;
import com.ruoyi.system.domain.Xqxxbab;
import com.ruoyi.system.service.ISysStaService;

/**
 * 统计报表Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-12-23
 */
@Service
public class SysStaServiceImpl implements ISysStaService {
	@Autowired
	private SysStaMapper sysStaMapper;

	@Autowired
	private XqxxbabMapper xqxxbabMapper;

	@Autowired
	private SysDeptMapper sysDeptMapper;

	@Autowired
	private FzxxbabMapper fzxxbabMapper;

	@Autowired
	private JzryrzxxbabMapper jzryrzxxbabMapper;

	@Autowired
	private SydwxxbabMapper sydwxxbabMapper;

	@Autowired
	private CyryxxbabMapper cyryxxbabMapper;

	@Autowired
	private MjsbxxbabMapper mjsbxxbabMapper;

	@Autowired
	private MjkxxbabMapper mjkxxbabMapper;

	@Autowired
	private FkbabMapper fkbabMapper;

	@Autowired
	private RycrbabMapper rycrbabMapper;

	/**
	 * 查询统计报表
	 * 
	 * @param id 统计报表ID
	 * @return 统计报表
	 */
	@Override
	public SysSta selectSysStaById(Long id) {
		return sysStaMapper.selectSysStaById(id);
	}

	/**
	 * 新增统计报表
	 * 
	 * @param sysSta 统计报表
	 * @return 结果
	 */
	@Override
	public int insertSysSta(SysSta sysSta) {
		return sysStaMapper.insertSysSta(sysSta);
	}

	/**
	 * 修改统计报表
	 * 
	 * @param sysSta 统计报表
	 * @return 结果
	 */
	@Override
	public int updateSysSta(SysSta sysSta) {
		return sysStaMapper.updateSysSta(sysSta);
	}

	/**
	 * 批量删除统计报表
	 * 
	 * @param ids 需要删除的统计报表ID
	 * @return 结果
	 */
	@Override
	public int deleteSysStaByIds(Long[] ids) {
		return sysStaMapper.deleteSysStaByIds(ids);
	}

	/**
	 * 删除统计报表信息
	 * 
	 * @param id 统计报表ID
	 * @return 结果
	 */
	@Override
	public int deleteSysStaById(Long id) {
		return sysStaMapper.deleteSysStaById(id);
	}

	/**
	 * 查询统计报表列表
	 * 
	 * @param sysSta 统计报表
	 * @return 统计报表
	 */
	@Override
	public List<SysSta> selectSysStaList(SysSta sysSta) {

		List<SysSta> qgajStaDataList = new ArrayList<SysSta>();
		// 查询所有小区信息
		List<Xqxxbab> xqList = xqxxbabMapper.selectAllXqxxbabList();

//		List<Xqxxbab> xqList=xqxxbabMapper.selectAllXqxxbabList();

		System.out.println("<===========>");

		System.out.println(xqList);

		System.out.println("<===========>");
//		
		for (int i = 0; i < xqList.size(); i++) {
			SysSta sta = new SysSta();
			// 获取本系统小区编码
			String xqbm = xqList.get(i).getBuildingId();
			// 获取治综系统小区编码
//			String zzxqbm=xqList.get(i).getZzssxqbm();

			// 获取小区名称
			String xqmc = xqList.get(i).getXqmc();
			sta.setXqmc(xqmc);

			// 获取分局编码
			String fjbm = xqbm.substring(0, 6) + "000000";
			sta.setFjbm(fjbm);

			// 获取分局名称
			String fjmc = sysDeptMapper.selectDeptById(Long.parseLong(fjbm)).getDeptName();
			sta.setFjmc(fjmc);

			// 获取归属派出所编码
			String gspcs = xqbm.substring(0, 9) + "000";
			sta.setGspcs(gspcs);

			// 获取派出所名称
			String pcsmc = sysDeptMapper.selectDeptById(Long.parseLong(gspcs)).getDeptName();
			sta.setPcsmc(pcsmc);
			// 获取建设公司名称
			sta.setJsgs("西安景煜网络科技有限公司");

			// 根据小区编码获取实有房屋的统计信息
			long syfw = fzxxbabMapper.getCountByXqbm(xqbm);
			sta.setSyfwsl(syfw);

			// 根据小区编码获取实有人口数量
			long syrk = jzryrzxxbabMapper.getCountByXqbm(xqbm);
			sta.setSyrksl(syrk);

			// 根据小区编码获取实有单位数量
			long sydw = sydwxxbabMapper.getCountByXqbm(xqbm);
			sta.setSydwsl(sydw);

			// 根据小区编码获取从业人员数量
			long cyrysl = cyryxxbabMapper.getCountByXqbm(xqbm);
			sta.setCyrysl(cyrysl);

			// 根据小区编码获取门禁设备数量
			long mjsbsl = mjsbxxbabMapper.getCountByXqbm(xqbm);
			sta.setMjsbsl(mjsbsl);

			// 根据小区编码获取门禁卡数量
			long mjksl = mjkxxbabMapper.getCountByXqbm(xqbm);
			sta.setMjksl(mjksl);

			// 根据小区编码获取访客数量
			long fksl = fkbabMapper.getCountByXqbm(xqbm);
			sta.setFksl(fksl);

			// 根据小区编码获取出入记录数量
			long crjlsl = rycrbabMapper.getCountByXqbm(xqbm);
			sta.setCrjlsl(crjlsl);

			// 根据小区编码获取出入照片数量
			long crzpsl = rycrbabMapper.getCountByXqbm(xqbm);
			sta.setCrzpsl(crzpsl);

			qgajStaDataList.add(sta);
		}

		// 根据派出所编码排序
		Collections.sort(qgajStaDataList, new Comparator<SysSta>() {

			@Override
			public int compare(SysSta o1, SysSta o2) {
				// TODO Auto-generated method stub
				return o1.getGspcs().compareTo(o2.getGspcs());
			}
		});
		// 根据区公安局编码排序
		Collections.sort(qgajStaDataList, new Comparator<SysSta>() {

			@Override
			public int compare(SysSta o1, SysSta o2) {
				// TODO Auto-generated method stub
				return o1.getFjbm().compareTo(o2.getFjbm());
			}

		});

		return qgajStaDataList;
//        return sysStaMapper.selectSysStaList(sysSta);
	}

	/**
	 * 查询统计报表列表
	 * 
	 * @param sysSta 统计报表
	 * @return 统计报表
	 */

//    public static void main(String[] args) {
//    	XqxxbabServiceImpl xServiceImpl=new XqxxbabServiceImpl();
//    	
//    	List<Xqxxbab> xqList=xServiceImpl.selectAllXqxxbabList();
//    	 
//		System.out.println("<===========>");
//		
//		System.out.println(xqList);
//
//		System.out.println("<===========>");
//		
//	}

	@Override
	public List<SysSta> selectSysStaListByXqbm(SysSta sysSta) {
		// TODO Auto-generated method stub
		// 申请一个ArrayList数组用来保存查询到的小区信息
		System.out.println("+++++++");
		System.out.println(sysSta.getParams());
		List<SysSta> qgajStaDataList = new ArrayList<SysSta>();
		// 查询所有小区信息
		List<Xqxxbab> xqList = xqxxbabMapper.selectAllXqxxbabList();

//		List<Xqxxbab> xqList=xqxxbabMapper.selectAllXqxxbabList();

		System.out.println("<===========>");

		System.out.println(xqList);

		System.out.println("<===========>");
//		
		for (int i = 0; i < xqList.size(); i++) {
			SysSta sta = new SysSta();
			// 获取本系统小区编码
			String xqbm = xqList.get(i).getBuildingId();
			// 获取治综系统小区编码
//			String zzxqbm=xqList.get(i).getZzssxqbm();

			// 获取小区名称
			String xqmc = xqList.get(i).getXqmc();
			sta.setXqmc(xqmc);

			// 获取分局编码
			String fjbm = xqbm.substring(0, 6) + "000000";
			sta.setFjbm(fjbm);

			// 获取分局名称
			String fjmc = sysDeptMapper.selectDeptById(Long.parseLong(fjbm)).getDeptName();
			sta.setFjmc(fjmc);

			// 获取归属派出所编码
			String gspcs = xqbm.substring(0, 9) + "000";
			sta.setGspcs(gspcs);

			// 获取派出所名称
			String pcsmc = sysDeptMapper.selectDeptById(Long.parseLong(gspcs)).getDeptName();
			sta.setPcsmc(pcsmc);
			// 获取建设公司名称
			sta.setJsgs("西安景煜网络科技有限公司");

			// 根据小区编码获取实有房屋的统计信息
			long syfw = fzxxbabMapper.getCountByXqbm(xqbm);
			sta.setSyfwsl(syfw);

			// 根据小区编码获取实有人口数量
			long syrk = jzryrzxxbabMapper.getCountByXqbm(xqbm);
			sta.setSyrksl(syrk);

			// 根据小区编码获取实有单位数量
			long sydw = sydwxxbabMapper.getCountByXqbm(xqbm);
			sta.setSydwsl(sydw);

			// 根据小区编码获取从业人员数量
			long cyrysl = cyryxxbabMapper.getCountByXqbm(xqbm);
			sta.setCyrysl(cyrysl);

			// 根据小区编码获取门禁设备数量
			long mjsbsl = mjsbxxbabMapper.getCountByXqbm(xqbm);
			sta.setMjsbsl(mjsbsl);

			// 根据小区编码获取门禁卡数量
			long mjksl = mjkxxbabMapper.getCountByXqbm(xqbm);
			sta.setMjksl(mjksl);

			// 根据小区编码获取访客数量
			long fksl = fkbabMapper.getCountByXqbm(xqbm);
			sta.setFksl(fksl);

			// 根据小区编码获取出入记录数量
			long crjlsl = rycrbabMapper.getCountByXqbm(xqbm);
			sta.setCrjlsl(crjlsl);

			// 根据小区编码获取出入照片数量
			long crzpsl = rycrbabMapper.getCountByXqbm(xqbm);
			sta.setCrzpsl(crzpsl);

			qgajStaDataList.add(sta);

		}

		// 根据区公安局编码和派出所编码对查询到的统计信息进行排序
//		for(int i=0;i<qgajStaDataList.size()-1;i++)
//		{
//			for(int j=i+1;j<qgajStaDataList.size();j++)
//			{
//				String fjbm_i=qgajStaDataList.get(i).getFjbm();   //分局i编码
//				String fjbm_j=qgajStaDataList.get(j).getFjbm();   //分局j编码
//				
//				String pcsbm_i=qgajStaDataList.get(i).getGspcs();  //派出所i编码
//				String pcsbm_j=qgajStaDataList.get(j).getGspcs();  //派出所j编码
//				if(Long.parseLong(fjbm_i)>Long.parseLong(fjbm_j))
//				{
//					
//				}
//			}
//		}

		// 根据派出所编码排序
		Collections.sort(qgajStaDataList, new Comparator<SysSta>() {

			@Override
			public int compare(SysSta o1, SysSta o2) {
				// TODO Auto-generated method stub
				return o1.getGspcs().compareTo(o2.getGspcs());
			}
		});
		// 根据区公安局编码排序
		Collections.sort(qgajStaDataList, new Comparator<SysSta>() {

			@Override
			public int compare(SysSta o1, SysSta o2) {
				// TODO Auto-generated method stub
				return o1.getFjbm().compareTo(o2.getFjbm());
			}

		});

		return qgajStaDataList;
	}

	@Override
	public List<SysQgajStaItem> selectAllSysStaListByQgajbm() {

		List<SysQgajStaItem> qgajStaDataList = new ArrayList<SysQgajStaItem>();

		// 查找出所有的区公安局部门ID
		List<Long> qganList = sysDeptMapper.getAllQgajbm();
//		System.out.println(qganList);

		for (int i = 0; i < qganList.size(); i++) {
			SysQgajStaItem qs = new SysQgajStaItem();

			// 获取部门ID
			long deptId = qganList.get(i);
			qs.setDeptId(deptId);

			// 获取部门名称
			String deptName = sysDeptMapper.getDeptNameByDeptId(deptId);
			qs.setDeptName(deptName);

			// 根据参数类型统计相关数据信息
			// if(type=="实有人口")
			long count = jzryrzxxbabMapper.getCountByDeptId(deptId); // 根据区公安局编码统计相关信息
			qs.setCount(count);

			qgajStaDataList.add(qs);
		}
		// TODO Auto-generated method stub
		return qgajStaDataList;
	}
}

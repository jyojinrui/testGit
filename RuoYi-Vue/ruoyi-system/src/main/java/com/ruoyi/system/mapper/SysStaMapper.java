package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysSta;
import com.ruoyi.system.domain.Xqxxbab;

/**
 * 统计报表Mapper接口
 * 
 * @author ruoyi
 * @date 2020-12-23
 */
public interface SysStaMapper 
{
    /**
     * 查询统计报表
     * 
     * @param id 统计报表ID
     * @return 统计报表
     */
    public SysSta selectSysStaById(Long id);

    /**
     * 查询统计报表列表
     * 
     * @param sysSta 统计报表
     * @return 统计报表集合
     */
    public List<SysSta> selectSysStaList(SysSta sysSta);
    
    public List<SysSta> selectSysStaListByXqbm(SysSta sysSta);
    

    /**
     * 新增统计报表
     * 
     * @param sysSta 统计报表
     * @return 结果
     */
    public int insertSysSta(SysSta sysSta);

    /**
     * 修改统计报表
     * 
     * @param sysSta 统计报表
     * @return 结果
     */
    public int updateSysSta(SysSta sysSta);

    /**
     * 删除统计报表
     * 
     * @param id 统计报表ID
     * @return 结果
     */
    public int deleteSysStaById(Long id);

    /**
     * 批量删除统计报表
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysStaByIds(Long[] ids);
    
    public List<Xqxxbab> selectAllXqxxbabList();
}

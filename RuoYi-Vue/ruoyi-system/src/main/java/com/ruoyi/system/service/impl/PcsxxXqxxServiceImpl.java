package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PcsxxXqxxMapper;
import com.ruoyi.system.domain.PcsxxXqxx;
import com.ruoyi.system.service.IPcsxxXqxxService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-10-22
 */
@Service
public class PcsxxXqxxServiceImpl implements IPcsxxXqxxService 
{
    @Autowired
    private PcsxxXqxxMapper pcsxxXqxxMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public PcsxxXqxx selectPcsxxXqxxById(Long id)
    {
        return pcsxxXqxxMapper.selectPcsxxXqxxById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param pcsxxXqxx 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<PcsxxXqxx> selectPcsxxXqxxList(PcsxxXqxx pcsxxXqxx)
    {
        return pcsxxXqxxMapper.selectPcsxxXqxxList(pcsxxXqxx);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param pcsxxXqxx 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertPcsxxXqxx(PcsxxXqxx pcsxxXqxx)
    {
        return pcsxxXqxxMapper.insertPcsxxXqxx(pcsxxXqxx);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param pcsxxXqxx 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updatePcsxxXqxx(PcsxxXqxx pcsxxXqxx)
    {
        return pcsxxXqxxMapper.updatePcsxxXqxx(pcsxxXqxx);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deletePcsxxXqxxByIds(Long[] ids)
    {
        return pcsxxXqxxMapper.deletePcsxxXqxxByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    @Override
    public int deletePcsxxXqxxById(Long id)
    {
        return pcsxxXqxxMapper.deletePcsxxXqxxById(id);
    }
}

package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.system.domain.SysQgajStaItem;
import com.ruoyi.system.domain.SysSta;

/**
 * 统计报表Service接口
 * 
 * @author ruoyi
 * @date 2020-12-23
 */
public interface ISysStaService 
{
    /**
     * 查询统计报表
     * 
     * @param id 统计报表ID
     * @return 统计报表
     */
    public SysSta selectSysStaById(Long id);

    /**
     * 查询统计报表列表
     * 
     * @param sysSta 统计报表
     * @return 统计报表集合
     */
    public List<SysSta> selectSysStaList(SysSta sysSta);

    /**
     * 根据小区编码查询小区数据 
     * 
     * @param sysSta 统计报表
     * @return 统计报表集合
     */
    public List<SysSta> selectSysStaListByXqbm(SysSta sysSta);
  

    public List<SysQgajStaItem> selectAllSysStaListByQgajbm();
    /**
     * 新增统计报表
     * 
     * @param sysSta 统计报表
     * @return 结果
     */
    public int insertSysSta(SysSta sysSta);

    /**
     * 修改统计报表
     * 
     * @param sysSta 统计报表
     * @return 结果
     */
    public int updateSysSta(SysSta sysSta);

    /**
     * 批量删除统计报表
     * 
     * @param ids 需要删除的统计报表ID
     * @return 结果
     */
    public int deleteSysStaByIds(Long[] ids);

    /**
     * 删除统计报表信息
     * 
     * @param id 统计报表ID
     * @return 结果
     */
    public int deleteSysStaById(Long id);
}

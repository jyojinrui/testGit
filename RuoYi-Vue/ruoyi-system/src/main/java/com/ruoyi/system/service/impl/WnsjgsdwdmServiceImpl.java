package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.WnsjgsdwdmMapper;
import com.ruoyi.system.domain.Wnsjgsdwdm;
import com.ruoyi.system.service.IWnsjgsdwdmService;

/**
 * 渭南数据归属单位代码Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-11-03
 */
@Service
public class WnsjgsdwdmServiceImpl implements IWnsjgsdwdmService 
{
    @Autowired
    private WnsjgsdwdmMapper wnsjgsdwdmMapper;

    /**
     * 查询渭南数据归属单位代码
     * 
     * @param id 渭南数据归属单位代码ID
     * @return 渭南数据归属单位代码
     */
    @Override
    public Wnsjgsdwdm selectWnsjgsdwdmById(Long id)
    {
        return wnsjgsdwdmMapper.selectWnsjgsdwdmById(id);
    }

    /**
     * 查询渭南数据归属单位代码列表
     * 
     * @param wnsjgsdwdm 渭南数据归属单位代码
     * @return 渭南数据归属单位代码
     */
    @Override
    public List<Wnsjgsdwdm> selectWnsjgsdwdmList(Wnsjgsdwdm wnsjgsdwdm)
    {
        return wnsjgsdwdmMapper.selectWnsjgsdwdmList(wnsjgsdwdm);
    }

    /**
     * 新增渭南数据归属单位代码
     * 
     * @param wnsjgsdwdm 渭南数据归属单位代码
     * @return 结果
     */
    @Override
    public int insertWnsjgsdwdm(Wnsjgsdwdm wnsjgsdwdm)
    {
        return wnsjgsdwdmMapper.insertWnsjgsdwdm(wnsjgsdwdm);
    }

    /**
     * 修改渭南数据归属单位代码
     * 
     * @param wnsjgsdwdm 渭南数据归属单位代码
     * @return 结果
     */
    @Override
    public int updateWnsjgsdwdm(Wnsjgsdwdm wnsjgsdwdm)
    {
        return wnsjgsdwdmMapper.updateWnsjgsdwdm(wnsjgsdwdm);
    }

    /**
     * 批量删除渭南数据归属单位代码
     * 
     * @param ids 需要删除的渭南数据归属单位代码ID
     * @return 结果
     */
    @Override
    public int deleteWnsjgsdwdmByIds(Long[] ids)
    {
        return wnsjgsdwdmMapper.deleteWnsjgsdwdmByIds(ids);
    }

    /**
     * 删除渭南数据归属单位代码信息
     * 
     * @param id 渭南数据归属单位代码ID
     * @return 结果
     */
    @Override
    public int deleteWnsjgsdwdmById(Long id)
    {
        return wnsjgsdwdmMapper.deleteWnsjgsdwdmById(id);
    }

	@Override
	public List<Wnsjgsdwdm> selectQgajList() {
		// TODO Auto-generated method stub
		return wnsjgsdwdmMapper.selectQgajList();
	}

	@Override
	public List<Wnsjgsdwdm> selectSubDeptByQgajDeptId(Long deptid) {
		// TODO Auto-generated method stub
		return wnsjgsdwdmMapper.selectSubDeptByQgajDeptId(deptid);
	}
}

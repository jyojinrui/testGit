package com.ruoyi.system.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 串码信息对象 cmxxb
 * 
 * @author Carry
 * @date 2020-10-04
 */
public class Cmxxb extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 业务流水号 */
    @Excel(name = "业务流水号")
    private String ywlsh;

    /** 串码信息 */
    @Excel(name = "串码信息")
    private String imis;

    /** 捕获时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "捕获时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date bhsj;

    /** 比对时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "比对时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date bdsj;

    /** 设备编码 */
    @Excel(name = "设备编码")
    private String sbbm;

    /** 比对状态 */
    @Excel(name = "比对状态")
    private String bdzt;

    /** mac地址 */
    @Excel(name = "mac地址")
    private String mac;

    /** 小区编码 */
    @Excel(name = "小区编码")
    private String buildingId;

    /** 机身码 */
    @Excel(name = "机身码")
    private String jishenma;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setYwlsh(String ywlsh) 
    {
        this.ywlsh = ywlsh;
    }

    public String getYwlsh() 
    {
        return ywlsh;
    }
    public void setImis(String imis) 
    {
        this.imis = imis;
    }

    public String getImis() 
    {
        return imis;
    }
    public void setBhsj(Date bhsj) 
    {
//    	System.out.println("=================>");
//    	System.out.println(bhsj);
//    	System.out.println("=================>");
        this.bhsj = bhsj;
    }

    public Date getBhsj() 
    {
        return bhsj;
    }
    public void setBdsj(Date bdsj) 
    {
        this.bdsj = bdsj;
    }

    public Date getBdsj() 
    {
        return bdsj;
    }
    public void setSbbm(String sbbm) 
    {
        this.sbbm = sbbm;
    }

    public String getSbbm() 
    {
        return sbbm;
    }
    public void setBdzt(String bdzt) 
    {
        this.bdzt = bdzt;
    }

    public String getBdzt() 
    {
        return bdzt;
    }
    public void setMac(String mac) 
    {
        this.mac = mac;
    }

    public String getMac() 
    {
        return mac;
    }
    public void setBuildingId(String buildingId) 
    {
        this.buildingId = buildingId;
    }

    public String getBuildingId() 
    {
        return buildingId;
    }
    public void setJishenma(String jishenma) 
    {
        this.jishenma = jishenma;
    }

    public String getJishenma() 
    {
        return jishenma;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ywlsh", getYwlsh())
            .append("imis", getImis())
            .append("bhsj", getBhsj())
            .append("bdsj", getBdsj())
            .append("sbbm", getSbbm())
            .append("bdzt", getBdzt())
            .append("mac", getMac())
            .append("buildingId", getBuildingId())
            .append("jishenma", getJishenma())
            .toString();
    }
}

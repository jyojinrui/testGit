package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysUserQgajAdminMapper;
import com.ruoyi.system.domain.SysUserQgajAdmin;
import com.ruoyi.system.service.ISysUserQgajAdminService;

/**
 * 区公安局管理员与公安局关系表Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-11-01
 */
@Service
public class SysUserQgajAdminServiceImpl implements ISysUserQgajAdminService 
{
    @Autowired
    private SysUserQgajAdminMapper sysUserQgajAdminMapper;

    /**
     * 查询区公安局管理员与公安局关系表
     * 
     * @param id 区公安局管理员与公安局关系表ID
     * @return 区公安局管理员与公安局关系表
     */
    @Override
    public SysUserQgajAdmin selectSysUserQgajAdminById(Long id)
    {
        return sysUserQgajAdminMapper.selectSysUserQgajAdminById(id);
    }

    /**
     * 查询区公安局管理员与公安局关系表列表
     * 
     * @param sysUserQgajAdmin 区公安局管理员与公安局关系表
     * @return 区公安局管理员与公安局关系表
     */
    @Override
    public List<SysUserQgajAdmin> selectSysUserQgajAdminList(SysUserQgajAdmin sysUserQgajAdmin)
    {
        return sysUserQgajAdminMapper.selectSysUserQgajAdminList(sysUserQgajAdmin);
    }

    /**
     * 新增区公安局管理员与公安局关系表
     * 
     * @param sysUserQgajAdmin 区公安局管理员与公安局关系表
     * @return 结果
     */
    @Override
    public int insertSysUserQgajAdmin(SysUserQgajAdmin sysUserQgajAdmin)
    {
        return sysUserQgajAdminMapper.insertSysUserQgajAdmin(sysUserQgajAdmin);
    }

    /**
     * 修改区公安局管理员与公安局关系表
     * 
     * @param sysUserQgajAdmin 区公安局管理员与公安局关系表
     * @return 结果
     */
    @Override
    public int updateSysUserQgajAdmin(SysUserQgajAdmin sysUserQgajAdmin)
    {
        return sysUserQgajAdminMapper.updateSysUserQgajAdmin(sysUserQgajAdmin);
    }

    /**
     * 批量删除区公安局管理员与公安局关系表
     * 
     * @param ids 需要删除的区公安局管理员与公安局关系表ID
     * @return 结果
     */
    @Override
    public int deleteSysUserQgajAdminByIds(Long[] ids)
    {
        return sysUserQgajAdminMapper.deleteSysUserQgajAdminByIds(ids);
    }

    /**
     * 删除区公安局管理员与公安局关系表信息
     * 
     * @param id 区公安局管理员与公安局关系表ID
     * @return 结果
     */
    @Override
    public int deleteSysUserQgajAdminById(Long id)
    {
        return sysUserQgajAdminMapper.deleteSysUserQgajAdminById(id);
    }
}

package com.ruoyi.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.common.core.domain.entity.SysPcsxxbab;
import com.ruoyi.system.domain.Pcsxxbab;

/**
 * 派出所信息Mapper接口
 * 
 * @author Carry
 * @date 2020-10-21
 */
public interface PcsxxbabMapper 
{
    /**
     * 查询派出所信息
     * 
     * @param id 派出所信息ID
     * @return 派出所信息
     */
    public Pcsxxbab selectPcsxxbabById(Long id);
    
    public String  selectPcsmcByPcsbm(String pcsbm);

    /**
     * 查询派出所信息列表
     * 
     * @param pcsxxbab 派出所信息
     * @return 派出所信息集合
     */
    public List<Pcsxxbab> selectPcsxxbabList(Pcsxxbab pcsxxbab);
//    public List<Pcsxxbab> selectSubDeptByQgajDeptId(Long deptid);
    /**
     * 新增派出所信息
     * 
     * @param pcsxxbab 派出所信息
     * @return 结果
     */
    public int insertPcsxxbab(SysPcsxxbab pcsxxbab);

    /**
     * 修改派出所信息
     * 
     * @param pcsxxbab 派出所信息
     * @return 结果
     */
    public int updatePcsxxbab(Pcsxxbab pcsxxbab);

    /**
     * 删除派出所信息
     * 
     * @param id 派出所信息ID
     * @return 结果
     */
    public int deletePcsxxbabById(Long id);

    /**
     * 批量删除派出所信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePcsxxbabByIds(Long[] ids);
    
    public List<Pcsxxbab> selectPcsxxbabListBySsqx(@Param("ssqx")String ssqx);
   
    public List<Pcsxxbab> selectPcsxxbabListByDeptId(@Param("deptId")Long deptId,@Param("deptIdOfTree")Long deptIdOfTree, @Param("pcsxxbab")Pcsxxbab pcsxxbab);
    	
}

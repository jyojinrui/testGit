package com.ruoyi.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.system.domain.Xqxxbab;
import com.ruoyi.system.domain.Zzxqxxbab;

/**
 * 小区管理Mapper接口
 * 
 * @author ruoyi
 * @date 2020-09-26
 */
public interface XqxxbabMapper 
{
	/**
	 * 根据小区管理员id获取小区 
	 * @param userId
	 * @return
	 */
	public Xqxxbab getXqxxByXqAdminId(@Param("userId")Long userId);
    /**
     * 查询小区管理
     * 
     * @param id 小区管理ID
     * @return 小区管理
     */
    public Xqxxbab selectXqxxbabById(String id);

    /**
     * 
     * @param ssxqbm
     * @return
     */
    public Xqxxbab selectXqxxbabBySsxqbm(String ssxqbm);
    /**
     * 查询小区管理列表
     * 
     * @param xqxxbab 小区管理
     * @return 小区管理集合
     */
    public List<Xqxxbab> selectXqxxbabList(Xqxxbab xqxxbab);
  
    public List<Xqxxbab> selectAllXqxxbabList();
    

    public List<Xqxxbab> selectXqxxbabListByDeptId(@Param("deptId")Long deptId,@Param("deptIdOfTree")Long deptIdOfTree,@Param("xqxxbab")Xqxxbab xqxxbab);
    
    
    /**
     * 新增小区管理
     * 
     * @param xqxxbab 小区管理
     * @return 结果
     */
    public int insertXqxxbab(Xqxxbab xqxxbab);

    /**
     * 修改小区管理
     * 
     * @param xqxxbab 小区管理
     * @return 结果
     */
    public int updateXqxxbab(Xqxxbab xqxxbab);

    /**
     * 删除小区管理
     * 
     * @param id 小区管理ID
     * @return 结果
     */
    public int deleteXqxxbabById(String id);

    /**
     * 批量删除小区管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteXqxxbabByIds(String[] ids);
    
	public List<Zzxqxxbab> getBzdzxxbabByXqbm(String buildingId);
}

package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 门禁设备管理对象 mjsbxxbab
 * 
 * @author Carry
 * @date 2020-09-29
 */
public class Mjsbxxbab extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 门禁编号 */
    @Excel(name = "门禁编号")
    private String mjbh;

    /** 门禁名称 */
    @Excel(name = "门禁名称")
    private String mjmc;

    /** 位置描述 */
    @Excel(name = "位置描述")
    private String wzms;

    /** 是否有摄像头 */
    @Excel(name = "是否有摄像头")
    private String sfysxt;

    /** 小区编码 */
    @Excel(name = "小区编码")
    private String ssxqbm;

    /** 地址二维码编码 */
    @Excel(name = "地址二维码编码")
    private String dzewmbm;

    /** 登记单位 */
    @Excel(name = "登记单位")
    private String djdw;

    /** 登记人姓名 */
    @Excel(name = "登记人姓名")
    private String djrxm;

    /** 登记时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "登记时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date djsj;

    /** 申报系统 */
    @Excel(name = "申报系统")
    private String sbxt;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMjbh(String mjbh) 
    {
        this.mjbh = mjbh;
    }

    public String getMjbh() 
    {
        return mjbh;
    }
    public void setMjmc(String mjmc) 
    {
        this.mjmc = mjmc;
    }

    public String getMjmc() 
    {
        return mjmc;
    }
    public void setWzms(String wzms) 
    {
        this.wzms = wzms;
    }

    public String getWzms() 
    {
        return wzms;
    }
    public void setSfysxt(String sfysxt) 
    {
        this.sfysxt = sfysxt;
    }

    public String getSfysxt() 
    {
        return sfysxt;
    }
    public void setSsxqbm(String ssxqbm) 
    {
        this.ssxqbm = ssxqbm;
    }

    public String getSsxqbm() 
    {
        return ssxqbm;
    }
    public void setDzewmbm(String dzewmbm) 
    {
        this.dzewmbm = dzewmbm;
    }

    public String getDzewmbm() 
    {
        return dzewmbm;
    }
    public void setDjdw(String djdw) 
    {
        this.djdw = djdw;
    }

    public String getDjdw() 
    {
        return djdw;
    }
    public void setDjrxm(String djrxm) 
    {
        this.djrxm = djrxm;
    }

    public String getDjrxm() 
    {
        return djrxm;
    }
    public void setDjsj(Date djsj) 
    {
        this.djsj = djsj;
    }

    public Date getDjsj() 
    {
        return djsj;
    }
    public void setSbxt(String sbxt) 
    {
        this.sbxt = sbxt;
    }

    public String getSbxt() 
    {
        return sbxt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("mjbh", getMjbh())
            .append("mjmc", getMjmc())
            .append("wzms", getWzms())
            .append("sfysxt", getSfysxt())
            .append("ssxqbm", getSsxqbm())
            .append("dzewmbm", getDzewmbm())
            .append("djdw", getDjdw())
            .append("djrxm", getDjrxm())
            .append("djsj", getDjsj())
            .append("sbxt", getSbxt())
            .toString();
    }
}

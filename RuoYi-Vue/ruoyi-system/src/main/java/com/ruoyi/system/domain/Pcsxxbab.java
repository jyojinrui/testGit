package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 派出所信息对象 pcsxxbab
 * 
 * @author Carry
 * @date 2020-10-21
 */
public class Pcsxxbab extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 派出所编码 */
    @Excel(name = "派出所编码")
    private String pcsbm;

    /** 地址二维码编码 */
    @Excel(name = "地址二维码编码")
    private String dzermbm;

    /** 派出所名称 */
    @Excel(name = "派出所名称")
    private String pcsmc;

    /** 派出所地址 */
    @Excel(name = "派出所地址")
    private String pcsdz;

    /** 所属区县 */
    @Excel(name = "所属区县")
    private String ssqx;

    /** 派出所负责人 */
    @Excel(name = "派出所负责人")
    private String pcsfzr;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String lxdh;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setPcsbm(String pcsbm) 
    {
        this.pcsbm = pcsbm;
    }

    public String getPcsbm() 
    {
        return pcsbm;
    }
    public void setDzermbm(String dzermbm) 
    {
        this.dzermbm = dzermbm;
    }

    public String getDzermbm() 
    {
        return dzermbm;
    }
    public void setPcsmc(String pcsmc) 
    {
        this.pcsmc = pcsmc;
    }

    public String getPcsmc() 
    {
        return pcsmc;
    }
    public void setPcsdz(String pcsdz) 
    {
        this.pcsdz = pcsdz;
    }

    public String getPcsdz() 
    {
        return pcsdz;
    }
    public void setSsqx(String ssqx) 
    {
        this.ssqx = ssqx;
    }

    public String getSsqx() 
    {
        return ssqx;
    }
    public void setPcsfzr(String pcsfzr) 
    {
        this.pcsfzr = pcsfzr;
    }

    public String getPcsfzr() 
    {
        return pcsfzr;
    }
    public void setLxdh(String lxdh) 
    {
        this.lxdh = lxdh;
    }

    public String getLxdh() 
    {
        return lxdh;
    }

    public Pcsxxbab(Long id, String pcsbm, String dzermbm, String pcsmc, String pcsdz, String ssqx, String pcsfzr,
			String lxdh) {
		super();
		this.id = id;
		this.pcsbm = pcsbm;
		this.dzermbm = dzermbm;
		this.pcsmc = pcsmc;
		this.pcsdz = pcsdz;
		this.ssqx = ssqx;
		this.pcsfzr = pcsfzr;
		this.lxdh = lxdh;
	}

	public Pcsxxbab() {
		super();
	}

	@Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("pcsbm", getPcsbm())
            .append("dzermbm", getDzermbm())
            .append("pcsmc", getPcsmc())
            .append("pcsdz", getPcsdz())
            .append("ssqx", getSsqx())
            .append("pcsfzr", getPcsfzr())
            .append("lxdh", getLxdh())
            .toString();
    }
}

package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Zzxqxxbab;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2021-12-20
 */
public interface IZzxqxxbabService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public Zzxqxxbab selectZzxqxxbabById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param zzxqxxbab 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<Zzxqxxbab> selectZzxqxxbabList(Zzxqxxbab zzxqxxbab);

    /**
     * 新增【请填写功能名称】
     * 
     * @param zzxqxxbab 【请填写功能名称】
     * @return 结果
     */
    public int insertZzxqxxbab(Zzxqxxbab zzxqxxbab);

    /**
     * 修改【请填写功能名称】
     * 
     * @param zzxqxxbab 【请填写功能名称】
     * @return 结果
     */
    public int updateZzxqxxbab(Zzxqxxbab zzxqxxbab);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】ID
     * @return 结果
     */
    public int deleteZzxqxxbabByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteZzxqxxbabById(Long id);
}

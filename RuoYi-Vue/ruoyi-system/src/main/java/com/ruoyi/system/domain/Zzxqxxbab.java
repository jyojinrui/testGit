package com.ruoyi.system.domain;

public class Zzxqxxbab {
	
	 /** 系统自动生成 */
    
    private String zzssxqbm;
    
    private String xqmc;
    
    private String bzdz;
    
    private String metaAddrId;

	public String getZzssxqbm() {
		return zzssxqbm;
	}

	public void setZzssxqbm(String zzssxqbm) {
		this.zzssxqbm = zzssxqbm;
	}

	public String getXqmc() {
		return xqmc;
	}

	public void setXqmc(String xqmc) {
		this.xqmc = xqmc;
	}

	public String getBzdz() {
		return bzdz;
	}

	public void setBzdz(String bzdz) {
		this.bzdz = bzdz;
	}

	public String getMetaAddrId() {
		return metaAddrId;
	}

	public void setMetaAddrId(String metaAddrId) {
		this.metaAddrId = metaAddrId;
	}

	public Zzxqxxbab() {
		super();
	}

	public Zzxqxxbab(String zzssxqbm, String xqmc, String bzdz, String metaAddrId) {
		super();
		this.zzssxqbm = zzssxqbm;
		this.xqmc = xqmc;
		this.bzdz = bzdz;
		this.metaAddrId = metaAddrId;
	}

	@Override
	public String toString() {
		return "Zzxqxxbab [zzssxqbm=" + zzssxqbm + ", xqmc=" + xqmc + ", bzdz=" + bzdz + ", metaAddrId=" + metaAddrId
				+ "]";
	}
    
    
}

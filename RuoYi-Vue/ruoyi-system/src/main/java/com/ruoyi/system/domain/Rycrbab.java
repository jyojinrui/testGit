package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 出入人员管理对象 rycrbab
 * 
 * @author ruoyi
 * @date 2020-09-29
 */
public class Rycrbab extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 刷卡编码 */
    @Excel(name = "刷卡编码")
    private String skbm;

    /** 所属小区编码 */
    @Excel(name = "所属小区编码")
    private String ssxqbm;
    
    /**
     * 小区名称
     */
    @Excel(name="小区名称")
    private String xqmc;
    
    /**
     * 派出所名称
     */
    @Excel(name="派出所名称")
    private String pcsmc;
    
    

    /** 门禁编号（小区编码+4位顺序号） */
    @Excel(name = "门禁编号", readConverterExp = "小=区编码+4位顺序号")
    private String mjbh;

    /** 公民身份号码（GB 11643） */
    @Excel(name = "公民身份号码", readConverterExp = "G=B,1=1643")
    private String gmsfhm;

    /** 姓名*/
    @Excel(name="姓名")
    private String xm;
    
    /** 出入类别 */
    @Excel(name = "出入类别")
    private String crlb;

    /** 抓拍照片远景 */
    @Excel(name = "抓拍照片远景")
    private String zpzpa;

    /** 抓拍照片近景 */
    @Excel(name = "抓拍照片近景")
    private String zpzpb;

    /** 抓拍时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "抓拍时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date zpsj;

    /** 申报系统 */
    @Excel(name = "申报系统")
    private String sbxt;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSkbm(String skbm) 
    {
        this.skbm = skbm;
    }

    public String getSkbm() 
    {
        return skbm;
    }
    public void setSsxqbm(String ssxqbm) 
    {
        this.ssxqbm = ssxqbm;
    }

    public String getSsxqbm() 
    {
        return ssxqbm;
    }
    
    public String getXqmc() {
		return xqmc;
	}

	public void setXqmc(String xqmc) {
		this.xqmc = xqmc;
	}

	public void setMjbh(String mjbh) 
    {
        this.mjbh = mjbh;
    }

    public String getMjbh() 
    {
        return mjbh;
    }
    public void setGmsfhm(String gmsfhm) 
    {
        this.gmsfhm = gmsfhm;
    }

    public String getGmsfhm() 
    {
        return gmsfhm;
    }
    public void setCrlb(String crlb) 
    {
        this.crlb = crlb;
    }

    public String getCrlb() 
    {
        return crlb;
    }
    public void setZpzpa(String zpzpa) 
    {
        this.zpzpa = zpzpa;
    }

    public String getZpzpa() 
    {
        return zpzpa;
    }
    public void setZpzpb(String path_zb) 
    {
        this.zpzpb = path_zb;
    }

    public String getZpzpb() 
    {
        return zpzpb;
    }
    public void setZpsj(Date zpsj) 
    {
        this.zpsj = zpsj;
    }

    public Date getZpsj() 
    {
        return zpsj;
    }
    public void setSbxt(String sbxt) 
    {
        this.sbxt = sbxt;
    }

    public String getSbxt() 
    {
        return sbxt;
    }
    

    public String getXm() {
		return xm;
	}

	public void setXm(String xm) {
		this.xm = xm;
	}

	
	public String getPcsmc() {
		return pcsmc;
	}

	public void setPcsmc(String pcsmc) {
		this.pcsmc = pcsmc;
	}

	public Rycrbab() {
		super();
	}

	public Rycrbab(Long id, String skbm, String ssxqbm, String xqmc, String pcsmc, String mjbh, String gmsfhm,
			String xm, String crlb, String zpzpa, String zpzpb, Date zpsj, String sbxt) {
		super();
		this.id = id;
		this.skbm = skbm;
		this.ssxqbm = ssxqbm;
		this.xqmc = xqmc;
		this.pcsmc = pcsmc;
		this.mjbh = mjbh;
		this.gmsfhm = gmsfhm;
		this.xm = xm;
		this.crlb = crlb;
		this.zpzpa = zpzpa;
		this.zpzpb = zpzpb;
		this.zpsj = zpsj;
		this.sbxt = sbxt;
	}

	public Rycrbab(Long id, String skbm, String ssxqbm, String mjbh, String gmsfhm, String xm, String crlb,
			String zpzpa, String zpzpb, Date zpsj, String sbxt) {
		super();
		this.id = id;
		this.skbm = skbm;
		this.ssxqbm = ssxqbm;
		this.mjbh = mjbh;
		this.gmsfhm = gmsfhm;
		this.xm = xm;
		this.crlb = crlb;
		this.zpzpa = zpzpa;
		this.zpzpb = zpzpb;
		this.zpsj = zpsj;
		this.sbxt = sbxt;
	}

	public Rycrbab(Long id, String skbm, String ssxqbm, String xqmc, String mjbh, String gmsfhm, String xm, String crlb,
			String zpzpa, String zpzpb, Date zpsj, String sbxt) {
		super();
		this.id = id;
		this.skbm = skbm;
		this.ssxqbm = ssxqbm;
		this.xqmc = xqmc;
		this.mjbh = mjbh;
		this.gmsfhm = gmsfhm;
		this.xm = xm;
		this.crlb = crlb;
		this.zpzpa = zpzpa;
		this.zpzpb = zpzpb;
		this.zpsj = zpsj;
		this.sbxt = sbxt;
	}

	@Override
	public String toString() {
		return "Rycrbab [id=" + id + ", skbm=" + skbm + ", ssxqbm=" + ssxqbm + ", xqmc=" + xqmc + ", pcsmc=" + pcsmc
				+ ", mjbh=" + mjbh + ", gmsfhm=" + gmsfhm + ", xm=" + xm + ", crlb=" + crlb + ", zpzpa=" + zpzpa
				+ ", zpzpb=" + zpzpb + ", zpsj=" + zpsj + ", sbxt=" + sbxt + "]";
	}
	
	
}

package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 从业人员信息对象 cyryxxbab
 * 
 * @author Carry
 * @date 2020-10-27
 */
public class Cyryxxbab extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 从业人员编码 */
    @Excel(name = "从业人员编码")
    private String cyrybm;

    /** 公民身份号码 */
    @Excel(name = "公民身份号码")
    private String gmsfhm;

    /** 姓名 */
    @Excel(name = "姓名")
    private String xm;

    /** 证件种类 */
    @Excel(name = "证件种类")
    private String zjzl;

    /** 证件号码 */
    @Excel(name = "证件号码")
    private String zjhm;

    /** 外文姓 */
    @Excel(name = "外文姓")
    private String wwx;

    /** 外文名 */
    @Excel(name = "外文名")
    private String wwm;

    /** 单位编码 */
    @Excel(name = "单位编码")
    private String dwbm;

    /** 单位名称 */
    @Excel(name = "单位名称")
    private String dmmc;

    /** 职业 */
    @Excel(name = "职业")
    private String zy;

    /** 职业类别 */
    @Excel(name = "职业类别")
    private String zylb;

    /** 登记公司编码 */
    @Excel(name = "登记公司编码")
    private String djdwgajgjgdm;

    /** 登记公司名称 */
    @Excel(name = "登记公司名称")
    private String djdwgajgjgmc;

    /** 登记人_姓名 */
    @Excel(name = "登记人_姓名")
    private String djr;

    /** 登记时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "登记时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date djsj;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String lxfs;

    /** 最后操作时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后操作时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date czsj;

    /** 对接小区id */
    @Excel(name = "对接小区id")
    private String computerid;

    /** 地址归属单位 */
    @Excel(name = "地址归属单位")
    private String dzgsdw;

    /** 注销人员 */
    @Excel(name = "注销人员")
    private String zxrid;

    /** 注销时间 */
    @Excel(name = "注销时间")
    private String zxsj;

    /** 小区申报编码 */
    @Excel(name = "小区申报编码")
    private String ssxqbm;

    /** 操作方式 */
    @Excel(name = "操作方式")
    private String procmode;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCyrybm(String cyrybm) 
    {
        this.cyrybm = cyrybm;
    }

    public String getCyrybm() 
    {
        return cyrybm;
    }
    public void setGmsfhm(String gmsfhm) 
    {
        this.gmsfhm = gmsfhm;
    }

    public String getGmsfhm() 
    {
        return gmsfhm;
    }
    public void setXm(String xm) 
    {
        this.xm = xm;
    }

    public String getXm() 
    {
        return xm;
    }
    public void setZjzl(String zjzl) 
    {
        this.zjzl = zjzl;
    }

    public String getZjzl() 
    {
        return zjzl;
    }
    public void setZjhm(String zjhm) 
    {
        this.zjhm = zjhm;
    }

    public String getZjhm() 
    {
        return zjhm;
    }
    public void setWwx(String wwx) 
    {
        this.wwx = wwx;
    }

    public String getWwx() 
    {
        return wwx;
    }
    public void setWwm(String wwm) 
    {
        this.wwm = wwm;
    }

    public String getWwm() 
    {
        return wwm;
    }
    public void setDwbm(String dwbm) 
    {
        this.dwbm = dwbm;
    }

    public String getDwbm() 
    {
        return dwbm;
    }
    public void setDmmc(String dmmc) 
    {
        this.dmmc = dmmc;
    }

    public String getDmmc() 
    {
        return dmmc;
    }
    public void setZy(String zy) 
    {
        this.zy = zy;
    }

    public String getZy() 
    {
        return zy;
    }
    public void setZylb(String zylb) 
    {
        this.zylb = zylb;
    }

    public String getZylb() 
    {
        return zylb;
    }
    public void setDjdwgajgjgdm(String djdwgajgjgdm) 
    {
        this.djdwgajgjgdm = djdwgajgjgdm;
    }

    public String getDjdwgajgjgdm() 
    {
        return djdwgajgjgdm;
    }
    public void setDjdwgajgjgmc(String djdwgajgjgmc) 
    {
        this.djdwgajgjgmc = djdwgajgjgmc;
    }

    public String getDjdwgajgjgmc() 
    {
        return djdwgajgjgmc;
    }
    public void setDjr(String djr) 
    {
        this.djr = djr;
    }

    public String getDjr() 
    {
        return djr;
    }
    public void setDjsj(Date djsj) 
    {
        this.djsj = djsj;
    }

    public Date getDjsj() 
    {
        return djsj;
    }
    public void setLxfs(String lxfs) 
    {
        this.lxfs = lxfs;
    }

    public String getLxfs() 
    {
        return lxfs;
    }
    public void setCzsj(Date czsj) 
    {
        this.czsj = czsj;
    }

    public Date getCzsj() 
    {
        return czsj;
    }
    public void setComputerid(String computerid) 
    {
        this.computerid = computerid;
    }

    public String getComputerid() 
    {
        return computerid;
    }
    public void setDzgsdw(String dzgsdw) 
    {
        this.dzgsdw = dzgsdw;
    }

    public String getDzgsdw() 
    {
        return dzgsdw;
    }
    public void setZxrid(String zxrid) 
    {
        this.zxrid = zxrid;
    }

    public String getZxrid() 
    {
        return zxrid;
    }
    public void setZxsj(String zxsj) 
    {
        this.zxsj = zxsj;
    }

    public String getZxsj() 
    {
        return zxsj;
    }
    public void setSsxqbm(String ssxqbm) 
    {
        this.ssxqbm = ssxqbm;
    }

    public String getSsxqbm() 
    {
        return ssxqbm;
    }
    public void setProcmode(String procmode) 
    {
        this.procmode = procmode;
    }

    public String getProcmode() 
    {
        return procmode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("cyrybm", getCyrybm())
            .append("gmsfhm", getGmsfhm())
            .append("xm", getXm())
            .append("zjzl", getZjzl())
            .append("zjhm", getZjhm())
            .append("wwx", getWwx())
            .append("wwm", getWwm())
            .append("dwbm", getDwbm())
            .append("dmmc", getDmmc())
            .append("zy", getZy())
            .append("zylb", getZylb())
            .append("djdwgajgjgdm", getDjdwgajgjgdm())
            .append("djdwgajgjgmc", getDjdwgajgjgmc())
            .append("djr", getDjr())
            .append("djsj", getDjsj())
            .append("lxfs", getLxfs())
            .append("czsj", getCzsj())
            .append("computerid", getComputerid())
            .append("dzgsdw", getDzgsdw())
            .append("zxrid", getZxrid())
            .append("zxsj", getZxsj())
            .append("ssxqbm", getSsxqbm())
            .append("procmode", getProcmode())
            .toString();
    }
}

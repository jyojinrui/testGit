package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Fkbab;

/**
 * 访客信息管理Service接口
 * 
 * @author Carry
 * @date 2020-09-29
 */
public interface IFkbabService 
{
    /**
     * 查询访客信息管理
     * @param id 访客信息管理ID
     * @return 访客信息管理
     */
    public Fkbab selectFkbabById(Long id);

    /**
     * 查询访客信息管理列表
     * @param fkbab 访客信息管理
     * @return 访客信息管理集合
     */
    public List<Fkbab> selectFkbabList(Fkbab fkbab);

    /**
     * 根据小区管理员id获取小区信息
     * @param userId
     * @param fkbab
     * @return
     */
    public List<Fkbab> selectFkbabListByXqAdminId(Long userId,Fkbab fkbab);
    
    public List<Fkbab> selectFkbabListByDeptId(Long deptId,Long deptIdOfTree,Fkbab fkbab);
    
    
    /**
     * 新增访客信息管理
     * 
     * @param fkbab 访客信息管理
     * @return 结果
     */
    public int insertFkbab(Fkbab fkbab);

    /**
     * 修改访客信息管理
     * 
     * @param fkbab 访客信息管理
     * @return 结果
     */
    public int updateFkbab(Fkbab fkbab);

    /**
     * 批量删除访客信息管理
     * 
     * @param ids 需要删除的访客信息管理ID
     * @return 结果
     */
    public int deleteFkbabByIds(Long[] ids);

    /**
     * 删除访客信息管理信息
     * 
     * @param id 访客信息管理ID
     * @return 结果
     */
    public int deleteFkbabById(Long id);
}

package com.ruoyi.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.system.domain.Tcxxbab;

/**
 * 停车信息管理Mapper接口
 * 
 * @author Carry
 * @date 2020-09-30
 */
public interface TcxxbabMapper {
	/**
	 * 查询停车信息管理
	 * 
	 * @param id 停车信息管理ID
	 * @return 停车信息管理
	 */
	public Tcxxbab selectTcxxbabById(Long id);

	/**
	 * 查询停车信息管理列表
	 * 
	 * @param tcxxbab 停车信息管理
	 * @return 停车信息管理集合
	 */
	public List<Tcxxbab> selectTcxxbabList(Tcxxbab tcxxbab);

	/**
	 * 根据小区管理员id获取所管理小区所有停车场所有停车信息
	 * 
	 * @param userId
	 * @param tcxxbab
	 * @return
	 */
	public List<Tcxxbab> selectTcxxbabListByXqAdminId(@Param("userId") Long userId, @Param("tcxxbab") Tcxxbab tcxxbab);

	public List<Tcxxbab> selectTcxxbabListDeptId(@Param("deptId") Long deptId, @Param("deptIdOfTree") Long deptIdOfTree,
			@Param("tcxxbab") Tcxxbab tcxxbab);

	public List<Tcxxbab> selectTodayTcxxbabListByXqAdminId(@Param("userId") Long userId,
			@Param("tcxxbab") Tcxxbab tcxxbab);

	public List<Tcxxbab> selectTodayTcxxbabListDeptId(@Param("deptId") Long deptId,
			@Param("deptIdOfTree") Long deptIdOfTree, @Param("tcxxbab") Tcxxbab tcxxbab);

	/**
	 * 新增停车信息管理
	 * 
	 * @param tcxxbab 停车信息管理
	 * @return 结果
	 */
	public int insertTcxxbab(Tcxxbab tcxxbab);

	/**
	 * 修改停车信息管理
	 * 
	 * @param tcxxbab 停车信息管理
	 * @return 结果
	 */
	public int updateTcxxbab(Tcxxbab tcxxbab);

	/**
	 * 删除停车信息管理
	 * 
	 * @param id 停车信息管理ID
	 * @return 结果
	 */
	public int deleteTcxxbabById(Long id);

	/**
	 * 批量删除停车信息管理
	 * 
	 * @param ids 需要删除的数据ID
	 * @return 结果
	 */
	public int deleteTcxxbabByIds(Long[] ids);
}

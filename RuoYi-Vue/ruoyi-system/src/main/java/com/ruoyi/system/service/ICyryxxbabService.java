package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Cyryxxbab;

/**
 * 从业人员信息Service接口
 * 
 * @author Carry
 * @date 2020-10-27
 */
public interface ICyryxxbabService 
{
    /**
     * 查询从业人员信息
     * 
     * @param id 从业人员信息ID
     * @return 从业人员信息
     */
    public Cyryxxbab selectCyryxxbabById(Long id);

    /**
     * 查询从业人员信息列表
     * 
     * @param cyryxxbab 从业人员信息
     * @return 从业人员信息集合
     */
    public List<Cyryxxbab> selectCyryxxbabList(Cyryxxbab cyryxxbab);

    /**
     * 新增从业人员信息
     * 
     * @param cyryxxbab 从业人员信息
     * @return 结果
     */
    public int insertCyryxxbab(Cyryxxbab cyryxxbab);

    /**
     * 修改从业人员信息
     * 
     * @param cyryxxbab 从业人员信息
     * @return 结果
     */
    public int updateCyryxxbab(Cyryxxbab cyryxxbab);

    /**
     * 批量删除从业人员信息
     * 
     * @param ids 需要删除的从业人员信息ID
     * @return 结果
     */
    public int deleteCyryxxbabByIds(Long[] ids);

    /**
     * 删除从业人员信息信息
     * 
     * @param id 从业人员信息ID
     * @return 结果
     */
    public int deleteCyryxxbabById(Long id);
}

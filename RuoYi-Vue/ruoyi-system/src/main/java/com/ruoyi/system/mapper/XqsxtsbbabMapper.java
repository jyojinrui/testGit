package com.ruoyi.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ruoyi.system.domain.Xqsxtsbbab;

/**
 * 小区摄像头设备Mapper接口
 * 
 * @author Carry
 * @date 2020-10-03
 */
public interface XqsxtsbbabMapper 
{
    /**
     * 查询小区摄像头设备
     * 
     * @param id 小区摄像头设备ID
     * @return 小区摄像头设备
     */
    public Xqsxtsbbab selectXqsxtsbbabById(Long id);

    
    public List<Xqsxtsbbab> selectXqsxtsbbabListByXqAdminId(@Param("userId")Long userId,@Param("xqsxtsbbab")Xqsxtsbbab xqsxtsbbab);

    public List<Xqsxtsbbab> selectXqsxtsbbabListByDeptId(@Param("deptId")Long deptId,@Param("xqsxtsbbab")Xqsxtsbbab xqsxtsbbab);

    
    /**
     * 查询小区摄像头设备列表
     * 
     * @param xqsxtsbbab 小区摄像头设备
     * @return 小区摄像头设备集合
     */
    public List<Xqsxtsbbab> selectXqsxtsbbabList(Xqsxtsbbab xqsxtsbbab);

    /**
     * 新增小区摄像头设备
     * 
     * @param xqsxtsbbab 小区摄像头设备
     * @return 结果
     */
    public int insertXqsxtsbbab(Xqsxtsbbab xqsxtsbbab);

    /**
     * 修改小区摄像头设备
     * 
     * @param xqsxtsbbab 小区摄像头设备
     * @return 结果
     */
    public int updateXqsxtsbbab(Xqsxtsbbab xqsxtsbbab);

    /**
     * 删除小区摄像头设备
     * 
     * @param id 小区摄像头设备ID
     * @return 结果
     */
    public int deleteXqsxtsbbabById(Long id);

    /**
     * 批量删除小区摄像头设备
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteXqsxtsbbabByIds(Long[] ids);
}

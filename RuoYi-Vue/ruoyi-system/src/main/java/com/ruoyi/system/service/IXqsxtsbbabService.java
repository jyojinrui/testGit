package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.Xqsxtsbbab;

/**
 * 小区摄像头设备Service接口
 * 
 * @author Carry
 * @date 2020-10-03
 */
public interface IXqsxtsbbabService 
{
    /**
     * 查询小区摄像头设备
     * 
     * @param id 小区摄像头设备ID
     * @return 小区摄像头设备
     */
    public Xqsxtsbbab selectXqsxtsbbabById(Long id);

    
    public List<Xqsxtsbbab> selectXqsxtsbbabListByXqAdminId(Long userId,Xqsxtsbbab xqsxtsbbab);
   
    public List<Xqsxtsbbab> selectXqsxtsbbabListByDeptId(Long deptId,Xqsxtsbbab xqsxtsbbab);
    
    
    /**
     * 查询小区摄像头设备列表
     * 
     * @param xqsxtsbbab 小区摄像头设备
     * @return 小区摄像头设备集合
     */
    public List<Xqsxtsbbab> selectXqsxtsbbabList(Xqsxtsbbab xqsxtsbbab);

    /**
     * 新增小区摄像头设备
     * 
     * @param xqsxtsbbab 小区摄像头设备
     * @return 结果
     */
    public int insertXqsxtsbbab(Xqsxtsbbab xqsxtsbbab);

    /**
     * 修改小区摄像头设备
     * 
     * @param xqsxtsbbab 小区摄像头设备
     * @return 结果
     */
    public int updateXqsxtsbbab(Xqsxtsbbab xqsxtsbbab);

    /**
     * 批量删除小区摄像头设备
     * 
     * @param ids 需要删除的小区摄像头设备ID
     * @return 结果
     */
    public int deleteXqsxtsbbabByIds(Long[] ids);

    /**
     * 删除小区摄像头设备信息
     * 
     * @param id 小区摄像头设备ID
     * @return 结果
     */
    public int deleteXqsxtsbbabById(Long id);
}

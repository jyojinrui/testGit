package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 派出所编码与区公安局编码对象 qgajbm_pcsbm
 * 
 * @author ruoyi
 * @date 2020-11-03
 */
public class QgajbmPcsbm extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 系统自动生成编号 */
    private Long id;

    /** 区公安局编码 */
    @Excel(name = "区公安局编码")
    private String qgajbm;

    /** 派出所编码 */
    @Excel(name = "派出所编码")
    private String pcsbm;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setQgajbm(String qgajbm) 
    {
        this.qgajbm = qgajbm;
    }

    public String getQgajbm() 
    {
        return qgajbm;
    }
    public void setPcsbm(String pcsbm) 
    {
        this.pcsbm = pcsbm;
    }

    public String getPcsbm() 
    {
        return pcsbm;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("qgajbm", getQgajbm())
            .append("pcsbm", getPcsbm())
            .toString();
    }
}

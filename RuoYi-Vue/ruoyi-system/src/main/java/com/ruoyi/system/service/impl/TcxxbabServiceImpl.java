package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TcxxbabMapper;
import com.ruoyi.system.domain.Tcxxbab;
import com.ruoyi.system.service.ITcxxbabService;

/**
 * 停车信息管理Service业务层处理
 * 
 * @author Carry
 * @date 2020-09-30
 */
@Service
public class TcxxbabServiceImpl implements ITcxxbabService 
{
    @Autowired
    private TcxxbabMapper tcxxbabMapper;

    /**
     * 查询停车信息管理
     * 
     * @param id 停车信息管理ID
     * @return 停车信息管理
     */
    @Override
    public Tcxxbab selectTcxxbabById(Long id)
    {
        return tcxxbabMapper.selectTcxxbabById(id);
    }

    /**
     * 查询停车信息管理列表
     * 
     * @param tcxxbab 停车信息管理
     * @return 停车信息管理
     */
    @Override
    public List<Tcxxbab> selectTcxxbabList(Tcxxbab tcxxbab)
    {
        return tcxxbabMapper.selectTcxxbabList(tcxxbab);
    }

    /**
     * 新增停车信息管理
     * 
     * @param tcxxbab 停车信息管理
     * @return 结果
     */
    @Override
    public int insertTcxxbab(Tcxxbab tcxxbab)
    {
        return tcxxbabMapper.insertTcxxbab(tcxxbab);
    }

    /**
     * 修改停车信息管理
     * 
     * @param tcxxbab 停车信息管理
     * @return 结果
     */
    @Override
    public int updateTcxxbab(Tcxxbab tcxxbab)
    {
        return tcxxbabMapper.updateTcxxbab(tcxxbab);
    }

    /**
     * 批量删除停车信息管理
     * 
     * @param ids 需要删除的停车信息管理ID
     * @return 结果
     */
    @Override
    public int deleteTcxxbabByIds(Long[] ids)
    {
        return tcxxbabMapper.deleteTcxxbabByIds(ids);
    }

    /**
     * 删除停车信息管理信息
     * 
     * @param id 停车信息管理ID
     * @return 结果
     */
    @Override
    public int deleteTcxxbabById(Long id)
    {
        return tcxxbabMapper.deleteTcxxbabById(id);
    }

    
    /**
     * 根据小区管理员id获取小区所有停车场中的所有停车信息
     */

	@Override
	public List<Tcxxbab> selectTcxxbabListByXqAdminId(Long userId, Tcxxbab tcxxbab) {
		// TODO Auto-generated method stub
		return tcxxbabMapper.selectTcxxbabListByXqAdminId(userId, tcxxbab);
	}

	@Override
	public List<Tcxxbab> selectTcxxbabListDeptId(Long deptId,Long deptIdOfTree,Tcxxbab tcxxbab) {
		// TODO Auto-generated method stub
		return tcxxbabMapper.selectTcxxbabListDeptId(deptId,deptIdOfTree,tcxxbab);
	}

	@Override
	public List<Tcxxbab> selectTodayTcxxbabListByXqAdminId(Long userId, Tcxxbab tcxxbab) {
		// TODO Auto-generated method stub
		return tcxxbabMapper.selectTodayTcxxbabListByXqAdminId(userId, tcxxbab);
	}

	@Override
	public List<Tcxxbab> selectTodayTcxxbabListDeptId(Long deptId, Long deptIdOfTree, Tcxxbab tcxxbab) {
		// TODO Auto-generated method stub
		return tcxxbabMapper.selectTodayTcxxbabListDeptId(deptId,deptIdOfTree,tcxxbab);

	}
}

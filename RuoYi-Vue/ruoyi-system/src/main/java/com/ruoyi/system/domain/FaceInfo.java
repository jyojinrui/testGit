package com.ruoyi.system.domain;

public class FaceInfo extends Info{
	private String customId;
	private String name;
	private Integer personType;
	private Integer tempCardType;
	private String cardValidBegin;
	private String cardValidEnd;
	private Integer EffectNumber;
	private String pic;
	private Integer cardType;
	private String idCard;
	
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	public Integer getCardType() {
		return cardType;
	}
	public void setCardType(Integer cardType) {
		this.cardType = cardType;
	}
	public String getCustomId() {
		return customId;
	}
	public void setCustomId(String customId) {
		this.customId = customId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getPersonType() {
		return personType;
	}
	public void setPersonType(Integer personType) {
		this.personType = personType;
	}
	public Integer getTempCardType() {
		return tempCardType;
	}
	public void setTempCardType(Integer tempCardType) {
		this.tempCardType = tempCardType;
	}
	public String getCardValidBegin() {
		return cardValidBegin;
	}
	public void setCardValidBegin(String cardValidBegin) {
		this.cardValidBegin = cardValidBegin;
	}
	public String getCardValidEnd() {
		return cardValidEnd;
	}
	public void setCardValidEnd(String cardValidEnd) {
		this.cardValidEnd = cardValidEnd;
	}
	public Integer getEffectNumber() {
		return EffectNumber;
	}
	public void setEffectNumber(Integer effectNumber) {
		EffectNumber = effectNumber;
	}
	public String getPic() {
		return pic;
	}
	public void setPic(String pic) {
		this.pic = pic;
	}
	
	public FaceInfo(String customId, String name, Integer personType, Integer tempCardType, String cardValidBegin,
			String cardValidEnd, Integer effectNumber, String pic, Integer cardType, String idCard) {
		super();
		this.customId = customId;
		this.name = name;
		this.personType = personType;
		this.tempCardType = tempCardType;
		this.cardValidBegin = cardValidBegin;
		this.cardValidEnd = cardValidEnd;
		EffectNumber = effectNumber;
		this.pic = pic;
		this.cardType = cardType;
		this.idCard = idCard;
	}
	@Override
	public String toString() {
		return "FaceInfo [customId=" + customId + ", name=" + name + ", personType=" + personType + ", tempCardType="
				+ tempCardType + ", cardValidBegin=" + cardValidBegin + ", cardValidEnd=" + cardValidEnd
				+ ", EffectNumber=" + EffectNumber + ", pic=" + pic + ", cardType=" + cardType + ", idCard=" + idCard
				+ "]";
	}
	public FaceInfo() {
		super();
	}
	
	
	
}

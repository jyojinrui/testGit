package com.ruoyi.web.controller.common.service;

import java.util.List;

import com.ruoyi.web.controller.common.domain.SysAppApplyDetails;

/**
 * App和申请信息关联Service接口
 * 
 * @author zhangsan
 * @date 2020-12-05
 */
public interface ISysAppApplyDetailsService {
    /**
     * 根据id查询App和申请信息关联
     * 
     * @param id App和申请信息关联ID
     * @return App和申请信息关联
     */
    public SysAppApplyDetails selectSysAppApplyDetailsById(int id);

    /**
     * 根据id查询App和申请信息关联
     *
     * @param applyInfoId 申请信息ID
     * @return App和申请信息关联
     */
    public SysAppApplyDetails selectSysAppApplyDetailsByApplyInfoId(int applyInfoId);

    /**
     * 查询App和申请信息关联列表
     * 
     * @param sysAppApplyDetails App和申请信息关联
     * @return App和申请信息关联集合
     */
    public List<SysAppApplyDetails> selectSysAppApplyDetailsList(SysAppApplyDetails sysAppApplyDetails);
    
    /**
     * 根据ids查询App和申请信息关联列表
     * 
     * @param ids App和申请信息关联ID列表
     * @return App和申请信息关联集合
     */
    public List<SysAppApplyDetails> selectSysAppApplyDetailsListByids(int[] ids);

    /**
     * 新增App和申请信息关联
     * 
     * @param sysAppApplyDetails App和申请信息关联
     * @return 结果
     */
    public int insertSysAppApplyDetails(SysAppApplyDetails sysAppApplyDetails);

    /**
     * 修改App和申请信息关联
     * 
     * @param sysAppApplyDetails App和申请信息关联
     * @return 结果
     */
    public int updateSysAppApplyDetails(SysAppApplyDetails sysAppApplyDetails);

    /**
     * 批量删除App和申请信息关联
     * 
     * @param ids 需要删除的App和申请信息关联ID
     * @return 结果
     */
    public int deleteSysAppApplyDetailsByIds(int[] ids);

    /**
     * 删除App和申请信息关联信息
     * 
     * @param id App和申请信息关联ID
     * @return 结果
     */
    public int deleteSysAppApplyDetailsById(int id);

}

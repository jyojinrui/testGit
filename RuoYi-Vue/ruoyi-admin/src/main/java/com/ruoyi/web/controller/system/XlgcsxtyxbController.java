package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Xlgcsxtyxb;
import com.ruoyi.system.service.IXlgcsxtyxbService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 雪亮工程摄像头运行Controller
 * 
 * @author Carry
 * @date 2020-10-04
 */
@Api("雪亮工程摄像头运行接口")
@RestController
@RequestMapping("/system/xlgcsxtyxb")
public class XlgcsxtyxbController extends BaseController
{
    @Autowired
    private IXlgcsxtyxbService xlgcsxtyxbService;
    /**
     * 通过管理员id获取对用功能的列表信息
     */
    @ApiOperation("通过管理员id获取雪亮工程摄像头运行信息")
    @GetMapping("/listxlgcsxtyxbbyxqadminid")
    public TableDataInfo listxlgcsxtyxbbyxqadminid(Long userId,Xlgcsxtyxb xlgcsxtyxb)
    {
        startPage();
        List<Xlgcsxtyxb> list = xlgcsxtyxbService.selectXlgcsxtyxbListByXqAdminId(userId,xlgcsxtyxb);
        return getDataTable(list);
    }
    
    
    @GetMapping("/listxlgcsxtyxbbydeptid")
    public TableDataInfo listxlgcsxtyxbbydeptid(Long deptId,Xlgcsxtyxb xlgcsxtyxb)
    {
    	System.out.println("=========>");
    	System.out.println(deptId);
    	System.out.println(xlgcsxtyxb);
    	System.out.println("=========>");
        startPage();
        List<Xlgcsxtyxb> list = xlgcsxtyxbService.selectXlgcsxtyxbListByDeptId(deptId,xlgcsxtyxb);
        return getDataTable(list);
    }
    
    /**
     * 查询雪亮工程摄像头运行列表
     */
    @ApiOperation("查询雪亮工程摄像头运行列表")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtyxb:list')")
    @GetMapping("/list")
    public TableDataInfo list(Xlgcsxtyxb xlgcsxtyxb)
    {
        startPage();
        List<Xlgcsxtyxb> list = xlgcsxtyxbService.selectXlgcsxtyxbList(xlgcsxtyxb);
        return getDataTable(list);
    }

    /**
     * 导出雪亮工程摄像头运行列表
     */
    @ApiOperation("导出雪亮工程摄像头运行列表")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtyxb:export')")
    @Log(title = "雪亮工程摄像头运行", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Xlgcsxtyxb xlgcsxtyxb)
    {
        List<Xlgcsxtyxb> list = xlgcsxtyxbService.selectXlgcsxtyxbList(xlgcsxtyxb);
        ExcelUtil<Xlgcsxtyxb> util = new ExcelUtil<Xlgcsxtyxb>(Xlgcsxtyxb.class);
        return util.exportExcel(list, "xlgcsxtyxb");
    }

    /**
     * 获取雪亮工程摄像头运行详细信息
     */
    @ApiOperation("获取雪亮工程摄像头运行详细信息")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtyxb:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(xlgcsxtyxbService.selectXlgcsxtyxbById(id));
    }

    /**
     * 新增雪亮工程摄像头运行
     */
    @ApiOperation("新增雪亮工程摄像头运行")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtyxb:add')")
    @Log(title = "雪亮工程摄像头运行", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Xlgcsxtyxb xlgcsxtyxb)
    {
        return toAjax(xlgcsxtyxbService.insertXlgcsxtyxb(xlgcsxtyxb));
    }

    /**
     * 修改雪亮工程摄像头运行
     */
    @ApiOperation("修改雪亮工程摄像头运行")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtyxb:edit')")
    @Log(title = "雪亮工程摄像头运行", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Xlgcsxtyxb xlgcsxtyxb)
    {
        return toAjax(xlgcsxtyxbService.updateXlgcsxtyxb(xlgcsxtyxb));
    }

    /**
     * 删除雪亮工程摄像头运行
     */
    @ApiOperation("删除雪亮工程摄像头运行")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtyxb:remove')")
    @Log(title = "雪亮工程摄像头运行", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(xlgcsxtyxbService.deleteXlgcsxtyxbByIds(ids));
    }
}

package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Fkbab;
import com.ruoyi.system.service.IFkbabService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;


/**
 * 访客信息管理Controller
 * 
 * @author Carry
 * @date 2020-09-29
 */
//@Api("访客信息接口")
@ApiIgnore
@RestController
@RequestMapping("/system/fkbab")
public class FkbabController extends BaseController
{
    @Autowired
    private IFkbabService fkbabService;

    /**
     * 根据小区管理员id获取访客信息
     * @param userId
     * @param fkbab
     * @return
     */
    @ApiOperation("根据小区管理员id获取访客信息")
    @GetMapping("/listfkbabbyxqadminid")
    public TableDataInfo listFkbabByXqAdminId(Long userId,Fkbab fkbab)
    {
    	startPage();
    	List<Fkbab> list=fkbabService.selectFkbabListByXqAdminId(userId,fkbab);
        return getDataTable(list);
    }
    
    
    @GetMapping("/listfkbabbydeptid")
    public TableDataInfo listfkbabbydeptid(Long deptId,Long deptIdOfTree,Fkbab fkbab)
    {

    	startPage();
    	List<Fkbab> list=fkbabService.selectFkbabListByDeptId(deptId,deptIdOfTree,fkbab);
        return getDataTable(list);
    }
    
    /**
     * 查询访客信息管理列表
     */
    @ApiOperation("查询访客信息管理列表")
    @PreAuthorize("@ss.hasPermi('system:fkbab:list')")
    @GetMapping("/list")
    public TableDataInfo list(Fkbab fkbab)
    {
        startPage();
        List<Fkbab> list = fkbabService.selectFkbabList(fkbab);
        return getDataTable(list);
    }

    /**
     * 导出访客信息管理列表
     */
    @ApiOperation("导出访客信息管理列表")
    @PreAuthorize("@ss.hasPermi('system:fkbab:export')")
    @Log(title = "访客信息管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Fkbab fkbab)
    {
        List<Fkbab> list = fkbabService.selectFkbabList(fkbab);
        ExcelUtil<Fkbab> util = new ExcelUtil<Fkbab>(Fkbab.class);
        return util.exportExcel(list, "fkbab");
    }

    /**
     * 获取访客信息管理详细信息
     */
    @ApiOperation("获取访客信息管理详细信息")
    @PreAuthorize("@ss.hasPermi('system:fkbab:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(fkbabService.selectFkbabById(id));
    }

    /**
     * 新增访客信息管理
     */
    @ApiOperation("新增访客信息管理")
    @PreAuthorize("@ss.hasPermi('system:fkbab:add')")
    @Log(title = "访客信息管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Fkbab fkbab)
    {
        return toAjax(fkbabService.insertFkbab(fkbab));
    }

    /**
     * 修改访客信息管理
     */
    @ApiOperation("修改访客信息管理")
    @PreAuthorize("@ss.hasPermi('system:fkbab:edit')")
    @Log(title = "访客信息管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Fkbab fkbab)
    {
        return toAjax(fkbabService.updateFkbab(fkbab));
    }

    /**
     * 删除访客信息管理
     */
    @ApiOperation("删除访客信息管理")
    @PreAuthorize("@ss.hasPermi('system:fkbab:remove')")
    @Log(title = "访客信息管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(fkbabService.deleteFkbabByIds(ids));
    }
}

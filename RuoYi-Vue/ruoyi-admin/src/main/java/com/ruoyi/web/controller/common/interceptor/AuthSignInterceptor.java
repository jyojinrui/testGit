package com.ruoyi.web.controller.common.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.DESUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.sign.Md5Utils;
import com.ruoyi.framework.web.service.TokenService;
import com.ruoyi.web.controller.common.domain.SysApp;
import com.ruoyi.web.controller.common.service.AppService;

/**
 * 校验第三方系统的签名
 */
@Component
public class AuthSignInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    AppService appService;

    @Autowired
    TokenService tokenService;

    // 执行controller方法之前
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        LoginUser loginUser = tokenService.getLoginUser(request);
        // 是否放行请求
        boolean isAllow = false;
        if (StringUtils.isNull(loginUser)) {
            // 响应状态码
            String code = null;
            // 响应消息
            String resStr = null;
            // 是否继续执行
            boolean isContinue = true;
            // 获取app_key参数值
            String appKey = request.getParameter("app_key");
            // app_key参数值为空时给出相应的错误提示并拦截请求
            if (StringUtils.isEmpty(appKey)) {
                code = HttpStatus.APPKEY_CAN_NOT_NULL + "";
                resStr = Constants.APPKEY_CAN_NOT_NULL;
                isContinue = false;
            }
            // 获取sign参数值
            String sign = request.getParameter("sign");
            // sign参数值为空时给出相应的错误提示并拦截请求
            if (isContinue && StringUtils.isEmpty(sign)) {
                code = HttpStatus.SIGN_CAN_NOT_NULL + "";
                resStr = Constants.SIGN_CAN_NOT_NULL;
                isContinue = false;
            }
            // 获取timestamp参数值
            String timestampStr = request.getParameter("timestamp");
            // timestamp参数值为空时给出相应的错误提示并拦截请求
            if (isContinue && StringUtils.isEmpty(timestampStr)) {
                code = HttpStatus.TIMESTAMP_CAN_NOT_NULL + "";
                resStr = Constants.TIMESTAMP_CAN_NOT_NULL;
                isContinue = false;
            }
            if (isContinue) {
                try {
                    // 解析出timestamp数值
                    long timestamp = Long.parseLong(StringUtils.trim(timestampStr));
                    // 计算时间差
                    long timeDiff = Math.abs(System.currentTimeMillis() - timestamp);
                    // 如果时间差超过30min，则给出错误提示并拦截请求
                    if (timeDiff - Constants.TIMEDIFF_LIMIT > 0) {
                        code = HttpStatus.TIME_OVER_LIMIT + "";
                        resStr = Constants.TIME_OVER_LIMIT;
                        isContinue = false;
                    }
                    if (isContinue) {
                        // 根据appKey查询app的详细信息
                        SysApp sysApp = appService.selectSysAppByAppKey(appKey);
                        // 如果app不为空，则验证是否审核通过并在审核通过下校验签名是否合法
                        if (sysApp != null) {
                            // 如果通过审核，则校验签名是否合法
                            if (sysApp.getAuditState() == 1) {
                                // 加密原文
                                String plainText = timestamp + "#" + sysApp.getAppSecret();
                                // 进行md5加密得到签名
                                String geneSign = Md5Utils.hash(plainText);
                                // 如果生成的签名不为空，则比较请求的签名和生成的签名是否相同
                                if (StringUtils.isNotEmpty(geneSign)) {
                                    // 如果请求的签名和生成的签名相同，则放行请求
                                    if (sign.equals(geneSign)) {
                                        isAllow = true;
                                		// 请求数据
                                		String data = ServletUtils.getRequestBody(request);
                                        if (StringUtils.isNotEmpty(data)) {
											// 获取解密密钥
											String appSecret = sysApp.getAppSecret();
											
											data = DESUtils.decrypt(appSecret, data);
											if (StringUtils.isEmpty(data)) {
												isAllow=false;
												code="500";
												resStr="解密后字符串为空";
											}else{
												request.setAttribute("data", data);
											}
										}
                                    } else { // 签名不正确
                                        code = HttpStatus.SIGN_IS_NOT_CORRECT + "";
                                        resStr = Constants.SIGN_IS_NOT_CORRECT;
                                    }
                                } else { // 系统生成签名时出现错误
                                    code = HttpStatus.ERROR + "";
                                    resStr = Constants.SYSTEM_INTERNAL_ERROR;
                                }
                            } else { // appKey未审核通过
                                code = HttpStatus.APP_IS_NOT_PASS + "";
                                resStr = Constants.APP_IS_NOT_PASS;
                            }

                        } else { // appKey不存在
                            code = HttpStatus.APPKEY_IS_NOT_EXIST + "";
                            resStr = Constants.APPKEY_IS_NOT_EXIST;
                        }
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            // 如果请求被拦截，则返回错误消息给第三方
            if (!isAllow) {
                // 设置响应格式为json，字符编码为utf-8
                response.setContentType("application/json;charset=UTF-8");
                // 将错误状态码和错误消息内容封装为json对象
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("code", code);
                jsonObject.put("message", resStr);
                // 返回错误消息给第三方
                response.getWriter().write(jsonObject.toString());
            }
            isContinue = true;
        } else {
            isAllow = true;
        }
        return isAllow;
    }
}

package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Xqsxtrtyxb;
import com.ruoyi.system.domain.Xqsxtywzkb;
import com.ruoyi.system.service.IXqsxtywzkbService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 运维状况Controller
 * 
 * @author Carry
 * @date 2020-10-03
 */
@Api("运维状况接口")
@RestController
@RequestMapping("/system/xqsxtywzkb")
public class XqsxtywzkbController extends BaseController
{
    @Autowired
    private IXqsxtywzkbService xqsxtywzkbService;

    /**
     * 根据小区管理员id获取小区摄像头运维状况信息
     * @param userId
     * @param xqsxtywzkb
     * @return
     */
    @ApiOperation("根据小区管理员id获取小区摄像头运维状况信息")
    @GetMapping("/listxqsxtywzkbbyxqadminid")
    public TableDataInfo listxqsxtywzkbbyxqadminid(Long userId,Xqsxtywzkb xqsxtywzkb)
    {
        startPage();
        List<Xqsxtywzkb> list = xqsxtywzkbService.selectXqsxtywzkbListByXqAdminId(userId,xqsxtywzkb);
        return getDataTable(list);
    }
    
    @GetMapping("/listxqsxtywzkbbydeptid")
    public TableDataInfo listxqsxtywzkbbydeptid(Long deptId,Xqsxtywzkb xqsxtywzkb)
    {
        startPage();                              
        List<Xqsxtywzkb> list = xqsxtywzkbService.selectXqsxtywzkbListByDeptId(deptId,xqsxtywzkb);
        return getDataTable(list);
    }
    /**
     * 查询运维状况列表
     */
    @ApiOperation("查询运维状况列表")
    @PreAuthorize("@ss.hasPermi('system:xqsxtywzkb:list')")
    @GetMapping("/list")
    public TableDataInfo list(Xqsxtywzkb xqsxtywzkb)
    {
        startPage();
        List<Xqsxtywzkb> list = xqsxtywzkbService.selectXqsxtywzkbList(xqsxtywzkb);
        return getDataTable(list);
    }

    /**
     * 导出运维状况列表
     */
    @ApiOperation("导出运维状况列表")
    @PreAuthorize("@ss.hasPermi('system:xqsxtywzkb:export')")
    @Log(title = "运维状况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Xqsxtywzkb xqsxtywzkb)
    {
        List<Xqsxtywzkb> list = xqsxtywzkbService.selectXqsxtywzkbList(xqsxtywzkb);
        ExcelUtil<Xqsxtywzkb> util = new ExcelUtil<Xqsxtywzkb>(Xqsxtywzkb.class);
        return util.exportExcel(list, "xqsxtywzkb");
    }

    /**
     * 获取运维状况详细信息
     */
    @ApiOperation("获取运维状况详细信息")
    @PreAuthorize("@ss.hasPermi('system:xqsxtywzkb:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(xqsxtywzkbService.selectXqsxtywzkbById(id));
    }

    /**
     * 新增运维状况
     */
    @ApiOperation("新增运维状况")
    @PreAuthorize("@ss.hasPermi('system:xqsxtywzkb:add')")
    @Log(title = "运维状况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Xqsxtywzkb xqsxtywzkb)
    {
        return toAjax(xqsxtywzkbService.insertXqsxtywzkb(xqsxtywzkb));
    }

    /**
     * 修改运维状况
     */
    @ApiOperation("修改运维状况")
    @PreAuthorize("@ss.hasPermi('system:xqsxtywzkb:edit')")
    @Log(title = "运维状况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Xqsxtywzkb xqsxtywzkb)
    {
        return toAjax(xqsxtywzkbService.updateXqsxtywzkb(xqsxtywzkb));
    }

    /**
     * 删除运维状况
     */
    @ApiOperation("删除运维状况")
    @PreAuthorize("@ss.hasPermi('system:xqsxtywzkb:remove')")
    @Log(title = "运维状况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(xqsxtywzkbService.deleteXqsxtywzkbByIds(ids));
    }
}

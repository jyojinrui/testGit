package com.ruoyi.web.controller.common.service;

import java.util.List;

import com.ruoyi.web.controller.common.domain.SysApp;

/**
 * 第三方系统app service接口
 */
public interface AppService {

    /**
     * 根据条件查询app列表
     *
     * @param sysApp 条件封装成的app
     * @return app集合
     */
    public List<SysApp> selectSysAppList(SysApp sysApp);

    /**
     * 根据appKey查询对应的app记录
     * @param appKey
     * @return
     */
    public SysApp selectSysAppByAppKey(String appKey);

    /**
     * 根据appKeys查询app列表
     *
     * @param appKeys appKey列表
     * @return app集合
     */
    public List<SysApp> selectSysAppListByAppKeys(String[] appKeys);

    /**
     * 新增app
     * @param sysApp
     * @return
     */
    public int addSysApp(SysApp sysApp);

    /**
     * 修改app的审核状态和审核时间
     * @param sysApp
     * @return
     */
    public int updateSysApp(SysApp sysApp);

	/**
     * 修改app的审核人信息
     * @param sysApp
     * @return
     */
    public int updateSysAppAuditor(SysApp sysApp);

    /**
     * 通过appKey删除app
     *
     * @param appKey
     * @return 结果
     */
    public int deleteSysAppByAppKey(String appKey);

    /**
     * 批量删除app
     *
     * @param appKeys 需要删除的appKey列表
     * @return 结果
     */
    public int deleteSysAppByAppKeys(String[] appKeys);
}

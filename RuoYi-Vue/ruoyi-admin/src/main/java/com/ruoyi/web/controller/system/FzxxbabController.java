package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Fzxxbab;
import com.ruoyi.system.domain.Xqxxbab;
import com.ruoyi.system.service.IFzxxbabService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 房主信息Controller
 * 
 * @author Carry
 * @date 2020-10-05
 */

@RestController
@RequestMapping("/system/fzxxbab")
//@Api("房主信息接口")
public class FzxxbabController extends BaseController
{
    @Autowired
    private IFzxxbabService fzxxbabService;

//    @PreAuthorize("@ss.hasPermi('system:fzxxbab:getlistbyxqadminid')")
    @ApiOperation("根据小区管理员id和设置的信息条件获取房主信息列表")
    @GetMapping("/getlistbyxqadminid")
    public TableDataInfo getListByXqAdminId(Long userId,Fzxxbab fzxxbab)
    {
    	startPage();
    	List<Fzxxbab> list = fzxxbabService.selectFzxxbabListByXqAdminId(userId,fzxxbab);
        return getDataTable(list);
    }
    
    @GetMapping("/getlistbypcsadminid")
    public TableDataInfo getListByPcsAdminId(Long userId,Fzxxbab fzxxbab)
    {
    	startPage();
    	List<Fzxxbab> list = fzxxbabService.selectFzxxbabListByPcsAdminId(userId,fzxxbab);
        return getDataTable(list);
    }
    
    @GetMapping("/listfzxxbabbydeptid")
    public TableDataInfo getFzxxbabListByDeptId(Long deptId, Long deptIdOfTree, Fzxxbab fzxxbab)
    {
    	startPage();
    	List<Fzxxbab> list = fzxxbabService.selectFzxxbabListByDeptId(deptId,deptIdOfTree,fzxxbab);
        return getDataTable(list);
    }
    
    
    /**
     * 查询房主信息列表
     */
//    @ApiOperation("获取房主信息列表")
//    @PreAuthorize("@ss.hasPermi('system:fzxxbab:list')")
//    @ApiIgnore
    @GetMapping("/list")
   
    public TableDataInfo list(Fzxxbab fzxxbab)
    {
        startPage();
        List<Fzxxbab> list = fzxxbabService.selectFzxxbabList(fzxxbab);
        return getDataTable(list);
    }

    /**
     * 导出房主信息列表
     */
    @ApiOperation("导出房主信息列表")
    @PreAuthorize("@ss.hasPermi('system:fzxxbab:export')")
    @Log(title = "房主信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Fzxxbab fzxxbab)
    {
        List<Fzxxbab> list = fzxxbabService.selectFzxxbabList(fzxxbab);
        ExcelUtil<Fzxxbab> util = new ExcelUtil<Fzxxbab>(Fzxxbab.class);
        return util.exportExcel(list, "fzxxbab");
    }

    /**
     * 获取房主信息详细信息
     */
    @ApiOperation("根据房主id获取房主详细信息")
    @PreAuthorize("@ss.hasPermi('system:fzxxbab:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(fzxxbabService.selectFzxxbabById(id));
    }

    /**
     * 新增房主信息
     */
    @ApiOperation("新增房主信息")
//    @PreAuthorize("@ss.hasPermi('system:fzxxbab:add')")
    @Log(title = "房主信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Fzxxbab fzxxbab)
    {
    	int c=1;
//        fzxxbab.setFwyt(fzxxbab.getFwyt()+1);
        return toAjax(fzxxbabService.insertFzxxbab(fzxxbab));
    }
    
    /**
     * 新增房主信息
     */
    @ApiOperation("新增房主信息")
//    @PreAuthorize("@ss.hasPermi('system:fzxxbab:add')")
    @Log(title = "房主信息", businessType = BusinessType.INSERT)
    @PostMapping("/others")
    public AjaxResult add()
    {
		// 获取解密后的数据
		String data = (String) ServletUtils.getRequest().getAttribute("data");
		if (StringUtils.isEmpty(data)) {
			return AjaxResult.error(HttpStatus.BAD_REQUEST, "解密后字符串为空");
		}

		Fzxxbab fzxxbab = new Fzxxbab();
		try{
			fzxxbab = JSONObject.parseObject(data, Fzxxbab.class);
		}catch (Exception e) {
			// TODO: handle exception
			return AjaxResult.error("无法解析此字符串");
		}
        return toAjax(fzxxbabService.insertFzxxbab(fzxxbab));
    }

    /**
     * 修改房主信息
     */
    @ApiOperation("修改房主信息")
    @PreAuthorize("@ss.hasPermi('system:fzxxbab:edit')")
    @Log(title = "房主信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Fzxxbab fzxxbab)
    {
        return toAjax(fzxxbabService.updateFzxxbab(fzxxbab));
    }

    /**
     * 删除房主信息
     */
    @ApiOperation("删除房主信息")
    @PreAuthorize("@ss.hasPermi('system:fzxxbab:remove')")
    @Log(title = "房主信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(fzxxbabService.deleteFzxxbabByIds(ids));
    }
}

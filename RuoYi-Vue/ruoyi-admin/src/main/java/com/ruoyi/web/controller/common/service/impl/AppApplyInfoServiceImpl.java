package com.ruoyi.web.controller.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.web.controller.common.domain.AppApplyInfo;
import com.ruoyi.web.controller.common.mapper.AppApplyInfoMapper;
import com.ruoyi.web.controller.common.service.AppApplyInfoService;

/**
 * 第三方系统app service接口实现类
 */
@Service
public class AppApplyInfoServiceImpl implements AppApplyInfoService {
    @Autowired
    private AppApplyInfoMapper appApplyInfoMapper;

    /**
     * 根据id查询对应的app申请信息
     * @param id
     * @return
     */
    @Override
    public AppApplyInfo selectAppApplyInfoById(int id) {
        return appApplyInfoMapper.selectAppApplyInfoById(id);
    }

    /**
     * 根据ids查询app申请信息列表
     *
     * @param ids app申请信息ID列表
     * @return app申请信息集合
     */
    @Override
    public List<AppApplyInfo> selectAppApplyInfoListByIds(int[] ids) {
        return appApplyInfoMapper.selectAppApplyInfoListByIds(ids);
    }

    /**
     * 带条件查询对应的app申请信息
     * @param appApplyInfo
     * @return
     */
    @Override
    public List<AppApplyInfo> selectAppApplyInfoByConditions(AppApplyInfo appApplyInfo) {
        return appApplyInfoMapper.selectAppApplyInfoByConditions(appApplyInfo);
    }

    /**
     * 新增app申请信息
     * @param appApplyInfo
     * @return
     */
    @Override
    public int addAppApplyInfo(AppApplyInfo appApplyInfo) {
        return appApplyInfoMapper.addAppApplyInfo(appApplyInfo);
    }

    /**
     * 修改app申请信息
     * @param appApplyInfo
     * @return
     */
    @Override
    public int updateAppApplyInfo(AppApplyInfo appApplyInfo) {
        return appApplyInfoMapper.updateAppApplyInfo(appApplyInfo);
    }

    /**
     * 通过用户ID删除app申请信息
     *
     * @param id 编号
     * @return 结果
     */
    @Override
    public int deleteAppApplyInfoById(int id) {
        return appApplyInfoMapper.deleteAppApplyInfoById(id);
    }

    /**
     * 批量删除app申请信息
     *
     * @param ids 需要删除的编号列表
     * @return 结果
     */
    @Override
    public int deleteAppApplyInfoByIds(int[] ids) {
        return appApplyInfoMapper.deleteAppApplyInfoByIds(ids);
    }
}

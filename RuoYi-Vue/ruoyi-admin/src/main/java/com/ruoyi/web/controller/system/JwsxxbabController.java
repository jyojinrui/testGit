package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Jwsxxbab;
import com.ruoyi.system.service.IJwsxxbabService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 警务室信息Controller
 * 
 * @author Carry
 * @date 2020-10-27
 */
@RestController
@RequestMapping("/system/jwsxxbab")
public class JwsxxbabController extends BaseController
{
    @Autowired
    private IJwsxxbabService jwsxxbabService;

    /**
     * 查询警务室信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:jwsxxbab:list')")
    @GetMapping("/list")
    public TableDataInfo list(Jwsxxbab jwsxxbab)
    {
        startPage();
        List<Jwsxxbab> list = jwsxxbabService.selectJwsxxbabList(jwsxxbab);
        return getDataTable(list);
    }
    
    
    @PreAuthorize("@ss.hasPermi('system:jwsxxbab:list')")
    @GetMapping("/listjwsxxbabbydeptid")
    public TableDataInfo listjwsxxbabbydeptid(Long deptId,Long deptIdOfTree,Jwsxxbab jwsxxbab)
    {
        startPage();
        List<Jwsxxbab> list = jwsxxbabService.selectJwsxxbabListByDeptId(deptId,deptIdOfTree,jwsxxbab);
        return getDataTable(list);
    }

    /**
     * 导出警务室信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:jwsxxbab:export')")
    @Log(title = "警务室信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Jwsxxbab jwsxxbab)
    {
        List<Jwsxxbab> list = jwsxxbabService.selectJwsxxbabList(jwsxxbab);
        ExcelUtil<Jwsxxbab> util = new ExcelUtil<Jwsxxbab>(Jwsxxbab.class);
        return util.exportExcel(list, "jwsxxbab");
    }

    /**
     * 获取警务室信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:jwsxxbab:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(jwsxxbabService.selectJwsxxbabById(id));
    }

    /**
     * 新增警务室信息
     */
    @PreAuthorize("@ss.hasPermi('system:jwsxxbab:add')")
    @Log(title = "警务室信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Jwsxxbab jwsxxbab)
    {
        return toAjax(jwsxxbabService.insertJwsxxbab(jwsxxbab));
    }

    /**
     * 修改警务室信息
     */
    @PreAuthorize("@ss.hasPermi('system:jwsxxbab:edit')")
    @Log(title = "警务室信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Jwsxxbab jwsxxbab)
    {
        return toAjax(jwsxxbabService.updateJwsxxbab(jwsxxbab));
    }

    /**
     * 删除警务室信息
     */
    @PreAuthorize("@ss.hasPermi('system:jwsxxbab:remove')")
    @Log(title = "警务室信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(jwsxxbabService.deleteJwsxxbabByIds(ids));
    }
}

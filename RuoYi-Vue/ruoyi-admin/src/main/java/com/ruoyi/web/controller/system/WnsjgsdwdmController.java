package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Wnsjgsdwdm;
import com.ruoyi.system.service.IWnsjgsdwdmService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 渭南数据归属单位代码Controller
 * 
 * @author ruoyi
 * @date 2020-11-03
 */
@RestController
@RequestMapping("/system/wnsjgsdwdm")
public class WnsjgsdwdmController extends BaseController
{
    @Autowired
    private IWnsjgsdwdmService wnsjgsdwdmService;

    /**
     * 查询渭南数据归属单位代码列表
     */
    @PreAuthorize("@ss.hasPermi('system:wnsjgsdwdm:list')")
    @GetMapping("/list")
    public TableDataInfo list(Wnsjgsdwdm wnsjgsdwdm)
    {
        startPage();
        List<Wnsjgsdwdm> list = wnsjgsdwdmService.selectWnsjgsdwdmList(wnsjgsdwdm);
        return getDataTable(list);
    }

    /**
     * 查询渭南数据归属单位代码列表
     */
//    @PreAuthorize("@ss.hasPermi('system:wnsjgsdwdm:list')")
    @GetMapping("/getqgajlist")
    public TableDataInfo getqgajlist()
    {
        startPage();
        List<Wnsjgsdwdm> list = wnsjgsdwdmService.selectQgajList();
        return getDataTable(list);
    }
    
    @GetMapping("/getsubdeptbyqgajdeptid/{deptid}")
    public TableDataInfo getSubDeptByQgajDeptId(@PathVariable("deptid") Long deptid)
    {
        startPage();
        List<Wnsjgsdwdm> list = wnsjgsdwdmService.selectSubDeptByQgajDeptId(deptid);
        return getDataTable(list);
    }
    
    
    
    
    
    /**
     * 导出渭南数据归属单位代码列表
     */
    @PreAuthorize("@ss.hasPermi('system:wnsjgsdwdm:export')")
    @Log(title = "渭南数据归属单位代码", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Wnsjgsdwdm wnsjgsdwdm)
    {
        List<Wnsjgsdwdm> list = wnsjgsdwdmService.selectWnsjgsdwdmList(wnsjgsdwdm);
        ExcelUtil<Wnsjgsdwdm> util = new ExcelUtil<Wnsjgsdwdm>(Wnsjgsdwdm.class);
        return util.exportExcel(list, "wnsjgsdwdm");
    }

    /**
     * 获取渭南数据归属单位代码详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:wnsjgsdwdm:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(wnsjgsdwdmService.selectWnsjgsdwdmById(id));
    }

    /**
     * 新增渭南数据归属单位代码
     */
    @PreAuthorize("@ss.hasPermi('system:wnsjgsdwdm:add')")
    @Log(title = "渭南数据归属单位代码", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Wnsjgsdwdm wnsjgsdwdm)
    {
        return toAjax(wnsjgsdwdmService.insertWnsjgsdwdm(wnsjgsdwdm));
    }

    /**
     * 修改渭南数据归属单位代码
     */
    @PreAuthorize("@ss.hasPermi('system:wnsjgsdwdm:edit')")
    @Log(title = "渭南数据归属单位代码", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Wnsjgsdwdm wnsjgsdwdm)
    {
        return toAjax(wnsjgsdwdmService.updateWnsjgsdwdm(wnsjgsdwdm));
    }

    /**
     * 删除渭南数据归属单位代码
     */
    @PreAuthorize("@ss.hasPermi('system:wnsjgsdwdm:remove')")
    @Log(title = "渭南数据归属单位代码", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(wnsjgsdwdmService.deleteWnsjgsdwdmByIds(ids));
    }
}

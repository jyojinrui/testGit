package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Xlgcsxtsbywzkbab;
import com.ruoyi.system.service.IXlgcsxtsbywzkbabService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 雪亮工程摄像头设备运行状况Controller
 * 
 * @author Carry
 * @date 2020-10-03
 */
@Api("雪亮工程摄像头设备运行状况接口")
@RestController
@RequestMapping("/system/xlgcsxtsbywzkbab")
public class XlgcsxtsbywzkbabController extends BaseController
{
    @Autowired
    private IXlgcsxtsbywzkbabService xlgcsxtsbywzkbabService;

    /**
     * 查询雪亮工程摄像头设备运行状况列表
     */
    @ApiOperation("查询雪亮工程摄像头设备运行状况列表")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtsbywzkbab:list')")
    @GetMapping("/list")
    public TableDataInfo list(Xlgcsxtsbywzkbab xlgcsxtsbywzkbab)
    {
        startPage();
        List<Xlgcsxtsbywzkbab> list = xlgcsxtsbywzkbabService.selectXlgcsxtsbywzkbabList(xlgcsxtsbywzkbab);
        return getDataTable(list);
    }
    /**
     * 通过管理员id获取对用功能的列表信息
     */
    @ApiOperation("通过管理员id获取雪亮工程摄像头设备运行状况信息")
    @GetMapping("/listxlgcsxtsbywzkbabbyxqadminid")
    public TableDataInfo listxlgcsxtsbywzkbabbyxqadminid(Long userId,Xlgcsxtsbywzkbab xlgcsxtsbywzkbab)
    {
        startPage();
        List<Xlgcsxtsbywzkbab> list = xlgcsxtsbywzkbabService.selectXlgcsxtsbywzkbabListByXqAdminId(userId,xlgcsxtsbywzkbab);
        return getDataTable(list);
    }
    
    @GetMapping("/listxlgcsxtsbywzkbabbydeptid")
    public TableDataInfo listxlgcsxtsbywzkbabbydeptid(Long deptId,Xlgcsxtsbywzkbab xlgcsxtsbywzkbab)
    {
    	
        startPage();
        List<Xlgcsxtsbywzkbab> list = xlgcsxtsbywzkbabService.selectXlgcsxtsbywzkbabListByDeptId(deptId,xlgcsxtsbywzkbab);
        return getDataTable(list);
    }
    
    /**
     * 导出雪亮工程摄像头设备运行状况列表
     */
    @ApiOperation("导出雪亮工程摄像头设备运行状况列表")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtsbywzkbab:export')")
    @Log(title = "雪亮工程摄像头设备运行状况", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Xlgcsxtsbywzkbab xlgcsxtsbywzkbab)
    {
        List<Xlgcsxtsbywzkbab> list = xlgcsxtsbywzkbabService.selectXlgcsxtsbywzkbabList(xlgcsxtsbywzkbab);
        ExcelUtil<Xlgcsxtsbywzkbab> util = new ExcelUtil<Xlgcsxtsbywzkbab>(Xlgcsxtsbywzkbab.class);
        return util.exportExcel(list, "xlgcsxtsbywzkbab");
    }

    /**
     * 获取雪亮工程摄像头设备运行状况详细信息
     */
    @ApiOperation("获取雪亮工程摄像头设备运行状况详细信息")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtsbywzkbab:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(xlgcsxtsbywzkbabService.selectXlgcsxtsbywzkbabById(id));
    }

    /**
     * 新增雪亮工程摄像头设备运行状况
     */
    @ApiOperation("新增雪亮工程摄像头设备运行状况")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtsbywzkbab:add')")
    @Log(title = "雪亮工程摄像头设备运行状况", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Xlgcsxtsbywzkbab xlgcsxtsbywzkbab)
    {
        return toAjax(xlgcsxtsbywzkbabService.insertXlgcsxtsbywzkbab(xlgcsxtsbywzkbab));
    }

    /**
     * 修改雪亮工程摄像头设备运行状况
     */
    @ApiOperation("修改雪亮工程摄像头设备运行状况")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtsbywzkbab:edit')")
    @Log(title = "雪亮工程摄像头设备运行状况", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Xlgcsxtsbywzkbab xlgcsxtsbywzkbab)
    {
        return toAjax(xlgcsxtsbywzkbabService.updateXlgcsxtsbywzkbab(xlgcsxtsbywzkbab));
    }

    /**
     * 删除雪亮工程摄像头设备运行状况
     */
    @ApiOperation("删除雪亮工程摄像头设备运行状况")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtsbywzkbab:remove')")
    @Log(title = "雪亮工程摄像头设备运行状况", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(xlgcsxtsbywzkbabService.deleteXlgcsxtsbywzkbabByIds(ids));
    }
}

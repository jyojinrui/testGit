package com.ruoyi.web.controller.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.web.controller.common.domain.SysAppApplyDetails;
import com.ruoyi.web.controller.common.service.ISysAppApplyDetailsService;

/**
 * App和申请信息关联Controller
 * 
 * @author zhangsan
 * @date 2020-12-05
 */
@RestController
@RequestMapping("${common.appApplyDetails.mainPath}")
public class SysAppApplyDetailsController extends BaseController {
    @Autowired
    private ISysAppApplyDetailsService sysAppApplyDetailsService;

    /**
     * 查询App和申请信息关联列表
     */
//    @PreAuthorize("@ss.hasPermi('common:appApplyDetails:list')")
    @GetMapping("${common.appApplyDetails.subPath.listPath}")
    public TableDataInfo list(SysAppApplyDetails sysAppApplyDetails) {
        startPage();
        List<SysAppApplyDetails> list = sysAppApplyDetailsService.selectSysAppApplyDetailsList(sysAppApplyDetails);
        return getDataTable(list);
    }



    /**
     * 导出符合查询条件的App和申请信息关联列表
     */
//    @PreAuthorize("@ss.hasPermi('common:appApplyDetails:export')")
    @Log(title = "App和申请信息关联", businessType = BusinessType.EXPORT)
    @GetMapping("${common.appApplyDetails.subPath.exportPath}")
    public AjaxResult export(SysAppApplyDetails sysAppApplyDetails) {
        List<SysAppApplyDetails> list = sysAppApplyDetailsService.selectSysAppApplyDetailsList(sysAppApplyDetails);
        ExcelUtil<SysAppApplyDetails> util = new ExcelUtil<SysAppApplyDetails>(SysAppApplyDetails.class);
        return util.exportExcel(list, "details");
    }
    
    /**
     * 导出选中的App和申请信息关联列表
     */
//    @PreAuthorize("@ss.hasPermi('common:appApplyDetails:export')")
    @Log(title = "App和申请信息关联", businessType = BusinessType.EXPORT)
    @GetMapping("${common.appApplyDetails.subPath.exportByIdsPath}")
    public AjaxResult export(@PathVariable int[] ids) {
        List<SysAppApplyDetails> list = sysAppApplyDetailsService.selectSysAppApplyDetailsListByids(ids);
        ExcelUtil<SysAppApplyDetails> util = new ExcelUtil<SysAppApplyDetails>(SysAppApplyDetails.class);
        return util.exportExcel(list, "details");
    }

    /**
     * 根据id获取App和申请信息关联详细信息
     */
//    @PreAuthorize("@ss.hasPermi('common:appApplyDetails:query')")
    @GetMapping("${common.appApplyDetails.subPath.getByIdPath}")
    public AjaxResult getInfoById(@PathVariable("id") Integer id) {
        return AjaxResult.success(sysAppApplyDetailsService.selectSysAppApplyDetailsById(id));
    }

    /**
     * 根据applyId获取App和申请信息关联详细信息
     */
//    @PreAuthorize("@ss.hasPermi('common:appApplyDetails:query')")
    @GetMapping("${common.appApplyDetails.subPath.getByApplyIdPath}")
    public AjaxResult getInfoByApplyId(@PathVariable("applyInfoId") int applyInfoId) {
        return AjaxResult.success(sysAppApplyDetailsService.selectSysAppApplyDetailsByApplyInfoId(applyInfoId));
    }

    /**
     * 新增App和申请信息关联
     */
//    @PreAuthorize("@ss.hasPermi('common:appApplyDetails:add')")
    @Log(title = "App和申请信息关联", businessType = BusinessType.INSERT)
    @PostMapping("${common.appApplyDetails.subPath.addPath}")
    public AjaxResult add(@RequestBody SysAppApplyDetails sysAppApplyDetails) {
        return toAjax(sysAppApplyDetailsService.insertSysAppApplyDetails(sysAppApplyDetails));
    }

    /**
     * 修改App和申请信息关联
     */
//    @PreAuthorize("@ss.hasPermi('common:appApplyDetails:edit')")
    @Log(title = "App和申请信息关联", businessType = BusinessType.UPDATE)
    @PutMapping("${common.appApplyDetails.subPath.editPath}")
    public AjaxResult edit(@RequestBody SysAppApplyDetails sysAppApplyDetails) {
        return toAjax(sysAppApplyDetailsService.updateSysAppApplyDetails(sysAppApplyDetails));
    }

    /**
     * 删除App和申请信息关联
     */
//    @PreAuthorize("@ss.hasPermi('common:appApplyDetails:remove')")
    @Log(title = "App和申请信息关联", businessType = BusinessType.DELETE)
	@DeleteMapping("${common.appApplyDetails.subPath.delByIdsPath}")
    public AjaxResult remove(@PathVariable int[] ids) {
        return toAjax(sysAppApplyDetailsService.deleteSysAppApplyDetailsByIds(ids));
    }
}

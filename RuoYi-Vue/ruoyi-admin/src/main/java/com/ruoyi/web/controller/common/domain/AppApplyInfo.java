package com.ruoyi.web.controller.common.domain;

import java.util.Date;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 第三方公司基本信息实体类
 */
public class AppApplyInfo extends BaseEntity {
    /**
     * 编号
     */
    @Excel(name = "编号")
    private int id;

    /**
     * 申请对接的公司名称
     */
    @Excel(name = "公司名称", width = 30)
    private String companyName;

    /**
     * 申请对接的应用名称
     */
    @Excel(name = "应用名称", width = 25)
    private String applicationName;

    /**
     * 申请人姓名
     */
    @Excel(name = "申请人")
    private String applicantName;

    /**
     * 申请人联系电话
     */
    @Excel(name = "联系电话")
    private String telPhone;

    /**
     * 申请时间
     */
    @Excel(name = "申请时间", width = 25, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date applyTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public String getTelPhone() {
        return telPhone;
    }

    public void setTelPhone(String telPhone) {
        this.telPhone = telPhone;
    }

    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    @Override
    public String toString() {
        return "AppApplyInfo{" +
                "id=" + id +
                ", companyName='" + companyName + '\'' +
                ", applicationName='" + applicationName + '\'' +
                ", applicantName='" + applicantName + '\'' +
                ", telPhone='" + telPhone + '\'' +
                ", applyTime=" + applyTime +
                '}';
    }
}

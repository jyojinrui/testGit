package com.ruoyi.web.controller.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.web.controller.common.domain.SysAppAudit;
import com.ruoyi.web.controller.common.mapper.SysAppAuditMapper;
import com.ruoyi.web.controller.common.service.ISysAppAuditService;

/**
 * 系统app审核Service业务层处理
 * 
 * @author zhangsan
 * @date 2020-12-12
 */
@Service
public class SysAppAuditServiceImpl implements ISysAppAuditService {
    @Autowired
    private SysAppAuditMapper sysAppAuditMapper;

    /**
     * 查询系统app审核
     * 
     * @param id 系统app审核ID
     * @return 系统app审核
     */
    @Override
    public SysAppAudit selectSysAppAuditById(String id) {
        return sysAppAuditMapper.selectSysAppAuditById(id);
    }

    /**
     * 查询系统app审核列表
     * 
     * @param sysAppAudit 系统app审核
     * @return 系统app审核
     */
    @Override
    public List<SysAppAudit> selectSysAppAuditList(SysAppAudit sysAppAudit) {
        return sysAppAuditMapper.selectSysAppAuditList(sysAppAudit);
    }
    
    /**
     * 根据ids查询系统app审核列表
     * 
     * @param ids 系统app审核ID列表
     * @return 系统app审核集合
     */
    public List<SysAppAudit> selectSysAppAuditListByids(String[] ids) {
    	return sysAppAuditMapper.selectSysAppAuditListByids(ids);
    }

    /**
     * 新增系统app审核
     * 
     * @param sysAppAudit 系统app审核
     * @return 结果
     */
    @Override
    public int insertSysAppAudit(SysAppAudit sysAppAudit) {
        // 随机生成15位长度的uuid并赋给审核人编号属性
        sysAppAudit.setId(IdUtils.generateAuditorId());
        return sysAppAuditMapper.insertSysAppAudit(sysAppAudit);
    }

    /**
     * 修改系统app审核
     * 
     * @param sysAppAudit 系统app审核
     * @return 结果
     */
    @Override
    public int updateSysAppAudit(SysAppAudit sysAppAudit) {
        return sysAppAuditMapper.updateSysAppAudit(sysAppAudit);
    }

    /**
     * 批量删除系统app审核
     * 
     * @param ids 需要删除的系统app审核ID
     * @return 结果
     */
    @Override
    public int deleteSysAppAuditByIds(String[] ids) {
        return sysAppAuditMapper.deleteSysAppAuditByIds(ids);
    }

    /**
     * 删除系统app审核信息
     * 
     * @param id 系统app审核ID
     * @return 结果
     */
    @Override
    public int deleteSysAppAuditById(String id) {
        return sysAppAuditMapper.deleteSysAppAuditById(id);
    }
}

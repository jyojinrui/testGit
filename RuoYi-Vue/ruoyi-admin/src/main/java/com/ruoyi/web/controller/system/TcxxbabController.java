package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Tcxxbab;
import com.ruoyi.system.service.ITcxxbabService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 停车信息管理Controller
 * 
 * @author Carry
 * @date 2020-09-30
 */
@Api("停车信息管理接口")
@RestController
@RequestMapping("/system/tcxxbab")
public class TcxxbabController extends BaseController
{
    @Autowired
    private ITcxxbabService tcxxbabService;

    /**
     * listtcxxbabbyxqadminid
     * 通过小区管理员id获取小区所有停车场的停车信息   
     */
    @ApiOperation("通过小区管理员id获取小区所有停车场的停车信息 ")
    @GetMapping("/listtcxxbabbyxqadminid")
    public TableDataInfo listtcxxbabbyxqadminid(Long userId,Tcxxbab tcxxbab)
    {
    	startPage();
        List<Tcxxbab> list = tcxxbabService.selectTcxxbabListByXqAdminId(userId,tcxxbab);
        return getDataTable(list);
    }
    
    
    @GetMapping("/listtcxxbabbydeptid")
    public TableDataInfo listtcxxbabbydeptid(Long deptId,Long deptIdOfTree, Tcxxbab tcxxbab)
    {
    	startPage();
        List<Tcxxbab> list = tcxxbabService.selectTcxxbabListDeptId(deptId,deptIdOfTree,tcxxbab);
        return getDataTable(list);
    }
    
    /**
     * listtcxxbabbyxqadminid
     * 通过小区管理员id获取小区所有停车场的停车信息   
     */
    @ApiOperation("通过小区管理员id获取小区所有停车场的停车信息 ")
    @GetMapping("/listtodaytcxxbabbyxqadminid")
    public TableDataInfo listtodaytcxxbabbyxqadminid(Long userId,Tcxxbab tcxxbab)
    {
    	startPage();
        List<Tcxxbab> list = tcxxbabService.selectTodayTcxxbabListByXqAdminId(userId,tcxxbab);
        return getDataTable(list);
    }
    
    
    @GetMapping("/listtodaytcxxbabbydeptid")
    public TableDataInfo listtodaytcxxbabbydeptid(Long deptId,Long deptIdOfTree, Tcxxbab tcxxbab)
    {
    	startPage();
        List<Tcxxbab> list = tcxxbabService.selectTodayTcxxbabListDeptId(deptId,deptIdOfTree,tcxxbab);
        return getDataTable(list);
    }
    
    
    
    
    
    /**
     * 查询停车信息管理列表
     */
    @ApiOperation("查询停车信息管理列表")
    @PreAuthorize("@ss.hasPermi('system:tcxxbab:list')")
    @GetMapping("/list")
    public TableDataInfo list(Tcxxbab tcxxbab)
    {
        startPage();
        List<Tcxxbab> list = tcxxbabService.selectTcxxbabList(tcxxbab);
        return getDataTable(list);
    }

    /**
     * 导出停车信息管理列表
     */
    @ApiOperation("导出停车信息管理列表")
    @PreAuthorize("@ss.hasPermi('system:tcxxbab:export')")
    @Log(title = "停车信息管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Tcxxbab tcxxbab)
    {
        List<Tcxxbab> list = tcxxbabService.selectTcxxbabList(tcxxbab);
        ExcelUtil<Tcxxbab> util = new ExcelUtil<Tcxxbab>(Tcxxbab.class);
        return util.exportExcel(list, "tcxxbab");
    }

    /**
     * 获取停车信息管理详细信息
     */
    @ApiOperation("获取停车信息管理详细信息")
    @PreAuthorize("@ss.hasPermi('system:tcxxbab:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(tcxxbabService.selectTcxxbabById(id));
    }

    /**
     * 新增停车信息管理
     */
    @ApiOperation("新增停车信息管理")
    @PreAuthorize("@ss.hasPermi('system:tcxxbab:add')")
    @Log(title = "停车信息管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Tcxxbab tcxxbab)
    {
        return toAjax(tcxxbabService.insertTcxxbab(tcxxbab));
    }

    /**
     * 修改停车信息管理
     */
    @ApiOperation("修改停车信息管理")
    @PreAuthorize("@ss.hasPermi('system:tcxxbab:edit')")
    @Log(title = "停车信息管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Tcxxbab tcxxbab)
    {
        return toAjax(tcxxbabService.updateTcxxbab(tcxxbab));
    }

    /**
     * 删除停车信息管理
     */
    @ApiOperation("删除停车信息管理")
    @PreAuthorize("@ss.hasPermi('system:tcxxbab:remove')")
    @Log(title = "停车信息管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tcxxbabService.deleteTcxxbabByIds(ids));
    }
}

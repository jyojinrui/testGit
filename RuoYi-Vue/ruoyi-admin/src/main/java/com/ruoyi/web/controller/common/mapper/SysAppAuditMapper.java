package com.ruoyi.web.controller.common.mapper;

import java.util.List;

import com.ruoyi.web.controller.common.domain.SysAppAudit;

/**
 * 系统app审核Mapper接口
 * 
 * @author zhangsan
 * @date 2020-12-12
 */
public interface SysAppAuditMapper {
    /**
     * 查询系统app审核
     * 
     * @param id 系统app审核ID
     * @return 系统app审核
     */
    public SysAppAudit selectSysAppAuditById(String id);

    /**
     * 查询系统app审核列表
     * 
     * @param sysAppAudit 系统app审核
     * @return 系统app审核集合
     */
    public List<SysAppAudit> selectSysAppAuditList(SysAppAudit sysAppAudit);
    
    /**
     * 根据ids查询系统app审核列表
     * 
     * @param ids 系统app审核ID列表
     * @return 系统app审核集合
     */
    public List<SysAppAudit> selectSysAppAuditListByids(String[] ids);

    /**
     * 新增系统app审核
     * 
     * @param sysAppAudit 系统app审核
     * @return 结果
     */
    public int insertSysAppAudit(SysAppAudit sysAppAudit);

    /**
     * 修改系统app审核
     * 
     * @param sysAppAudit 系统app审核
     * @return 结果
     */
    public int updateSysAppAudit(SysAppAudit sysAppAudit);

    /**
     * 删除系统app审核
     * 
     * @param id 系统app审核ID
     * @return 结果
     */
    public int deleteSysAppAuditById(String id);

    /**
     * 批量删除系统app审核
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysAppAuditByIds(String[] ids);
}

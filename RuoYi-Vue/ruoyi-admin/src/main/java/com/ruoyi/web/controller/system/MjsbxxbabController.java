package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Mjsbxxbab;
import com.ruoyi.system.service.IMjsbxxbabService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * 门禁设备管理Controller
 * 
 * @author Carry
 * @date 2020-09-29
 */
@Api("门禁设备管理接口")
@RestController
@RequestMapping("/system/mjsbxxbab")
public class MjsbxxbabController extends BaseController
{
    @Autowired
    private IMjsbxxbabService mjsbxxbabService;

    /**
     * 查询门禁设备管理列表
     */
    @ApiOperation("查询门禁设备管理列表")
    @PreAuthorize("@ss.hasPermi('system:mjsbxxbab:list')")
    @GetMapping("/list")
    public TableDataInfo list(Mjsbxxbab mjsbxxbab)
    {
        startPage();
        List<Mjsbxxbab> list = mjsbxxbabService.selectMjsbxxbabList(mjsbxxbab);
        return getDataTable(list);
    }

    /**
     * 根据小区管理员id获取管辖门禁信息
     * @param userId
     * @param mjsbxxbab
     * @return
     */
    @ApiOperation("根据小区管理员id获取管辖门禁信息")
    @GetMapping("/listmjsbxxbabbyxaadminid")
    public TableDataInfo listmjsbxxbabbyxaadminid(Long userId,Mjsbxxbab mjsbxxbab) {
    	startPage();
    	List<Mjsbxxbab> list = mjsbxxbabService.selectMjsbxxbabListByXqAdminId(userId,mjsbxxbab);
        return getDataTable(list);
    }
    
    
    @GetMapping("/listmjsbxxbabbydeptid")
    public TableDataInfo listmjsbxxbabbydeptid(Long deptId,Long deptIdOfTree,Mjsbxxbab mjsbxxbab) {
    	startPage();
    	List<Mjsbxxbab> list = mjsbxxbabService.selectMjsbxxbabListByDeptId(deptId,deptIdOfTree,mjsbxxbab);
        return getDataTable(list);
    }
    /**
     * 导出门禁设备管理列表
     */
    @ApiOperation("导出门禁设备管理列表")
    @PreAuthorize("@ss.hasPermi('system:mjsbxxbab:export')")
    @Log(title = "门禁设备管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Mjsbxxbab mjsbxxbab)
    {
        List<Mjsbxxbab> list = mjsbxxbabService.selectMjsbxxbabList(mjsbxxbab);
        ExcelUtil<Mjsbxxbab> util = new ExcelUtil<Mjsbxxbab>(Mjsbxxbab.class);
        return util.exportExcel(list, "mjsbxxbab");
    }

    /**
     * 获取门禁设备管理详细信息
     */
    @ApiOperation("获取门禁设备管理详细信息")
    @PreAuthorize("@ss.hasPermi('system:mjsbxxbab:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(mjsbxxbabService.selectMjsbxxbabById(id));
    }

    /**
     * 新增门禁设备管理
     */
    @ApiOperation("新增门禁设备管理")
    @PreAuthorize("@ss.hasPermi('system:mjsbxxbab:add')")
    @Log(title = "门禁设备管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Mjsbxxbab mjsbxxbab)
    {
        return toAjax(mjsbxxbabService.insertMjsbxxbab(mjsbxxbab));
    }

    /**
     * 修改门禁设备管理
     */
    @ApiOperation("修改门禁设备管理")
    @PreAuthorize("@ss.hasPermi('system:mjsbxxbab:edit')")
    @Log(title = "门禁设备管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Mjsbxxbab mjsbxxbab)
    {
        return toAjax(mjsbxxbabService.updateMjsbxxbab(mjsbxxbab));
    }

    /**
     * 删除门禁设备管理
     */
    @ApiOperation("删除门禁设备管理")
    @PreAuthorize("@ss.hasPermi('system:mjsbxxbab:remove')")
    @Log(title = "门禁设备管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mjsbxxbabService.deleteMjsbxxbabByIds(ids));
    }
}

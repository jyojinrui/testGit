package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Jzrylkxxbab;
import com.ruoyi.system.service.IJzrylkxxbabService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * 离开人员管理Controller
 * 
 * @author ruoyi
 * @date 2020-09-28
 */
@Api("离开人员管理接口")
@RestController
@RequestMapping("/system/jzrylkxxbab")
public class JzrylkxxbabController extends BaseController
{
    @Autowired
    private IJzrylkxxbabService jzrylkxxbabService;

    /**
     * 根据小区管理员id查询离开人员信息列表
     */
    @ApiOperation("根据小区管理员id查询离开人员信息列表")
    @GetMapping("/listjzrylkxxbabbyxqadminid")
    public TableDataInfo listjzrylkxxbabbyxqadminid(Long userId,Jzrylkxxbab jzrylkxxbab ){
    	startPage();
        List<Jzrylkxxbab> list = jzrylkxxbabService.selectJzrylkxxbabListByXqAdminId(userId,jzrylkxxbab);
        return getDataTable(list);
    }
    
    
    @GetMapping("/listjzrylkxxbabbydeptid")
    public TableDataInfo listJzrylkxxbabByDeptId(Long deptId,Long deptIdOfTree,Jzrylkxxbab jzrylkxxbab ){
    	startPage();
        List<Jzrylkxxbab> list = jzrylkxxbabService.selectJzrylkxxbabListByDeptId(deptId,deptIdOfTree,jzrylkxxbab);
        return getDataTable(list);
    }
    /**
     * 查询离开人员管理列表
     */
    @ApiOperation("查询离开人员管理列表")
    @PreAuthorize("@ss.hasPermi('system:jzrylkxxbab:list')")
    @GetMapping("/list")
    public TableDataInfo list(Jzrylkxxbab jzrylkxxbab)
    {
        startPage();
        List<Jzrylkxxbab> list = jzrylkxxbabService.selectJzrylkxxbabList(jzrylkxxbab);
        return getDataTable(list);
    }

    /**
     * 导出离开人员管理列表
     */
    @ApiOperation("导出离开人员管理列表")
    @PreAuthorize("@ss.hasPermi('system:jzrylkxxbab:export')")
    @Log(title = "离开人员管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Jzrylkxxbab jzrylkxxbab)
    {
        List<Jzrylkxxbab> list = jzrylkxxbabService.selectJzrylkxxbabList(jzrylkxxbab);
        ExcelUtil<Jzrylkxxbab> util = new ExcelUtil<Jzrylkxxbab>(Jzrylkxxbab.class);
        return util.exportExcel(list, "jzrylkxxbab");
    }

    /**
     * 获取离开人员管理详细信息
     */
    @ApiOperation("获取离开人员管理详细信息")
    @PreAuthorize("@ss.hasPermi('system:jzrylkxxbab:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(jzrylkxxbabService.selectJzrylkxxbabById(id));
    }

    /**
     * 新增离开人员管理
     */
    @ApiOperation("新增离开人员管理")
    @PreAuthorize("@ss.hasPermi('system:jzrylkxxbab:add')")
    @Log(title = "离开人员管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Jzrylkxxbab jzrylkxxbab)
    {
    	System.out.println("====>"+jzrylkxxbab);
        return toAjax(jzrylkxxbabService.insertJzrylkxxbab(jzrylkxxbab));
    }

    /**
     * 修改离开人员管理
     */
    @ApiOperation("修改离开人员管理")
    @PreAuthorize("@ss.hasPermi('system:jzrylkxxbab:edit')")
    @Log(title = "离开人员管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Jzrylkxxbab jzrylkxxbab)
    {
        return toAjax(jzrylkxxbabService.updateJzrylkxxbab(jzrylkxxbab));
    }

    /**
     * 删除离开人员管理
     */
    @ApiOperation("删除离开人员管理")
    @PreAuthorize("@ss.hasPermi('system:jzrylkxxbab:remove')")
    @Log(title = "离开人员管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(jzrylkxxbabService.deleteJzrylkxxbabByIds(ids));
    }
}

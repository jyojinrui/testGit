package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Xqsxtrtyxb;
import com.ruoyi.system.service.IXqsxtrtyxbService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 摄像头人体运行Controller
 * 
 * @author Carry
 * @date 2020-10-03
 */
@Api("摄像头人体运行接口")
@RestController
@RequestMapping("/system/xqsxtrtyxb")
public class XqsxtrtyxbController extends BaseController
{
    @Autowired
    private IXqsxtrtyxbService xqsxtrtyxbService;

    
    /**
     * 根据小区管理员id获取小区摄像头人体运行情况信息
     * @param userId
     * @param xqsxtrtyxb
     * @return
     */   
    @ApiOperation("根据小区管理员id获取小区摄像头人体运行情况信息")
    @GetMapping("/listxqsxtrtyxbbyxqadminid")
    public TableDataInfo listxqsxtywzkbbyxqadminid(Long userId,Xqsxtrtyxb xqsxtrtyxb)
    {
        startPage();                              
        List<Xqsxtrtyxb> list = xqsxtrtyxbService.selectXqsxtrtyxbListByXqAdminId(userId,xqsxtrtyxb);
        return getDataTable(list);
    }
    
    
    
    @GetMapping("/listxqsxtrtyxbbydeptid")
    public TableDataInfo listxqsxtrtyxbbydeptid(Long deptId,Xqsxtrtyxb xqsxtrtyxb)
    {
        startPage();                              
        List<Xqsxtrtyxb> list = xqsxtrtyxbService.selectXqsxtrtyxbListByDeptId(deptId,xqsxtrtyxb);
        return getDataTable(list);
    }
    
    /**
     * 查询摄像头人体运行列表
     */
    @ApiOperation("查询摄像头人体运行列表")
    @PreAuthorize("@ss.hasPermi('system:xqsxtrtyxb:list')")
    @GetMapping("/list")
    public TableDataInfo list(Xqsxtrtyxb xqsxtrtyxb)
    {
        startPage();
        List<Xqsxtrtyxb> list = xqsxtrtyxbService.selectXqsxtrtyxbList(xqsxtrtyxb);
        return getDataTable(list);
    }

    /**
     * 导出摄像头人体运行列表
     */
    @ApiOperation("导出摄像头人体运行列表")
    @PreAuthorize("@ss.hasPermi('system:xqsxtrtyxb:export')")
    @Log(title = "摄像头人体运行", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Xqsxtrtyxb xqsxtrtyxb)
    {
        List<Xqsxtrtyxb> list = xqsxtrtyxbService.selectXqsxtrtyxbList(xqsxtrtyxb);
        ExcelUtil<Xqsxtrtyxb> util = new ExcelUtil<Xqsxtrtyxb>(Xqsxtrtyxb.class);
        return util.exportExcel(list, "xqsxtrtyxb");
    }

    /**
     * 获取摄像头人体运行详细信息
     */
    @ApiOperation("获取摄像头人体运行详细信息")
    @PreAuthorize("@ss.hasPermi('system:xqsxtrtyxb:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(xqsxtrtyxbService.selectXqsxtrtyxbById(id));
    }

    /**
     * 新增摄像头人体运行
     */
    @ApiOperation("新增摄像头人体运行")
    @PreAuthorize("@ss.hasPermi('system:xqsxtrtyxb:add')")
    @Log(title = "摄像头人体运行", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Xqsxtrtyxb xqsxtrtyxb)
    {
        return toAjax(xqsxtrtyxbService.insertXqsxtrtyxb(xqsxtrtyxb));
    }

    /**
     * 修改摄像头人体运行
     */
    @ApiOperation("修改摄像头人体运行")
    @PreAuthorize("@ss.hasPermi('system:xqsxtrtyxb:edit')")
    @Log(title = "摄像头人体运行", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Xqsxtrtyxb xqsxtrtyxb)
    {
        return toAjax(xqsxtrtyxbService.updateXqsxtrtyxb(xqsxtrtyxb));
    }

    /**
     * 删除摄像头人体运行
     */
    @ApiOperation("删除摄像头人体运行")
    @PreAuthorize("@ss.hasPermi('system:xqsxtrtyxb:remove')")
    @Log(title = "摄像头人体运行", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(xqsxtrtyxbService.deleteXqsxtrtyxbByIds(ids));
    }
}

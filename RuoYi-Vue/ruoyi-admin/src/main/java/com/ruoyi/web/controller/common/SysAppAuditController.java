package com.ruoyi.web.controller.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.web.controller.common.domain.SysAppAudit;
import com.ruoyi.web.controller.common.service.ISysAppAuditService;


/**
 * 系统app审核Controller
 * 
 * @author zhangsan
 * @date 2020-12-12
 */
@RestController
@RequestMapping("${common.appAuditor.mainPath}")
public class SysAppAuditController extends BaseController {
    @Autowired
    private ISysAppAuditService sysAppAuditService;

    /**
     * 查询系统app审核列表
     */
//    @PreAuthorize("@ss.hasPermi('system:audit:list')")
    @GetMapping("${common.appAuditor.subPath.listPath}")
    public TableDataInfo list(SysAppAudit sysAppAudit) {
        startPage();
        List<SysAppAudit> list = sysAppAuditService.selectSysAppAuditList(sysAppAudit);
        return getDataTable(list);
    }

    /**
     * 导出符合查询条件的系统app审核列表
     */
    @PreAuthorize("@ss.hasPermi('system:audit:export')")
    @Log(title = "系统app审核", businessType = BusinessType.EXPORT)
    @GetMapping("${common.appAuditor.subPath.exportPath}")
    public AjaxResult export(SysAppAudit sysAppAudit) {
        List<SysAppAudit> list = sysAppAuditService.selectSysAppAuditList(sysAppAudit);
        ExcelUtil<SysAppAudit> util = new ExcelUtil<SysAppAudit>(SysAppAudit.class);
        return util.exportExcel(list, "audit");
    }
    
    /**
     * 导出选中的系统app审核列表
     */
    @PreAuthorize("@ss.hasPermi('system:audit:export')")
    @Log(title = "系统app审核", businessType = BusinessType.EXPORT)
    @GetMapping("${common.appAuditor.subPath.exportByIdsPath}")
    public AjaxResult export(@PathVariable String[] ids) {
        List<SysAppAudit> list = sysAppAuditService.selectSysAppAuditListByids(ids);
        ExcelUtil<SysAppAudit> util = new ExcelUtil<SysAppAudit>(SysAppAudit.class);
        return util.exportExcel(list, "audit");
    }

    /**
     * 获取系统app审核详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:audit:query')")
    @GetMapping("${common.appAuditor.subPath.getByIdPath}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(sysAppAuditService.selectSysAppAuditById(id));
    }

    /**
     * 新增系统app审核
     */
    @PreAuthorize("@ss.hasPermi('system:audit:add')")
    @Log(title = "系统app审核", businessType = BusinessType.INSERT)
    @PostMapping("${common.appAuditor.subPath.addPath}")
    public AjaxResult add(@RequestBody SysAppAudit sysAppAudit) {
        return toAjax(sysAppAuditService.insertSysAppAudit(sysAppAudit));
    }

    /**
     * 修改系统app审核
     */
    @PreAuthorize("@ss.hasPermi('system:audit:edit')")
    @Log(title = "系统app审核", businessType = BusinessType.UPDATE)
    @PutMapping("${common.appAuditor.subPath.editPath}")
    public AjaxResult edit(@RequestBody SysAppAudit sysAppAudit) {
        return toAjax(sysAppAuditService.updateSysAppAudit(sysAppAudit));
    }

    /**
     * 删除系统app审核
     */
    @PreAuthorize("@ss.hasPermi('system:audit:remove')")
    @Log(title = "系统app审核", businessType = BusinessType.DELETE)
	@DeleteMapping("${common.appAuditor.subPath.delByIdsPath}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(sysAppAuditService.deleteSysAppAuditByIds(ids));
    }
}

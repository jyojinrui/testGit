package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Xqxxbab;
import com.ruoyi.system.domain.Zzxqxxbab;
import com.ruoyi.system.service.IXqxxbabService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 小区管理Controller
 * 
 * @author ruoyi
 * @date 2020-09-26
 */
@Api("小区管理接口")
@RestController
@RequestMapping("/system/xqxxbab")
public class XqxxbabController extends BaseController {
	@Autowired
	private IXqxxbabService xqxxbabService;

	/**
	 * 查询小区管理列表
	 */
	@ApiOperation("查询小区管理列表")
	@PreAuthorize("@ss.hasPermi('system:xqxxbab:list')")
	@PostMapping("/list")
	public TableDataInfo list(Xqxxbab xqxxbab) {
		startPage();
		List<Xqxxbab> list = xqxxbabService.selectXqxxbabList(xqxxbab);
		return getDataTable(list);
	}
	
	
	@GetMapping("/listxqxxbabbydeptid")
	public TableDataInfo listxqxxbabbydeptid(Long deptId,Long deptIdOfTree,Xqxxbab xqxxbab) {
//		System.out.println("========>");
//		System.out.println(deptId);
//		System.out.println(deptIdOfTree);
//		System.out.println(xqxxbab);
//		System.out.println("=======>");
		startPage();
		List<Xqxxbab> list = xqxxbabService.selectXqxxbabListByDeptId(deptId,deptIdOfTree,xqxxbab);
		return getDataTable(list);
	}

	/**
	 * getxqxxbyxqadminid 根据小区管理员信息查看小区信息
	 */
	@ApiOperation("根据小区管理员信息查看小区信息")
//  @PreAuthorize("@ss.hasPermi('system:fzxxbab:getlistbyxqadminid')")
	@GetMapping("/getxqxxbyadminid/{id}")
	public Xqxxbab getXqxxByXqAdminId(@PathVariable("id")Long userId) {
//		System.out.println("===========>"+userId);
		
//		return new Xqxxbab();
		return xqxxbabService.getXqxxByXqAdminId(userId);
	}
	
	
	@GetMapping("/listBzdzxxbabByXqbm/{buildingId}")
	public List<Zzxqxxbab> listBzdzxxbabByXqbm(@PathVariable("buildingId")String buildingId) {
//		System.out.println("===========>"+userId);
		
		String S=buildingId;
//		return new Xqxxbab();
		return xqxxbabService.getBzdzxxbabByXqbm(buildingId);
	}
	
	
	/**
	 * 导出小区管理列表
	 */
	@ApiOperation("导出小区管理列表")
	@PreAuthorize("@ss.hasPermi('system:xqxxbab:export')")
	@Log(title = "小区管理", businessType = BusinessType.EXPORT)
	@GetMapping("/export")
	public AjaxResult export(Xqxxbab xqxxbab) {
		List<Xqxxbab> list = xqxxbabService.selectXqxxbabList(xqxxbab);
		ExcelUtil<Xqxxbab> util = new ExcelUtil<Xqxxbab>(Xqxxbab.class);
		return util.exportExcel(list, "xqxxbab");
	}

	/**
	 * 获取小区管理详细信息
	 */
	@ApiOperation("获取小区管理详细信息")
	@PreAuthorize("@ss.hasPermi('system:xqxxbab:query')")
	@GetMapping(value = "/{id}")
	public AjaxResult getInfo(@PathVariable("id") String id) {
		return AjaxResult.success(xqxxbabService.selectXqxxbabById(id));
	}

	/**
	 * 新增小区管理
	 */
	@ApiOperation("新增小区管理")
	@PreAuthorize("@ss.hasPermi('system:xqxxbab:add')")
	@Log(title = "小区管理", businessType = BusinessType.INSERT)
	@PostMapping
	public AjaxResult add(@RequestBody Xqxxbab xqxxbab) {
		return toAjax(xqxxbabService.insertXqxxbab(xqxxbab));
	}

	/**
	 * 修改小区管理
	 */
	@ApiOperation("修改小区管理")
	@PreAuthorize("@ss.hasPermi('system:xqxxbab:edit')")
	@Log(title = "小区管理", businessType = BusinessType.UPDATE)
	@PutMapping
	public AjaxResult edit(@RequestBody Xqxxbab xqxxbab) {
		return toAjax(xqxxbabService.updateXqxxbab(xqxxbab));
	}

	/**
	 * 删除小区管理
	 */
	@ApiOperation("删除小区管理")
	@PreAuthorize("@ss.hasPermi('system:xqxxbab:remove')")
	@Log(title = "小区管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public AjaxResult remove(@PathVariable String[] ids) {
		return toAjax(xqxxbabService.deleteXqxxbabByIds(ids));
	}
}

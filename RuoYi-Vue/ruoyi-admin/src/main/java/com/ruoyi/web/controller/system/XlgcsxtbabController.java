package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Xlgcsxtbab;
import com.ruoyi.system.service.IXlgcsxtbabService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 雪亮工程摄像头设备Controller
 * 
 * @author Carry
 * @date 2020-10-03
 */
@Api("雪亮工程摄像头设备接口")
@RestController
@RequestMapping("/system/xlgcsxtbab")
public class XlgcsxtbabController extends BaseController
{
    @Autowired
    private IXlgcsxtbabService xlgcsxtbabService;

    /**
     * 查询雪亮工程摄像头设备列表
     */
    @ApiOperation("查询雪亮工程摄像头设备列表")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtbab:list')")
    @GetMapping("/list")
    public TableDataInfo list(Xlgcsxtbab xlgcsxtbab)
    {
        startPage();
        List<Xlgcsxtbab> list = xlgcsxtbabService.selectXlgcsxtbabList(xlgcsxtbab);
        return getDataTable(list);
    }
    /**
     * 通过管理员id获取雪亮工程摄像头设备信息
     */
    @ApiOperation("通过管理员id获取雪亮工程摄像头设备信息")
    @GetMapping("/listxlgcsxtbabbyxqadminid")
    public TableDataInfo listxlgcsxtbabbyxqadminid(Long userId,Xlgcsxtbab xlgcsxtbab)
    {
        startPage();
        List<Xlgcsxtbab> list = xlgcsxtbabService.selectXlgcsxtbabListByXqAdminId(userId,xlgcsxtbab);
        return getDataTable(list);
    }
    
    
    @GetMapping("/listxlgcsxtbabbydeptid")
    public TableDataInfo listxlgcsxtbabbydeptid(Long deptId,Xlgcsxtbab xlgcsxtbab)
    {
        startPage();
        List<Xlgcsxtbab> list = xlgcsxtbabService.selectXlgcsxtbabListByDeptId(deptId,xlgcsxtbab);
        return getDataTable(list);
    }
    /**
     * 导出雪亮工程摄像头设备列表
     */
    @ApiOperation("导出雪亮工程摄像头设备列表")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtbab:export')")
    @Log(title = "雪亮工程摄像头设备", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Xlgcsxtbab xlgcsxtbab)
    {
        List<Xlgcsxtbab> list = xlgcsxtbabService.selectXlgcsxtbabList(xlgcsxtbab);
        ExcelUtil<Xlgcsxtbab> util = new ExcelUtil<Xlgcsxtbab>(Xlgcsxtbab.class);
        return util.exportExcel(list, "xlgcsxtbab");
    }

    /**
     * 获取雪亮工程摄像头设备详细信息
     */
    @ApiOperation("获取雪亮工程摄像头设备详细信息")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtbab:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(xlgcsxtbabService.selectXlgcsxtbabById(id));
    }

    /**
     * 新增雪亮工程摄像头设备
     */
    @ApiOperation("新增雪亮工程摄像头设备")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtbab:add')")
    @Log(title = "雪亮工程摄像头设备", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Xlgcsxtbab xlgcsxtbab)
    {
        return toAjax(xlgcsxtbabService.insertXlgcsxtbab(xlgcsxtbab));
    }

    /**
     * 修改雪亮工程摄像头设备
     */
    @ApiOperation("修改雪亮工程摄像头设备")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtbab:edit')")
    @Log(title = "雪亮工程摄像头设备", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Xlgcsxtbab xlgcsxtbab)
    {
        return toAjax(xlgcsxtbabService.updateXlgcsxtbab(xlgcsxtbab));
    }

    /**
     * 删除雪亮工程摄像头设备
     */
    @ApiOperation("删除雪亮工程摄像头设备")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtbab:remove')")
    @Log(title = "雪亮工程摄像头设备", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(xlgcsxtbabService.deleteXlgcsxtbabByIds(ids));
    }
}

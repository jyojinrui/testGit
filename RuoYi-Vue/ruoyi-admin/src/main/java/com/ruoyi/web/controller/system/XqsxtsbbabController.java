package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Xqsxtsbbab;
import com.ruoyi.system.service.IXqsxtsbbabService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 小区摄像头设备Controller
 * 
 * @author Carry
 * @date 2020-10-03
 */
@Api("小区摄像头设备接口")
@RestController
@RequestMapping("/system/xqsxtsbbab")
public class XqsxtsbbabController extends BaseController
{
    @Autowired
    private IXqsxtsbbabService xqsxtsbbabService;

    
    /**
     * 通过管理员id获取对应功能的列表信息
     */
    @ApiOperation("通过管理员id获取小区摄像头设备信息")
    @GetMapping("/listxqsxtsbbabbyxqadminid")
    public TableDataInfo listxqsxtsbbabbyxqadminid(Long userId,Xqsxtsbbab xqsxtsbbab)
    {
        startPage();
        List<Xqsxtsbbab> list = xqsxtsbbabService.selectXqsxtsbbabListByXqAdminId(userId,xqsxtsbbab);
        return getDataTable(list);
    }
    
    
    @GetMapping("/listxqsxtsbbabbydeptid")
    public TableDataInfo listxqsxtsbbabbydeptid(Long deptId,Xqsxtsbbab xqsxtsbbab)
    {
        startPage();
        List<Xqsxtsbbab> list = xqsxtsbbabService.selectXqsxtsbbabListByDeptId(deptId,xqsxtsbbab);
        return getDataTable(list);
    }
    /**
     * 查询小区摄像头设备列表
     */
    @ApiOperation("查询小区摄像头设备列表")
    @PreAuthorize("@ss.hasPermi('system:xqsxtsbbab:list')")
    @GetMapping("/list")
    public TableDataInfo list(Xqsxtsbbab xqsxtsbbab)
    {
        startPage();
        List<Xqsxtsbbab> list = xqsxtsbbabService.selectXqsxtsbbabList(xqsxtsbbab);
        return getDataTable(list);
    }

    /**
     * 导出小区摄像头设备列表
     */
    @ApiOperation("导出小区摄像头设备列表")
    @PreAuthorize("@ss.hasPermi('system:xqsxtsbbab:export')")
    @Log(title = "小区摄像头设备", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Xqsxtsbbab xqsxtsbbab)
    {
        List<Xqsxtsbbab> list = xqsxtsbbabService.selectXqsxtsbbabList(xqsxtsbbab);
        ExcelUtil<Xqsxtsbbab> util = new ExcelUtil<Xqsxtsbbab>(Xqsxtsbbab.class);
        return util.exportExcel(list, "xqsxtsbbab");
    }

    /**
     * 获取小区摄像头设备详细信息
     */
    @ApiOperation("获取小区摄像头设备详细信息")
    @PreAuthorize("@ss.hasPermi('system:xqsxtsbbab:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(xqsxtsbbabService.selectXqsxtsbbabById(id));
    }

    /**
     * 新增小区摄像头设备
     */
    @ApiOperation("新增小区摄像头设备")
    @PreAuthorize("@ss.hasPermi('system:xqsxtsbbab:add')")
    @Log(title = "小区摄像头设备", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Xqsxtsbbab xqsxtsbbab)
    {
        return toAjax(xqsxtsbbabService.insertXqsxtsbbab(xqsxtsbbab));
    }

    /**
     * 修改小区摄像头设备
     */
    @ApiOperation("修改小区摄像头设备")
    @PreAuthorize("@ss.hasPermi('system:xqsxtsbbab:edit')")
    @Log(title = "小区摄像头设备", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Xqsxtsbbab xqsxtsbbab)
    {
        return toAjax(xqsxtsbbabService.updateXqsxtsbbab(xqsxtsbbab));
    }

    /**
     * 删除小区摄像头设备
     */
    @ApiOperation("删除小区摄像头设备")
    @PreAuthorize("@ss.hasPermi('system:xqsxtsbbab:remove')")
    @Log(title = "小区摄像头设备", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(xqsxtsbbabService.deleteXqsxtsbbabByIds(ids));
    }
}

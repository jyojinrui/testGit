package com.ruoyi.web.controller.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.web.controller.common.domain.SysApp;
import com.ruoyi.web.controller.common.mapper.AppMapper;
import com.ruoyi.web.controller.common.service.AppService;

/**
 * 第三方系统app service接口实现类
 */
@Service
public class AppServiceImpl implements AppService {
    @Autowired
    AppMapper appMapper;

    /**
     * 根据条件查询app列表
     *
     * @param sysApp 条件封装成的app
     * @return app集合
     */
    @Override
    public List<SysApp> selectSysAppList(SysApp sysApp) {
        return appMapper.selectSysAppList(sysApp);
    }

    /**
     * 根据appKey查询对应的app记录
     * @param appKey
     * @return
     */
    @Override
    public SysApp selectSysAppByAppKey(String appKey) {
        return appMapper.selectSysAppByAppKey(appKey);
    }

    /**
     * 根据appKeys查询app列表
     *
     * @param appKeys appKey列表
     * @return app集合
     */
    @Override
    public List<SysApp> selectSysAppListByAppKeys(String[] appKeys) {
        return appMapper.selectSysAppListByAppKeys(appKeys);
    }

    /**
     * 新增app
     * @param sysApp
     * @return
     */
    @Override
    public int addSysApp(SysApp sysApp) {
        return appMapper.addSysApp(sysApp);
    }

    /**
     * 修改app的审核状态和审核时间
     * @param sysApp
     * @return
     */
    @Override
    public int updateSysApp(SysApp sysApp) {
        return appMapper.updateSysApp(sysApp);
    }

	/**
     * 修改app的审核人信息
     * @param sysApp
     * @return
     */
    @Override
    public int updateSysAppAuditor(SysApp sysApp) {
        return appMapper.updateSysAppAuditor(sysApp);
    }

    /**
     * 通过appKey删除app
     *
     * @param appKey
     * @return 结果
     */
    @Override
    public int deleteSysAppByAppKey(String appKey) {
        return appMapper.deleteSysAppByAppKey(appKey);
    }

    /**
     * 批量删除app
     *
     * @param appKeys 需要删除的appKey列表
     * @return 结果
     */
    @Override
    public int deleteSysAppByAppKeys(String[] appKeys) {
        return appMapper.deleteSysAppByAppKeys(appKeys);
    }
}

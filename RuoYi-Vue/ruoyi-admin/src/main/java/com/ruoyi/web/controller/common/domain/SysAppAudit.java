package com.ruoyi.web.controller.common.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 系统app审核对象 sys_app_audit
 *
 * @author zhangsan
 * @date 2020-12-12
 */
public class SysAppAudit extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** app审核人的编号 */
    @Excel(name = "审核人编号")
    private String id;

    /** app审核人的姓名 */
    @Excel(name = "审核人姓名")
    private String auditorName;

    /** app审核人的身份证号码 */
    @Excel(name = "审核人身份证号码")
    private String auditorIdentifier;

    /** app审核人的联系电话 */
    @Excel(name = "审核人联系电话")
    private String auditorPhone;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "创建时间")
    private Date createTime;

    /** 修改时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "修改时间")
    private Date updateTime;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setAuditorIdentifier(String auditorIdentifier) {
        this.auditorIdentifier = auditorIdentifier;
    }

    public String getAuditorIdentifier() {
        return auditorIdentifier;
    }

    public void setAuditorName(String auditorName) {
        this.auditorName = auditorName;
    }

    public String getAuditorName() {
        return auditorName;
    }

    public void setAuditorPhone(String auditorPhone) {
        this.auditorPhone = auditorPhone;
    }

    public String getAuditorPhone() {
        return auditorPhone;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public Date getUpdateTime() {
        return updateTime;
    }

    @Override
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("auditorIdentifier", getAuditorIdentifier())
                .append("auditorName", getAuditorName())
                .append("auditorPhone", getAuditorPhone())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}

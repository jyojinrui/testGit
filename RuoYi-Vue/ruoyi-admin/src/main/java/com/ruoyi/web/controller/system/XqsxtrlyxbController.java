package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Xqsxtrlyxb;
import com.ruoyi.system.service.IXqsxtrlyxbService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 人脸运行数据Controller
 * 
 * @author Carry
 * @date 2020-10-03
 */
@Api("人脸运行数据接口")
@RestController
@RequestMapping("/system/xqsxtrlyxb")
public class XqsxtrlyxbController extends BaseController
{
    @Autowired
    private IXqsxtrlyxbService xqsxtrlyxbService;

    /**
     * 通过管理员id获取对用功能的列表信息
     */
    @ApiOperation("通过管理员id获取人脸运行数据信息")
    @GetMapping("/listxqsxtrlyxbbyxqadminid")
    public TableDataInfo listxqsxtrlyxbbyxqadminid(Long userId,Xqsxtrlyxb xqsxtrlyxb)
    {
        startPage();
        List<Xqsxtrlyxb> list = xqsxtrlyxbService.selectXqsxtrlyxbListByXqAdminId(userId,xqsxtrlyxb);
        return getDataTable(list);
    }
    
    
    @GetMapping("/listxqsxtrlyxbbydeptid")
    public TableDataInfo listxqsxtrlyxbbydeptid(Long deptId,Xqsxtrlyxb xqsxtrlyxb)
    {
        startPage();
        List<Xqsxtrlyxb> list = xqsxtrlyxbService.selectXqsxtrlyxbListByDeptId(deptId,xqsxtrlyxb);
        return getDataTable(list);
    }
    /**
     * 查询人脸运行数据列表
     */
    @ApiOperation("查询人脸运行数据列表")
    @PreAuthorize("@ss.hasPermi('system:xqsxtrlyxb:list')")
    @GetMapping("/list")
    public TableDataInfo list(Xqsxtrlyxb xqsxtrlyxb)
    {
        startPage();
        List<Xqsxtrlyxb> list = xqsxtrlyxbService.selectXqsxtrlyxbList(xqsxtrlyxb);
        return getDataTable(list);
    }

    /**
     * 导出人脸运行数据列表
     */
    @ApiOperation("导出人脸运行数据列表")
    @PreAuthorize("@ss.hasPermi('system:xqsxtrlyxb:export')")
    @Log(title = "人脸运行数据", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Xqsxtrlyxb xqsxtrlyxb)
    {
        List<Xqsxtrlyxb> list = xqsxtrlyxbService.selectXqsxtrlyxbList(xqsxtrlyxb);
        ExcelUtil<Xqsxtrlyxb> util = new ExcelUtil<Xqsxtrlyxb>(Xqsxtrlyxb.class);
        return util.exportExcel(list, "xqsxtrlyxb");
    }

    /**
     * 获取人脸运行数据详细信息
     */
    @ApiOperation("获取人脸运行数据详细信息")
    @PreAuthorize("@ss.hasPermi('system:xqsxtrlyxb:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(xqsxtrlyxbService.selectXqsxtrlyxbById(id));
    }

    /**
     * 新增人脸运行数据
     */
    @ApiOperation("新增人脸运行数据")
    @PreAuthorize("@ss.hasPermi('system:xqsxtrlyxb:add')")
    @Log(title = "人脸运行数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Xqsxtrlyxb xqsxtrlyxb)
    {
        return toAjax(xqsxtrlyxbService.insertXqsxtrlyxb(xqsxtrlyxb));
    }

    /**
     * 修改人脸运行数据
     */
    @ApiOperation("修改人脸运行数据")
    @PreAuthorize("@ss.hasPermi('system:xqsxtrlyxb:edit')")
    @Log(title = "人脸运行数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Xqsxtrlyxb xqsxtrlyxb)
    {
        return toAjax(xqsxtrlyxbService.updateXqsxtrlyxb(xqsxtrlyxb));
    }

    /**
     * 删除人脸运行数据
     */
    @ApiOperation("删除人脸运行数据")
    @PreAuthorize("@ss.hasPermi('system:xqsxtrlyxb:remove')")
    @Log(title = "人脸运行数据", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(xqsxtrlyxbService.deleteXqsxtrlyxbByIds(ids));
    }
}

package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Xlgcsxtrtyxb;
import com.ruoyi.system.service.IXlgcsxtrtyxbService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 雪亮工程摄像头人体运行数据Controller
 * 
 * @author Carry
 * @date 2020-10-04
 */
@Api("雪亮工程摄像头人体运行数据接口")
@RestController
@RequestMapping("/system/xlgcsxtrtyxb")
public class XlgcsxtrtyxbController extends BaseController
{
    @Autowired
    private IXlgcsxtrtyxbService xlgcsxtrtyxbService;

/**
 * 通过管理员id获取对用功能的列表信息
 */	
    @ApiOperation("通过管理员id获取雪亮工程摄像头人体运行数据信息")
    @GetMapping("/listxlgcsxtrtyxbbyxqadminid")
    public TableDataInfo list(Long userId,Xlgcsxtrtyxb xlgcsxtrtyxb)
    {
        startPage();
        List<Xlgcsxtrtyxb> list = xlgcsxtrtyxbService.selectXlgcsxtrtyxbListByXqAdminId(userId,xlgcsxtrtyxb);
        return getDataTable(list);
    }
    
    @GetMapping("/listxlgcsxtrtyxbbydeptid")
    public TableDataInfo listxlgcsxtrtyxbbydeptid(Long deptId,Xlgcsxtrtyxb xlgcsxtrtyxb)
    {
    	
    	
        startPage();
        List<Xlgcsxtrtyxb> list = xlgcsxtrtyxbService.selectXlgcsxtrtyxbListByDeptId(deptId,xlgcsxtrtyxb);
        return getDataTable(list);
    }
    
    /**
     * 查询雪亮工程摄像头人体运行数据列表
     */
    @ApiOperation("查询雪亮工程摄像头人体运行数据列表")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtrtyxb:list')")
    @GetMapping("/list")
    public TableDataInfo list(Xlgcsxtrtyxb xlgcsxtrtyxb)
    {
        startPage();
        List<Xlgcsxtrtyxb> list = xlgcsxtrtyxbService.selectXlgcsxtrtyxbList(xlgcsxtrtyxb);
        return getDataTable(list);
    }

    /**
     * 导出雪亮工程摄像头人体运行数据列表
     */
    @ApiOperation("导出雪亮工程摄像头人体运行数据列表")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtrtyxb:export')")
    @Log(title = "雪亮工程摄像头人体运行数据", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Xlgcsxtrtyxb xlgcsxtrtyxb)
    {
        List<Xlgcsxtrtyxb> list = xlgcsxtrtyxbService.selectXlgcsxtrtyxbList(xlgcsxtrtyxb);
        ExcelUtil<Xlgcsxtrtyxb> util = new ExcelUtil<Xlgcsxtrtyxb>(Xlgcsxtrtyxb.class);
        return util.exportExcel(list, "xlgcsxtrtyxb");
    }

    /**
     * 获取雪亮工程摄像头人体运行数据详细信息
     */
    @ApiOperation("获取雪亮工程摄像头人体运行数据详细信息")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtrtyxb:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(xlgcsxtrtyxbService.selectXlgcsxtrtyxbById(id));
    }

    /**
     * 新增雪亮工程摄像头人体运行数据
     */
    @ApiOperation("新增雪亮工程摄像头人体运行数据")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtrtyxb:add')")
    @Log(title = "雪亮工程摄像头人体运行数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Xlgcsxtrtyxb xlgcsxtrtyxb)
    {
        return toAjax(xlgcsxtrtyxbService.insertXlgcsxtrtyxb(xlgcsxtrtyxb));
    }

    /**
     * 修改雪亮工程摄像头人体运行数据
     */
    @ApiOperation("修改雪亮工程摄像头人体运行数据")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtrtyxb:edit')")
    @Log(title = "雪亮工程摄像头人体运行数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Xlgcsxtrtyxb xlgcsxtrtyxb)
    {
        return toAjax(xlgcsxtrtyxbService.updateXlgcsxtrtyxb(xlgcsxtrtyxb));
    }

    /**
     * 删除雪亮工程摄像头人体运行数据
     */
    @ApiOperation("删除雪亮工程摄像头人体运行数据")
    @PreAuthorize("@ss.hasPermi('system:xlgcsxtrtyxb:remove')")
    @Log(title = "雪亮工程摄像头人体运行数据", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(xlgcsxtrtyxbService.deleteXlgcsxtrtyxbByIds(ids));
    }
}

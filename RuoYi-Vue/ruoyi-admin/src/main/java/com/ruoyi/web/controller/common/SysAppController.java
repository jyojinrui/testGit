package com.ruoyi.web.controller.common;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.web.controller.common.domain.AppApplyInfo;
import com.ruoyi.web.controller.common.domain.SysApp;
import com.ruoyi.web.controller.common.domain.SysAppApplyDetails;
import com.ruoyi.web.controller.common.domain.SysAppAudit;
import com.ruoyi.web.controller.common.service.AppApplyInfoService;
import com.ruoyi.web.controller.common.service.AppService;
import com.ruoyi.web.controller.common.service.ISysAppApplyDetailsService;
import com.ruoyi.web.controller.common.service.ISysAppAuditService;
import com.ruoyi.web.controller.common.utils.AppGenUtils;


/**
 * 第三方系统app controller
 */
@RestController
@RequestMapping("${common.app.mainPath}")
public class SysAppController extends BaseController {
    private static final Logger log = LoggerFactory.getLogger(CommonController.class);

    @Autowired
    ISysAppAuditService sysAppAuditService;

    @Autowired
    AppService appService;

    @Autowired
    AppApplyInfoService appApplyInfoService;

    @Autowired
    ISysAppApplyDetailsService sysAppApplyDetailsService;

    /**
     * 查询所有的app
     * @param sysApp
     * @return
     */
    @PreAuthorize("@ss.hasPermi('common:app:list')")
    @GetMapping("${common.app.subPath.listPath}")
    public TableDataInfo list(SysApp sysApp) {
        startPage();
        List<SysApp> list = appService.selectSysAppList(sysApp);
        return getDataTable(list);
    }

    /**
     * 根据appKey查看第三方系统app的审核状态
     */
//    @PreAuthorize("@ss.hasPermi('common:app:query')")
    @GetMapping("${common.app.subPath.getAuditByAppKeyPath}")
    public AjaxResult getAppAuditState(@RequestParam("appKey") String appKey) {
        SysApp sysApp = appService.selectSysAppByAppKey(appKey);
        JSONObject jsonObject = null;
        String msg = null;
        boolean isExist = false;
        if (sysApp == null) {
            msg = Constants.APPKEY_IS_NOT_EXIST;
        } else {
            isExist = true;
            if (sysApp.getAuditState() == 0) {
                msg = Constants.APP_AUDIT_NOT_PASS;
            } else if (sysApp.getAuditState() == 1) {
                msg = Constants.APP_AUDIT_PASS;
            } else if (sysApp.getAuditState() == 2) {
                msg = Constants.APP_AUDITING;
            }
            jsonObject = new JSONObject();
            jsonObject.put(Constants.APP_AUDIT_STATE, msg);
            // 如果不为空，默认将查询到的第一个审核人信息(包括姓名和联系电话)返回给申请者
            jsonObject.put(Constants.APP_AUDITOR_NAME, sysApp.getAuditorName());
            jsonObject.put(Constants.APP_AUDITOR_PHONE, sysApp.getAuditorPhone());
        }
        return isExist ? AjaxResult.success(jsonObject) : AjaxResult.error(HttpStatus.APPKEY_IS_NOT_EXIST, msg);
    }

    /**
     * 根据appKey获取第三方系统app详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:app:query')")
    @GetMapping("${common.app.subPath.getByAppKeyPath}")
    public AjaxResult getAppInfo(@PathVariable String appKey) {
        return AjaxResult.success(appService.selectSysAppByAppKey(appKey));
    }

    /**
     * 新增app
     * @param appApplyInfo
     * @return
     */
    @Log(title = "app管理", businessType = BusinessType.INSERT)
    @PostMapping("${common.app.subPath.addPath}")
    public AjaxResult addSysApp(@RequestBody AppApplyInfo appApplyInfo) {
        System.out.println(appApplyInfo);
        JSONObject jsonObject = null;
        SysApp sysApp = null;
        SysAppApplyDetails sysAppApplyDetails = null;
        // 是否保存数据成功，成功数值则大于0
        int res1 = 0, res2 = 0, res3 = 0;
        int code = 200;
        String message = null;
        try {
            // 如果申请信息不为空，继续执行
            if (appApplyInfo != null) {
                // 判断申请信息中每一项参数名是否正确，参数是否有值
                if (appApplyInfo.getCompanyName() == null) {
                    code = HttpStatus.COMPANY_INFO_ERROR;
                    message = Constants.COMPANY_INFO_ERROR;
                } else if (appApplyInfo.getApplicationName() == null) {
                    code = HttpStatus.APPLICATION_INFO_ERROR;
                    message = Constants.APPLICATION_INFO_ERROR;
                } else if (appApplyInfo.getApplicantName() == null) {
                    code = HttpStatus.APPLICANT_INFO_ERROR;
                    message = Constants.APPLICANT_INFO_ERROR;
                } else if (appApplyInfo.getTelPhone() == null) {
                    code = HttpStatus.PHONE_INFO_ERROR;
                    message = Constants.PHONE_INFO_ERROR;
                } else {
                    // 根据appApplyInfo查询本地的app申请信息
                    List<AppApplyInfo> localAppApplyInfos = appApplyInfoService.selectAppApplyInfoByConditions(appApplyInfo);
                    // 如果appApplyInfo不存在，则保存到本地数据库
                    if (localAppApplyInfos == null || localAppApplyInfos.size() == 0) {
                        res1 = appApplyInfoService.addAppApplyInfo(appApplyInfo);
                        if (res1 > 0) {
                            // 并为其随机分配一个app
                            sysApp = AppGenUtils.randomGenApp();
                            // 如果分配成功，则将app信息封装为json返回并保存到本地数据库
                            if (sysApp != null) {
                                // 查询本地保存的审核人信息集合
                                List<SysAppAudit> sysAppAudits = sysAppAuditService.selectSysAppAuditList(null);
                                // 判断本地保存的审核人信息集合是否为空
                                if (sysAppAudits != null && sysAppAudits.size() >= 1) {
                                    // 并将此审核人信息保存到对应的app上
                                    sysApp.setAuditorId(sysAppAudits.get(0).getId());
                                    sysApp.setAuditorName(sysAppAudits.get(0).getAuditorName());
                                    sysApp.setAuditorPhone(sysAppAudits.get(0).getAuditorPhone());
                                    res2 = appService.addSysApp(sysApp);
                                    if (res2 > 0) {
                                        // 将分配的app和关联的申请信息记录保存到本地数据库
                                        sysAppApplyDetails = new SysAppApplyDetails();
                                        sysAppApplyDetails.setAppKey(sysApp.getAppKey());
                                        sysAppApplyDetails.setApplyInfoId(appApplyInfo.getId());
                                        res3 = sysAppApplyDetailsService.insertSysAppApplyDetails(sysAppApplyDetails);
                                        if (res3 > 0) {
                                            jsonObject = new JSONObject();
                                            jsonObject.put(Constants.APP_KEY_NAME, sysApp.getAppKey());
                                            jsonObject.put(Constants.APP_SECRET_NAME, sysApp.getAppSecret());
                                            // 如果不为空，默认将查询到的第一个审核人信息(包括姓名和联系电话)返回给申请者
                                            jsonObject.put(Constants.APP_AUDITOR_NAME, sysAppAudits.get(0).getAuditorName());
                                            jsonObject.put(Constants.APP_AUDITOR_PHONE, sysAppAudits.get(0).getAuditorPhone());
                                        } else {
                                            // 如果保存app和关联的申请信息记录失败，则提示无法保存app和关联的申请信息记录，请联系管理员
                                            code = HttpStatus.CAN_NOT_SAVE_APPLY_DETAILS;
                                            message = Constants.CAN_NOT_SAVE_APPLY_DETAILS;
                                        }
                                    } else {
                                        // 如果保存app失败，则提示无法保存app信息，请联系管理员
                                        code = HttpStatus.CAN_NOT_SAVE_APP;
                                        message = Constants.CAN_NOT_SAVE_APP;
                                    }
                                } else {
                                    // 如果审核人信息为空，则提示无法保存app信息，请联系管理员
                                    code = HttpStatus.CAN_NOT_SAVE_APP;
                                    message = Constants.CAN_NOT_SAVE_APP;
                                }
                            } else {
                                // 如果生成app失败，则提示无法生成app，请联系管理员
                                code = HttpStatus.CAN_NOT_GEN_APP;
                                message = Constants.CAN_NOT_GEN_APP;
                            }
                        } else {
                            // 如果保存申请信息失败，则提示无法保存申请信息，请联系管理员
                            code = HttpStatus.CAN_NOT_SAVE_APPLY_INFO;
                            message = Constants.CAN_NOT_SAVE_APPLY_INFO;
                        }
                    } else {
                        // 否则提示用户appApplyInfo已存在，无需重复申请
                        code = HttpStatus.CAN_NOT_APPLY_AGAIN;
                        message = Constants.CAN_NOT_APPLY_AGAIN;
                    }
                }
            } else { // 否则提示用户申请信息不能为空
                code = HttpStatus.APP_APPLY_INFO_CAN_NOT_NULL;
                message = Constants.APP_APPLY_INFO_CAN_NOT_NULL;
            }
        } catch (Exception e) {
            // 系统内部出现错误
            code = HttpStatus.ERROR;
            message = Constants.SYSTEM_INTERNAL_ERROR;
            // 删除新插入的数据
            if (res1 > 0 && appApplyInfo != null) {
                appApplyInfoService.deleteAppApplyInfoById(appApplyInfo.getId());
            }
            if (res2 > 0 && sysApp != null) {
                appService.deleteSysAppByAppKey(sysApp.getAppKey());
            }
            if (res3 > 0 && sysAppApplyDetails != null) {
                sysAppApplyDetailsService.deleteSysAppApplyDetailsById(sysAppApplyDetails.getId());
            }
            // 打印错误信息到控制台
            e.printStackTrace();
        } finally {
            return jsonObject == null ? AjaxResult.error(code, message) :
                    AjaxResult.success("分配成功，请等待审核！", jsonObject);
        }
    }

    /**
     * 修改app的审核状态
     * @param sysApp
     * @return
     */
    @PreAuthorize("@ss.hasPermi('common:app:edit')")
    @Log(title = "app管理", businessType = BusinessType.UPDATE)
    @PutMapping("${common.app.subPath.editPath}")
    public AjaxResult updateApp(@RequestBody SysApp sysApp) {
        return toAjax(appService.updateSysApp(sysApp));
    }

    /**
     * 更换app的审核人
     * @param sysApp
     * @return
     */
    @PreAuthorize("@ss.hasPermi('common:app:edit')")
    @Log(title = "app管理", businessType = BusinessType.UPDATE)
    @PutMapping("${common.app.subPath.changeAuditorPath}")
    public AjaxResult changeAuditor(@RequestBody SysApp sysApp) {
        return toAjax(appService.updateSysAppAuditor(sysApp));
    }

    /**
     * 删除app
     */
    @PreAuthorize("@ss.hasPermi('common:app:remove')")
    @Log(title = "app管理", businessType = BusinessType.DELETE)
    @DeleteMapping("${common.app.subPath.delByAppKeysPath}")
    public AjaxResult remove(@PathVariable String[] appKeys) {
        return toAjax(appService.deleteSysAppByAppKeys(appKeys));
    }

    /**
     * 导出符合查询条件的app列表
     */
    @PreAuthorize("@ss.hasPermi('common:app:export')")
    @Log(title = "app管理", businessType = BusinessType.EXPORT)
    @GetMapping("${common.app.subPath.exportPath}")
    public AjaxResult export(SysApp sysApp) {
        List<SysApp> list = appService.selectSysAppList(sysApp);
        ExcelUtil<SysApp> util = new ExcelUtil<SysApp>(SysApp.class);
        return util.exportExcel(list, "app");
    }

    /**
     * 导出选中的app列表
     */
    @PreAuthorize("@ss.hasPermi('wrench:app:export')")
    @Log(title = "app管理", businessType = BusinessType.EXPORT)
    @GetMapping("${common.app.subPath.exportByAppKeysPath}")
    public AjaxResult export(@PathVariable String[] appKeys) {
        List<SysApp> list = appService.selectSysAppListByAppKeys(appKeys);
        ExcelUtil<SysApp> util = new ExcelUtil<SysApp>(SysApp.class);
        return util.exportExcel(list, "app");
    }
}

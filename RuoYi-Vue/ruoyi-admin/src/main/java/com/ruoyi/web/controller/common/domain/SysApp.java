package com.ruoyi.web.controller.common.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 第三方系统app实体类
 */
public class SysApp extends BaseEntity {
    /**
     * 随机生成的16位字符串
     */
    @Excel(name = "appKey", width = 25)
    private String appKey;

    /**
     * 随机生成的20位字符串
     */
    @Excel(name = "appSecret", width = 30)
    private String appSecret;

    /**
     * 审核状态（0-否，1-是, 2-审核中）
     */
    @Excel(name = "审核状态",readConverterExp = "0=未通过,1=通过,2=审核中")
    private Integer auditState;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 25, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** app审核人的编号 */
    @Excel(name = "审核人编号")
    private String auditorId;

    /** app审核人的姓名 */
    @Excel(name = "审核人姓名")
    private String auditorName;

    /** app审核人的联系电话 */
    @Excel(name = "审核人联系电话")
    private String auditorPhone;

    /**
     * 审核时间
     */
    @Excel(name = "审核时间", width = 25, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date auditTime;

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public Integer getAuditState() {
        return auditState;
    }

    public void setAuditState(Integer auditState) {
        this.auditState = auditState;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getAuditorName() {
        return auditorName;
    }

    public void setAuditorName(String auditorName) {
        this.auditorName = auditorName;
    }

    public String getAuditorId() {
        return auditorId;
    }

    public void setAuditorId(String auditorId) {
        this.auditorId = auditorId;
    }

    public String getAuditorPhone() {
        return auditorPhone;
    }

    public void setAuditorPhone(String auditorPhone) {
        this.auditorPhone = auditorPhone;
    }

    public Date getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Date auditTime) {
        this.auditTime = auditTime;
    }

    @Override
    public String toString() {
        return "SysApp{" +
                "appKey='" + appKey + '\'' +
                ", appSecret='" + appSecret + '\'' +
                ", auditState=" + auditState +
                ", createTime=" + createTime +
                ", auditorName=" + auditorName +
                ", auditTime=" + auditTime +
                '}';
    }
}

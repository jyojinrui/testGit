package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysQgajStaItem;
import com.ruoyi.system.domain.SysSta;
import com.ruoyi.system.service.ISysStaService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 统计报表Controller
 * 
 * @author ruoyi
 * @date 2020-12-23
 */
@RestController
@RequestMapping("/system/sta")
public class SysStaController extends BaseController
{
    @Autowired
    private ISysStaService sysStaService;

    /**
     * 查询统计报表列表
     */
    @PreAuthorize("@ss.hasPermi('system:sta:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysSta sysSta)
    {
        startPage();
        List<SysSta> list = sysStaService.selectSysStaList(sysSta);
        return getDataTable(list);
    }

    /**
     * 根据小区编码统计各个小区的数据
     */
    @PreAuthorize("@ss.hasPermi('system:sta:list')")
    @GetMapping("/liststabyxqbm")
    public TableDataInfo listByXqbm(SysSta sysSta)
    {
        startPage();
        List<SysSta> list = sysStaService.selectSysStaListByXqbm(sysSta);
        
        return getDataTable(list);
        
    }
    
    /**
     * 根据区公安局编码统计各局的数据信息(不考虑分页的情况)
     */
    @PreAuthorize("@ss.hasPermi('system:sta:list')")
    @GetMapping("/listallstagroupbyqgajbm")
    public TableDataInfo listallstagroupbyqgajbm()
    {
        List<SysQgajStaItem> list = sysStaService.selectAllSysStaListByQgajbm(); 
        return getDataTable(list);
        
    }
    
    
    
    /**
     * 导出统计报表列表
     */
    @PreAuthorize("@ss.hasPermi('system:sta:export')")
    @Log(title = "统计报表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysSta sysSta)
    {
        List<SysSta> list = sysStaService.selectSysStaList(sysSta);
        
        ExcelUtil<SysSta> util = new ExcelUtil<SysSta>(SysSta.class);
        return util.exportExcel(list, "sta");
    }

    /**
     * 获取统计报表详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:sta:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(sysStaService.selectSysStaById(id));
    }

    /**
     * 新增统计报表
     */
    @PreAuthorize("@ss.hasPermi('system:sta:add')")
    @Log(title = "统计报表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysSta sysSta)
    {
        return toAjax(sysStaService.insertSysSta(sysSta));
    }

    /**
     * 修改统计报表
     */
    @PreAuthorize("@ss.hasPermi('system:sta:edit')")
    @Log(title = "统计报表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysSta sysSta)
    {
        return toAjax(sysStaService.updateSysSta(sysSta));
    }

    /**
     * 删除统计报表
     */
    @PreAuthorize("@ss.hasPermi('system:sta:remove')")
    @Log(title = "统计报表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sysStaService.deleteSysStaByIds(ids));
    }
}

package com.ruoyi.web.controller.common.mapper;

import java.util.List;

import com.ruoyi.web.controller.common.domain.AppApplyInfo;

/**
 * app申请信息 mapper接口
 */
public interface AppApplyInfoMapper {

    /**
     * 查询所有app申请列表
     * @param appApplyInfo 条件封装成的app申请信息对象
     * @return app申请信息集合
     */
    public List<AppApplyInfo> selectAppApplyInfoByConditions(AppApplyInfo appApplyInfo);

    /**
     * 根据id查询对应的app申请信息
     * @param id
     * @return
     */
    public AppApplyInfo selectAppApplyInfoById(int id);

    /**
     * 根据ids查询app申请信息列表
     *
     * @param ids app申请信息ID列表
     * @return app申请信息集合
     */
    public List<AppApplyInfo> selectAppApplyInfoListByIds(int[] ids);

    /**
     * 新增app申请信息
     * @param appApplyInfo
     * @return
     */
    public int addAppApplyInfo(AppApplyInfo appApplyInfo);

    /**
     * 修改app申请信息
     * @param appApplyInfo
     * @return
     */
    public int updateAppApplyInfo(AppApplyInfo appApplyInfo);

    /**
     * 通过用户ID删除app申请信息
     *
     * @param id 编号
     * @return 结果
     */
    public int deleteAppApplyInfoById(int id);

    /**
     * 批量删除app申请信息
     *
     * @param ids 需要删除的编号列表
     * @return 结果
     */
    public int deleteAppApplyInfoByIds(int[] ids);
}

package com.ruoyi.web.controller.common.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * App和申请信息关联对象 sys_app_apply_details
 * 
 * @author zhangsan
 * @date 2020-12-05
 */
public class SysAppApplyDetails extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private int id;

    /** app申请信息编号 */
    @Excel(name = "app申请信息编号")
    private int applyInfoId;

    /** appKey */
    @Excel(name = "appKey")
    private String appKey;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    public void setApplyInfoId(int applyInfoId) {
        this.applyInfoId = applyInfoId;
    }

    public int getApplyInfoId() {
        return applyInfoId;
    }
    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppKey() {
        return appKey;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("applyInfoId", getApplyInfoId())
            .append("appKey", getAppKey())
            .toString();
    }
}

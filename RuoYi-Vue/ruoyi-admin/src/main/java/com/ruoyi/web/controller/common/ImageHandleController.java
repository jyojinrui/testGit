package com.ruoyi.web.controller.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.FTPUtils;
import com.ruoyi.common.utils.MD5Util;
import com.ruoyi.common.utils.file.FileUtils;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.util.Base64;

/**
 * 图片处理Controller
 * 
 * @author ruoyi
 * @date 2020-11-10
 */
@RestController
@RequestMapping("/common/imgHandler")
public class ImageHandleController {
	@Value("${ruoyi.profile}")
	private String fileUploadPath;

	@Value("${ftpServer.ip}")
	private String ip;

	@Value("${ftpServer.port}")
	private int port;

	@Value("${ftpServer.username}")
	private String username;

	@Value("${ftpServer.password}")
	private String password;

	@Value("${ftpServer.downPath}")
	private String downPath;
	
	
	@Value("${ftpServer.vice_username}")
	private String vice_username;

	@Value("${ftpServer.vice_password}")
	private String vice_password;
	
	@Value("${deployEnvironment.outeruploadImg}")
	private String outeruploadImg;

	@Value("${deployEnvironment.inneruploadImg}")
	private String inneruploadImg;
	
	@Value("${deployEnvironment.outergetImg}")
	private String outergetImg;

	@Value("${deployEnvironment.innergetImg}")
	private String innergetImg;
	

	/**
	 * 上传证件照片
	 * 
	 * @param req
	 * @param multiReq
	 * @return
	 * @throws IOException
	 */
	
	//部署在外网上传图片
	@RequestMapping(value = "/imgUploadWw")
	public AjaxResult imgUploadWw(HttpServletRequest req, MultipartHttpServletRequest multiReq) throws IOException {
		// 获取图片上传的公共路径（文件夹）
		File imgDirPath = new File(fileUploadPath);
		if (!imgDirPath.exists()) {
			// 若不存在文件夹，则创建一个文件夹
			imgDirPath.mkdirs();
		}
		String buildingId = multiReq.getHeader("buildingId");
		// 获取市、区、派出所、警务室以及小区编码
		String profilePicture = "identificationPhotos";
		String city = buildingId.substring(0, 4);
		String area = buildingId.substring(4, 6);
		String policeStation = buildingId.substring(6, 9);
		String policeOffice = buildingId.substring(9, 12);
		String community = buildingId.substring(12, buildingId.length());
		String subDirPath = fileUploadPath + "/" + profilePicture + "/" + city + "/" + area + "/" + policeStation + "/"
				+ policeOffice + "/" + community;
		imgDirPath = new File(subDirPath);
		if (!imgDirPath.exists()) {
			// 若不存在文件夹，则创建一个文件夹
			imgDirPath.mkdirs();
		}
		MultipartFile file = multiReq.getFile("file");

//		InputStream in=new FileInputStream();

		// 获取图片原文件名
		String originalFilename = file.getOriginalFilename();
		// 获取图片格式后缀
		String suffix = originalFilename.substring(originalFilename.indexOf("."));
		// 重新生成图片名称
		String localFileName = MD5Util.md5(file.getInputStream()) + System.currentTimeMillis() + suffix;

		File localFile = new File(subDirPath + "/" + localFileName);
		try {

			file.transferTo(localFile);
			
			// 利用ftp将图片上传到远程的远程
			FileInputStream fis = new FileInputStream(localFile);
			FileInputStream fis_vice = new FileInputStream(localFile);
		
			FTPUtils.storeFile(ip, port, username, password, "/" + profilePicture + "/" + city + "/" + area + "/"
					+ policeStation + "/" + policeOffice + "/" + community, localFileName, fis);
	
			//利用ftp将图片文件上传到远程的远程FTP通道文件夹中
			FTPUtils.storeFile(ip, port, vice_username, vice_password, "/" + profilePicture + "/" + city + "/" + area + "/"
					+ policeStation + "/" + policeOffice + "/" + community, localFileName, fis_vice);
	
		} catch (IOException e) {
			e.printStackTrace();
			return AjaxResult.error("照片上传失败，请联系管理员！");
		}
		System.out.println("照片上传成功!");

		AjaxResult ajaxResult = new AjaxResult();
		ajaxResult.put("imgUrl", "/" + profilePicture + "/" + city + "/" + area + "/" + policeStation + "/"
				+ policeOffice + "/" + community + "/" + localFileName);
		return ajaxResult;
	}

	
	//本地开发与部署在内网上传图片
	@RequestMapping(value = "/imgUpload")
	public AjaxResult imgUploadNw(HttpServletRequest req, MultipartHttpServletRequest multiReq) throws IOException {

		System.out.println("<=========================>");
		// 获取图片上传的公共路径（文件夹）
		File imgDirPath = new File(fileUploadPath);
		if (!imgDirPath.exists()) {
			// 若不存在文件夹，则创建一个文件夹
			imgDirPath.mkdirs();
		}
		
		String buildingId = multiReq.getHeader("buildingId");
		// 获取市、区、派出所、警务室以及小区编码
		String profilePicture = "identificationPhotos";
		String city = buildingId.substring(0, 4);
		String area = buildingId.substring(4, 6);
		String policeStation = buildingId.substring(6, 9);
		String policeOffice = buildingId.substring(9, 12);
		String community = buildingId.substring(12, buildingId.length());
		String subDirPath = fileUploadPath + "/" + profilePicture + "/" + city + "/" + area + "/" + policeStation + "/"
				+ policeOffice + "/" + community;
		imgDirPath = new File(subDirPath);

		if (!imgDirPath.exists()) {
			// 若不存在文件夹，则创建一个文件夹
			imgDirPath.mkdirs();
		}
		MultipartFile file = multiReq.getFile("file");
		// 获取图片原文件名
		String originalFilename = file.getOriginalFilename();
		// 获取图片格式后缀
		String suffix = originalFilename.substring(originalFilename.indexOf("."));
		// 重新生成图片名称
		String localFileName = MD5Util.md5(file.getInputStream()) + System.currentTimeMillis() + suffix;
		File localFile = new File(subDirPath + "/" + localFileName);
		try {

			file.transferTo(localFile);
		} catch (IOException e) {
			e.printStackTrace();
			return AjaxResult.error("照片上传失败，请联系管理员！");
		}
		System.out.println("照片上传成功，保存位置：" + subDirPath + "/" + localFileName);
		AjaxResult ajaxResult = new AjaxResult();
		ajaxResult.put("imgUrl", "/" + profilePicture + "/" + city + "/" + area + "/" + policeStation + "/"
				+ policeOffice + "/" + community + "/" + localFileName);
		return ajaxResult;
	}

	// 将图片以Blob的形式存储到数据库中
	@RequestMapping(value = "/imgUploadBlob")
	public AjaxResult imgUploadBlob(HttpServletRequest req, MultipartHttpServletRequest multiReq) throws IOException {

		// 获取图片上传的公共路径（文件夹）
		File imgDirPath = new File(fileUploadPath);
		if (!imgDirPath.exists()) {
			// 若不存在文件夹，则创建一个文件夹
			imgDirPath.mkdirs();
		}
		String buildingId = multiReq.getHeader("buildingId");
		// 获取市、区、派出所、警务室以及小区编码
		String profilePicture = "identificationPhotos";
		String city = buildingId.substring(0, 4);
		String area = buildingId.substring(4, 6);
		String policeStation = buildingId.substring(6, 9);
		String policeOffice = buildingId.substring(9, 12);
		String community = buildingId.substring(12, buildingId.length());
		String subDirPath = fileUploadPath + "/" + profilePicture + "/" + city + "/" + area + "/" + policeStation + "/"
				+ policeOffice + "/" + community;
		imgDirPath = new File(subDirPath);

		if (!imgDirPath.exists()) {
			// 若不存在文件夹，则创建一个文件夹
			imgDirPath.mkdirs();
		}
		MultipartFile file = multiReq.getFile("file");

		// 获取图片原文件名
		String originalFilename = file.getOriginalFilename();
		// 获取图片格式后缀
		String suffix = originalFilename.substring(originalFilename.indexOf("."));
		// 重新生成图片名称
		String localFileName = MD5Util.md5(file.getInputStream()) + System.currentTimeMillis() + suffix;
		File localFile = new File(subDirPath + "/" + localFileName);
		try {

//			file.transferTo(localFile);
//
//			FileInputStream fis = new FileInputStream(localFile);

//			InputStream fis1=file.getInputStream();

			CommonsMultipartFile cFile = (CommonsMultipartFile) file;
			DiskFileItem fileItem = (DiskFileItem) cFile.getFileItem();
			InputStream inputStream = fileItem.getInputStream();
			int a = 1;

		} catch (IOException e) {
			e.printStackTrace();
			return AjaxResult.error("照片上传失败，请联系管理员！");
		}
		System.out.println("照片上传成功，保存位置：" + subDirPath + "/" + localFileName);
		AjaxResult ajaxResult = new AjaxResult();
		ajaxResult.put("imgUrl", "/" + profilePicture + "/" + city + "/" + area + "/" + policeStation + "/"
				+ policeOffice + "/" + community + "/" + localFileName);
		return ajaxResult;
	}

	/**
	 * 删除证件照片
	 * 
	 * @param file1
	 * @return
	 */
	@RequestMapping("/delFile")
	public String delFile(@RequestParam("file") MultipartFile file1) {
		String resultInfo = null;
		String originalFilename = file1.getOriginalFilename();
		String path = fileUploadPath + "/images/" + originalFilename;
		File file = new File(path);
		System.out.println(file.exists());
		if (file.exists()) {// 文件是否存在
			if (file.delete()) {// 存在就删了
				resultInfo = "图片删除成功";
			} else {
				resultInfo = "图片删除失败";
			}
		} else {
			resultInfo = "文件不存在！";
		}
		return resultInfo;
	}

	// 本地开发与部署在内网获取图片
	@RequestMapping(value="/getImg")
	public void getImgNw(@RequestParam(value = "fileRelativePath", required = true) String fileRelativePath,
			HttpServletResponse response) throws IOException {
		System.out.println("fileRelativePath");
		System.out.println(fileRelativePath);
		System.out.println("fileRelativePath");
		if (fileRelativePath.equals("") || fileRelativePath == null)
			return;
		FileInputStream fis = null;
		OutputStream os = null;
		String realPath = fileUploadPath + fileRelativePath;
		try {
			fis = new FileInputStream(realPath);
			System.out.println("照片已找到，本地路径为：" + realPath);
			os = response.getOutputStream();
			int count = 0;
			byte[] buffer = new byte[1024 * 3];
			while ((count = fis.read(buffer)) != -1) {
				os.write(buffer, 0, count);
				os.flush();
			}
		} catch (Exception e) {
			System.out.println("照片未找到！");
			e.printStackTrace();
		} finally {
			try {
				fis.close();
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	//部署在外网获取图片
	@RequestMapping(value="/getImgWw")
	public void getImgWw(@RequestParam(value = "fileRelativePath", required = true) String fileRelativePath,
			HttpServletResponse response) throws IOException {

		if (fileRelativePath.equals("") || fileRelativePath == null)
			return;
		// 内网存储的图片的相对路径
		String filePath = fileRelativePath.substring(0, fileRelativePath.lastIndexOf("/"));
		// 内网存储的图片的名称
		String fileName = fileRelativePath.substring(fileRelativePath.lastIndexOf("/") + 1);

		// 将图片下载到外网的downPath路径下
		// 利用ftp将图片上传到内网

		FTPUtils.retrieveFile(ip, port, username, password, filePath, fileName, downPath);
		// 获取外网图片的路径
		String realPath = downPath + File.separator + fileName;
		FileInputStream fis = null;
		OutputStream os = null;
		try {
			fis = new FileInputStream(realPath);
			StringBuilder str = new StringBuilder();
			os = response.getOutputStream();
			int count = 0;

			byte[] buffer = new byte[1024 * 3];
			while ((count = fis.read(buffer)) != -1) {
				os.write(buffer, 0, count);
				os.flush();
			}
//			byte[] buffer = new byte[fis.available()];
//			fis.read(buffer);
//			for (byte bf : buffer) {
//				str.append(Integer.toBinaryString(bf));
//			}
//			Base64.Encoder encoder=Base64.getEncoder();
//		    return encoder.encodeToString(str.toString().getBytes());	
//			return str.toString();
		} catch (Exception e) {
			System.out.println("照片未找到！");
			e.printStackTrace();
		} finally {
			try {
				// 删除外网的图片及其保存的路径文件夹
//				FileUtils.delFolder(downPath);
				fis.close();
				os.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
//		return null;
	}
}

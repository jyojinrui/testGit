package com.ruoyi.web.controller.common.utils;

import java.util.Date;

import com.ruoyi.common.utils.sign.Md5Utils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.web.controller.common.domain.SysApp;

public class AppGenUtils {

    public static void main(String[] args) {
        SysApp sysApp = randomGenApp();
        System.out.println(sysApp);
        if (sysApp != null) {
            System.out.println("sign: " + genSign(sysApp.getCreateTime().getTime(), sysApp.getAppSecret()));
        }
    }

    /**
     * 随机生成app
     * @return
     */
    public static SysApp randomGenApp() {
        String appKey = IdUtils.generateAppKey();
        String appSecret = IdUtils.generateAppSecret();
        long timestamp = System.currentTimeMillis();
        SysApp sysApp = new SysApp();
        sysApp.setAppKey(appKey);
        sysApp.setAppSecret(appSecret);
        sysApp.setAuditState(2);
        sysApp.setCreateTime(new Date(timestamp));
        return sysApp;
    }

    /**
     * 生成签名
     * @param timestamp
     * @param appSecret
     * @return
     */
    public static String genSign(long timestamp, String appSecret) {
        return Md5Utils.hash(timestamp + "#" + appSecret);
    }
}

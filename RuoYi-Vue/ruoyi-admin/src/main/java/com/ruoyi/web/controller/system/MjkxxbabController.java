package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Mjkxxbab;
import com.ruoyi.system.service.IMjkxxbabService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 门禁卡Controller
 * 
 * @author Carry
 * @date 2020-09-29
 */
@Api("门禁卡信息接口")
@RestController
@RequestMapping("/system/mjkxxbab")
public class MjkxxbabController extends BaseController
{
    @Autowired
    private IMjkxxbabService mjkxxbabService;

    /**
     * 根据小区管理员查看小区门禁卡信息
     */
    @ApiOperation("根据小区管理员查看小区门禁卡信息")
    @GetMapping("/listmjkxxbabbyxqadminid")
    public TableDataInfo listmjkxxbabbyxqadminid(Long userId,Mjkxxbab mjkxxbab)
    {
        startPage();
        List<Mjkxxbab> list = mjkxxbabService.selectMjkxxbabByXqAdminId(userId,mjkxxbab);
        return getDataTable(list);
    }
    
    @GetMapping("/listmjkxxbabbydeptid")
    public TableDataInfo listMjkxxbabByDeptId(Long deptId,Long deptIdOfTree,Mjkxxbab mjkxxbab)
    {
        startPage();
        List<Mjkxxbab> list = mjkxxbabService.selectMjkxxbabListByDeptId(deptId, deptIdOfTree,mjkxxbab);
        return getDataTable(list);
    }
    
    /**
     * 查询门禁卡列表
     */
    @ApiOperation("查询门禁卡列表")
    @PreAuthorize("@ss.hasPermi('system:mjkxxbab:list')")
    @GetMapping("/list")
    public TableDataInfo list(Mjkxxbab mjkxxbab)
    {
        startPage();
        List<Mjkxxbab> list = mjkxxbabService.selectMjkxxbabList(mjkxxbab);
        return getDataTable(list);
    }

    
 
    /**
     * 导出门禁卡列表
     */
    @ApiOperation("导出门禁卡列表")
    @PreAuthorize("@ss.hasPermi('system:mjkxxbab:export')")
    @Log(title = "门禁卡", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Mjkxxbab mjkxxbab)
    {
        List<Mjkxxbab> list = mjkxxbabService.selectMjkxxbabList(mjkxxbab);
        ExcelUtil<Mjkxxbab> util = new ExcelUtil<Mjkxxbab>(Mjkxxbab.class);
        return util.exportExcel(list, "mjkxxbab");
    }

    /**
     * 获取门禁卡详细信息
     */
    @ApiOperation("获取门禁卡详细信息")
    @PreAuthorize("@ss.hasPermi('system:mjkxxbab:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(mjkxxbabService.selectMjkxxbabById(id));
    }

    /**
     * 新增门禁卡
     */
    @ApiOperation("新增门禁卡")
    @PreAuthorize("@ss.hasPermi('system:mjkxxbab:add')")
    @Log(title = "门禁卡", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Mjkxxbab mjkxxbab)
    {
        return toAjax(mjkxxbabService.insertMjkxxbab(mjkxxbab));
    }

    /**
     * 修改门禁卡
     */
    @ApiOperation("修改门禁卡")
    @PreAuthorize("@ss.hasPermi('system:mjkxxbab:edit')")
    @Log(title = "门禁卡", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Mjkxxbab mjkxxbab)
    {
        return toAjax(mjkxxbabService.updateMjkxxbab(mjkxxbab));
    }

    /**
     * 删除门禁卡
     */
    @ApiOperation("删除门禁卡")
    @PreAuthorize("@ss.hasPermi('system:mjkxxbab:remove')")
    @Log(title = "门禁卡", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(mjkxxbabService.deleteMjkxxbabByIds(ids));
    }
}

package com.ruoyi.web.controller.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.web.controller.common.domain.AppApplyInfo;
import com.ruoyi.web.controller.common.service.AppApplyInfoService;

/**
 * app申请信息Controller
 * 
 * @author zhangsan
 * @date 2020-12-05
 */
@RestController
@RequestMapping("${common.appApplyInfo.mainPath}")
public class AppApplyInfoController extends BaseController {
    @Autowired
    private AppApplyInfoService appApplyInfoService;

    /**
     * 查询app申请信息列表
     */
    @PreAuthorize("@ss.hasPermi('common:appApply:list')")
    @GetMapping("${common.appApplyInfo.subPath.listPath}")
    public TableDataInfo list(AppApplyInfo appApplyInfo) {
        startPage();
        List<AppApplyInfo> list = appApplyInfoService.selectAppApplyInfoByConditions(appApplyInfo);
        return getDataTable(list);
    }

    /**
     * 导出符合查询条件的app申请信息列表
     */
    @PreAuthorize("@ss.hasPermi('common:appApply:export')")
    @Log(title = "app申请信息", businessType = BusinessType.EXPORT)
    @GetMapping("${common.appApplyInfo.subPath.exportPath}")
    public AjaxResult export(AppApplyInfo appApplyInfo) {
        List<AppApplyInfo> list = appApplyInfoService.selectAppApplyInfoByConditions(appApplyInfo);
        ExcelUtil<AppApplyInfo> util = new ExcelUtil<AppApplyInfo>(AppApplyInfo.class);
        return util.exportExcel(list, "appApply");
    }
    
    /**
     * 导出选中的app申请信息列表
     */
    @PreAuthorize("@ss.hasPermi('common:appApply:export')")
    @Log(title = "app申请信息", businessType = BusinessType.EXPORT)
    @GetMapping("${common.appApplyInfo.subPath.exportByIdsPath}")
    public AjaxResult export(@PathVariable int[] ids) {
        List<AppApplyInfo> list = appApplyInfoService.selectAppApplyInfoListByIds(ids);
        ExcelUtil<AppApplyInfo> util = new ExcelUtil<AppApplyInfo>(AppApplyInfo.class);
        return util.exportExcel(list, "appApply");
    }

    /**
     * 获取app申请信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('common:appApply:query')")
    @GetMapping("${common.appApplyInfo.subPath.getByIdPath}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(appApplyInfoService.selectAppApplyInfoById(id));
    }

    /**
     * 新增app申请信息
     */
    @PreAuthorize("@ss.hasPermi('common:appApply:add')")
    @Log(title = "app申请信息", businessType = BusinessType.INSERT)
    @PostMapping("${common.appApplyInfo.subPath.addPath}")
    public AjaxResult add(@RequestBody AppApplyInfo appApplyInfo) {
        return toAjax(appApplyInfoService.addAppApplyInfo(appApplyInfo));
    }

    /**
     * 修改app申请信息
     */
    @PreAuthorize("@ss.hasPermi('common:appApply:edit')")
    @Log(title = "app申请信息", businessType = BusinessType.UPDATE)
    @PutMapping("${common.appApplyInfo.subPath.editPath}")
    public AjaxResult edit(@RequestBody AppApplyInfo appApplyInfo) {
        return toAjax(appApplyInfoService.updateAppApplyInfo(appApplyInfo));
    }

    /**
     * 删除app申请信息
     */
    @PreAuthorize("@ss.hasPermi('common:appApply:remove')")
    @Log(title = "app申请信息", businessType = BusinessType.DELETE)
	@DeleteMapping("${common.appApplyInfo.subPath.delByIdsPath}")
    public AjaxResult remove(@PathVariable int[] ids) {
        return toAjax(appApplyInfoService.deleteAppApplyInfoByIds(ids));
    }
}

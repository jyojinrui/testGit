package com.ruoyi.web.controller.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruoyi.web.controller.common.domain.SysAppApplyDetails;
import com.ruoyi.web.controller.common.mapper.SysAppApplyDetailsMapper;
import com.ruoyi.web.controller.common.service.ISysAppApplyDetailsService;

/**
 * App和申请信息关联Service业务层处理
 * 
 * @author zhangsan
 * @date 2020-12-05
 */
@Service
public class SysAppApplyDetailsServiceImpl implements ISysAppApplyDetailsService {
    @Autowired
    private SysAppApplyDetailsMapper sysAppApplyDetailsMapper;

    /**
     * 查询App和申请信息关联
     *
     * @param id App和申请信息关联ID
     * @return App和申请信息关联
     */
    @Override
    public SysAppApplyDetails selectSysAppApplyDetailsById(int id) {
        return sysAppApplyDetailsMapper.selectSysAppApplyDetailsById(id);
    }

    /**
     * 根据id查询App和申请信息关联
     *
     * @param applyInfoId 申请信息ID
     * @return App和申请信息关联
     */
    @Override
    public SysAppApplyDetails selectSysAppApplyDetailsByApplyInfoId(int applyInfoId) {
        return sysAppApplyDetailsMapper.selectSysAppApplyDetailsByApplyInfoId(applyInfoId);
    }

    /**
     * 查询App和申请信息关联列表
     *
     * @param sysAppApplyDetails App和申请信息关联
     * @return App和申请信息关联集合
     */
    @Override
    public List<SysAppApplyDetails> selectSysAppApplyDetailsList(SysAppApplyDetails sysAppApplyDetails) {
        return sysAppApplyDetailsMapper.selectSysAppApplyDetailsList(sysAppApplyDetails);
    }

    /**
     * 根据ids查询App和申请信息关联列表
     *
     * @param ids App和申请信息关联ID列表
     * @return App和申请信息关联集合
     */
    @Override
    public List<SysAppApplyDetails> selectSysAppApplyDetailsListByids(int[] ids) {
        return sysAppApplyDetailsMapper.selectSysAppApplyDetailsListByids(ids);
    }

    /**
     * 新增App和申请信息关联
     *
     * @param sysAppApplyDetails App和申请信息关联
     * @return 结果
     */
    @Override
    public int insertSysAppApplyDetails(SysAppApplyDetails sysAppApplyDetails) {
        return sysAppApplyDetailsMapper.insertSysAppApplyDetails(sysAppApplyDetails);
    }

    /**
     * 修改App和申请信息关联
     *
     * @param sysAppApplyDetails App和申请信息关联
     * @return 结果
     */
    @Override
    public int updateSysAppApplyDetails(SysAppApplyDetails sysAppApplyDetails) {
        return sysAppApplyDetailsMapper.updateSysAppApplyDetails(sysAppApplyDetails);
    }

    /**
     * 批量删除App和申请信息关联
     *
     * @param ids 需要删除的App和申请信息关联ID
     * @return 结果
     */
    @Override
    public int deleteSysAppApplyDetailsByIds(int[] ids) {
        return sysAppApplyDetailsMapper.deleteSysAppApplyDetailsByIds(ids);
    }

    /**
     * 删除App和申请信息关联信息
     *
     * @param id App和申请信息关联ID
     * @return 结果
     */
    @Override
    public int deleteSysAppApplyDetailsById(int id) {
        return sysAppApplyDetailsMapper.deleteSysAppApplyDetailsById(id);
    }
}

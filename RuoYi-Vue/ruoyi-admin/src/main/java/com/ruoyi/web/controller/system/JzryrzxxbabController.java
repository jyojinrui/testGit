package com.ruoyi.web.controller.system;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ImageHandleUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.domain.Jzryrzxxbab;
import com.ruoyi.system.service.IJzryrzxxbabService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 居住人员Controller
 * 
 * @author ruoyi
 * @date 2020-09-27
 */
@RestController
@RequestMapping("/system/jzryrzxxbab")
@Api("入住人员管理接口")
public class JzryrzxxbabController extends BaseController {
	private ImageHandleUtils imageHandleUtils = SpringUtils.getBean(ImageHandleUtils.class);
	@Autowired
	private IJzryrzxxbabService jzryrzxxbabService;

	/**
	 * listJzryrzxxbabbyxqadminid 根据小区管理员id获取入住人员信息
	 */
	@ApiOperation("根据小区管理员id获取入住人员信息")
	@GetMapping("/listjzryrzxxbabbyxqadminid")
	public TableDataInfo listbyxqadminid(Long userId, Jzryrzxxbab jzryrzxxbab) {
		startPage();
		List<Jzryrzxxbab> list = jzryrzxxbabService.selectJzryrzxxbabListByXqAdminId(userId, jzryrzxxbab);
//		String res1 = new String(list.get(0).getBlobzp());
		return getDataTable(list);
	}

	@GetMapping("/listjzryrzxxbabbydeptid")
	public TableDataInfo listjzryrzxxbabbydeptid(Long deptId, Long deptIdOfTree, Jzryrzxxbab jzryrzxxbab) {
		startPage();
		List<Jzryrzxxbab> list = jzryrzxxbabService.selectJzryrzxxbabListByDeptId(deptId, deptIdOfTree, jzryrzxxbab);
		return getDataTable(list);
	}

	/**
	 * 查询居住人员列表
	 */
//	@ApiOperation("查询居住人员列表")
//    @PreAuthorize("@ss.hasPermi('system:jzryrzxxbab:list')")
	@GetMapping("/list")
	public TableDataInfo list(Jzryrzxxbab jzryrzxxbab) {
		startPage();
		List<Jzryrzxxbab> list = jzryrzxxbabService.selectJzryrzxxbabList(jzryrzxxbab);

		return getDataTable(list);
	}

	/**
	 * 导出居住人员列表
	 */
	@ApiOperation("导出居住人员列表")
	@PreAuthorize("@ss.hasPermi('system:jzryrzxxbab:export')")
	@Log(title = "居住人员", businessType = BusinessType.EXPORT)
	@GetMapping("/export")
	public AjaxResult export(Jzryrzxxbab jzryrzxxbab) {
		List<Jzryrzxxbab> list = jzryrzxxbabService.selectJzryrzxxbabList(jzryrzxxbab);
		ExcelUtil<Jzryrzxxbab> util = new ExcelUtil<Jzryrzxxbab>(Jzryrzxxbab.class);
		return util.exportExcel(list, "jzryrzxxbab");
	}

	/**
	 * 获取居住人员详细信息
	 */
	@ApiOperation("获取居住人员详细信息")
//    @PreAuthorize("@ss.hasPermi('system:jzryrzxxbab:query')")
	@GetMapping(value = "/{id}")
	public AjaxResult getInfo(@PathVariable("id") Long id) {

		return AjaxResult.success(jzryrzxxbabService.selectJzryrzxxbabById(id));
	}

	/**
	 * 新增居住人员
	 * @throws IOException 
	 */
	@ApiOperation("新增居住人员")
//    @PreAuthorize("@ss.hasPermi('system:jzryrzxxbab:add')")
	@Log(title = "居住人员", businessType = BusinessType.INSERT)
	@PostMapping
	public AjaxResult add(@RequestBody Jzryrzxxbab jzryrzxxbab) throws IOException {
		System.out.println(jzryrzxxbab);
		jzryrzxxbab.setOpetype(0); // 操作类型为增加
		return toAjax(jzryrzxxbabService.insertJzryrzxxbab(jzryrzxxbab));
	}
	


	/**
	 * 新增居住人员
	 * @throws IOException 
	 */
	@ApiOperation("新增居住人员")
//    @PreAuthorize("@ss.hasPermi('system:jzryrzxxbab:add')")
	@Log(title = "居住人员", businessType = BusinessType.INSERT)
	@PostMapping("/others")
	public AjaxResult add() throws IOException {
		
		// 获取解密后的数据
		String data = (String) ServletUtils.getRequest().getAttribute("data");
		if (StringUtils.isEmpty(data)) {
			return AjaxResult.error(HttpStatus.BAD_REQUEST, "解密后字符串为空");
		}

		Jzryrzxxbab jzryrzxxbab = new Jzryrzxxbab();
		try{

			jzryrzxxbab = JSONObject.parseObject(data, Jzryrzxxbab.class);
		}catch (Exception e) {
			// TODO: handle exception

			return AjaxResult.error("无法解析此字符串");
		}
		jzryrzxxbab.setOpetype(0); // 操作类型为增加

		//存储照片
		String zp=jzryrzxxbab.getZp();
		if(StringUtils.isNotEmpty(zp)){
			byte[] zpByteArray=StringUtils.readByteArrayFromBase64(zp);
			String zpPath=(String)imageHandleUtils.imgUpload(zpByteArray, jzryrzxxbab.getBuildingId(), "identificationPhotos").get("imgUrl");
			jzryrzxxbab.setZp(zpPath);
		}
		return toAjax(jzryrzxxbabService.insertJzryrzxxbab(jzryrzxxbab));
	}

	/**
	 * 修改居住人员
	 * @throws IOException 
	 */
	@ApiOperation("修改居住人员")
	@PreAuthorize("@ss.hasPermi('system:jzryrzxxbab:edit')")
	@Log(title = "居住人员", businessType = BusinessType.UPDATE)
	@PutMapping
	public AjaxResult edit(@RequestBody Jzryrzxxbab jzryrzxxbab) throws IOException {
		jzryrzxxbab.setOpetype(1);
		return toAjax(jzryrzxxbabService.updateJzryrzxxbab(jzryrzxxbab));
	}

	/**
	 * 删除居住人员
	 */
	@ApiOperation("删除居住人员")
	@PreAuthorize("@ss.hasPermi('system:jzryrzxxbab:remove')")
	@Log(title = "居住人员", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public AjaxResult remove(@PathVariable Long[] ids) {
		


		return toAjax(jzryrzxxbabService.deleteJzryrzxxbabByIds(ids));
	}

}

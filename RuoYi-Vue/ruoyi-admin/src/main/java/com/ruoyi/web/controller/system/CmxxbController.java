package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Cmxxb;
import com.ruoyi.system.service.ICmxxbService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 串码信息Controller
 * 
 * @author Carry
 * @date 2020-10-04
 */
@RestController
@RequestMapping("/system/cmxxb")
@Api("串码信息接口")
public class CmxxbController extends BaseController
{
    @Autowired
    private ICmxxbService cmxxbService;

    /**
     * 查询串码信息列表
     */
    @ApiOperation("查询串码信息列表")
    @PreAuthorize("@ss.hasPermi('system:cmxxb:list')")
    @GetMapping("/list")
          //
    public TableDataInfo list(Cmxxb cmxxb)
    {
        startPage();
        List<Cmxxb> list = cmxxbService.selectCmxxbList(cmxxb);
        return getDataTable(list);
    }
    
    @ApiOperation("用用户ID查询串码信息列表")
    @GetMapping("/listcmxxbbyxqadminid")
    public TableDataInfo listcmxxbbyxqadminid(Long userId,Cmxxb cmxxb)
    {
        startPage();
        List<Cmxxb> list = cmxxbService.selectCmxxbListByXqAdminId(userId,cmxxb);
        return getDataTable(list);
    }

    
    @GetMapping("/listcmxxbbydeptid")
    public TableDataInfo listcmxxbbydeptid(Long deptId,Cmxxb cmxxb)
    {
        startPage();
        List<Cmxxb> list = cmxxbService.selectCmxxbListByDeptId(deptId,cmxxb);
        return getDataTable(list);
    }
    /**
     * 导出串码信息列表
     */
    @ApiOperation("导出串码信息列表")
    @PreAuthorize("@ss.hasPermi('system:cmxxb:export')")
    @Log(title = "串码信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Cmxxb cmxxb)
    {
        List<Cmxxb> list = cmxxbService.selectCmxxbList(cmxxb);
        ExcelUtil<Cmxxb> util = new ExcelUtil<Cmxxb>(Cmxxb.class);
        return util.exportExcel(list, "cmxxb");
    }

    /**
     * 获取串码信息详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:cmxxb:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation("获取串码信息列表")     
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
    	if(cmxxbService.selectCmxxbById(id)==null)
    		return AjaxResult.error();
        return AjaxResult.success(cmxxbService.selectCmxxbById(id));
    }

    /**
     * 新增串码信息
     */
    @ApiOperation("新增串码信息")
    @PreAuthorize("@ss.hasPermi('system:cmxxb:add')")
    @Log(title = "串码信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Cmxxb cmxxb)
    {
        return toAjax(cmxxbService.insertCmxxb(cmxxb));
    }

    /**
     * 修改串码信息
     */
    @ApiOperation("修改串码信息")
    @PreAuthorize("@ss.hasPermi('system:cmxxb:edit')")
    @Log(title = "串码信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Cmxxb cmxxb)
    {
        return toAjax(cmxxbService.updateCmxxb(cmxxb));
    }

    /**
     * 删除串码信息
     */
    @ApiOperation("删除串码信息")
    @PreAuthorize("@ss.hasPermi('system:cmxxb:remove')")
    @Log(title = "串码信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cmxxbService.deleteCmxxbByIds(ids));
    }
}

package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysPcsxxbab;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Cmxxb;
import com.ruoyi.system.domain.Pcsxxbab;
import com.ruoyi.system.domain.Xqxxbab;
import com.ruoyi.system.service.IPcsxxbabService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * 派出所信息Controller
 * 
 * @author Carry
 * @date 2020-10-21
 */
@Api("派出所信息接口")
@RestController
@RequestMapping("/system/pcsxxbab")
public class PcsxxbabController extends BaseController {
	@Autowired
	private IPcsxxbabService pcsxxbabService;

	/**
	 * 查询派出所信息列表
	 */
	@ApiOperation("查询派出所信息列表")
	@PreAuthorize("@ss.hasPermi('system:pcsxxbab:list')")
	@GetMapping("/list")
	public TableDataInfo list(Pcsxxbab pcsxxbab) {
		startPage();
		List<Pcsxxbab> list = pcsxxbabService.selectPcsxxbabList(pcsxxbab);
		return getDataTable(list);
	}

	
	/**
	 * 查询派出所信息列表
	 */
	@ApiOperation("查询派出所信息列表")
	@PreAuthorize("@ss.hasPermi('system:pcsxxbab:list')")
	@GetMapping("/listpcsxxbabbydeptid")
	public TableDataInfo listpcsxxbabbydeptid(Long deptId,Long deptIdOfTree,Pcsxxbab pcsxxbab) {
		startPage();
		List<Pcsxxbab> list = pcsxxbabService.selectPcsxxbabListByDeptId(deptId,deptIdOfTree,pcsxxbab);
		return getDataTable(list);
	}
	/**
	 * 导出派出所信息列表
	 */
	@ApiOperation("导出派出所信息列表")
	@PreAuthorize("@ss.hasPermi('system:pcsxxbab:export')")
	@Log(title = "派出所信息", businessType = BusinessType.EXPORT)
	@GetMapping("/export")
	public AjaxResult export(Pcsxxbab pcsxxbab) {
		List<Pcsxxbab> list = pcsxxbabService.selectPcsxxbabList(pcsxxbab);
		ExcelUtil<Pcsxxbab> util = new ExcelUtil<Pcsxxbab>(Pcsxxbab.class);
		return util.exportExcel(list, "pcsxxbab");
	}

	/**
	 * 获取派出所信息详细信息
	 */
	@ApiOperation("获取派出所信息详细信息")
	@PreAuthorize("@ss.hasPermi('system:pcsxxbab:query')")
	@GetMapping(value = "/{id}")
	public AjaxResult getInfo(@PathVariable("id") Long id) {
		return AjaxResult.success(pcsxxbabService.selectPcsxxbabById(id));
	}

	
//	@PreAuthorize("@ss.hasPermi('system:pcsxxbab:query')")
//	@GetMapping(value = "/getsubdeptbyqgajdeptid/{deptid}")
//	public AjaxResult getSubDeptByqgajDeptId(@PathVariable("deptid") Long deptid) {
//		return AjaxResult.success(pcsxxbabService.selectSubDeptByQgajDeptId(deptid));
//	}
	
	/**
	 * 根据所属区县获取派出所信息
	 * 
	 * @param ssqx
	 * @return
	 */
	@ApiOperation("根据所属区县获取派出所信息")
	@PostMapping("/listpcsbyssqx")
	public List<Pcsxxbab> listPcsBySsqx(@RequestBody String ssqx) {
		List<Pcsxxbab> list = pcsxxbabService.selectPcsxxbabListBySsqx(ssqx);
		return list;
	}

	/**
	 * 新增派出所信息
	 */
	@ApiOperation("新增派出所信息")
//	@PreAuthorize("@ss.hasPermi('system:pcsxxbab:add')")
	@Log(title = "派出所信息", businessType = BusinessType.INSERT)
	@PostMapping
	public AjaxResult add(@RequestBody SysPcsxxbab pcsxxbab) {
//		System.out.println("================>");
//		System.out.println(pcsxxbab);
//		System.out.println("================>");
		return toAjax(pcsxxbabService.insertPcsxxbab(pcsxxbab));
	}

	/**
	 * 修改派出所信息
	 */
	@ApiOperation("修改派出所信息")
	@PreAuthorize("@ss.hasPermi('system:pcsxxbab:edit')")
	@Log(title = "派出所信息", businessType = BusinessType.UPDATE)
	@PutMapping
	public AjaxResult edit(@RequestBody Pcsxxbab pcsxxbab) {
		return toAjax(pcsxxbabService.updatePcsxxbab(pcsxxbab));
	}

	/**
	 * 删除派出所信息
	 */
	@ApiOperation("删除派出所信息")
	@PreAuthorize("@ss.hasPermi('system:pcsxxbab:remove')")
	@Log(title = "派出所信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public AjaxResult remove(@PathVariable Long[] ids) {
		return toAjax(pcsxxbabService.deletePcsxxbabByIds(ids));
	}
}

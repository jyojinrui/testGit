package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Sydwxxbab;
import com.ruoyi.system.service.ISydwxxbabService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 实有单位信息Controller
 * 
 * @author Carry
 * @date 2020-10-27
 */
@Api("实有单位信息接口")
@RestController
@RequestMapping("/system/sydwxxbab")
public class SydwxxbabController extends BaseController
{
    @Autowired
    private ISydwxxbabService sydwxxbabService;

    /**
     * 查询实有单位信息列表
     */
//    @ApiOperation("查询实有单位列表")
//    @PreAuthorize("@ss.hasPermi('system:sydwxxbab:list')")
    @GetMapping("/list")
    public TableDataInfo list(Sydwxxbab sydwxxbab)
    {
        startPage();
        List<Sydwxxbab> list = sydwxxbabService.selectSydwxxbabList(sydwxxbab);
        return getDataTable(list);
    }

    /**
     * 导出实有单位信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:sydwxxbab:export')")
    @Log(title = "实有单位信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Sydwxxbab sydwxxbab)
    {
        List<Sydwxxbab> list = sydwxxbabService.selectSydwxxbabList(sydwxxbab);
        ExcelUtil<Sydwxxbab> util = new ExcelUtil<Sydwxxbab>(Sydwxxbab.class);
        return util.exportExcel(list, "sydwxxbab");
    }

    /**
     * 获取实有单位信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:sydwxxbab:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(sydwxxbabService.selectSydwxxbabById(id));
    }

    /**
     * 新增实有单位信息
     */
    @ApiOperation("新增实有单位信息")
//    @PreAuthorize("@ss.hasPermi('system:sydwxxbab:add')")
    @Log(title = "实有单位信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Sydwxxbab sydwxxbab)
    {
        return toAjax(sydwxxbabService.insertSydwxxbab(sydwxxbab));
    }

    /**
     * 修改实有单位信息
     */
    @PreAuthorize("@ss.hasPermi('system:sydwxxbab:edit')")
    @Log(title = "实有单位信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Sydwxxbab sydwxxbab)
    {
        return toAjax(sydwxxbabService.updateSydwxxbab(sydwxxbab));
    }

    /**
     * 删除实有单位信息
     */
    @PreAuthorize("@ss.hasPermi('system:sydwxxbab:remove')")
    @Log(title = "实有单位信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sydwxxbabService.deleteSydwxxbabByIds(ids));
    }
}

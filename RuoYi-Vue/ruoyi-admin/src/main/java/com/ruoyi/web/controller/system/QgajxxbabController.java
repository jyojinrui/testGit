package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.statistics.QgajSta;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Qgajxxbab;
import com.ruoyi.system.service.IQgajxxbabService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 区公安局信息Controller
 * 
 * @author Carry
 * @date 2020-10-25
 */
@Api("区公安局信息接口")
@RestController
@RequestMapping("/system/qgajxxbab")
public class QgajxxbabController extends BaseController
{
    @Autowired
    private IQgajxxbabService qgajxxbabService;

    /**
     * 查询区公安局信息列表
     */
    @ApiOperation("查询区公安局信息列表")
    @PreAuthorize("@ss.hasPermi('system:qgajxxbab:list')")
    @GetMapping("/list")
    public TableDataInfo list(Qgajxxbab qgajxxbab)
    {
        startPage();
        List<Qgajxxbab> list = qgajxxbabService.selectQgajxxbabList(qgajxxbab);
        return getDataTable(list);
    }
    
    @ApiOperation("查询区公安局信息列表")
    @PreAuthorize("@ss.hasPermi('system:qgajxxbab:list')")
    @GetMapping("/listqgajxxbabbydeptid")
    public TableDataInfo listqgajxxbabbydeptid(Long deptId,Long deptIdOfTree,Qgajxxbab qgajxxbab)
    {
    	System.out.println(deptId);
    	System.out.println(deptIdOfTree);
    	System.out.println(qgajxxbab);
        startPage();
        List<Qgajxxbab> list = qgajxxbabService.selectQgajxxbabListByDeptId(deptId,deptIdOfTree,qgajxxbab);
        return getDataTable(list);
    }
    
    
    /**
     * 导出区公安局信息列表
     */
    @ApiOperation("导出区公安局信息列表")
    @PreAuthorize("@ss.hasPermi('system:qgajxxbab:export')")
    @Log(title = "区公安局信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Qgajxxbab qgajxxbab)
    {
        List<Qgajxxbab> list = qgajxxbabService.selectQgajxxbabList(qgajxxbab);
        ExcelUtil<Qgajxxbab> util = new ExcelUtil<Qgajxxbab>(Qgajxxbab.class);
        return util.exportExcel(list, "qgajxxbab");
    }

    /**
     * 获取区公安局信息详细信息
     */
    @ApiOperation("通过管理员id获取区公安局信息详细信息")
    @GetMapping(value = "/getqgajxxbabbyadminid/{adminid}")
    public AjaxResult getInfoByAdminId(@PathVariable("adminid") Long adminid)
    {
//    	System.out.println(adminid);
//    	System.out.println("==>");
        return AjaxResult.success(qgajxxbabService.selectQgajxxbabByAdminId(adminid));
    }
    
    
    /**
     * 统计区公安局信息
     */
    @ApiOperation("通过管理员id获取区公安局信息详细信息")
    @GetMapping(value = "/getqgajstadata")
    public AjaxResult getqgajstadata()
    {
//    	System.out.println(adminid);
    	System.out.println("<==>");
    	List <QgajSta> list= (qgajxxbabService.selectQgajStaDataList());
    	System.out.println("<==>");
    	System.out.println(list);
        return AjaxResult.success(list);
    }
    
    /**
     * 获取区公安局信息详细信息
     */
    @ApiOperation("获取区公安局信息详细信息")
    @PreAuthorize("@ss.hasPermi('system:qgajxxbab:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(qgajxxbabService.selectQgajxxbabById(id));
    }
    
    

    /**
     * 新增区公安局信息
     */
    @ApiOperation("新增区公安局信息")
//    @PreAuthorize("@ss.hasPermi('system:qgajxxbab:add')")
    @Log(title = "区公安局信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Qgajxxbab qgajxxbab)
    {
        return toAjax(qgajxxbabService.insertQgajxxbab(qgajxxbab));
    }

    /**
     * 修改区公安局信息
     */
    @ApiOperation("修改区公安局信息")
    @PreAuthorize("@ss.hasPermi('system:qgajxxbab:edit')")
    @Log(title = "区公安局信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Qgajxxbab qgajxxbab)
    {
        return toAjax(qgajxxbabService.updateQgajxxbab(qgajxxbab));
    }

    /**
     * 删除区公安局信息
     */
    @ApiOperation("删除区公安局信息")
    @PreAuthorize("@ss.hasPermi('system:qgajxxbab:remove')")
    @Log(title = "区公安局信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(qgajxxbabService.deleteQgajxxbabByIds(ids));
    }
}

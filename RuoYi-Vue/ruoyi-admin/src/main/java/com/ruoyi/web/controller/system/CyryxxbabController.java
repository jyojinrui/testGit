package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Cyryxxbab;
import com.ruoyi.system.service.ICyryxxbabService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 从业人员信息Controller
 * 
 * @author Carry
 * @date 2020-10-27
 */
@Api("从业人员信息接口")
@RestController
@RequestMapping("/system/cyryxxbab")
public class CyryxxbabController extends BaseController
{
    @Autowired
    private ICyryxxbabService cyryxxbabService;

    /**
     * 查询从业人员信息列表
     */
//    @PreAuthorize("@ss.hasPermi('system:cyryxxbab:list')")
//    @ApiOperation("查询")
    @GetMapping("/list")
    public TableDataInfo list(Cyryxxbab cyryxxbab)
    {
        startPage();
        List<Cyryxxbab> list = cyryxxbabService.selectCyryxxbabList(cyryxxbab);
        return getDataTable(list);
    }

    /**
     * 导出从业人员信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:cyryxxbab:export')")
    @Log(title = "从业人员信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Cyryxxbab cyryxxbab)
    {
        List<Cyryxxbab> list = cyryxxbabService.selectCyryxxbabList(cyryxxbab);
        ExcelUtil<Cyryxxbab> util = new ExcelUtil<Cyryxxbab>(Cyryxxbab.class);
        return util.exportExcel(list, "cyryxxbab");
    }

    /**
     * 获取从业人员信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:cyryxxbab:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(cyryxxbabService.selectCyryxxbabById(id));
    }

    /**
     * 新增从业人员信息
     */
    @ApiOperation("新增从业人员信息")
//    @PreAuthorize("@ss.hasPermi('system:cyryxxbab:add')")
    @Log(title = "从业人员信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Cyryxxbab cyryxxbab)
    {
        return toAjax(cyryxxbabService.insertCyryxxbab(cyryxxbab));
    }

    /**
     * 修改从业人员信息
     */
    @PreAuthorize("@ss.hasPermi('system:cyryxxbab:edit')")
    @Log(title = "从业人员信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Cyryxxbab cyryxxbab)
    {
        return toAjax(cyryxxbabService.updateCyryxxbab(cyryxxbab));
    }

    /**
     * 删除从业人员信息
     */
    @PreAuthorize("@ss.hasPermi('system:cyryxxbab:remove')")
    @Log(title = "从业人员信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(cyryxxbabService.deleteCyryxxbabByIds(ids));
    }
}

package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Zzxqxxbab;
import com.ruoyi.system.service.IZzxqxxbabService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2021-12-20
 */
@RestController
@RequestMapping("/system/zzxqxxbab")
public class ZzxqxxbabController extends BaseController
{
    @Autowired
    private IZzxqxxbabService zzxqxxbabService;

    /**
     * 查询【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('system:zzxqxxbab:list')")
    @GetMapping("/list")
    public TableDataInfo list(Zzxqxxbab zzxqxxbab)
    {
        startPage();
        List<Zzxqxxbab> list = zzxqxxbabService.selectZzxqxxbabList(zzxqxxbab);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('system:zzxqxxbab:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Zzxqxxbab zzxqxxbab)
    {
        List<Zzxqxxbab> list = zzxqxxbabService.selectZzxqxxbabList(zzxqxxbab);
        ExcelUtil<Zzxqxxbab> util = new ExcelUtil<Zzxqxxbab>(Zzxqxxbab.class);
        return util.exportExcel(list, "zzxqxxbab");
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:zzxqxxbab:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(zzxqxxbabService.selectZzxqxxbabById(id));
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
//    @PreAuthorize("@ss.hasPermi('system:zzxqxxbab:query')")
    @GetMapping(value = "/{zzssxqbm}")
    public AjaxResult getOthers(@PathVariable("zzssxqbm") String zzssxqbm)
    {
    	Zzxqxxbab zzxqxxbab=new Zzxqxxbab();
    	zzxqxxbab.setZzssxqbm(zzssxqbm);
        return AjaxResult.success(zzxqxxbabService.selectZzxqxxbabList(zzxqxxbab));
    }

    /**
     * 新增【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:zzxqxxbab:add')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Zzxqxxbab zzxqxxbab)
    {
        return toAjax(zzxqxxbabService.insertZzxqxxbab(zzxqxxbab));
    }

    /**
     * 修改【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:zzxqxxbab:edit')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Zzxqxxbab zzxqxxbab)
    {
        return toAjax(zzxqxxbabService.updateZzxqxxbab(zzxqxxbab));
    }

    /**
     * 删除【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('system:zzxqxxbab:remove')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(zzxqxxbabService.deleteZzxqxxbabByIds(ids));
    }
}

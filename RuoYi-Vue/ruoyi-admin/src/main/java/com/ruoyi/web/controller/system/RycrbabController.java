package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Fzxxbab;
import com.ruoyi.system.domain.Rycrbab;
import com.ruoyi.system.service.IJzryrzxxbabService;
import com.ruoyi.system.service.IRycrbabService;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.utils.sign.AESUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 出入人员管理Controller
 * 
 * @author ruoyi
 * @date 2020-09-29
 */
@Api("出入人员管理接口")
@RestController
@RequestMapping("/system/rycrbab")
public class RycrbabController extends BaseController {
	@Autowired
	private IRycrbabService rycrbabService;

	@Autowired
	private IJzryrzxxbabService jzryrzxxbabService;

	/**
	 * listrycrbabbyxqadminid
	 */
	@ApiOperation("通过管理员id获得出入人员信息列表")
	@GetMapping("/listrycrbabbyxqadminid")
	public TableDataInfo listRycrbabByXqAdminId(Long userId, Rycrbab rycrbab) {
		startPage();
		List<Rycrbab> list = rycrbabService.selectRycrbabListByXqAdminId(userId, rycrbab);
		return getDataTable(list);
	}

	@GetMapping("/listtodayrycrbabbyxqadminid")
	public TableDataInfo listTodayRycrbabByXqAdminId(Long userId, Rycrbab rycrbab) {
		startPage();
		List<Rycrbab> list = rycrbabService.selectTodayRycrbabListByXqAdminId(userId, rycrbab);
		return getDataTable(list);
	}
	
	@GetMapping("/countallrycrbabbyxqadminid")
	public long countAllRycrbabListByXqAdminId(Long userId, Rycrbab rycrbab) {
		
		long total = rycrbabService.countAllRycrbabByXqAdminId(userId, rycrbab);
		return total;
	}
	
	@GetMapping("/counttodayrycrbabbyxqadminid")
	public long countTodayRycrbabListByXqAdminId(Long userId, Rycrbab rycrbab) {
		
		long total = rycrbabService.countTodayRycrbabByXqAdminId(userId, rycrbab);
		return total;
	}
	
	
	@GetMapping("/listallrycrbabbyxqadminid")
	public TableDataInfo listAllRycrbabByXqAdminId(Long userId, Rycrbab rycrbab) {
		startPage();
		List<Rycrbab> list = rycrbabService.selectAllRycrbabListByXqAdminId(userId, rycrbab);
		return getDataTable(list);
	}

	/**
	 * 查询出入人员管理列表
	 */
//    @ApiOperation("查询出入人员管理列表")
//    @PreAuthorize("@ss.hasPermi('system:rycrbab:list')")
	@GetMapping("/list")
	public TableDataInfo list(Rycrbab rycrbab) {
		startPage();
		List<Rycrbab> list = rycrbabService.selectRycrbabList(rycrbab);
		return getDataTable(list);
	}

//    listrycrbabbydeptid

	@GetMapping("/listrycrbabbydeptid")
	public TableDataInfo getRycrbabListByDeptId(Long deptId, Long deptIdOfTree, Rycrbab fzxxbab) {
		startPage();
		List<Rycrbab> list = rycrbabService.selectRycrbabListByDeptId(deptId, deptIdOfTree, fzxxbab);
		return getDataTable(list);

	}

	@GetMapping("/listtodayrycrbabbydeptid")
	public TableDataInfo getTodayRycrbabListByDeptId(Long deptId, Long deptIdOfTree, Rycrbab fzxxbab) {
		startPage();
		
		List<Rycrbab> list = rycrbabService.selectTodayRycrbabListByDeptId(deptId, deptIdOfTree, fzxxbab);
		return getDataTable(list);

	}
	@GetMapping("/listallrycrbabbydeptid")
	public TableDataInfo getAllRycrbabListByDeptId(Long deptId, Long deptIdOfTree, Rycrbab fzxxbab) {
		startPage();
		
		List<Rycrbab> list = rycrbabService.selectAllRycrbabListByDeptId(deptId, deptIdOfTree, fzxxbab);
		return getDataTable(list);
	}
	
	@GetMapping("/countallrycrbabbydeptid")
	public long countAllRycrbabListByDeptId(Long deptId, Long deptIdOfTree, Rycrbab fzxxbab) {
		
		long total = rycrbabService.countAllRycrbabByDeptId(deptId, deptIdOfTree, fzxxbab);
		return total;
	}
	
	@GetMapping("/counttodayrycrbabbydeptid")
	public long countTodayRycrbabListByDeptId(Long deptId, Long deptIdOfTree, Rycrbab fzxxbab) {
		
		long total = rycrbabService.countTodayRycrbabByDeptId(deptId, deptIdOfTree, fzxxbab);
		return total;
	}
	
	

	
	/**
	 * 导出出入人员管理列表
	 */
	@ApiOperation("导出出入人员管理列表")
	@PreAuthorize("@ss.hasPermi('system:rycrbab:export')")
	@Log(title = "出入人员管理", businessType = BusinessType.EXPORT)
	@GetMapping("/export")
	public AjaxResult export(Rycrbab rycrbab) {
		List<Rycrbab> list = rycrbabService.selectRycrbabList(rycrbab);
		ExcelUtil<Rycrbab> util = new ExcelUtil<Rycrbab>(Rycrbab.class);
		return util.exportExcel(list, "rycrbab");
	}

	/**
	 * 获取出入人员管理详细信息
	 */
	@ApiOperation("获取出入人员管理详细信息")
	@PreAuthorize("@ss.hasPermi('system:rycrbab:query')")
	@GetMapping(value = "/{id}")
	public AjaxResult getInfo(@PathVariable("id") Long id) {
		return AjaxResult.success(rycrbabService.selectRycrbabById(id));
	}

	/**
	 * 新增出入人员管理
	 */
//	@ApiOperation("新增出入人员管理")
//    @PreAuthorize("@ss.hasPermi('system:rycrbab:add')")
	@Log(title = "出入人员管理", businessType = BusinessType.INSERT)
	@PostMapping
	public AjaxResult add(@RequestBody Rycrbab rycrbab) {
		String sfz=rycrbab.getGmsfhm();         //通过接口传过来的身份证信息未加密
        String en_sfz=AESUtil.encrypt(sfz);     //对身份证加密
		String xm=jzryrzxxbabService.getXmByGmsfzh(en_sfz);   //根据身份证信息获取姓名
		System.out.println("xm");
		System.out.println(xm);
		System.out.println("xm");
		if(xm==null)
		{
			xm="未知人员";
		}
		rycrbab.setXm(xm);
		
		System.out.println(rycrbab);
		return toAjax(rycrbabService.insertRycrbab(rycrbab));
	}

	@ApiOperation("新增出入人员接口")
//  @PreAuthorize("@ss.hasPermi('system:rycrbab:add')")
	@Log(title = "新增出入人员接口", businessType = BusinessType.INSERT)
	@PostMapping("/addIRycrbab")
	public AjaxResult addIRycrbab(@RequestParam String keyId, @RequestBody Rycrbab rcRycrbab) {

		System.out.println(keyId);
		return toAjax(1);
//		return toAjax(rycrbabService.insertRycrbab(rycrbab));
	}
	

    
    /**
     * 新增房主信息
     */
    @ApiOperation("新增出入人员信息")
//    @PreAuthorize("@ss.hasPermi('system:fzxxbab:add')")
    @Log(title = "出入人员信息", businessType = BusinessType.INSERT)
    @PostMapping("/others")
    public AjaxResult add()
    {
		// 获取解密后的数据
		String data = (String) ServletUtils.getRequest().getAttribute("data");
		if (StringUtils.isEmpty(data)) {
			return AjaxResult.error(HttpStatus.BAD_REQUEST, "解密后字符串为空");
		}

		Rycrbab rycrbab = new Rycrbab();
		try{
			rycrbab = JSONObject.parseObject(data, Rycrbab.class);
		}catch (Exception e) {
			// TODO: handle exception
			return AjaxResult.error("无法解析此字符串");
		}
        return toAjax(rycrbabService.insertRycrbab(rycrbab));
    }

	/**
	 * 修改出入人员管理
	 */
	@ApiOperation("修改出入人员管理")
	@PreAuthorize("@ss.hasPermi('system:rycrbab:edit')")
	@Log(title = "出入人员管理", businessType = BusinessType.UPDATE)
	@PutMapping
	public AjaxResult edit(@RequestBody Rycrbab rycrbab) {
		return toAjax(rycrbabService.updateRycrbab(rycrbab));
	}

	/**
	 * 删除出入人员管理
	 */
	@ApiOperation("删除出入人员管理")
	@PreAuthorize("@ss.hasPermi('system:rycrbab:remove')")
	@Log(title = "出入人员管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public AjaxResult remove(@PathVariable Long[] ids) {
		return toAjax(rycrbabService.deleteRycrbabByIds(ids));
	}
}

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Test {
    public static int minDeletions(String s)
    {
    	int []a=new int [26];
    	char [] cs=s.toCharArray();
    	for(char c:cs)
    	{
    		a[c-'a']++;
    	}
    	Set<Integer> set=new HashSet<>();
    	int res=0;
    	for(int i:a)
    	{
    		if(i!=0)
    		{
    			while(set.contains(i))
    			{
    				i--;
    				res++;
    			}
    			if(i!=0)set.add(i);
    		}
    	}
    	return res;
    }
    public static void main(String []args)
    {
    	Scanner scanner=new Scanner(System.in);
    	while(scanner.hasNextLine())
    	{
    		String s=scanner.nextLine();
    		int res=minDeletions(s);
    		System.out.println(res);
    	}
    }
}

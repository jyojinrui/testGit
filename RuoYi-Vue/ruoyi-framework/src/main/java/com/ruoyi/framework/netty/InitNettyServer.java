package com.ruoyi.framework.netty;

import java.util.Locale;
import org.springframework.stereotype.Component;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;
@Component
public class InitNettyServer {
	
	// 创建唯一实例
	private static InitNettyServer nettyServer = new InitNettyServer();
	
    private static int bossThreadNum = 10;
    private static int workThreadNum = 10;
    //用于服务器端接受客户端的连接
    private static EventLoopGroup bossGruop = null;
    //用于网络事件的处理
    private static EventLoopGroup workGroup = null;
    
    // 私有化构造器
    private InitNettyServer() {}
    
	//初始化并启动netty服务器
    public void init(String ip,int port) throws InterruptedException {
    	System.out.println("ip: " + ip + ", port: " + port);
    	try {
    		ServerBootstrap b = new ServerBootstrap();
            boolean islinux = System.getProperty("os.name").toLowerCase(Locale.ENGLISH).contains("linux");
            if (islinux){
                bossGruop = new EpollEventLoopGroup(bossThreadNum);
                workGroup = new EpollEventLoopGroup(workThreadNum);
                b.channel(EpollServerSocketChannel.class);
            }else{
                bossGruop = new NioEventLoopGroup(bossThreadNum);
                workGroup = new NioEventLoopGroup(workThreadNum);
                b.channel(NioServerSocketChannel.class);
            }
            b.group(bossGruop,workGroup);
            // 维持长连接
            b.childOption(ChannelOption.SO_KEEPALIVE,true);
            // 当服务器请求处理线程全满时，用于临时存放已完成三次握手的请求的队列的最大长度
            b.option(ChannelOption.SO_BACKLOG, 1024);
            b.childHandler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel ch) throws Exception {
                	ChannelPipeline pipeline = ch.pipeline();
                	// 解决半包和粘包
                    pipeline.addLast("frameDecoder", new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4));
                    pipeline.addLast("frameEncoder", new LengthFieldPrepender(4));
                    // 编码通道处理
                    pipeline.addLast("decode", new StringDecoder(CharsetUtil.UTF_8));
                    // 转码通道处理
                    pipeline.addLast("encode", new StringEncoder(CharsetUtil.UTF_8));
                    // 分包处理器
//                    pipeline.addLast("package", new PackageHandler());
                    // 自定义业务处理器（负责下发工艺程序/工作序列）
                    pipeline.addLast("chat1", new ServerHandler());
                }
            });
            
            // 启动服务器并开启通道
            b.bind(port).sync().channel();
            System.out.println("netty服务端启动成功");
		} catch (Exception e) {
			e.printStackTrace();
			workGroup.shutdownGracefully();
			bossGruop.shutdownGracefully();
			System.out.println("netty服务器已关闭！");
		}
        
    }
    
    /**
     * 获取唯一实例
     * @return
     */
    public static InitNettyServer getInstance() {
		return nettyServer;
	}
    
    /**
     * 获取bossGruop
     * @return
     */
    public static EventLoopGroup getBossGroup() {
		return bossGruop;
	}
    
    /**
     * 获取workGruop
     * @return
     */
    public static EventLoopGroup getWorkGroup() {
		return workGroup;
	}
}

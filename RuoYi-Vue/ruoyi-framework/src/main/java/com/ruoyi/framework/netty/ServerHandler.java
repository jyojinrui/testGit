package com.ruoyi.framework.netty;

import java.net.SocketAddress;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.domain.transfer.CamUser;
import com.ruoyi.common.utils.IdVertifyUtils;
import com.ruoyi.common.utils.ImageHandleUtils;
import com.ruoyi.common.utils.sign.AESUtil;
import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.system.domain.Rycrbab;
import com.ruoyi.system.domain.SysNettyRecord;
import com.ruoyi.system.service.IJzryrzxxbabService;
import com.ruoyi.system.service.IRycrbabService;
import com.ruoyi.system.service.ISysNettyRecordService;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.ChannelHandler.Sharable;

@Sharable
@Component
public class ServerHandler extends SimpleChannelInboundHandler<String> {
	private IJzryrzxxbabService jzryrzxxbabService = SpringUtils.getBean(IJzryrzxxbabService.class);
	private IRycrbabService rycrbabService = SpringUtils.getBean(IRycrbabService.class);

	private ISysNettyRecordService sysNettyRecordService = SpringUtils.getBean(ISysNettyRecordService.class);

	// 定义属性

	public ServerHandler() {
		super();
	}

	/**
	 * 读取和处理客户端发送的数据
	 * 
	 * @param channelHandlerContext 通道处理器上下文
	 * @param o                     客户端发送的数据
	 */
	@Override
	protected void channelRead0(ChannelHandlerContext channelHandlerContext, String data) throws Exception {

		// 获取客户端的ip地址和端口号
		SocketAddress clientAddr = channelHandlerContext.channel().remoteAddress();
		// 将客户端的IP地址分离出来并保存
		String clientIpAddr = clientAddr.toString().substring(1, clientAddr.toString().indexOf(":"));
		// 打印客户端发送的内容
//		System.out.println("msg: " + data);	
		JSONObject jsonObject = JSONObject.parseObject(data);

		int cmdType = jsonObject.getInteger("cmdType");
		if (cmdType == 1) // 请求向人脸识别设备上派送数据
		{
			String sxtbm = jsonObject.getString("sxtbm"); // 表示传过来的摄像头编码

			// 根据摄像头编码获取小区编码
			String buildingId = sxtbm.substring(0, 21);

			int isXqExist = sysNettyRecordService.ssxqbmExist(buildingId);
			if (isXqExist == 0) // 如果数据库中不存在该小区编码 添加该小区的相关信息
			{
				SysNettyRecord sR = new SysNettyRecord();
				sR.setSsxqbm(buildingId);
				sR.setLastreqtime(new Date(0)); // 将最后操作时间设置成 可表示的最早时间
				sysNettyRecordService.insertSysNettyRecord(sR);
			}

			// 根据小区编码获取最后一次下发时间戳
			long lastReqStamp = sysNettyRecordService.selectSysNettyRecordBySsxqbm(buildingId).getLastreqtime()
					.getTime();

			// 将最后的请求时间更新到表中
			Date lastreqtime = new Date();
			sysNettyRecordService.updateSysNettyRecordBySsxqbm(buildingId, lastreqtime);

			long timeStamp = System.currentTimeMillis(); // 获取当前时间
			List<CamUser> re = jzryrzxxbabService.selectInfoToCamByXqId(buildingId, lastReqStamp / 1000,
					timeStamp / 1000);

			JSONObject rootJson = new JSONObject(); // 最外层的json
			JSONArray lev1 = new JSONArray();
			for (int i = 0; i < re.size(); i++) {
				if (re.get(i).getOpetype() == 0) { // 数据库中0,1,2分别表示增加、修改、删除
					JSONObject jObject = new JSONObject();

					jObject.put("opetype", 1); // 增加 //派送到人脸识别设备分别用1,2,3表示增加、修改、删除
					jObject.put("id", re.get(i).getGmsfhm());
					jObject.put("name", re.get(i).getXm());
					JSONArray jArray = new JSONArray();
					jArray.add(re.get(i).getBinzp());
					jObject.put("image", jArray);
					lev1.add(jObject);
				} else if (re.get(i).getOpetype() == 1) {
					JSONObject jObject = new JSONObject();
					jObject.put("opetype", 2); // 修改
					jObject.put("id", re.get(i).getGmsfhm());
					jObject.put("name", re.get(i).getXm());
					JSONArray jArray = new JSONArray();
					jArray.add(re.get(i).getBinzp());

					jObject.put("image", jArray);
					lev1.add(jObject);

				} else if (re.get(i).getOpetype() == 2) {
					JSONObject jObject = new JSONObject();
					jObject.put("opetype", 3); // 删除
					jObject.put("id", re.get(i).getGmsfhm());
					lev1.add(jObject);
				}
			}
			rootJson.put("array", lev1);
			rootJson.put("buildingId", buildingId);
			channelHandlerContext.channel().writeAndFlush(rootJson.toJSONString());
		} else if (cmdType == 2) // 向Java服务器派送数据
		{
			System.out.println("<=============>");
			System.out.println("cmdType: " + jsonObject);
			System.out.println("<=============>");

			String sxtbm = jsonObject.getString("sxtbm"); // 摄像头编码30位 （人脸识别设备中点位编号信息）

			// 根据摄像头编码获取小区编码
			String buildingId = sxtbm.substring(0, 21); // 小区编码21位
			String mjbh = sxtbm.substring(0, 25); // 门禁编号25位
			String gmsfzh = jsonObject.getString("GMSFZH"); // 获取公民身份证
//			Base64.Encoder encoder = Base64.getEncoder();    //base64加密
//			Base64.Decoder decoder = Base64.getDecoder();    //base64解密
			/**
			 * 中翔设备的身份证信息先base64加密后逆置,解密的话需先逆置再base64解密
			 */
			if(IdVertifyUtils.idCardVerify(gmsfzh))  //验证是否为有效身份证  
			{
				String zpzpa = jsonObject.getString("ZPZPA");//	
				String zpzpb = jsonObject.getString("ZPZPB"); //
				String crlx = jsonObject.getString("CRLB");//
				long zpsj = jsonObject.getLong("ZPSJ");//
				Rycrbab r = new Rycrbab(); // 将传过来的信息封装到出入人员对象在
				r.setSsxqbm(buildingId);
				r.setMjbh(mjbh);
				r.setGmsfhm(gmsfzh);
				r.setCrlb(crlx);
				r.setZpzpa(zpzpa);
				r.setZpzpb(zpzpb);
				r.setSbxt("西安景煜网络科技有限公司");
				Date date = new Date(zpsj * 1000);
				r.setZpsj(date);
				rycrbabService.insertRycrbab(r);
			}
			
			

		} else if (cmdType == 11) {
			String gmsfzh = jsonObject.getString("gmsfzh");
			String name = jsonObject.getString("name");
			System.out.println(gmsfzh + name + "数据添加失败");

		} else if (cmdType == 22) {
			String gmsfzh = jsonObject.getString("gmsfzh");
			String name = jsonObject.getString("name");
			System.out.println(gmsfzh + name + "数据修改失败");
		} else if (cmdType == 33) {
			String gmsfzh = jsonObject.getString("gmsfzh");
			System.out.println(gmsfzh + "数据删除失败");
		}

//		channelHandlerContext.channel().read();

		// 向客户端推送信息

	}

	/**
	 * 在建立连接时发送消息
	 *
	 * @param ctx
	 * @throws Exception
	 */
	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		Channel channel = ctx.channel();
		boolean active = channel.isActive();
		if (active) {
			System.out.println("客户端[" + channel.remoteAddress() + "]已上线");
		} else {
			System.out.println("客户端[" + channel.remoteAddress() + "]已离线");
		}
	}

	/**
	 * 退出时发送消息
	 * 
	 * @param ctx
	 * @throws Exception
	 */
	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		Channel channel = ctx.channel();
		boolean active = channel.isActive();
		if (active) {
			System.out.println("客户端[" + channel.remoteAddress() + "]已上线");
		} else {
			System.out.println("客户端[" + channel.remoteAddress() + "]已离线");
		}
	}

	public static int byteArrayToInt(byte[] bytes) {
		int value = 0;
		// 由高位到低位
		for (int i = 0; i < 4; i++) {
			int shift = (4 - 1 - i) * 8;
			value += (bytes[i] & 0x000000FF) << shift;// 往高位游
		}
		return value;
	}

	public static byte[] IntToByteArray(int n) {
		byte[] b = new byte[4];
		b[3] = (byte) (n & 0xff);
		b[2] = (byte) (n >> 8 & 0xff);
		b[1] = (byte) (n >> 16 & 0xff);
		b[0] = (byte) (n >> 24 & 0xff);
		return b;
	}

	/**
	 * 异常捕获
	 * 
	 * @param ctx
	 * @param e
	 * @throws Exception
	 */
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable e) throws Exception {
		// 打印错误信息
		e.printStackTrace();
		// 获取通道
		Channel channel = ctx.channel();
		// 打印断开连接的提示
		System.out.println("客户端[" + channel.remoteAddress() + "]关闭了连接");
		// 关闭通道
		ctx.close().sync();
	}
}

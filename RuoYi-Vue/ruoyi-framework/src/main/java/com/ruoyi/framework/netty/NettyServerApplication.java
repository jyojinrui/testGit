package com.ruoyi.framework.netty;

import java.net.InetAddress;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NettyServerApplication implements CommandLineRunner {
	
	@Value("${netty.tcp.server.port}")
    private Integer port;

    @Override
    public void run(String... strings) throws Exception {
		String host = InetAddress.getLocalHost().getHostAddress();
		InitNettyServer nettyServer = InitNettyServer.getInstance();
		nettyServer.init(host, port);
    }
}
package com.ruoyi.common.constant;

import io.jsonwebtoken.Claims;

/**
 * 通用常量信息
 */
public class Constants {
	/**
	 * UTF-8 字符集
	 */
	public static final String UTF8 = "UTF-8";

	/**
	 * GBK 字符集
	 */
	public static final String GBK = "GBK";

	/**
	 * http请求
	 */
	public static final String HTTP = "http://";

	/**
	 * https请求
	 */
	public static final String HTTPS = "https://";

	/**
	 * 通用成功标识
	 */
	public static final String SUCCESS = "0";

	/**
	 * 通用失败标识
	 */
	public static final String FAIL = "1";

	/**
	 * 登录成功
	 */
	public static final String LOGIN_SUCCESS = "Success";

	/**
	 * 注销
	 */
	public static final String LOGOUT = "Logout";

	/**
	 * 登录失败
	 */
	public static final String LOGIN_FAIL = "Error";

	/**
	 * 验证码 redis key
	 */
	public static final String CAPTCHA_CODE_KEY = "captcha_codes:";

	/**
	 * 登录用户 redis key
	 */
	public static final String LOGIN_TOKEN_KEY = "login_tokens:";
	
	/**
	 * 登录用户编号 redis key
	 */
	public static final String LOGIN_USERID_KEY = "login_userid:";

	/**
	 * 验证码有效期（分钟）
	 */
	public static final Integer CAPTCHA_EXPIRATION = 2;

	/**
	 * 令牌
	 */
	public static final String TOKEN = "token";

	/**
	 * 令牌前缀
	 */
	public static final String TOKEN_PREFIX = "Bearer ";

	/**
	 * 令牌前缀
	 */
	public static final String LOGIN_USER_KEY = "login_user_key";

	/**
	 * 用户ID
	 */
	public static final String JWT_USERID = "userid";

	/**
	 * 用户名称
	 */
	public static final String JWT_USERNAME = Claims.SUBJECT;

	/**
	 * 用户头像
	 */
	public static final String JWT_AVATAR = "avatar";

	/**
	 * 创建时间
	 */
	public static final String JWT_CREATED = "created";

	/**
	 * 用户权限
	 */
	public static final String JWT_AUTHORITIES = "authorities";

	/**
	 * 参数管理 cache key
	 */
	public static final String SYS_CONFIG_KEY = "sys_config:";

	/**
	 * 字典管理 cache key
	 */
	public static final String SYS_DICT_KEY = "sys_dict:";

	/**
	 * 资源映射路径 前缀
	 */
	public static final String RESOURCE_PREFIX = "/profile";
	
	/**
     * 防重提交 redis key
     */
    public static final String REPEAT_SUBMIT_KEY = "repeat_submit:";

	/**
	 * appKey的名称
	 */
	public static final String APP_KEY_NAME = "appKey";

	/**
	 * appKey的固定长度：15
	 */
	public static final int APP_KEY_LENGTH = 15;

	/**
	 * 审核人编号的固定长度：15
	 */
	public static final int AUDITOR_ID_LENGTH = 15;

	/**
	 * 审核人的姓名
	 */
	public static final String APP_AUDITOR_NAME = "审核人";

	/**
	 * 审核人的姓名
	 */
	public static final String APP_AUDIT_STATE = "审核状态";

	/**
	 * 审核人的联系电话
	 */
	public static final String APP_AUDITOR_PHONE = "联系电话";

	/**
	 * appSecret的名称
	 */
	public static final String APP_SECRET_NAME = "appSecret";

	/**
	 * appSecret的固定长度：30
	 */
	public static final int APP_SECRET_LENGTH = 20;

	/**
	 * appKey不能为空
	 */
	public static final String APPKEY_CAN_NOT_NULL = "appKey不能为空！";

	/**
	 * sign不能为空
	 */
	public static final String SIGN_CAN_NOT_NULL = "sign不能为空！";

	/**
	 * timestamp不能为空
	 */
	public static final String TIMESTAMP_CAN_NOT_NULL = "timestamp不能为空！";

	/**
	 * 时间相差不能超过30min
	 */
	public static final String TIME_OVER_LIMIT = "时间相差超过30min！";

	/**
	 * sign不正确
	 */
	public static final String SIGN_IS_NOT_CORRECT = "sign不正确！";

	/**
	 * 服务器出现故障，请联系管理员
	 */
	public static final String SYSTEM_INTERNAL_ERROR = "服务器出现故障，请联系管理员！";

	/**
	 * 服务器出现故障，请联系管理员
	 */
	public static final String APP_IS_NOT_PASS = "此appKey还未审核通过！";

	/**
	 * appKey不存在
	 */
	public static final String APPKEY_IS_NOT_EXIST = "appKey不存在！";

	/**
	 * 时间相差最大上限
	 */
	public static final int TIMEDIFF_LIMIT = 30 * 60 * 1000;

	/**
	 * 公司名称为空或者相关参数名有误
	 */
	public static final String COMPANY_INFO_ERROR = "公司名称为空或者相关参数名有误";

	/**
	 * 应用名称为空或者相关参数名有误
	 */
	public static final String APPLICATION_INFO_ERROR = "应用名称为空或者相关参数名有误";

	/**
	 * 申请人名称为空或者相关参数名有误
	 */
	public static final String APPLICANT_INFO_ERROR = "申请人名称为空或者相关参数名有误";

	/**
	 * 申请人电话为空或者相关参数名有误
	 */
	public static final String PHONE_INFO_ERROR = "申请人电话为空或者相关参数名有误";

	/**
	 * 申请信息不能为空
	 */
	public static final String APP_APPLY_INFO_CAN_NOT_NULL = "申请信息不能为空！";

	/**
	 * 已申请，无需重复申请
	 */
	public static final String CAN_NOT_APPLY_AGAIN = "同一个应用只能申请一次！";

	/**
	 * app分配失败，请联系管理员
	 */
	public static final String DISPATCH_APP_FAILURE = "app分配失败，请联系管理员！";

	/**
	 * 无法保存申请信息，请联系管理员
	 */
	public static final String CAN_NOT_SAVE_APPLY_INFO = "无法保存申请信息，请联系管理员！";

	/**
	 * 无法生成app，请联系管理员
	 */
	public static final String CAN_NOT_GEN_APP = "无法生成app，请联系管理员！";

	/**
	 * 无法保存app信息，请联系管理员
	 */
	public static final String CAN_NOT_SAVE_APP = "无法保存app信息，请联系管理员！";

	/**
	 * 无法保存app和关联的申请信息记录，请联系管理员
	 */
	public static final String CAN_NOT_SAVE_APPLY_DETAILS = "无法保存app和关联的申请信息记录，请联系管理员！";

	/**
	 * app审核状态：审核未通过
	 */
	public static final String APP_AUDIT_NOT_PASS = "审核未通过，请联系管理员！";

	/**
	 * app审核状态：审核通过
	 */
	public static final String APP_AUDIT_PASS = "恭喜，审核通过！";

	/**
	 * app审核的状态：审核中
	 */
	public static final String APP_AUDITING = "审核中";

	/**
	 * app审核状态：审核未通过
	 */
	public static final int AUDIT_NOT_PASS = 0;

	/**
	 * app审核状态：审核通过
	 */
	public static final int AUDIT_PASS = 1;

	/**
	 * 审核的状态：审核中
	 */
	public static final int AUDITING = 2;


	public static final String MQTT_TOPIC_PREFIX = "mqtt/face/";
	public static final String MQTT_TOPIC_SNAP_SUFFIX = "/Snap";
	public static final String MQTT_TOPIC_REC_SUFFIX = "/Rec";
	public static final String MQTT_TOPIC_ACK_SUFFIX = "/Ack";
}


package com.ruoyi.common.constant;

/**
 * 返回状态码
 */
public class HttpStatus {
	/**
	 * 操作成功
	 */
	public static final int SUCCESS = 200;

	/**
	 * 对象创建成功
	 */
	public static final int CREATED = 201;

	/**
	 * 请求已经被接受
	 */
	public static final int ACCEPTED = 202;

	/**
	 * 操作已经执行成功，但是没有返回数据
	 */
	public static final int NO_CONTENT = 204;

	/**
	 * 资源已被移除
	 */
	public static final int MOVED_PERM = 301;

	/**
	 * 重定向
	 */
	public static final int SEE_OTHER = 303;

	/**
	 * 资源没有被修改
	 */
	public static final int NOT_MODIFIED = 304;

	/**
	 * 参数列表错误（缺少，格式不匹配）
	 */
	public static final int BAD_REQUEST = 400;

	/**
	 * 未授权
	 */
	public static final int UNAUTHORIZED = 401;

	/**
	 * 访问受限，授权过期
	 */
	public static final int FORBIDDEN = 403;

	/**
	 * 资源，服务未找到
	 */
	public static final int NOT_FOUND = 404;

	/**
	 * 不允许的http方法
	 */
	public static final int BAD_METHOD = 405;

	/**
	 * 资源冲突，或者资源被锁
	 */
	public static final int CONFLICT = 409;

	/**
	 * 不支持的数据，媒体类型
	 */
	public static final int UNSUPPORTED_TYPE = 415;

	/**
	 * 系统内部错误
	 */
	public static final int ERROR = 500;

	/**
	 * 接口未实现
	 */
	public static final int NOT_IMPLEMENTED = 501;

	/**
	 * 公司名称为空或者相关参数名有误
	 */
	public static final int COMPANY_INFO_ERROR = 601;

	/**
	 * 应用名称为空或者相关参数名有误
	 */
	public static final int APPLICATION_INFO_ERROR = 602;

	/**
	 * 申请人名称为空或者相关参数名有误
	 */
	public static final int APPLICANT_INFO_ERROR = 603;

	/**
	 * 申请人电话为空或者相关参数名有误
	 */
	public static final int PHONE_INFO_ERROR = 604;

	/**
	 * appKey不能为空
	 */
	public static final int APPKEY_CAN_NOT_NULL = 1001;

	/**
	 * sign不能为空
	 */
	public static final int SIGN_CAN_NOT_NULL = 1002;

	/**
	 * timestamp不能为空
	 */
	public static final int TIMESTAMP_CAN_NOT_NULL = 1003;

	/**
	 * 时间相差不能超过30min
	 */
	public static final int TIME_OVER_LIMIT = 1004;

	/**
	 * sign不正确
	 */
	public static final int SIGN_IS_NOT_CORRECT = 1005;

	/**
	 * appKey未审核通过
	 */
	public static final int APP_IS_NOT_PASS = 1006;

	/**
	 * appKey不存在
	 */
	public static final int APPKEY_IS_NOT_EXIST = 1007;

	/**
	 * 申请信息不能为空
	 */
	public static final int APP_APPLY_INFO_CAN_NOT_NULL = 1008;

	/**
	 * 已申请，无需重复申请
	 */
	public static final int CAN_NOT_APPLY_AGAIN = 1009;

	/**
	 * app分配失败，请联系管理员
	 */
	public static final int DISPATCH_APP_FAILURE = 1010;

	/**
	 * 无法保存申请信息，请联系管理员
	 */
	public static final int CAN_NOT_SAVE_APPLY_INFO = 1011;

	/**
	 * 无法生成app，请联系管理员
	 */
	public static final int CAN_NOT_GEN_APP = 1012;

	/**
	 * 无法保存app信息，请联系管理员
	 */
	public static final int CAN_NOT_SAVE_APP = 1013;

	/**
	 * 无法保存app和关联的申请信息记录，请联系管理员
	 */
	public static final int CAN_NOT_SAVE_APPLY_DETAILS = 1014;

}

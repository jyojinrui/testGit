package com.ruoyi.common.core.domain.socket;

public class PacketHead {
	private Integer cmdType;
	private Integer contentLen;
	public Integer getCmdType() {
		return cmdType;
	}
	public void setCmdType(Integer cmdType) {
		this.cmdType = cmdType;
	}
	public Integer getContentLen() {
		return contentLen;
	}
	public void setContentLen(Integer contentLen) {
		this.contentLen = contentLen;
	}
	
	public PacketHead() {
		super();
	}
	public PacketHead(Integer cmdType, Integer contentLen) {
		super();
		this.cmdType = cmdType;
		this.contentLen = contentLen;
	}
	@Override
	public String toString() {
		return "PacketHead [cmdType=" + cmdType + ", contentLen=" + contentLen + "]";
	}
	

}

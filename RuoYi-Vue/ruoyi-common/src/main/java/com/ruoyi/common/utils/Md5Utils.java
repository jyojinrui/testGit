package com.ruoyi.common.utils;

import java.security.MessageDigest;

public class Md5Utils {

	public final static String md5(String s) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'a', 'b', 'c', 'd', 'e', 'f' };
		try {
			byte[] btInput = s.getBytes();
			/**
			 * java.security.MessageDigest类用于为应用程序提供信息摘要算法的功能，
			 * 如 MD5 或 SHA 算法。简单点说就是用于生成散列码。
			 * 信息摘要是安全的单向哈希函数，它接收随意大小的数据，输出固定长度的哈希值。
			 * MessageDigest 通过其getInstance系列静态函数来进行实例化和初始化。
			 * MessageDigest 对象通过使用 update 方法处理数据。
			 * 不论什么时候都能够调用 reset 方法重置摘要。一旦全部须要更新的数据都已经被更新了，
			 * 应该调用 digest 方法之中的一个完毕哈希计算并返回结果。
			 * 
			 * MessageDigest 的实现可任意选择是否实现 Cloneable 接口。
			 * client应用程能够通过尝试复制和捕获 CloneNotSupportedException 測试可复制性
			 */
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			mdInst.update(btInput);
			byte[] md = mdInst.digest();
			
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;//k表示转换结果中对应的字符位置
			for (int i = 0; i < j; i++) {//从第一个字节开始
				byte byte0 = md[i];//取第i个字节
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];//逻辑右移，将符号位一起右移
				str[k++] = hexDigits[byte0 & 0xf];//取字节中低 4 位的数字转换
			}
			return new String(str);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}

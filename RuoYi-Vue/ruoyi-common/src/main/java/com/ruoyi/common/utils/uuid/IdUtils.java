package com.ruoyi.common.utils.uuid;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.utils.uuid.UUID;

/**
 * ID生成器工具类
 * 
 * @author ruoyi
 */
public class IdUtils
{
    /**
     * 获取随机UUID
     * 
     * @return 随机UUID
     */
    public static String randomUUID()
    {
        return UUID.randomUUID().toString();
    }

    /**
     * 简化的UUID，去掉了横线
     * 
     * @return 简化的UUID，去掉了横线
     */
    public static String simpleUUID()
    {
        return UUID.randomUUID().toString(true);
    }

    /**
     * 获取随机UUID，使用性能更好的ThreadLocalRandom生成UUID
     * 
     * @return 随机UUID
     */
    public static String fastUUID()
    {
        return UUID.fastUUID().toString();
    }

    /**
     * 简化的UUID，去掉了横线，使用性能更好的ThreadLocalRandom生成UUID
     * 
     * @return 简化的UUID，去掉了横线
     */
    public static String fastSimpleUUID()
    {
        return UUID.fastUUID().toString(true);
    }

	/**
	 * 随机生成审核人编号
	 * @return
	 */
	public static String generateAuditorId() {
		return UUID.fastUUID().toString(true).substring(0, Constants.AUDITOR_ID_LENGTH);
	}
    
    /**
	 * 随机生成AppKey
	 * @return
	 */
	public static String generateAppKey() {
		return UUID.fastUUID().toString(true).substring(0, 16);
	}

	/**
	 * 随机生成AppSecret
	 * @return
	 */
	public static String generateAppSecret() {
		return UUID.fastUUID().toString(true).substring(0, 20);
	}

}

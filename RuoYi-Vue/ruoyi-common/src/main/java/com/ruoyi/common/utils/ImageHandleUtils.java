package com.ruoyi.common.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.FileImageOutputStream;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import com.ruoyi.common.core.domain.AjaxResult;

@Component
public class ImageHandleUtils {

	@Value("${ruoyi.profile}")
	private String fileUploadPath;

	@Value("${ftpServer.ip}")
	private String ip;

	@Value("${ftpServer.port}")
	private int port;

	@Value("${ftpServer.username}")
	private String username;

	@Value("${ftpServer.password}")
	private String password;

	@Value("${ftpServer.vice_username}")
	private String vice_username;

	@Value("${ftpServer.vice_password}")
	private String vice_password;

	@Value("${ftpServer.downPath}")
	private String downPath;

	// 根据相对路径将本地图片转化成byte数组 JzryrzxxbabImpl类中
	public byte[] getImgBin(String fileRelativePath) throws IOException {

		// 内网存储的图片的相对路径
		String filePath = fileRelativePath.substring(0, fileRelativePath.lastIndexOf("/"));
		// 内网存储的图片的名称
		String fileName = fileRelativePath.substring(fileRelativePath.lastIndexOf("/") + 1);

		String realPath = fileUploadPath + fileRelativePath;

		FileInputStream fis = null;

		try {
			fis = new FileInputStream(realPath);
			StringBuilder str = new StringBuilder();
			byte[] buffer = new byte[fis.available()];
			fis.read(buffer);
			return buffer;
		} catch (Exception e) {
			System.out.println("照片未找到！");
			e.printStackTrace();
		} finally {
			try {
				// 删除外网的图片及其保存的路径文件夹
//				FileUtils.delFolder(downPath);
				fis.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

//	根据相对路径将外网图片转化成byte数组
	public byte[] getImgBinWw(String fileRelativePath) throws IOException {
		if (fileRelativePath == null || fileRelativePath == "")
			return null;
		System.out.println("图片路径：" + fileRelativePath);
		// 内网存储的图片的相对路径
		String filePath = fileRelativePath.substring(0, fileRelativePath.lastIndexOf("/"));
		// 内网存储的图片的名称
		String fileName = fileRelativePath.substring(fileRelativePath.lastIndexOf("/") + 1);

		// 将图片下载到外网的downPath路径下

		FTPUtils.retrieveFile(ip, port, username, password, filePath, fileName, downPath);

		// 获取外网图片的路径
		String realPath = downPath + File.separator + fileName;

		FileInputStream fis = null;

		try {
			fis = new FileInputStream(realPath);
			StringBuilder str = new StringBuilder();

			byte[] buffer = new byte[fis.available()];
			fis.read(buffer);
			return buffer;
		} catch (Exception e) {
			System.out.println("照片未找到！");
			e.printStackTrace();
		} finally {
			try {
				// 删除外网的图片及其保存的路径文件夹
//				FileUtils.delFolder(downPath);
				fis.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	// 图片到byte数组
	public static byte[] image2byte(String path) {
		byte[] data = null;
		FileImageInputStream input = null;
		try {
			input = new FileImageInputStream(new File(path));
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			byte[] buf = new byte[1024];
			int numBytesRead = 0;
			while ((numBytesRead = input.read(buf)) != -1) {
				output.write(buf, 0, numBytesRead);
			}
			data = output.toByteArray();
			output.close();
			input.close();
		} catch (FileNotFoundException ex1) {
			ex1.printStackTrace();
		} catch (IOException ex1) {
			ex1.printStackTrace();
		}
		return data;
	}

	public static void byte2image(byte[] data, String path) {
		if (data.length < 3 || path.equals(""))
			return;
		try {
			FileImageOutputStream imageOutput = new FileImageOutputStream(new File(path));
			imageOutput.write(data, 0, data.length);
			imageOutput.close();
			System.out.println("Make Picture success,Please find image in " + path);
		} catch (Exception ex) {
			System.out.println("Exception: " + ex);
			ex.printStackTrace();
		}
	}

	// 根据将二进制数组转化成图片 存储到对应的路径中（本地）
	public AjaxResult imgUpload(byte[] byte_img, String buildingId, String photosPath) throws IOException {

		// 获取图片上传的公共路径（文件夹）
		File imgDirPath = new File(fileUploadPath);
//		String upTest = fileUploadPath;
//		System.out.println(upTest);
		if (!imgDirPath.exists()) {
			// 若不存在文件夹，则创建一个文件夹
			imgDirPath.mkdirs();
		}
//		String buildingId = multiReq.getHeader("buildingId");
		// 获取市、区、派出所、警务室以及小区编码
		String profilePicture = photosPath;
		String city = buildingId.substring(0, 4);
		String area = buildingId.substring(4, 6);
		String policeStation = buildingId.substring(6, 9);
		String policeOffice = buildingId.substring(9, 12);
		String community = buildingId.substring(12, buildingId.length());
		String subDirPath = fileUploadPath + "/" + profilePicture + "/" + city + "/" + area + "/" + policeStation + "/"
				+ policeOffice + "/" + community;
		imgDirPath = new File(subDirPath);

		if (!imgDirPath.exists()) {
			// 若不存在文件夹，则创建一个文件夹
			imgDirPath.mkdirs();
		}

//		MultipartFile file = multiReq.getFile(byte_img);

		InputStream inputStream = new ByteArrayInputStream(byte_img);
		MultipartFile file = new MockMultipartFile("test.jpg", byte_img); // test表示这个文件的名称

		// 获取图片原文件名
//		String originalFilename = file.getOriginalFilename();
		String originalFilename = "test.jpg";
		// 获取图片格式后缀
		String suffix = originalFilename.substring(originalFilename.indexOf("."));
//	    String suffix="jpg";	
		// 重新生成图片名称
		String localFileName = MD5Util.md5(file.getInputStream()) + System.currentTimeMillis() + suffix;
		File localFile = new File(subDirPath + "/" + localFileName);
		try {
			file.transferTo(localFile);
		} catch (IOException e) {
			e.printStackTrace();
			return AjaxResult.error("照片上传失败，请联系管理员！");
		}
		System.out.println("照片上传成功，保存位置：" + subDirPath + "/" + localFileName);
		AjaxResult ajaxResult = new AjaxResult();
		ajaxResult.put("imgUrl", "/" + profilePicture + "/" + city + "/" + area + "/" + policeStation + "/"
				+ policeOffice + "/" + community + "/" + localFileName);
		return ajaxResult;
	}

	// 根据将二进制数组转化成图片 存储到对应的路径中（外网）
	public AjaxResult imgUploadWw(byte[] byte_img, String buildingId, String photosPath) throws IOException {

		// 获取图片上传的公共路径（文件夹）
		File imgDirPath = new File(fileUploadPath);
//			String upTest = fileUploadPath;
//			System.out.println(upTest);
		if (!imgDirPath.exists()) {
			// 若不存在文件夹，则创建一个文件夹
			imgDirPath.mkdirs();
		}
//			String buildingId = multiReq.getHeader("buildingId");
		// 获取市、区、派出所、警务室以及小区编码
		String profilePicture = photosPath;
		String city = buildingId.substring(0, 4);
		String area = buildingId.substring(4, 6);
		String policeStation = buildingId.substring(6, 9);
		String policeOffice = buildingId.substring(9, 12);
		String community = buildingId.substring(12, buildingId.length());
		String subDirPath = fileUploadPath + "/" + profilePicture + "/" + city + "/" + area + "/" + policeStation + "/"
				+ policeOffice + "/" + community;
		imgDirPath = new File(subDirPath);

		if (!imgDirPath.exists()) {
			// 若不存在文件夹，则创建一个文件夹
			imgDirPath.mkdirs();
		}

//			MultipartFile file = multiReq.getFile(byte_img);

		InputStream inputStream = new ByteArrayInputStream(byte_img);
		MultipartFile file = new MockMultipartFile("test.jpg", byte_img); // test表示这个文件的名称

		// 获取图片原文件名
//			String originalFilename = file.getOriginalFilename();
		String originalFilename = "test.jpg";
		// 获取图片格式后缀
		String suffix = originalFilename.substring(originalFilename.indexOf("."));
//		    String suffix="jpg";	
		// 重新生成图片名称
		String localFileName = MD5Util.md5(file.getInputStream()) + System.currentTimeMillis() + suffix;
		File localFile = new File(subDirPath + "/" + localFileName);
		try {
			file.transferTo(localFile);
			FileInputStream fis = new FileInputStream(localFile);
			FileInputStream fis_vice = new FileInputStream(localFile);
			// 利用ftp将图片上传到远程的远程
			FTPUtils.storeFile(ip, port, username, password, "/" + profilePicture + "/" + city + "/" + area + "/"
					+ policeStation + "/" + policeOffice + "/" + community, localFileName, fis);
			// 利用ftp将图片上传到远程的远程的FTP通道
			FTPUtils.storeFile2(ip, port, vice_username, vice_password, "/" + profilePicture + "/" + city + "/" + area
					+ "/" + policeStation + "/" + policeOffice + "/" + community, localFileName, fis_vice);

		} catch (IOException e) {
			e.printStackTrace();
			return AjaxResult.error("照片上传失败，请联系管理员！");
		}
		System.out.println("照片上传成功，保存位置：" + subDirPath + "/" + localFileName);
		AjaxResult ajaxResult = new AjaxResult();
		ajaxResult.put("imgUrl", "/" + profilePicture + "/" + city + "/" + area + "/" + policeStation + "/"
				+ policeOffice + "/" + community + "/" + localFileName);
		return ajaxResult;
	}

}

package com.ruoyi.common.core.domain.statistics;

import com.ruoyi.common.annotation.Excel;

public class QgajSta {
	@Excel(name="小区个数")
	private Long xqgs;
	
	@Excel(name = "分局编码")
	private Long qgajbm;
	
	@Excel(name = "分局名称")
	private String fjmc;
	
	@Excel(name = "建设公司")
	private String jsgs;
	
	@Excel(name = "实有房屋")
	private long syfw;
	
	@Excel(name = "实有人口")
	private long syrk;
	
	@Excel(name = "实有单位")
	private long sydw;
	
	@Excel(name = "从业人员")
	private long cyry;
	
	@Excel(name = "门禁设备")
	private long mjsb;
	
	@Excel(name = "门禁卡")
	private long mjk;
	
	@Excel(name = "访客数据")
	private long fksj;
	
	@Excel(name = "出入记录")
	private long crjl;
	
	@Excel(name = "出入照片数")
	private long crzp;
	

	public Long getQgajbm() {
		return qgajbm;
	}


	public void setQgajbm(Long qgajbm) {
		this.qgajbm = qgajbm;
	}


	public String getFjmc() {
		return fjmc;
	}


	public void setFjmc(String fjmc) {
		this.fjmc = fjmc;
	}


	public String getJsgs() {
		return jsgs;
	}


	public void setJsgs(String jsgs) {
		this.jsgs = jsgs;
	}


	public long getSyfw() {
		return syfw;
	}


	public void setSyfw(long syfw) {
		this.syfw = syfw;
	}


	public long getSyrk() {
		return syrk;
	}


	public void setSyrk(long syrk) {
		this.syrk = syrk;
	}


	public long getSydw() {
		return sydw;
	}


	public void setSydw(long sydw) {
		this.sydw = sydw;
	}


	public long getCyry() {
		return cyry;
	}


	public void setCyry(long cyry) {
		this.cyry = cyry;
	}


	public long getMjsb() {
		return mjsb;
	}


	public void setMjsb(long mjsb) {
		this.mjsb = mjsb;
	}


	public long getMjk() {
		return mjk;
	}


	public void setMjk(long mjk) {
		this.mjk = mjk;
	}


	public long getFksj() {
		return fksj;
	}


	public void setFksj(long fksj) {
		this.fksj = fksj;
	}


	public long getCrjl() {
		return crjl;
	}


	public void setCrjl(long crjl) {
		this.crjl = crjl;
	}


	public long getCrzp() {
		return crzp;
	}


	public void setCrzp(long crzp) {
		this.crzp = crzp;
	}


	public Long getXqgs() {
		return xqgs;
	}


	public void setXqgs(Long xqgs) {
		this.xqgs = xqgs;
	}


	public QgajSta() {
		super();
	}


	


	public QgajSta(Long xqgs, Long qgajbm, String fjmc, String jsgs, long syfw, long syrk, long sydw, long cyry,
			long mjsb, long mjk, long fksj, long crjl, long crzp) {
		super();
		this.xqgs = xqgs;
		this.qgajbm = qgajbm;
		this.fjmc = fjmc;
		this.jsgs = jsgs;
		this.syfw = syfw;
		this.syrk = syrk;
		this.sydw = sydw;
		this.cyry = cyry;
		this.mjsb = mjsb;
		this.mjk = mjk;
		this.fksj = fksj;
		this.crjl = crjl;
		this.crzp = crzp;
	}


	@Override
	public String toString() {
		return "QgajSta [xqgs=" + xqgs + ", qgajbm=" + qgajbm + ", fjmc=" + fjmc + ", jsgs=" + jsgs + ", syfw=" + syfw
				+ ", syrk=" + syrk + ", sydw=" + sydw + ", cyry=" + cyry + ", mjsb=" + mjsb + ", mjk=" + mjk + ", fksj="
				+ fksj + ", crjl=" + crjl + ", crzp=" + crzp + "]";
	}


    
	
	
}

package com.ruoyi.common.utils;

public class IdVertifyUtils {
	// Java实现18位身份证号码的校验码计算校验
	public static void main(String[] args) {
		// 从百度百科中找到的身份证号码例子
		String idCard = "360421199610284011";
	}

	public static boolean idCardVerify(String idCard) {
		// 仅适用于18位标准身份证号码
		if (idCard.length() != 18) {
			return false;
		}
		// 使身份证号码中的字母升大写
		idCard = idCard.toUpperCase();
		char[] idCardChars = idCard.toCharArray();
		// 重点1：加权因子计算
		final int[] factors = { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 };
		int sumFactor = 0;
		for (int i = 0; i < factors.length; i++) {
			// 数字的ASCII码是48-57
			int value = idCardChars[i] - 48;
			// 纯数字校验
			if (value < 0 || value > 9) {
				return false;
			}
			sumFactor += factors[i] * value;
		}
		// 重点2：校验码比对
		final char[] verifyCode = { '1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2' };
		// author: tiange1314520@qq.com
		return idCardChars[17] == verifyCode[sumFactor % 11];
	}
}

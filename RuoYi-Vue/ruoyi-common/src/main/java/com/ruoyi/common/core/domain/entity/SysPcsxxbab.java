package com.ruoyi.common.core.domain.entity;

import com.ruoyi.common.annotation.Excel;

public class SysPcsxxbab {
	
	 /** 系统自动生成编号 */
    private Long id;

    /** 派出所编码 */
    @Excel(name = "派出所编码")
    private String pcsbm;

    /** 地址二维码编码 */
    @Excel(name = "地址二维码编码")
    private String dzermbm;

    /** 派出所名称 */
    @Excel(name = "派出所名称")
    private String pcsmc;

    /** 派出所地址 */
    @Excel(name = "派出所地址")
    private String pcsdz;

    /** 所属区县 */
    @Excel(name = "所属区县")
    private String ssqx;

    /** 派出所负责人 */
    @Excel(name = "派出所负责人")
    private String pcsfzr;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String lxdh;
    
    /** 区公安局编码 */
    @Excel(name="区公安局编码")
    private String qgajbm;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPcsbm() {
		return pcsbm;
	}

	public void setPcsbm(String pcsbm) {
		this.pcsbm = pcsbm;
	}

	public String getDzermbm() {
		return dzermbm;
	}

	public void setDzermbm(String dzermbm) {
		this.dzermbm = dzermbm;
	}

	public String getPcsmc() {
		return pcsmc;
	}

	public void setPcsmc(String pcsmc) {
		this.pcsmc = pcsmc;
	}

	public String getPcsdz() {
		return pcsdz;
	}

	public void setPcsdz(String pcsdz) {
		this.pcsdz = pcsdz;
	}

	public String getSsqx() {
		return ssqx;
	}

	public void setSsqx(String ssqx) {
		this.ssqx = ssqx;
	}

	public String getPcsfzr() {
		return pcsfzr;
	}

	public void setPcsfzr(String pcsfzr) {
		this.pcsfzr = pcsfzr;
	}

	public String getLxdh() {
		return lxdh;
	}

	public void setLxdh(String lxdh) {
		this.lxdh = lxdh;
	}

	public String getQgajbm() {
		return qgajbm;
	}

	public void setQgajbm(String qgajbm) {
		this.qgajbm = qgajbm;
	}

	public SysPcsxxbab() {
		super();
	}

	public SysPcsxxbab(Long id, String pcsbm, String dzermbm, String pcsmc, String pcsdz, String ssqx, String pcsfzr,
			String lxdh, String qgajbm) {
		super();
		this.id = id;
		this.pcsbm = pcsbm;
		this.dzermbm = dzermbm;
		this.pcsmc = pcsmc;
		this.pcsdz = pcsdz;
		this.ssqx = ssqx;
		this.pcsfzr = pcsfzr;
		this.lxdh = lxdh;
		this.qgajbm = qgajbm;
	}

	@Override
	public String toString() {
		return "SysPcsxxbab [id=" + id + ", pcsbm=" + pcsbm + ", dzermbm=" + dzermbm + ", pcsmc=" + pcsmc + ", pcsdz="
				+ pcsdz + ", ssqx=" + ssqx + ", pcsfzr=" + pcsfzr + ", lxdh=" + lxdh + ", qgajbm=" + qgajbm + "]";
	}
    

}

package com.ruoyi.common.core.domain.transfer;

import java.util.Arrays;

import com.ruoyi.common.annotation.Excel;

public class CamUser {
	 /** 公民身份号码 */
    @Excel(name = "公民身份号码")
    private String gmsfhm;

    /** 姓名 */
    @Excel(name = "姓名")
    private String xm;
    
    /** 照片*/
    @Excel(name = "照片")
    private String zp;
    
    /**二进制招聘*/
    @Excel(name="binzp")
    private String binzp;
    
    @Excel(name="opetype")
    private int opetype;

	public String getGmsfhm() {
		return gmsfhm;
	}

	public void setGmsfhm(String gmsfhm) {
		this.gmsfhm = gmsfhm;
	}

	public String getXm() {
		return xm;
	}

	public void setXm(String xm) {
		this.xm = xm;
	}

	public String getZp() {
		return zp;
	}

	public void setZp(String zp) {
		this.zp = zp;
	}

	public String getBinzp() {
		return binzp;
	}

	public void setBinzp(String binzp) {
		this.binzp = binzp;
	}

	public int getOpetype() {
		return opetype;
	}

	public void setOpetype(int opetype) {
		this.opetype = opetype;
	}

	public CamUser() {
		super();
	}

	public CamUser(String gmsfhm, String xm, String zp, String binzp, int opetype) {
		super();
		this.gmsfhm = gmsfhm;
		this.xm = xm;
		this.zp = zp;
		this.binzp = binzp;
		this.opetype = opetype;
	}

	public CamUser(String gmsfhm, String xm, String zp, String binzp) {
		super();
		this.gmsfhm = gmsfhm;
		this.xm = xm;
		this.zp = zp;
		this.binzp = binzp;
	}

	@Override
	public String toString() {
		return "CamUser [gmsfhm=" + gmsfhm + ", xm=" + xm + ", zp=" + zp + ", binzp=" + binzp + ", opetype=" + opetype
				+ "]";
	}

	
	

}

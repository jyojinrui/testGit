import request from '@/utils/request'

// 查询从业人员信息列表
export function listCyryxxbab(query) {
  return request({
    url: '/system/cyryxxbab/list',
    method: 'get',
    params: query
  })
}

// 查询从业人员信息详细
export function getCyryxxbab(id) {
  return request({
    url: '/system/cyryxxbab/' + id,
    method: 'get'
  })
}

// 新增从业人员信息
export function addCyryxxbab(data) {
  return request({
    url: '/system/cyryxxbab',
    method: 'post',
    data: data
  })
}

// 修改从业人员信息
export function updateCyryxxbab(data) {
  return request({
    url: '/system/cyryxxbab',
    method: 'put',
    data: data
  })
}

// 删除从业人员信息
export function delCyryxxbab(id) {
  return request({
    url: '/system/cyryxxbab/' + id,
    method: 'delete'
  })
}

// 导出从业人员信息
export function exportCyryxxbab(query) {
  return request({
    url: '/system/cyryxxbab/export',
    method: 'get',
    params: query
  })
}
import request from '@/utils/request'


export function listXlgcsxtyxbByDeptId(query) {
  return request({
    url: '/system/xlgcsxtyxb/listxlgcsxtyxbbydeptid',
    method: 'get',
    params: query
  })
}

export function listXlgcsxtyxbByXqAdminId(query) {
  return request({
    url: '/system/xlgcsxtyxb/listxlgcsxtyxbbyxqadminid',
    method: 'get',
    params: query
  })
}

// 查询雪亮工程摄像头运行列表
export function listXlgcsxtyxb(query) {
  return request({
    url: '/system/xlgcsxtyxb/list',
    method: 'get',
    params: query
  })
}

// 查询雪亮工程摄像头运行详细
export function getXlgcsxtyxb(id) {
  return request({
    url: '/system/xlgcsxtyxb/' + id,
    method: 'get'
  })
}

// 新增雪亮工程摄像头运行
export function addXlgcsxtyxb(data) {
  return request({
    url: '/system/xlgcsxtyxb',
    method: 'post',
    data: data
  })
}

// 修改雪亮工程摄像头运行
export function updateXlgcsxtyxb(data) {
  return request({
    url: '/system/xlgcsxtyxb',
    method: 'put',
    data: data
  })
}

// 删除雪亮工程摄像头运行
export function delXlgcsxtyxb(id) {
  return request({
    url: '/system/xlgcsxtyxb/' + id,
    method: 'delete'
  })
}

// 导出雪亮工程摄像头运行
export function exportXlgcsxtyxb(query) {
  return request({
    url: '/system/xlgcsxtyxb/export',
    method: 'get',
    params: query
  })
}
import request from '@/utils/request'
//根据小区管理员id获取小区门禁卡信息



export function listMjkxxbabByDeptId(query) {
  return request({
    url: '/system/mjkxxbab/listmjkxxbabbydeptid',
    method: 'get',
    params: query
  })
}

//listMjkxxbabByXqAdminId
export function listMjkxxbabByXqAdminId(query) {
  return request({
    url: '/system/mjkxxbab/listmjkxxbabbyxqadminid',
    method: 'get',
    params: query
  })
}



// 查询门禁卡列表
export function listMjkxxbab(query) {
  return request({
    url: '/system/mjkxxbab/list',
    method: 'get',
    params: query
  })
}

// 查询门禁卡详细
export function getMjkxxbab(id) {
  return request({
    url: '/system/mjkxxbab/' + id,
    method: 'get'
  })
}

// 新增门禁卡
export function addMjkxxbab(data) {
  return request({
    url: '/system/mjkxxbab',
    method: 'post',
    data: data
  })
}

// 修改门禁卡
export function updateMjkxxbab(data) {
  return request({
    url: '/system/mjkxxbab',
    method: 'put',
    data: data
  })
}

// 删除门禁卡
export function delMjkxxbab(id) {
  return request({
    url: '/system/mjkxxbab/' + id,
    method: 'delete'
  })
}

// 导出门禁卡
export function exportMjkxxbab(query) {
  return request({
    url: '/system/mjkxxbab/export',
    method: 'get',
    params: query
  })
}
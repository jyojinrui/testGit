import request from '@/utils/request'

//通过小区编码获取标准地址信息
export function listBzdzxxbabByXqbm(buildingId) {
  return request({
    url: '/system/xqxxbab/listBzdzxxbabByXqbm/'+buildingId,
    method: 'get',
  })
}



export function listXqxxbabByDeptId(query) {
  return request({
    url: '/system/xqxxbab/listxqxxbabbydeptid',
    method: 'get',
    params: query
  })
}

// 查询小区列表
export function listXqxxbab(query) {
  return request({
    url: '/system/xqxxbab/list',
    method: 'post',
    data: query
  })
}

//根据小区管理员查询小区信息
export function getXqxxByXqAdminId(id) {
  return request({
    url: '/system/xqxxbab/getxqxxbyadminid/' + id,
    method: 'get',
    // params: query
  })
}

// 查询小区详细
export function getXqxxbab(id) {
  return request({
    url: '/system/xqxxbab/' + id,
    method: 'get'
  })
}

// 新增小区
export function addXqxxbab(data) {
  return request({
    url: '/system/xqxxbab',
    method: 'post',
    data: data
  })
}

// 修改小区
export function updateXqxxbab(data) {
  return request({
    url: '/system/xqxxbab',
    method: 'put',
    data: data
  })
}

// 删除小区
export function delXqxxbab(id) {
  return request({
    url: '/system/xqxxbab/' + id,
    method: 'delete'
  })
}

// 导出小区
export function exportXqxxbab(query) {
  return request({
    url: '/system/xqxxbab/export',
    method: 'get',
    params: query
  })
}
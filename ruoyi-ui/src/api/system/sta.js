import request from '@/utils/request'


// 根据区公安局编码查询统计报表列表
export function listAllStaGroupByQgajbm(query) {
  return request({
    url: '/system/sta/listallstagroupbyqgajbm',
    method: 'get',
    params: query
  })
}

// 根据小区编码查询统计报表列表
export function listStaByXqbm(query) {
  return request({
    url: '/system/sta/liststabyxqbm',
    method: 'get',
    params: query
  })
}

// 查询统计报表列表
export function listSta(query) {
  return request({
    url: '/system/sta/list',
    method: 'get',
    params: query
  })
}

// 查询统计报表详细
export function getSta(id) {
  return request({
    url: '/system/sta/' + id,
    method: 'get'
  })
}

// 新增统计报表
export function addSta(data) {
  return request({
    url: '/system/sta',
    method: 'post',
    data: data
  })
}

// 修改统计报表
export function updateSta(data) {
  return request({
    url: '/system/sta',
    method: 'put',
    data: data
  })
}

// 删除统计报表
export function delSta(id) {
  return request({
    url: '/system/sta/' + id,
    method: 'delete'
  })
}

// 导出统计报表
export function exportSta(query) {
  return request({
    url: '/system/sta/export',
    method: 'get',
    params: query
  })
}
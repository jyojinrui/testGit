import request from '@/utils/request'

// listXqsxtywzkbByDeptId
export function listXqsxtywzkbByDeptId(query) {
  return request({
    url: '/system/xqsxtywzkb/listxqsxtywzkbbydeptid',
    method: 'get',
    params: query
  })
}
// listXqsxtywzkbByXqAdminId
//根据小区管理员id获取小区摄像头运维状况信息
export function listXqsxtywzkbByXqAdminId(query) {
  return request({
    url: '/system/xqsxtywzkb/listxqsxtywzkbbyxqadminid',
    method: 'get',
    params: query
  })
}
// 查询运维状况列表
export function listXqsxtywzkb(query) {
  return request({
    url: '/system/xqsxtywzkb/list',
    method: 'get',
    params: query
  })
}

// 查询运维状况详细
export function getXqsxtywzkb(id) {
  return request({
    url: '/system/xqsxtywzkb/' + id,
    method: 'get'
  })
}

// 新增运维状况
export function addXqsxtywzkb(data) {
  return request({
    url: '/system/xqsxtywzkb',
    method: 'post',
    data: data
  })
}

// 修改运维状况
export function updateXqsxtywzkb(data) {
  return request({
    url: '/system/xqsxtywzkb',
    method: 'put',
    data: data
  })
}

// 删除运维状况
export function delXqsxtywzkb(id) {
  return request({
    url: '/system/xqsxtywzkb/' + id,
    method: 'delete'
  })
}

// 导出运维状况
export function exportXqsxtywzkb(query) {
  return request({
    url: '/system/xqsxtywzkb/export',
    method: 'get',
    params: query
  })
}
import request from '@/utils/request'

// 查询实有单位信息列表
export function listSydwxxbab(query) {
  return request({
    url: '/system/sydwxxbab/list',
    method: 'get',
    params: query
  })
}

// 查询实有单位信息详细
export function getSydwxxbab(id) {
  return request({
    url: '/system/sydwxxbab/' + id,
    method: 'get'
  })
}

// 新增实有单位信息
export function addSydwxxbab(data) {
  return request({
    url: '/system/sydwxxbab',
    method: 'post',
    data: data
  })
}

// 修改实有单位信息
export function updateSydwxxbab(data) {
  return request({
    url: '/system/sydwxxbab',
    method: 'put',
    data: data
  })
}

// 删除实有单位信息
export function delSydwxxbab(id) {
  return request({
    url: '/system/sydwxxbab/' + id,
    method: 'delete'
  })
}

// 导出实有单位信息
export function exportSydwxxbab(query) {
  return request({
    url: '/system/sydwxxbab/export',
    method: 'get',
    params: query
  })
}
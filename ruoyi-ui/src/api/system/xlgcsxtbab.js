import request from '@/utils/request'
// listXlgcsxtbabByXqAdminId


export function listXlgcsxtbabByDeptId(query) {
  return request({
    url: '/system/xlgcsxtbab/listxlgcsxtbabbydeptid',
    method: 'get',
    params: query
  })
}

export function listXlgcsxtbabByXqAdminId(query) {
  return request({
    url: '/system/xlgcsxtbab/listxlgcsxtbabbyxqadminid',
    method: 'get',
    params: query
  })
}


// 查询雪亮工程摄像头设备列表
export function listXlgcsxtbab(query) {
  return request({
    url: '/system/xlgcsxtbab/list',
    method: 'get',
    params: query
  })
}

// 查询雪亮工程摄像头设备详细
export function getXlgcsxtbab(id) {
  return request({
    url: '/system/xlgcsxtbab/' + id,
    method: 'get'
  })
}

// 新增雪亮工程摄像头设备
export function addXlgcsxtbab(data) {
  return request({
    url: '/system/xlgcsxtbab',
    method: 'post',
    data: data
  })
}

// 修改雪亮工程摄像头设备
export function updateXlgcsxtbab(data) {
  return request({
    url: '/system/xlgcsxtbab',
    method: 'put',
    data: data
  })
}

// 删除雪亮工程摄像头设备
export function delXlgcsxtbab(id) {
  return request({
    url: '/system/xlgcsxtbab/' + id,
    method: 'delete'
  })
}

// 导出雪亮工程摄像头设备
export function exportXlgcsxtbab(query) {
  return request({
    url: '/system/xlgcsxtbab/export',
    method: 'get',
    params: query
  })
}
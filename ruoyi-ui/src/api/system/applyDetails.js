import request from '@/utils/request'

// 查询App和申请信息关联列表
export function listApplyDetails(query) {
  return request({
    url: '/common/appApplyDetails/list',
    method: 'get',
    params: query
  })
}

// 根据id查询App和申请信息关联详细
export function getApplyDetails(id) {
  return request({
    url: '/common/appApplyDetails/' + id,
    method: 'get'
  })
}

// 根据applyInfoId查询App和申请信息关联详细
export function getApplyDetailsByApplyInfoId(applyInfoId) {
  return request({
    url: '/common/appApplyDetails/getByApplyId/' + applyInfoId,
    method: 'get'
  })
}

// 新增App和申请信息关联
export function addApplyDetails(data) {
  return request({
    url: '/common/appApplyDetails/addAppApplyDetails',
    method: 'post',
    data: data
  })
}

// 修改App和申请信息关联
export function updateApplyDetails(data) {
  return request({
    url: '/common/appApplyDetails/updateApplyDetails',
    method: 'put',
    data: data
  })
}

// 删除App和申请信息关联
export function delApplyDetails(ids) {
  return request({
    url: '/common/appApplyDetails/delAppApplyDetails/' + ids,
    method: 'delete'
  })
}

// 导出符合查询条件的App和申请信息关联
export function exportSearchedApplyDetails(query) {
  return request({
    url: '/common/appApplyDetails/export',
    method: 'get',
    params: query
  })
}

// 导出选中的App和申请信息关联
export function exportSelectedApplyDetails(ids) {
  return request({
    url: '/common/appApplyDetails/export/' + ids,
    method: 'get'
  })
}
import request from '@/utils/request'

export function listCmxxbByDeptId(query) {
  return request({
    url: '/system/cmxxb/listcmxxbbydeptid',
    method: 'get',
    params: query
  })
}

// listCmxxbByXqAdminId
export function listCmxxbByXqAdminId(query) {
  return request({
    url: '/system/cmxxb/listcmxxbbyxqadminid',
    method: 'get',
    params: query
  })
}

// 查询串码信息列表
export function listCmxxb(query) {
  return request({
    url: '/system/cmxxb/list',
    method: 'get',
    params: query
  })
}

// 查询串码信息详细
export function getCmxxb(id) {
  return request({
    url: '/system/cmxxb/' + id,
    method: 'get'
  })
}

// 新增串码信息
export function addCmxxb(data) {
  return request({
    url: '/system/cmxxb',
    method: 'post',
    data: data
  })
}

// 修改串码信息
export function updateCmxxb(data) {
  return request({
    url: '/system/cmxxb',
    method: 'put',
    data: data
  })
}

// 删除串码信息
export function delCmxxb(id) {
  return request({
    url: '/system/cmxxb/' + id,
    method: 'delete'
  })
}

// 导出串码信息
export function exportCmxxb(query) {
  return request({
    url: '/system/cmxxb/export',
    method: 'get',
    params: query
  })
}
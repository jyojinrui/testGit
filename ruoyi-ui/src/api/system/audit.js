import request from '@/utils/request'

// 查询系统app审核列表
export function listAudit(query) {
  return request({
    url: '/common/appAuditor/list',
    method: 'get',
    params: query
  })
}

// 查询系统app审核详细
export function getAudit(id) {
  return request({
    url: '/common/appAuditor/' + id,
    method: 'get'
  })
}

// 新增系统app审核
export function addAudit(data) {
  return request({
    url: '/common/appAuditor',
    method: 'post',
    data: data
  })
}

// 修改系统app审核
export function updateAudit(data) {
  return request({
    url: '/common/appAuditor',
    method: 'put',
    data: data
  })
}

// 删除系统app审核
export function delAudit(id) {
  return request({
    url: '/common/appAuditor/' + id,
    method: 'delete'
  })
}

// 导出符合查询条件的系统app审核
export function exportSearchedAudit(query) {
  return request({
    url: '/common/appAuditor/export',
    method: 'get',
    params: query
  })
}

// 导出选中的系统app审核
export function exportSelectedAudit(id) {
  return request({
    url: '/common/appAuditor/export/' + id,
    method: 'get'
  })
}
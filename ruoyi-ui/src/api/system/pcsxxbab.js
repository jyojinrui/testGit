import request from '@/utils/request'

//根据当前区公安局管理员的部门id获取管辖的派出所
                
export function getSubDeptByQgajDeptId(deptid) {
  return request({
    url: '/system/wnsjgsdwdm/getsubdeptbyqgajdeptid/'+deptid,
    method: 'get',
  })
}
// 查询派出所信息详细
export function getPcsxxbab(id) {
  return request({
    url: '/system/pcsxxbab/' + id,
    method: 'get'
  })
}
//根据所属区县获取派出所信息
export function listPcsBySsqx(query) {
  return request({
    url: '/system/pcsxxbab/listpcsbyssqx',
    method: 'post',
    data: query
  })
}



export function listPcsxxbabByDeptId(query) {
  return request({
    url: '/system/pcsxxbab/listpcsxxbabbydeptid',
    method: 'get',
    params: query
  })
}

// 查询派出所信息列表
export function listPcsxxbab(query) {
  return request({
    url: '/system/pcsxxbab/list',
    method: 'get',
    params: query
  })
}



// 新增派出所信息
export function addPcsxxbab(data) {
  return request({
    url: '/system/pcsxxbab',
    method: 'post',
    data: data
  })
}

// 修改派出所信息
export function updatePcsxxbab(data) {
  return request({
    url: '/system/pcsxxbab',
    method: 'put',
    data: data
  })
}

// 删除派出所信息
export function delPcsxxbab(id) {
  return request({
    url: '/system/pcsxxbab/' + id,
    method: 'delete'
  })
}

// 导出派出所信息
export function exportPcsxxbab(query) {
  return request({
    url: '/system/pcsxxbab/export',
    method: 'get',
    params: query
  })
}
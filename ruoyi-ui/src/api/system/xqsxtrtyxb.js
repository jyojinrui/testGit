import request from '@/utils/request'


export function listXqsxtrtyxbByDeptId(query) {
  return request({
    url: '/system/xqsxtrtyxb/listxqsxtrtyxbbydeptid',
    method: 'get',
    params: query
  })
}
//listXqsxtrtyxbByXaAdminId
export function listXqsxtrtyxbByXqAdminId(query) {
  return request({
    url: '/system/xqsxtrtyxb/listxqsxtrtyxbbyxqadminid',
    method: 'get',
    params: query
  })
}

// 查询摄像头人体运行列表
export function listXqsxtrtyxb(query) {
  return request({
    url: '/system/xqsxtrtyxb/list',
    method: 'get',
    params: query
  })
}

// 查询摄像头人体运行详细
export function getXqsxtrtyxb(id) {
  return request({
    url: '/system/xqsxtrtyxb/' + id,
    method: 'get'
  })
}

// 新增摄像头人体运行
export function addXqsxtrtyxb(data) {
  return request({
    url: '/system/xqsxtrtyxb',
    method: 'post',
    data: data
  })
}

// 修改摄像头人体运行
export function updateXqsxtrtyxb(data) {
  return request({
    url: '/system/xqsxtrtyxb',
    method: 'put',
    data: data
  })
}

// 删除摄像头人体运行
export function delXqsxtrtyxb(id) {
  return request({
    url: '/system/xqsxtrtyxb/' + id,
    method: 'delete'
  })
}

// 导出摄像头人体运行
export function exportXqsxtrtyxb(query) {
  return request({
    url: '/system/xqsxtrtyxb/export',
    method: 'get',
    params: query
  })
}
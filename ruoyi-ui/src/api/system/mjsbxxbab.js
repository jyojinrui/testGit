import request from '@/utils/request'
//listMjsbxxbabByXaAdminId


export function listMjsbxxbabByDeptId(query)
{
  return request({
    url:'/system/mjsbxxbab/listmjsbxxbabbydeptid',
    method:'get',
    params:query
  })
}

//根据小区管理员id查询门禁设备信息列表    还要考虑后面搜索情况 
export function listMjsbxxbabByXaAdminId(query)
{
  return request({
    url:'/system/mjsbxxbab/listmjsbxxbabbyxaadminid',
    method:'get',
    params:query
  })
}

// 查询门禁设备管理列表
export function listMjsbxxbab(query) {
  return request({
    url: '/system/mjsbxxbab/list',
    method: 'get',
    params: query
  })
}

// 查询门禁设备管理详细
export function getMjsbxxbab(id) {
  return request({
    url: '/system/mjsbxxbab/' + id,
    method: 'get'
  })
}

// 新增门禁设备管理
export function addMjsbxxbab(data) {
  return request({
    url: '/system/mjsbxxbab',
    method: 'post',
    data: data
  })
}

// 修改门禁设备管理
export function updateMjsbxxbab(data) {
  return request({
    url: '/system/mjsbxxbab',
    method: 'put',
    data: data
  })
}

// 删除门禁设备管理
export function delMjsbxxbab(id) {
  return request({
    url: '/system/mjsbxxbab/' + id,
    method: 'delete'
  })
}

// 导出门禁设备管理
export function exportMjsbxxbab(query) {
  return request({
    url: '/system/mjsbxxbab/export',
    method: 'get',
    params: query
  })
}
import request from '@/utils/request'


//警务室管理员 派出所管理员 区公安局管理员 市公安局管理员共用这个方法（小区编号长度21  小区管理员无法使用该方法）
export function listFzxxbabByDeptId(query)
{
  return request({
    url:'/system/fzxxbab/listfzxxbabbydeptid',
    method:'get',
    params:query
  })
}
//根据小区管理员id查询房主信息列表 
//考虑到后面搜索的情况传入的是JSON格式的参数  包含搜索要用到的信息
export function listFzxxbabByXqAdminId(query)
{
  return request({
    url:'/system/fzxxbab/getlistbyxqadminid',
    method:'get',
    params:query
  })
}


//根据派出所管理员id查询管辖区域房主信息列表 
export function listFzxxbabByPcsAdminId(query)
{
  return request({
    url:'/system/fzxxbab/getlistbypcsadminid',
    method:'get',
    params:query
  })
}

// 查询房主信息列表
export function listFzxxbab(query) {
  return request({
    url: '/system/fzxxbab/list',
    method: 'get',
    params: query
  })
}

// 查询房主信息详细
export function getFzxxbab(id) {
  return request({
    url: '/system/fzxxbab/' + id,
    method: 'get'
  })
}

// 新增房主信息
export function addFzxxbab(data) {
  return request({
    url: '/system/fzxxbab',
    method: 'post',
    data: data
  })
}

// 修改房主信息
export function updateFzxxbab(data) {
  return request({
    url: '/system/fzxxbab',
    method: 'put',
    data: data
  })
}

// 删除房主信息
export function delFzxxbab(id) {
  return request({
    url: '/system/fzxxbab/' + id,
    method: 'delete'
  })
}

// 导出房主信息
export function exportFzxxbab(query) {
  return request({
    url: '/system/fzxxbab/export',
    method: 'get',
    params: query
  })
}
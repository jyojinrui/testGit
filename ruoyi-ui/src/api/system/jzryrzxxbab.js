import request from '@/utils/request'

export function listJzryrzxxbabByDeptId(query)
{
  return request({
    url:'/system/jzryrzxxbab/listjzryrzxxbabbydeptid',
    method:'get',
    params:query
  })
}

//根据小区管理员id查询入住人员信息列表    还要考虑后面搜索情况 
export function listJzryrzxxbabByXqAdminId(query)
{
  return request({
    url:'/system/jzryrzxxbab/listjzryrzxxbabbyxqadminid',
    method:'get',
    params:query
  })
}

// 查询居住人员列表
export function listJzryrzxxbab(query) {
  return request({
    url: '/system/jzryrzxxbab/list',
    method: 'get',
    params: query
  })
}

// 查询居住人员详细
export function getJzryrzxxbab(id) {
  return request({
    url: '/system/jzryrzxxbab/' + id,
    method: 'get'
  })
}

// 新增居住人员
export function addJzryrzxxbab(data) {
  return request({
    url: '/system/jzryrzxxbab',
    method: 'post',
    data: data
  })
}

// 修改居住人员
export function updateJzryrzxxbab(data) {
  return request({
    url: '/system/jzryrzxxbab',
    method: 'put',
    data: data
  })
}

// 删除居住人员
export function delJzryrzxxbab(id) {
  return request({
    url: '/system/jzryrzxxbab/' + id,
    method: 'delete'
  })
}

// 上传图片
export function addImgData(data, buildingId) {
  return request({
    url: '/common/imgHandler/imgUpload',
    method: 'post',  
    headers: {
      'Content-Type': 'multipart/form-data',
      'buildingId': buildingId,      //小区编码
      'commonFilePath': 'identificationPhotos', // 公共的存储文件夹
      'uploadType': 'syrk'
    },
    data: data    // 参数需要是单一的formData形式
  })
}

// 导出居住人员
export function exportJzryrzxxbab(query) {
  return request({
    url: '/system/jzryrzxxbab/export',
    method: 'get',
    params: query
  })
}
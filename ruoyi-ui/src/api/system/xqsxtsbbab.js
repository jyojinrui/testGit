import request from '@/utils/request'


export function listXqsxtsbbabByDeptId(query) {
  return request({
    url: '/system/xqsxtsbbab/listxqsxtsbbabbydeptid',
    method: 'get',
    params: query
  })
}

// 查询小区摄像头设备列表
export function listXqsxtsbbabByXqAdminId(query) {
  return request({
    url: '/system/xqsxtsbbab/listxqsxtsbbabbyxqadminid',
    method: 'get',
    params: query
  })
}

// 查询小区摄像头设备列表
export function listXqsxtsbbab(query) {
  return request({
    url: '/system/xqsxtsbbab/list',
    method: 'get',
    params: query
  })
}

// 查询小区摄像头设备详细
export function getXqsxtsbbab(id) {
  return request({
    url: '/system/xqsxtsbbab/' + id,
    method: 'get'
  })
}

// 新增小区摄像头设备
export function addXqsxtsbbab(data) {
  return request({
    url: '/system/xqsxtsbbab',
    method: 'post',
    data: data
  })
}

// 修改小区摄像头设备
export function updateXqsxtsbbab(data) {
  return request({
    url: '/system/xqsxtsbbab',
    method: 'put',
    data: data
  })
}

// 删除小区摄像头设备
export function delXqsxtsbbab(id) {
  return request({
    url: '/system/xqsxtsbbab/' + id,
    method: 'delete'
  })
}

// 导出小区摄像头设备
export function exportXqsxtsbbab(query) {
  return request({
    url: '/system/xqsxtsbbab/export',
    method: 'get',
    params: query
  })
}
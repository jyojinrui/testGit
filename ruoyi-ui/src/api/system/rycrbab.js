import request from '@/utils/request'

export function listTodayRycrbabByDeptId(query) {
  return request({
    url: '/system/rycrbab/listtodayrycrbabbydeptid',
    method: 'get',
    params: query
  })
}
export function listAllRycrbabByDeptId(query) {
  return request({
    url: '/system/rycrbab/listallrycrbabbydeptid',
    method: 'get',
    params: query
  })
}

export function countAllRycrbabByDeptId(query) {
  return request({
    url: '/system/rycrbab/countallrycrbabbydeptid',
    method: 'get',
    params: query
  })
}


export function countTodayRycrbabByDeptId(query) {
  return request({
    url: '/system/rycrbab/counttodayrycrbabbydeptid',
    method: 'get',
    params: query
  })
}


export function countAllRycrbabByXqAdminId(query) {
  return request({
    url: '/system/rycrbab/countallrycrbabbyxqadminid',
    method: 'get',
    params: query
  })
}


export function countTodayRycrbabByXqAdminId(query) {
  return request({
    url: '/system/rycrbab/counttodayrycrbabbyxqadminid',
    method: 'get',
    params: query
  })
}






//根据管理员部门id获取小区出入人员信息
//警务室管理员 派出所管理员 区公安局管理员 市公安局管理员共用这个方法（小区编号长度21  小区管理员无法使用该方法）
export function listRycrbabByDeptId(query) {
  return request({
    url: '/system/rycrbab/listrycrbabbydeptid',
    method: 'get',
    params: query
  })
}

export function listTodayRycrbabByXqAdminId(query) {
  return request({
    url: '/system/rycrbab/listtodayrycrbabbyxqadminid',
    method: 'get',
    params: query
  })
}
export function listAllRycrbabByXqAdminId(query) {
  return request({
    url: '/system/rycrbab/listallrycrbabbyxqadminid',
    method: 'get',
    params: query
  })
}



// listRycrbabByXqAdminId
//根据小区管理员id获取小区出入人员信息
export function listRycrbabByXqAdminId(query) {
  return request({
    url: '/system/rycrbab/listrycrbabbyxqadminid',
    method: 'get',
    params: query
  })
}

// 查询出入人员管理列表
export function listRycrbab(query) {
  return request({
    url: '/system/rycrbab/list',
    method: 'get',
    params: query
  })
}

// 查询出入人员管理详细
export function getRycrbab(id) {
  return request({
    url: '/system/rycrbab/' + id,
    method: 'get'
  })
}

// 新增出入人员管理
export function addRycrbab(data) {
  return request({
    url: '/system/rycrbab',
    method: 'post',
    data: data
  })
}

// 上传图片
export function addImgData(data, buildingId) {
  return request({
    url: '/common/imgHandler/imgUploadBd',
    method: 'post',  
    headers: {
      'Content-Type': 'multipart/form-data',
      'buildingId': buildingId,      //小区编码
      'commonFilePath': 'rycrPhotos', // 公共的存储文件夹
      'uploadType': 'rycr'
    },
    data: data    // 参数需要是单一的formData形式
  })
}

// 修改出入人员管理
export function updateRycrbab(data) {
  return request({
    url: '/system/rycrbab',
    method: 'put',
    data: data
  })
}

// 删除出入人员管理
export function delRycrbab(id) {
  return request({
    url: '/system/rycrbab/' + id,
    method: 'delete'
  })
}

// 导出出入人员管理
export function exportRycrbab(query) {
  return request({
    url: '/system/rycrbab/export',
    method: 'get',
    params: query
  })
}
import request from '@/utils/request'

// listXqsxtrlyxbByDeptId
export function listXqsxtrlyxbByDeptId(query) {
  return request({
    url: '/system/xqsxtrlyxb/listxqsxtrlyxbbydeptid',
    method: 'get',
    params: query
  })
}

// 查询人脸运行数据列表
export function listXqsxtrlyxbByXqAdminId(query) {
  return request({
    url: '/system/xqsxtrlyxb/listxqsxtrlyxbbyxqadminid',
    method: 'get',
    params: query
  })
}

// 查询人脸运行数据列表
export function listXqsxtrlyxb(query) {
  return request({
    url: '/system/xqsxtrlyxb/list',
    method: 'get',
    params: query
  })
}

// 查询人脸运行数据详细
export function getXqsxtrlyxb(id) {
  return request({
    url: '/system/xqsxtrlyxb/' + id,
    method: 'get'
  })
}

// 新增人脸运行数据
export function addXqsxtrlyxb(data) {
  return request({
    url: '/system/xqsxtrlyxb',
    method: 'post',
    data: data
  })
}

// 修改人脸运行数据
export function updateXqsxtrlyxb(data) {
  return request({
    url: '/system/xqsxtrlyxb',
    method: 'put',
    data: data
  })
}

// 删除人脸运行数据
export function delXqsxtrlyxb(id) {
  return request({
    url: '/system/xqsxtrlyxb/' + id,
    method: 'delete'
  })
}

// 导出人脸运行数据
export function exportXqsxtrlyxb(query) {
  return request({
    url: '/system/xqsxtrlyxb/export',
    method: 'get',
    params: query
  })
}
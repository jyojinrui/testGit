import request from '@/utils/request'

// 查询app申请信息列表
export function listApplyInfo(query) {
  return request({
    url: '/common/appApply/list',
    method: 'get',
    params: query
  })
}

// 查询app申请信息详细
export function getApplyInfo(id) {
  return request({
    url: '/common/appApply/' + id,
    method: 'get'
  })
}

// 新增app申请信息
export function addApplyInfo(data) {
  return request({
    url: '/common/appApply/addAppApplyInfo',
    method: 'post',
    data: data
  })
}

// 修改app申请信息
export function updateApplyInfo(data) {
  return request({
    url: '/common/appApply/updateApplyInfo',
    method: 'put',
    data: data
  })
}

// 删除app申请信息
export function delApplyInfo(ids) {
  return request({
    url: '/common/delAppApplyInfo/' + ids,
    method: 'delete'
  })
}

// 导出符合查询条件的app申请信息
export function exportSearchedApplyInfo(query) {
  return request({
    url: '/common/appApply/export',
    method: 'get',
    params: query
  })
}

// 导出选中的app申请信息
export function exportSelectedApplyInfo(ids) {
  return request({
    url: '/common/appApply/export/' + ids,
    method: 'get'
  })
}
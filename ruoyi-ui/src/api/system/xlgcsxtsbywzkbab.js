import request from '@/utils/request'


export function listXlgcsxtsbywzkbabByDeptId(query) {
  return request({
    url: '/system/xlgcsxtsbywzkbab/listxlgcsxtsbywzkbabbydeptid',
    method: 'get',
    params: query
  })
}
//listXlgcsxtsbywzkbabByXqAdminId
export function listXlgcsxtsbywzkbabByXqAdminId(query) {
  return request({
    url: '/system/xlgcsxtsbywzkbab/listxlgcsxtsbywzkbabbyxqadminid',
    method: 'get',
    params: query
  })
}

// 查询雪亮工程摄像头设备运行状况列表
export function listXlgcsxtsbywzkbab(query) {
  return request({
    url: '/system/xlgcsxtsbywzkbab/list',
    method: 'get',
    params: query
  })
}

// 查询雪亮工程摄像头设备运行状况详细
export function getXlgcsxtsbywzkbab(id) {
  return request({
    url: '/system/xlgcsxtsbywzkbab/' + id,
    method: 'get'
  })
}

// 新增雪亮工程摄像头设备运行状况
export function addXlgcsxtsbywzkbab(data) {
  return request({
    url: '/system/xlgcsxtsbywzkbab',
    method: 'post',
    data: data
  })
}

// 修改雪亮工程摄像头设备运行状况
export function updateXlgcsxtsbywzkbab(data) {
  return request({
    url: '/system/xlgcsxtsbywzkbab',
    method: 'put',
    data: data
  })
}

// 删除雪亮工程摄像头设备运行状况
export function delXlgcsxtsbywzkbab(id) {
  return request({
    url: '/system/xlgcsxtsbywzkbab/' + id,
    method: 'delete'
  })
}

// 导出雪亮工程摄像头设备运行状况
export function exportXlgcsxtsbywzkbab(query) {
  return request({
    url: '/system/xlgcsxtsbywzkbab/export',
    method: 'get',
    params: query
  })
}
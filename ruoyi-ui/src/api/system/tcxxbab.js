import request from '@/utils/request'

//警务室管理员 派出所管理员 区公安局管理员 市公安局管理员共用这个方法（小区编号长度21  小区管理员无法使用该方法）
export function listTodayTcxxbabByDeptId(query) {
  return request({
    url: '/system/tcxxbab/listtodaytcxxbabbydeptid',
    method: 'get',
    params: query
  })
}
// 查询停车信息管理列表
export function listTodayTcxxbabByXqAdminId(query) {
  return request({
    url: '/system/tcxxbab/listtodaytcxxbabbyxqadminid',
    method: 'get',
    params: query
  })
}



//警务室管理员 派出所管理员 区公安局管理员 市公安局管理员共用这个方法（小区编号长度21  小区管理员无法使用该方法）
export function listTcxxbabByDeptId(query) {
  return request({
    url: '/system/tcxxbab/listtcxxbabbydeptid',
    method: 'get',
    params: query
  })
}
// 查询停车信息管理列表
export function listTcxxbabByXqAdminId(query) {
  return request({
    url: '/system/tcxxbab/listtcxxbabbyxqadminid',
    method: 'get',
    params: query
  })
}

// 查询停车信息管理列表
export function listTcxxbab(query) {
  return request({
    url: '/system/tcxxbab/list',
    method: 'get',
    params: query
  })
}

// 查询停车信息管理详细
export function getTcxxbab(id) {
  return request({
    url: '/system/tcxxbab/' + id,
    method: 'get'
  })
}

// 新增停车信息管理
export function addTcxxbab(data) {
  return request({
    url: '/system/tcxxbab',
    method: 'post',
    data: data
  })
}

// 修改停车信息管理
export function updateTcxxbab(data) {
  return request({
    url: '/system/tcxxbab',
    method: 'put',
    data: data
  })
}

// 删除停车信息管理
export function delTcxxbab(id) {
  return request({
    url: '/system/tcxxbab/' + id,
    method: 'delete'
  })
}

// 导出停车信息管理
export function exportTcxxbab(query) {
  return request({
    url: '/system/tcxxbab/export',
    method: 'get',
    params: query
  })
}
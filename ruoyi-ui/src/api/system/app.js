import request from '@/utils/request'

// 查询app列表
export function listApp(query) {
  return request({
    url: '/common/app/list',
    method: 'get',
    params: query
  })
}

// 查询app详细
export function getApp(appKey) {
  return request({
    url: '/common/app/getAppInfo/' + appKey,
    method: 'get'
  })
}

// 新增app
export function addApp(data) {
  return request({
    url: '/common/app/addApp',
    method: 'post',
    data: data
  })
}

// 修改app
export function updateApp(data) {
  return request({
    url: '/common/app/updateApp',
    method: 'put',
    data: data
  })
}

// 更换app审核人
export function changeAppAuditor(data) {
  return request({
    url: '/common/app/changeAppAuditor',
    method: 'put',
    data: data
  })
}

// 删除app
export function delApp(appKeys) {
  return request({
    url: '/common/app/delApp/' + appKeys,
    method: 'delete'
  })
}

// 导出符合查询条件的app
export function exportSearchedApp(query) {
  return request({
    url: '/common/app/export',
    method: 'get',
    params: query
  })
}

// 导出选中的app
export function exportSelectedApp(appKeys) {
  return request({
    url: '/common/app/export/' + appKeys,
    method: 'get'
  })
}
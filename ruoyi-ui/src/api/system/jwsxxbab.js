import request from '@/utils/request'

// 查询警务室信息列表
export function listJwsxxbab(query) {
  return request({
    url: '/system/jwsxxbab/list',
    method: 'get',
    params: query
  })
}


// 查询警务室信息列表
export function listJwsxxbabByDeptId(query) {
  return request({
    url: '/system/jwsxxbab/listjwsxxbabbydeptid',
    method: 'get',
    params: query
  })
}
// 查询警务室信息详细
export function getJwsxxbab(id) {
  return request({
    url: '/system/jwsxxbab/' + id,
    method: 'get'
  })
}

// 新增警务室信息
export function addJwsxxbab(data) {
  return request({
    url: '/system/jwsxxbab',
    method: 'post',
    data: data
  })
}

// 修改警务室信息
export function updateJwsxxbab(data) {
  return request({
    url: '/system/jwsxxbab',
    method: 'put',
    data: data
  })
}

// 删除警务室信息
export function delJwsxxbab(id) {
  return request({
    url: '/system/jwsxxbab/' + id,
    method: 'delete'
  })
}

// 导出警务室信息
export function exportJwsxxbab(query) {
  return request({
    url: '/system/jwsxxbab/export',
    method: 'get',
    params: query
  })
}
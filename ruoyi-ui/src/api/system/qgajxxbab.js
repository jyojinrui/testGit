import request from '@/utils/request'

//获取各个区县级的
export function listQgajStaData(){
  return request({
    url: '/system/qgajxxbab/getqgajstadata',
    method: 'get',
  })
}



//从渭南数据归属单位代码表中获取区公安局信息
export function getQgajxxListFromWnsjgsdwdmb(){
  return request({
    url: '/system/wnsjgsdwdm/getqgajlist',
    method: 'get',
  })
}



// 查询区公安局信息列表
export function listQgajxxbab(query) {
  return request({
    url: '/system/qgajxxbab/list',
    method: 'get',
    params: query
  })
}
export function listQgajxxbabByDeptId(query){
  return request({
    url: '/system/qgajxxbab/listqgajxxbabbydeptid',
    method: 'get',
    params: query
  })
}
export function getQgajxxbabByAdminId(adminid) {
  return request({
    url: '/system/qgajxxbab/getqgajxxbabbyadminid/'+adminid,
    method: 'get',
  })
}



// 查询区公安局信息详细
export function getQgajxxbab(id) {
  return request({
    url: '/system/qgajxxbab/' + id,
    method: 'get'
  })
}



// 新增区公安局信息
export function addQgajxxbab(data) {
  return request({
    url: '/system/qgajxxbab',
    method: 'post',
    data: data
  })
}

// 修改区公安局信息
export function updateQgajxxbab(data) {
  return request({
    url: '/system/qgajxxbab',
    method: 'put',
    data: data
  })
}

// 删除区公安局信息
export function delQgajxxbab(id) {
  return request({
    url: '/system/qgajxxbab/' + id,
    method: 'delete'
  })
}

// 导出区公安局信息
export function exportQgajxxbab(query) {
  return request({
    url: '/system/qgajxxbab/export',
    method: 'get',
    params: query
  })
}
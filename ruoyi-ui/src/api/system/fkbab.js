import request from '@/utils/request'

export function listFkbabByDeptId(query) {
  return request({
    url: '/system/fkbab/listfkbabbydeptid',
    method: 'get',
    params: query
  })
}

//listFkbabByXqAdminId
//通过小区管理员Id获取小区访客信息
export function listFkbabByXqAdminId(query) {
  return request({
    url: '/system/fkbab/listfkbabbyxqadminid',
    method: 'get',
    params: query
  })
}

// 查询访客信息管理列表
export function listFkbab(query) {
  return request({
    url: '/system/fkbab/list',
    method: 'get',
    params: query
  })
}

// 查询访客信息管理详细
export function getFkbab(id) {
  return request({
    url: '/system/fkbab/' + id,
    method: 'get'
  })
}

// 新增访客信息管理
export function addFkbab(data) {
  return request({
    url: '/system/fkbab',
    method: 'post',
    data: data
  })
}

// 修改访客信息管理
export function updateFkbab(data) {
  return request({
    url: '/system/fkbab',
    method: 'put',
    data: data
  })
}

// 上传图片
export function addImgData(data, buildingId) {
  return request({
    url: '/system/imgHandler/imgUpload',
    method: 'post',  
    headers: {
      'Content-Type': 'multipart/form-data',
      'buildingId': buildingId,      //小区编码
      'commonFilePath': 'visitorPhotos', // 公共的存储的文件夹
      'uploadType': 'fk'
    },
    data: data    // 参数需要是单一的formData形式
  })
}

// 删除访客信息管理
export function delFkbab(id) {
  return request({
    url: '/system/fkbab/' + id,
    method: 'delete'
  })
}

// 导出访客信息管理
export function exportFkbab(query) {
  return request({
    url: '/system/fkbab/export',
    method: 'get',
    params: query
  })
}
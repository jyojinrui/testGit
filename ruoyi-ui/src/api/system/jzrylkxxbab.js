import request from '@/utils/request'

//警务室管理员 派出所管理员 区公安局管理员 市公安局管理员共用这个方法（小区编号长度21  小区管理员无法使用该方法）
export function listJzrylkxxbabByDeptId(query)
{
  return request({
    url:'/system/jzrylkxxbab/listjzrylkxxbabbydeptid',
    method:'get',
    params:query
  })
}

//根据小区管理员id查询离开人员信息列表    还要考虑后面搜索情况 
export function listJzrylkxxbabByXqAdminId(query)
{
  return request({
    url:'/system/jzrylkxxbab/listjzrylkxxbabbyxqadminid',
    method:'get',
    params:query
  })
}

// 查询离开人员管理列表
export function listJzrylkxxbab(query) {
  return request({
    url: '/system/jzrylkxxbab/list',
    method: 'get',
    params: query
  })
}

// 查询离开人员管理详细
export function getJzrylkxxbab(id) {
  return request({
    url: '/system/jzrylkxxbab/' + id,
    method: 'get'
  })
}

// 新增离开人员管理
export function addJzrylkxxbab(data) {
  return request({
    url: '/system/jzrylkxxbab',
    method: 'post',
    data: data
  })
}

// 修改离开人员管理
export function updateJzrylkxxbab(data) {
  return request({
    url: '/system/jzrylkxxbab',
    method: 'put',
    data: data
  })
}

// 删除离开人员管理
export function delJzrylkxxbab(id) {
  return request({
    url: '/system/jzrylkxxbab/' + id,
    method: 'delete'
  })
}

// 导出离开人员管理
export function exportJzrylkxxbab(query) {
  return request({
    url: '/system/jzrylkxxbab/export',
    method: 'get',
    params: query
  })
}
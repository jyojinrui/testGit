import request from '@/utils/request'


export function listXlgcsxtrtyxbByDeptId(query) {
  return request({
    url: '/system/xlgcsxtrtyxb/listxlgcsxtrtyxbbydeptid',
    method: 'get',
    params: query
  })
}
// listXlgcsxtrtyxbByXqAdminId
export function listXlgcsxtrtyxbByXqAdminId(query) {
  return request({
    url: '/system/xlgcsxtrtyxb/listxlgcsxtrtyxbbyxqadminid',
    method: 'get',
    params: query
  })
}

// 查询雪亮工程摄像头人体运行数据列表
export function listXlgcsxtrtyxb(query) {
  return request({
    url: '/system/xlgcsxtrtyxb/list',
    method: 'get',
    params: query
  })
}

// 查询雪亮工程摄像头人体运行数据详细
export function getXlgcsxtrtyxb(id) {
  return request({
    url: '/system/xlgcsxtrtyxb/' + id,
    method: 'get'
  })
}

// 新增雪亮工程摄像头人体运行数据
export function addXlgcsxtrtyxb(data) {
  return request({
    url: '/system/xlgcsxtrtyxb',
    method: 'post',
    data: data
  })
}

// 修改雪亮工程摄像头人体运行数据
export function updateXlgcsxtrtyxb(data) {
  return request({
    url: '/system/xlgcsxtrtyxb',
    method: 'put',
    data: data
  })
}

// 删除雪亮工程摄像头人体运行数据
export function delXlgcsxtrtyxb(id) {
  return request({
    url: '/system/xlgcsxtrtyxb/' + id,
    method: 'delete'
  })
}

// 导出雪亮工程摄像头人体运行数据
export function exportXlgcsxtrtyxb(query) {
  return request({
    url: '/system/xlgcsxtrtyxb/export',
    method: 'get',
    params: query
  })
}